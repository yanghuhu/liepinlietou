//
//  DataDefault.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "DataDefault.h"

#define App_TabBarCount_Key @"App_TabBarCount_Key"
#define App_FirstLogin      @"App_FirstLogin"

@implementation DataDefault


+ (NSString *)getKeyWithRoleInfo:(NSString *)key {
    return [self getRealKey:key isPrivate:YES];
}

+ (NSString *)getRealKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    UserModel * user = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel;
    return [self getRoleKeyWithRole:user key:key isPrivate:isPrivate];
}

+ (NSString *)getRoleKeyWithRole:(UserModel *)role key:(NSString *)key isPrivate:(BOOL)isPrivate
{
    NSString *keyReal = key;
    if (isPrivate && role) {
        keyReal = [NSString stringWithFormat:@"%@_%ld",key,(long)role.uid];
    }
    return keyReal;
}

+ (id)objectForKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    return [userDefaults objectForKey:keyReal];
}

+ (void)setObject:(id)value forKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    if (!value) {
        return;
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    [userDefaults setObject:value forKey:keyReal];
    [userDefaults synchronize];
}

+ (NSInteger)integerForKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    return [userDefaults integerForKey:keyReal];
}

+ (void)setInteger:(NSInteger)integer forKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:integer forKey:keyReal];
    [userDefaults synchronize];
}

+ (BOOL)boolForKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    return [userDefaults boolForKey:keyReal];
}

+ (void)setBool:(BOOL)integer forKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:integer forKey:keyReal];
    [userDefaults synchronize];
}

+ (NSString *)stringForKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    return [userDefaults stringForKey:keyReal];
}

+ (void)setString:(NSString *)string forKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    [self setObject:string forKey:key isPrivate:isPrivate];
}

+ (void)removeWithKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    [userDefaults removeObjectForKey:keyReal];
    [userDefaults synchronize];
}

+(void)setTabBarCount:(NSInteger)count
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:count forKey:App_TabBarCount_Key];
    [userDefaults synchronize];
}

+(NSInteger)getTabBarCount
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults integerForKey:App_TabBarCount_Key] ;
}


+ (NSInteger)numberForTodayDoWithKey:(NSString *)key isPrivate:(BOOL)isPrivate{

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd";
    NSString *todayStr = [df stringFromDate:[NSDate date]];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id result = [userDefaults objectForKey:keyReal];
    NSInteger number = 0;
    if (result && [result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * re = result;
        NSArray * allkey = [re allKeys];
        BOOL isHad = NO;
        for (NSString * timeString in allkey) {
            if ([timeString isEqualToString:todayStr]) {
                isHad = YES;
                break;
            }
        }
        if (isHad) {
            number = [re[todayStr] intValue];
            return number;
        }else{
            return number;
        }
    }
    return number;
}

+ (void)asceNumberForTodayDoWithKey:(NSString *)key isPrivate:(BOOL)isPrivate{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd";
    NSString *todayStr = [df stringFromDate:[NSDate date]];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id result = [userDefaults objectForKey:keyReal];
    NSInteger number = 0;
    if (result && [result isKindOfClass:[NSDictionary class]]) {
        NSDictionary * re = result;
        NSArray * allkey = [re allKeys];
        BOOL isHad = NO;
        for (NSString * timeString in allkey) {
            if ([timeString isEqualToString:todayStr]) {
                isHad = YES;
                break;
            }
        }
        if (isHad) {
            number = [re[todayStr] intValue];
        }
        number += 1;
        NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:re];
        [dic setObject:@(number) forKey:todayStr];
        [userDefaults setObject:dic forKey:keyReal];
        [userDefaults synchronize];

    }else{
        NSDictionary * dic = @{todayStr:@(1)};
        [userDefaults setObject:dic forKey:keyReal];
        [userDefaults synchronize];
    }
}

+ (BOOL)boolForTodayDoWithKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd";
    NSString *todayStr = [df stringFromDate:[NSDate date]];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id result = [userDefaults objectForKey:keyReal];
    if (result && [result isKindOfClass:[NSArray class]]) {
        NSArray *re = result;
        BOOL done = [re.firstObject boolValue];
        NSString *lastTimeValue = re.lastObject;
        if ([lastTimeValue isEqualToString:todayStr]) {
            return done;
        }
        else {
            return NO;
        }
    }
    return NO;
}


+ (void)setBoolForTodayDo:(BOOL)done forKey:(NSString *)key isPrivate:(BOOL)isPrivate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd";
    NSString *todayStr = [df stringFromDate:[NSDate date]];
    NSString *keyReal = [self getRealKey:key isPrivate:isPrivate];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@[@(done),todayStr] forKey:keyReal];
    [userDefaults synchronize];
}

+(BOOL)isAPPFirstLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    BOOL isFirst =  [userDefault objectForKey:App_FirstLogin];
    if (!isFirst) {
        [userDefault setBool:YES forKey:App_FirstLogin];
    }
    return !isFirst;
}
@end
