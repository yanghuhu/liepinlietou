//
//  SecLocalizable.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/3.
//  Copyright © 2017年 Michael. All rights reserved.
//

#ifndef SecLocalizable_h
#define SecLocalizable_h

#define SecondTable @"SecLocalizable"

#define ThirdLoginbuttonTitle NSLocalizedStringFromTable(@"Third login",SecondTable,@"Third login button title")

#endif /* SecLocalizable_h */
