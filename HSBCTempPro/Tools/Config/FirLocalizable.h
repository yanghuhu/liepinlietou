//
//  FirLocalizable.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/3.
//  Copyright © 2017年 Michael. All rights reserved.
//

#ifndef FirLocalizable_h
#define FirLocalizable_h

#define FirstTable @"FirLocalizable"

#define  LoginButtonAndViewTitle NSLocalizedStringFromTable(@"login",FirstTable, @"Login Button And View Title")
#define  ForgetPasswordButtonTitle   NSLocalizedStringFromTable(@"Forget password?",FirstTable,@"Forget Password Button Title")
#define  RegisterButtonAndPageTitle  NSLocalizedStringFromTable(@"Register",FirstTable,@"Register Button And Page Title")
#define  LoginCancelAlert    NSLocalizedStringFromTable(@"Login Cancel",FirstTable,@"Third login cancel alert")
#define  LoginFailureAlert    NSLocalizedStringFromTable(@"Login Failure",FirstTable,@"Login Failure alert")
#define  LoginSuccesslAlert    NSLocalizedStringFromTable(@"Login Success",FirstTable,@"Login success alert")
#define  LoginUsernamePlaceholder    NSLocalizedStringFromTable(@"Username/Phone",FirstTable,@"login username placeholder")
#define  LoginPasswordPlaceholder    NSLocalizedStringFromTable(@"Password",FirstTable,@"Login Password Placeholder")

#endif /* FirLocalizable_h */
