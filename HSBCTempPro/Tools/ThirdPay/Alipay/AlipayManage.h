//
//  AlipayManage.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlipayManage : NSObject


@property (nonatomic , strong) NSString * orderId;
@property (nonatomic , assign) CGFloat price;

@property (nonatomic , strong) UIViewController * viewcontroller;

- (void)doAPPay;
@end
