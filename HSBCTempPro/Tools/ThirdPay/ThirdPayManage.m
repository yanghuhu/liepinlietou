//
//  ThirdPayManage.m
//  HSBCTempPro
//
//  Created by Michael on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ThirdPayManage.h"

@implementation ThirdPayManage

- (AlipayManage *)alipayManage{
    _alipayManage = [[AlipayManage alloc] init];
    return _alipayManage;
}

- (void)payWithType:(ThirdPayType)type OrderId:(NSString *)orderId  price:(CGFloat)price{
    if (type == ThirdPayTypeAlipay) {
        self.alipayManage.price = price;
        self.alipayManage.orderId = orderId;
        [self.alipayManage doAPPay];
    }else if (type == ThirdPayTypeWechat){
        
    }else if (type == ThirdPayTypeBank){
        
    }
}

@end
