//
//  ThirdPayManage.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlipayManage.h"

typedef enum{
    ThirdPayTypeAlipay,
    ThirdPayTypeWechat,
    ThirdPayTypeBank,
}ThirdPayType;

typedef void (^PayFinishBlock)();


@interface ThirdPayManage : NSObject

@property (nonatomic , strong)  AlipayManage * alipayManage;
@property (nonatomic , assign)  PayFinishBlock payFinsihBlock;

- (void)payWithType:(ThirdPayType)type OrderId:(NSInteger)orderId  price:(CGFloat)price;

@end
