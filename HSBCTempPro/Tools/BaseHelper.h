//
//  BaseHelper.h
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static NSString *  const PositionCandidateOrderStateRecommended = @"Recommended";  // 已推荐
static NSString * const PositionCandidateOrderStateAccepted = @"Accepted";  // hr接受推荐
static NSString * const PositionCandidateOrderStateRejected = @"Rejected";  // hr 拒绝推荐
static NSString * const PositionCandidateOrderStateWait_Appointment = @"Wait_Appointment";  // 待约面
static NSString *  const PositionCandidateOrderStateWait_Interview = @"Wait_Interview";  // 待面试
static NSString * const PositionCandidateOrderStateInterview_Failed = @"Interview_Failed";  // 面试未通过
static NSString * const PositionCandidateOrderStateWaint_Offer = @"Waint_Offer";  // 待offer
static NSString * const PositionCandidateOrderStateWait_Register = @"Wait_Register";  // 待入职
static NSString * const PositionCandidateOrderStateNot_Register = @"No_Register";  //  未入职
static NSString * const PositionCandidateOrderStateWait_Positive = @"Wait_Positive";  // 待转正
static NSString * const PositionCandidateOrderStatePositive = @"Positive";  // 转正
static NSString * const PositionCandidateOrderStateNot_Position = @"No_Positive";  //  未转正

// 拼音为显示中文日期，日期格式显示日期
typedef NS_ENUM(NSInteger, DateFormatType) {
    kDateFormatTypeNY,
    kDateFormatTypeYYYYMM,
    kDateFormatTypeYYYYMMDD,
    kDateFormatTypeYYYYMMDDHHMMSS,
    kDateFormatTypeYYYYMMDDHHMM,
    kDateFormatTypeMMDD,
    kDateFormatTypeMM_DD,
    kDateFormatTypeHHMM,
    kDateFormatTypeMM__DD,
    kDateFormatTypeMMDDHHMM,
};

typedef  enum{
    kResponseTypeNetWorkError = 100,     //网络错误，连接失败
    kResponseTypeNetWorkErrorSocketDisconnect = 101,     //socket断开连接
    kResponseTypeNetWorkTimeout = 400,  //请求超时
    kResponseTypeSessionExpire = 401,    // session 过期

    kResponseTypeNotFound = 404,    //  未找到制定信息

    
    kResponseTypeDataTranstionError = 501,   // 网络请求数据转换失败
    kResponseTypeDataParseError = 502,   // 网络返回数据解析错误
    kResponseTypeUnknown = 503,       // 未知错误

    kResponseTypeNoData = 600, // 没有数据
}ResponseType;


typedef void (^RequestSuccessBlock)();
typedef void (^RequestFailureBlock)(NSString *error, ResponseType responseType);
typedef NSArray* (^NetworkHandleBlock)(id response);
typedef void (^RequestFailureAndLocalBlock)(NSString *error, ResponseType responseType, id localData);

@interface BaseHelper : NSObject

// 去掉tableview空行横线
+ (void)setExtraCellLineHidden: (UITableView *)tableView;

// 初始化lable
+ (UILabel *) labelWithFrame:(CGRect)frame
                        text:(NSString *)text
                    fontSize:(CGFloat)fontSize
                   textColor:(UIColor *)color
                 isMultiLine:(BOOL)multiLine
                       width:(int *)width;


// 初始化TextField
+ (UITextField *)textFieldWithFrame:(CGRect)frame
                             holder:(NSString *)holderText
                          textColor:(UIColor *)color
                           fontSize:(CGFloat)fontSize;

// 初始化button
+ (UIButton*)btWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font frame:(CGRect)frame bkImageNomal:(NSString *)imageBkNomal  bkImageSelect:(NSString *)imageBkSelect  imageNomal:(NSString *)imageNomal  imageSelect:(NSString *)imageSelect imageHightlight:(NSString *)imageHight withTatget:(id)target action:(SEL)select;

// 将数字转为字符串
+ (NSString *)numberString:(NSInteger)number;

/**
 *  修改字符串部分文字的部分颜色
 *
 *  @param source     字符串
 *  @param font       字体大小
 *  @param color      需要修改字体的颜色
 *  @param range      位置和长度
 *  @param otherColor 未修改部分的字体颜色
 *
 *  @return   NSAttributedString字符串
 */
+ (NSAttributedString *)getAttributeStringWithSource:(NSString *)source withFont:(UIFont *)font color:(UIColor *)color range:(NSRange)range otherTextColor:(UIColor *)otherColor;

/**
 *  修改字符串部分文字的部分颜色
 *
 *  @param source       字符串源
 *  @param target       要修改颜色的目标字符串
 *  @param font         字体大小
 *  @param color        需要修改字体的颜色
 *  @param customFont   其他默认字体
 *  @param customColor  其他默认颜色
 *
 *  @return   NSAttributedString字符串
 */

+ (NSAttributedString *)setSourceString:(NSString *)source
                           targetString:(NSString *)target
                                forFont:(UIFont *)font
                                  color:(UIColor *)color
                             customFont:(UIFont *)customFont
                            customColor:(UIColor *)customColor;

/**
 *  修改字符串部分文字的部分颜色
 *
 *  @param source       字符串源
 *  @param firstTarget  第一段要修改颜色的目标字符串
 *  @param secondTarget 第二段需要修改颜色的目标字符串
 *  @param font         字体大小
 *  @param color        需要修改字体的颜色
 *  @param customFont   其他默认字体
 *  @param customColor  其他默认颜色
 *
 *  @return NSAttributedString字符串
 */
+ (NSAttributedString *)setSourceString:(NSString *)source
                      firstTargetString:(NSString *)firstTarget
                     secondTargetString:(NSString *)secondTarget
                                forFont:(UIFont *)font
                                  color:(UIColor *)color
                             customFont:(UIFont *)customFont
                            customColor:(UIColor *)customColor;


/**
 *  显示图标和文字
 *
 *  @param view      <#view description#>
 *  @param imageName <#imageName description#>
 *  @param alignment 1、上下排列 2、左右排列
 *  @param text      提示文字
 */
+ (void)showProgressHudInView:(UIView *)view alignment:(NSInteger)alignment imageName:(NSString *)imageName text:(NSString *)text action:(void(^)(void))action;


/**
 *  UI层网络请求失败后的回调显示文字,显示于当前windown（只有当不是网络异常的情况才会提示）
 *
 *  @param text         提示文字
 *  @param responseType 请求数据响应标记类型
 */
+ (void)showProgressHudWithFailText:(NSString *)text responseType:(ResponseType)responseType;


/**
 *  UI层请求失败后的回调显示文字,显示于特定view（只有当不是网络异常的情况才会提示）
 *
 *  @param view         提示层在此view中显示
 *  @param text         提示文字
 *  @param responseType 请求数据响应标记类型
 */
+ (void)showProgressHudInView:(UIView *)view failText:(NSString *)text responseType:(ResponseType)responseType;

/**
 *  显示一小段文字，并自动隐藏
 *
 *  @param text 显示的文字
 */
+ (void)showProgressHudWithText:(NSString *)text;

/**
 *  显示菊花
 */
+ (void)showProgressLoading;
+ (void)showProgressHud:(NSString *)labelText showLoading:(BOOL)showLoading canHide:(BOOL)canHide;
+ (void)showProgressHud:(NSString *)labelText detailsLabel:(NSString *)details showLoading:(BOOL)showLoading canHide:(BOOL)canHide;

/**
 *  显示不会自动消失的菊花
 *
 *  @param view        所在的view里添加
 */
+ (void)showProgressLoadingInView:(UIView *)view;


/**
 *
 *  @param labelText 要显示的文字
 */
+ (void)showMultiLineProgressHub:(NSString *)labelText;

/**
 *  显示菊花加截图标
 *
 *  @param view        所在的view里添加
 *  @param showLoading 是否出现菊花
 *  @param canHide     是否2.5秒后自动消失
 */
+ (void)showProgressHudInView:(UIView *)view showLoading:(BOOL)showLoading canHide:(BOOL)canHide;

+ (void)showProgressHudInView:(UIView *)view labelText:(NSString *)labelText detailsLabel:(NSString *)details  showLoading:(BOOL)showLoading canHide:(BOOL)canHide;

+ (void)hideProgressHudInView:(UIView *)view;

+ (void)hideProgressHud;

+ (void)showProgressHudInView:(UIView *)view labelText:(NSString *)labelText detailsLabel:(NSString *)details  showLoading:(BOOL)showLoading canHide:(BOOL)canHide withTag:(NSInteger)tag;


//  获取字符串显示所需size, 宽高无限制时用 MAXFLOAT
+ (CGSize)getSizeWithString:(NSString *)string font:(UIFont *)font contentWidth:(CGFloat)width contentHight:(CGFloat)height;

+ (CGSize)getSizeWithString:(NSString *)string font:(UIFont *)font contentWidth:(CGFloat)width contentHight:(CGFloat)height breakMode:(NSLineBreakMode)mode;

// 创建未读消息lable
+ (UILabel *)createUnreadLabelCount:(NSInteger)count;

#pragma mark - Document methods

+ (NSString *)documentPath;
+ (NSString *)tempPath;
+ (NSString *)cachePath;
+ (NSString *)stringFromDictionary:(NSDictionary *)dict;


+ (NSString *)pastDateFormat:(NSDate *)date;

/**
 NSDate解析
 
 @param date NSDate数据
 @return 今天、昨天、xx,xx月
 */
+ (NSString *)parseDateFormate:(NSDate *)date;

/**
NSDate解析
 
 @param date NSDate数据
 @return 返回刚刚、xx分钟前、x小时前、昨天。。。。
 */
+ (NSString *)formateDate:(NSDate *)date;

+ (NSString *)stringWithTimeIntevl:(NSTimeInterval)timeinterval format:(DateFormatType)dateType;
+ (NSDate *)getDateFromString:(NSString *)string withFormat:(NSString *)format;
+ (NSString *)stringFromDate:(NSDate *)date format:(DateFormatType)dateType;

/**
 *  通过日期换算成一个星期中第几天
 *
 *  @param inputDate 日期
 *
 *  @return 今天星期
 */
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate;

/*
 * h5 url添加参数
 */
+ (NSString *)addUrl:(NSString *)url paramFromDictionary:(NSDictionary *)param;

+ (BOOL)isValidateMobile:(NSString *)mobileNum;

/**
 *  是否有效的邮箱地址
 *
 *  @param email 邮箱地址
 *
 *  @return BOOL
 */
+ (BOOL)isValidEmail:(NSString *)email;

/**
 *  是否有效的连接
 *
 *  @param link 连接URL
 *
 *  @return BOOL
 */
+ (BOOL)isValidateHttp:(NSString *)link;

/**
 * 将该视图放大再缩小 
 * scale:放大的倍数
 */
+ (void)animationWithView:(id)view scale:(CGFloat)scale;

/**
 *  字符串匹配
 *
 *  @param searchText   搜索的字符串
 *  @param originalText 要进行对比的字符串
 *
 *  @return 返回YES or NO
 */
+ (BOOL)isMatchWithSeatchText:(NSString *)searchText originalText:(NSString *)originalText;

/**
 *   修改显示内容中部分字体的颜色
 *
 *  @param string         对应的字符串
 *  @param stringLocation 需要修改颜色的字体的长度
 *
 *  @return 修改颜色后的NSMutableAttributedString
 */
+ (NSMutableAttributedString *)setRangeString:(NSString *)string stringLocation:(int)stringLocation;

/**
 *  更换字符串分割后后的两部分颜色
 */
+ (NSMutableAttributedString *)setRangeString:(NSString *)string stringLocation:(int)stringLocation part1Color:(UIColor *)part1Color part2Color:(UIColor *)part2Color;

/**
 *  更换window的根视图
 */
+ (void)windowChangeRootViewController:(UIViewController*)controller;

/**
* 将汉字转换为拼音
 */
+ (NSString *)chineseToPinyin:(NSString *)chineseString withSpace:(BOOL)withSpace;

/**
 * 打电话
 */
+ (void)phoneCallWithNum:(NSString *)phoneNum;

/*
 * 获取默认头像
 *      gender :性别;  1:男；2：女
 */
+ (UIImage *)avatarPlaceHolderWithGender:(NSInteger)gender;
+ (NSString *)fixLineFeedWithString:(NSString *)string;
+ (NSString *)removeLineFeedWithString:(NSString *)string;

/*
 *  图片去色
 */
+(UIImage *)grayImage:(UIImage *)sourceImage;

/*
 *  为日期添加年数，月数，日数
 *      date  原始date
 *      month  需添加月数
 *      day    需添加的天数
 *      year    需添加的年数
 */
+ (NSDate *)date:(NSDate *)date addMonth:(NSInteger)month day:(NSInteger)day year:(NSInteger)year;

/*
 * 获取本地缓存的用户
 */
+(UserModel *)getUserFromDataDefault;

/*
 *  缓存登录用户到本地
 */
+ (void)saveUserToDataDefault:(NSDictionary *)infoDic;

/*
 *  清除本地缓存的用户
 */
+ (void)deleUserFromDataDefault;

/*
 * 本地简历路径
 */
+ (NSString *)resumeFolderPath;

/*
 *  存储resume 文件
 */
+ (void)thirdResumeFileSave:(NSURL *)filePath;

/*
 * 简历的本地路劲
 */
+ (NSString *)resumeLocalPahtWithName:(NSString *)fileName;

/*
 *  获取window最顶层的viewcontroller
 */
+ (UIViewController *)windowTopViewController;

/**
 *  判断密码位数 目前只限制位数>=8位 不做其他限制
 *
 *  @param text        密码
 */
+ (BOOL)passwordlength:(NSString *)text;

@end
