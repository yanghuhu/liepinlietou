//
//  BaseHelper.m
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "BaseHelper.h"
#import "NSString+YYAdd.h"
#import "UIView+YYAdd.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "NSDictionary+YYAdd.h"

@implementation BaseHelper

+ (void)setExtraCellLineHidden: (UITableView *)tableView {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    [tableView setTableFooterView:view];
}

/**
 *
 *
 *  @param frame     坐标
 *  @param text      文本内容
 *  @param fontSize  字体大小
 *  @param color     颜色值
 *  @param multiLine 是否需要换行  默认为No,如果需要换行则为Yes
 *  @param width    宽
 *
 *  @return     UILable
 */
+ (UILabel *)labelWithFrame:(CGRect)frame
                       text:(NSString *)text
                   fontSize:(CGFloat)fontSize
                  textColor:(UIColor *)color
                isMultiLine:(BOOL)multiLine
                      width:(int *)width {
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    label.backgroundColor = [UIColor clearColor];
    CGSize labelSize = CGSizeZero;
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]};
    labelSize = [text boundingRectWithSize:CGSizeMake(frame.size.width, 0) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    if (multiLine) {
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByCharWrapping;
        frame.size = labelSize;
        label.frame = frame;
    }
    if (width) {
        *width = labelSize.width;
    }
    
    label.text = text;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.textColor = color;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

/**
 *
 *
 *  @param frame     坐标
 *  @param holderText 提示内容
 *  @param fontSize  字体大小
 *  @param color     颜色值
 *
 *  @return  UITextField
 */

+ (UITextField *)textFieldWithFrame:(CGRect)frame holder:(NSString *)holderText textColor:(UIColor *)color fontSize:(CGFloat)fontSize
{
    UITextField *textField = [[UITextField alloc] initWithFrame:frame];
    textField.placeholder = holderText;
    textField.textColor = color;
    textField.font = [UIFont systemFontOfSize:fontSize];
    textField.textAlignment = NSTextAlignmentLeft;
    return textField;
}

+ (UIButton*)btWithTitle:(NSString *)title titleColor:(UIColor *)color font:(UIFont*)font frame:(CGRect)frame bkImageNomal:(NSString *)imageBkNomal  bkImageSelect:(NSString *)imageBkSelect  imageNomal:(NSString *)imageNomal  imageSelect:(NSString *)imageSelect imageHightlight:(NSString *)imageHight withTatget:(id)target action:(SEL)select{
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button addTarget:target action:select forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    button.titleLabel.font = font;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    if (imageNomal) {
        [button setImage:[UIImage imageNamed:imageNomal] forState:UIControlStateNormal];
    }
    if (imageSelect) {
        [button setImage:[UIImage imageNamed:imageSelect] forState:UIControlStateSelected];
    }
    if (imageBkNomal) {
        [button setBackgroundImage:[UIImage imageNamed:imageBkNomal] forState:UIControlStateNormal];
    }
    if (imageBkSelect) {
        [button setBackgroundImage:[UIImage imageNamed:imageBkSelect] forState:UIControlStateSelected];
    }
    return button;
}


+ (NSString *)numberString:(NSInteger)number
{
    NSString *text = nil;
    if (number <= 0) {
        text = nil;
        return [NSString stringWithFormat:@"0"];
    }
    text = [NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:number]];
    return  text;
}

+ (NSAttributedString *)getAttributeStringWithSource:(NSString *)source withFont:(UIFont *)font color:(UIColor *)color range:(NSRange)range otherTextColor:(UIColor *)otherColor
{
    NSMutableAttributedString *attributtes = [[NSMutableAttributedString alloc] initWithString:source];
    NSRange rangePart1 = NSMakeRange(0, range.location);
    NSRange rangePart3 = NSMakeRange(range.location +range.length, source.length - rangePart1.length - range.length);
    [attributtes addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, source.length)];
    [attributtes addAttribute:NSForegroundColorAttributeName value:otherColor range:rangePart1];
    [attributtes addAttribute:NSForegroundColorAttributeName value:color range:range];
    [attributtes addAttribute:NSFontAttributeName value:font range:range];
    [attributtes addAttribute:NSForegroundColorAttributeName value:otherColor range:rangePart3];
    return attributtes;
    
}


+ (NSAttributedString *)setSourceString:(NSString *)source targetString:(NSString *)target forFont:(UIFont *)font color:(UIColor *)color customFont:(UIFont *)customFont customColor:(UIColor *)customColor
{
    if (![source isNotBlank] || ![target isNotBlank]) {
        return nil;
    }
    
    NSMutableAttributedString *attributtes = [[NSMutableAttributedString alloc] initWithString:source];
    NSRange range = [source rangeOfString:target];
    if (range.location != NSNotFound) {
        NSRange rangePart1 = NSMakeRange(0,range.location);
        NSRange rangePart2 = range;
        NSUInteger rangePart3StartIndex = range.location+range.length;
        NSRange rangePart3 = NSMakeRange(rangePart3StartIndex,source.length-rangePart3StartIndex);
        
        [attributtes addAttribute:NSFontAttributeName value:customFont range:NSMakeRange(0, source.length)];
        [attributtes addAttribute:NSForegroundColorAttributeName value:customColor range:rangePart1];
        [attributtes addAttribute:NSFontAttributeName value:font range:rangePart2];
        [attributtes addAttribute:NSForegroundColorAttributeName value:color range:rangePart2];
        [attributtes addAttribute:NSForegroundColorAttributeName value:customColor range:rangePart3];
    }
    return attributtes;
}


+ (NSAttributedString *)setSourceString:(NSString *)source firstTargetString:(NSString *)firstTarget secondTargetString:(NSString *)secondTarget forFont:(UIFont *)font color:(UIColor *)color customFont:(UIFont *)customFont customColor:(UIColor *)customColor
{
    NSMutableAttributedString *attributtes = [[NSMutableAttributedString alloc] initWithString:source];
    NSRange firstRange = [source rangeOfString:firstTarget];
    NSRange secondRange = [source rangeOfString:secondTarget options:NSBackwardsSearch];
    if (firstRange.location != NSNotFound && secondRange.location != NSNotFound ) {
        //第一段需要处理的文字
        NSRange firstRangePart1 = NSMakeRange(0,firstRange.location);
        NSRange firstRangePart2 = firstRange;
        NSUInteger firstRangePart3StartIndex = firstRange.location + firstRange.length;
        NSRange firstRangePart3 = NSMakeRange(firstRangePart3StartIndex, secondRange.location - firstRangePart3StartIndex);
        NSRange firstRangePart4 = secondRange;
        NSUInteger firstRangePart5StartIndex =  firstRangePart4.location + firstRangePart4.length;
        NSRange firstRangePart5 = NSMakeRange(firstRangePart5StartIndex, source.length - firstRangePart5StartIndex);
        
        [attributtes addAttribute:NSFontAttributeName value:customFont range:NSMakeRange(0, source.length)];
        [attributtes addAttribute:NSForegroundColorAttributeName value:customColor range:firstRangePart1];
        
        [attributtes addAttribute:NSFontAttributeName value:font range:firstRangePart2];
        [attributtes addAttribute:NSForegroundColorAttributeName value:color range:firstRangePart2];
        
        [attributtes addAttribute:NSForegroundColorAttributeName value:customColor range:firstRangePart3];
        
        [attributtes addAttribute:NSFontAttributeName value:font range:firstRangePart4];
        [attributtes addAttribute:NSForegroundColorAttributeName value:color range:firstRangePart4];
        
        [attributtes addAttribute:NSForegroundColorAttributeName value:customColor range:firstRangePart5];
    }
    return attributtes;
}


+ (void)showProgressHudInView:(UIView *)view alignment:(NSInteger)alignment imageName:(NSString *)imageName text:(NSString *)text action:(void(^)(void))action {
    
    if (alignment == 1) {
        CGFloat delay = 2;
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
        HUD.mode = MBProgressHUDModeCustomView;
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        HUD.labelText = text;
        if (action) {
            action();
        }
        [HUD hide:YES afterDelay:delay];
    }
    else {
        
        CGFloat delay = 3;
        UIImage *image = [UIImage imageNamed:imageName];
        CGSize sizeForText = [BaseHelper sizeOfString:text withFont:[UIFont systemFontOfSize:15]];
        
        CGFloat width = MIN(ScreenWidth - 40, sizeForText.width + image.size.width + 40);
        __block UIButton *customView = [[UIButton alloc] initWithFrame:CGRectMake((view.frame.size.width - width)/2, 0, width, 40)];
        customView.layer.masksToBounds = YES;
        customView.layer.cornerRadius = 4.0;
        customView.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.600];
        customView.userInteractionEnabled = NO;
        [customView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        customView.titleLabel.font = [UIFont systemFontOfSize:15];
        [customView setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [customView setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [customView setImage:image forState:UIControlStateNormal];
        [customView setTitle:text forState:UIControlStateNormal];
        [view addSubview:customView];
        customView.top = view.frame.size.height;
        
        [UIView animateWithDuration:delay animations:^{
            customView.center = view.center;
        }completion:^(BOOL finished) {
            [customView removeFromSuperview];
            customView = nil;
        }];
    }
}

+ (CGSize)sizeOfString:(NSString *)string withFont:(UIFont *)font
{
    CGSize stringSize = CGSizeZero;
    stringSize = [string sizeWithAttributes:@{NSFontAttributeName:font}];
    
    return stringSize;
}


+ (NSString *)getErrorDefault:(NSString *)error
{
    return [self getStringDefault:error backupString:@"获取数据失败"];
}
+ (NSString *)getStringDefault:(NSString *)input backupString:(NSString *)backupString
{
    if (input.length>0) {
        return input;
    }
    return backupString;
}

+ (void)showProgressHudWithFailText:(NSString *)text responseType:(ResponseType)responseType
{
    if (text) {
        [BaseHelper showProgressHudWithText:text];
        return;
    }
    if (responseType!=kResponseTypeNetWorkTimeout) {//这个网络问题将会统一在网络层里处理。
        [self showProgressHud:nil detailsLabel:[BaseHelper getErrorDefault:text] showLoading:NO canHide:YES];
    }
}


+ (void)showProgressHud:(NSString *)labelText detailsLabel:(NSString *)details showLoading:(BOOL)showLoading canHide:(BOOL)canHide {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showProgressHud:labelText detailsLabel:details showLoading:showLoading canHide:canHide window:window];
    
    //    UIWindow *loginWindow = [UIApplication loginWindow];
    //    if (window!=loginWindow) {
    //        [self showProgressHud:labelText detailsLabel:details showLoading:showLoading canHide:canHide window:loginWindow];
    //    }
}

+ (void)showProgressHud:(NSString *)labelText detailsLabel:(NSString *)details showLoading:(BOOL)showLoading canHide:(BOOL)canHide window:(UIWindow *)window
{
    [MBProgressHUD hideHUDForView:window animated:YES];
    
    MBProgressHUD *progressHud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    progressHud.animationType = MBProgressHUDAnimationZoom;
    progressHud.mode = MBProgressHUDModeText;
    progressHud.detailsLabelText = details;
    if (showLoading) {
        progressHud.mode = MBProgressHUDModeIndeterminate;
    }
    progressHud.labelText = labelText;
    if (canHide) {
        [progressHud hide:YES afterDelay:1.5f];
    }
}

+ (void)showProgressHudInView:(UIView *)view failText:(NSString *)text responseType:(ResponseType)responseType
{
    if (text) {
        [BaseHelper showMultiLineProgressHub:text inView:view];
        return;
    }
    if (responseType!=kResponseTypeNetWorkTimeout && responseType!=kResponseTypeSessionExpire) {//这个网络问题将会统一在网络层里处理。
        [BaseHelper showProgressHudInView:view labelText:nil detailsLabel:[BaseHelper getErrorDefault:text] showLoading:NO canHide:YES];
    }
}

+ (void)showProgressHudInView:(UIView *)view labelText:(NSString *)labelText detailsLabel:(NSString *)details  showLoading:(BOOL)showLoading canHide:(BOOL)canHide
{
    [MBProgressHUD hideHUDForView:view animated:YES];
    
    MBProgressHUD *progressHud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    progressHud.animationType = MBProgressHUDAnimationZoom;
    progressHud.mode = MBProgressHUDModeText;
    progressHud.detailsLabelText = details;
    if (showLoading) {
        progressHud.mode = MBProgressHUDModeIndeterminate;
    }
    progressHud.labelText = labelText;
    if (canHide) {
        [progressHud hide:YES afterDelay:1.5f];
    }
}


+ (void)showProgressHudWithText:(NSString *)text
{
    [self showProgressHud:nil detailsLabel:text showLoading:NO canHide:YES];
}

+ (void)showProgressLoading
{
    [self showProgressHud:nil detailsLabel:nil showLoading:YES canHide:NO];
}


+ (void)showProgressHud:(NSString *)labelText showLoading:(BOOL)showLoading canHide:(BOOL)canHide {
    [self showProgressHud:labelText detailsLabel:nil showLoading:showLoading canHide:canHide];
}

+ (void)showMultiLineProgressHub:(NSString *)labelText {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self showMultiLineProgressHub:labelText inView:window];
    
    //    UIWindow *loginWindow = [UIApplication loginWindow];
    //    if (window!=loginWindow) {
    //        [self showMultiLineProgressHub:labelText inView:loginWindow];
    //    }
}

+ (void)showMultiLineProgressHub:(NSString *)labelText inView:(UIView *)view{
    [MBProgressHUD hideHUDForView:view animated:YES];
    
    MBProgressHUD *progressHud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    progressHud.animationType = MBProgressHUDAnimationZoom;
    progressHud.mode = MBProgressHUDModeText;
    progressHud.detailsLabelText = labelText;
    progressHud.detailsLabelFont = [UIFont systemFontOfSize:16];
    [progressHud hide:YES afterDelay:1.5f];
}

+ (void)showProgressLoadingInView:(UIView *)view
{
    [self showProgressHudInView:view showLoading:YES canHide:NO];
}

+ (void)showProgressHudInView:(UIView *)view showLoading:(BOOL)showLoading canHide:(BOOL)canHide
{
    [self showProgressHudInView:view labelText:nil detailsLabel:nil showLoading:showLoading canHide:canHide];
}

+ (void)hideProgressHudInView:(UIView *)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

+ (void)hideProgressHud {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD hideHUDForView:window animated:YES];
    
    //    UIWindow *loginWindow = [UIApplication loginWindow];
    //    if (window!=loginWindow) {
    //        [MBProgressHUD hideHUDForView:loginWindow animated:YES];
    //    }
}

+ (void)showProgressHudInView:(UIView *)view labelText:(NSString *)labelText detailsLabel:(NSString *)details  showLoading:(BOOL)showLoading canHide:(BOOL)canHide withTag:(NSInteger)tag
{
    [MBProgressHUD hideHUDForView:view animated:YES];
    
    NSArray *array  = [MBProgressHUD allHUDsForView:view];
    if ([array count]>0) {
        
    }
    MBProgressHUD *progressHud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    progressHud.animationType = MBProgressHUDAnimationZoom;
    progressHud.mode = MBProgressHUDModeText;
    progressHud.detailsLabelText = details;
    if (showLoading) {
        progressHud.mode = MBProgressHUDModeIndeterminate;
    }
    progressHud.labelText = labelText;
    if (canHide) {
        [progressHud hide:YES afterDelay:1.5f];
    }
}

+ (CGSize)getSizeWithString:(NSString *)string font:(UIFont *)font contentWidth:(CGFloat)width contentHight:(CGFloat)height {
    CGSize labelSize = CGSizeZero;
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return labelSize;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    labelSize = [string boundingRectWithSize:CGSizeMake((width ? width : MAXFLOAT), (height ? height : MAXFLOAT)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    labelSize.height = ceil(labelSize.height);
    labelSize.width = ceil(labelSize.width);
    return labelSize;
}

+ (CGSize)getSizeWithString:(NSString *)string font:(UIFont *)font contentWidth:(CGFloat)width contentHight:(CGFloat)height breakMode:(NSLineBreakMode)mode
{
    CGSize labelSize = CGSizeZero;
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return labelSize;
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = mode;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    labelSize = [string boundingRectWithSize:CGSizeMake((width ? width : MAXFLOAT), (height ? height : MAXFLOAT)) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    labelSize.height = ceil(labelSize.height);
    labelSize.width = ceil(labelSize.width);
    return labelSize;
}


+ (UILabel *)createUnreadLabelCount:(NSInteger)count
{
    
    NSString *countStr = nil;
    if (count == 0) {
        countStr = nil;
    }else if(count>99 )
    {
        countStr = @"99+";
    }else
    {
        countStr = [NSString stringWithFormat:@"%d",(int)count];
    }
    CGSize size = [BaseHelper sizeOfString:countStr withFont:[UIFont systemFontOfSize:11]];
    UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAX(size.width, 15), size.height)];
    countLabel.text = countStr;
    countLabel.backgroundColor = [UIColor redColor];
    countLabel.textColor = [UIColor whiteColor];
    countLabel.adjustsFontSizeToFitWidth = YES;
    countLabel.textAlignment = NSTextAlignmentCenter;
    countLabel.contentMode = UIViewContentModeTop;
    countLabel.layer.cornerRadius =size.height/2;
    countLabel.clipsToBounds  = YES;
    
    return countLabel;
}


+ (NSString *)documentPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)tempPath
{
    return NSTemporaryDirectory();
}

+ (NSString *)cachePath
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)stringFromDictionary:(NSDictionary *)dict
{
    if (dict) {
        NSMutableString *string = [NSMutableString string];
        for (NSString *key in dict) {
            [string appendFormat:@"%@=%@&", key, [dict objectForKey:key]];
        }
        if (string.length) {
            [string deleteCharactersInRange:NSMakeRange(string.length - 1, 1)];
            return string;
        }
    }
    return nil;
}


+ (NSString *)pastDateFormat:(NSDate *)date {
    if (!date) {
        return @"";
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd HH:mm";
    NSDate *otherTime = [df dateFromString:@"1990-01-01 00:00"];
    
    NSInteger timeToday = ((NSInteger)[[NSDate date] timeIntervalSinceDate:otherTime]) / (3600 * 24);
    NSInteger dateTime = ((NSInteger)[date timeIntervalSinceDate:otherTime]) / (3600 * 24);
    NSInteger timeDif = timeToday - dateTime;
    if (timeDif == 0) {
        df.dateFormat = @"今天 HH:mm";
    } else if (timeDif == 1) {
        df.dateFormat = @"昨天 HH:mm";
    } else if (timeDif == 2) {
        df.dateFormat = @"前天 HH:mm";
    } else if (timeDif < 30 && timeDif > 2){
        df.dateFormat = @"MM-dd HH:mm";
    } else {
        df.dateFormat = @"yyyy-MM-dd";
    }
    return [df stringFromDate:date];
}
// ======== TenderAdd ========
+ (NSString *)parseDateFormate:(NSDate *)date{
    if (!date) {
        return @"";
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd HH:mm";
    
    NSDate *senddate=[NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: senddate];
    NSDate *localDate = [senddate dateByAddingTimeInterval: interval];
    NSInteger timeToday = ((NSInteger)[localDate timeIntervalSince1970]) / (3600 * 24);
    NSInteger dateTime = ((NSInteger)[date timeIntervalSince1970]) / (3600 * 24);
    NSInteger timeDif = timeToday - dateTime;
    if (timeDif == 0) {
        df.dateFormat = @"今天";
    } else if (timeDif == 1) {
        df.dateFormat = @"昨天";
    } else if (timeDif == 2) {
        df.dateFormat = @"前天";
    } else if (timeDif < 30 && timeDif > 2){
        df.dateFormat = @"ddMM月";
    } else {
        df.dateFormat = @"ddMM月";
    }
    return [df stringFromDate:date];
}

+ (NSString *)formateDate:(NSDate *)date{

    if (!date) {
        return @"";
    }
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone localTimeZone]];
    df.dateFormat = @"yyyy-MM-dd HH:mm";
    
    NSDate *localDate = [NSDate date];
    
    NSTimeInterval timeDif = [localDate timeIntervalSinceDate:date];
    NSString *dateStr = [[NSString alloc] init];
    if (timeDif<=60) {  //1分钟以内的
        dateStr = @"刚刚";
        
    }else if(timeDif<=60*60){  //一个小时以内的
        
        int mins = timeDif/60;
        dateStr = [NSString stringWithFormat:@"%d分钟前",mins];
        
    }else if(timeDif<=60*60*24){  //在两天内的
        
        [df setDateFormat:@"YYYY-MM-dd"];
        NSString * need_yMd = [df stringFromDate:date];
        NSString *now_yMd = [df stringFromDate:localDate];
        
        [df setDateFormat:@"HH:mm"];
        if ([need_yMd isEqualToString:now_yMd]) {
            //在同一天
            dateStr = [NSString stringWithFormat:@"今天 %@",[df stringFromDate:date]];
        }else{
            //昨天
            dateStr = [NSString stringWithFormat:@"昨天 %@",[df stringFromDate:date]];
        }
    }else {
        
        [df setDateFormat:@"yyyy"];
        NSString * yearStr = [df stringFromDate:date];
        NSString *nowYear = [df stringFromDate:localDate];
        
        if ([yearStr isEqualToString:nowYear]) {
            //在同一年
            [df setDateFormat:@"MM-dd"];
            dateStr = [df stringFromDate:date];
        }else{
            [df setDateFormat:@"yyyy/MM/dd"];
            dateStr = [df stringFromDate:date];
        }
    }
    return dateStr;
}

// ======== TenderAdd ========
+ (NSDate *)getDateFromString:(NSString *)string withFormat:(NSString *)format
{
    if (!string||[string isEqualToString:@""]) {
        return nil;
    }
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[NSLocale systemLocale]];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [inputFormatter setTimeZone:timeZone];
    if (!format) {
        [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }else{
        [inputFormatter setDateFormat:format];
    }
    NSDate* inputDate = [inputFormatter dateFromString:string];
    return inputDate;
}

+ (NSString *)stringWithTimeIntevl:(NSTimeInterval)timeinterval format:(DateFormatType)dateType{
    NSDate * data = [NSDate dateWithTimeIntervalSince1970:timeinterval];
    return  [BaseHelper stringFromDate:data format:dateType];
}

+ (NSString *)stringFromDate:(NSDate *)date format:(DateFormatType)dateType {
    static NSDateFormatter *df;
    if (df == nil) {
        df = [[NSDateFormatter alloc] init];
    }
    NSString *format = @"yyyy-MM-dd HH:mm";
    switch (dateType) {
        case kDateFormatTypeNY:
            format = @"yyyy年MM月";
            break;
        case kDateFormatTypeYYYYMM:
            format = @"yyyy-MM";
            break;
        case kDateFormatTypeYYYYMMDD:
            format = @"yyyy-MM-dd";
            break;
        case kDateFormatTypeYYYYMMDDHHMMSS:
            format = @"yyyy-MM-dd HH:mm:ss";
            break;
        case kDateFormatTypeMMDD:
            format = @"MM月dd日";
            break;
        case kDateFormatTypeMM_DD:
            format = @"MM-dd";
            break;
        case kDateFormatTypeMM__DD:
            format = @"M/d";
            break;
        case kDateFormatTypeHHMM:
            format = @"HH:mm";
            break;
        case kDateFormatTypeMMDDHHMM:
            format = @"MM-dd HH:mm";
            break;
        default:
            break;
    }
    df.dateFormat = format;
    return [df stringFromDate:date];
}

+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周末", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    [calendar setTimeZone: timeZone];
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    return [weekdays objectAtIndex:theComponents.weekday];
}

+ (NSString *)addUrl:(NSString *)url paramFromDictionary:(NSDictionary *)param{
    
    NSArray * allKeys = [param allKeys];
    NSMutableString * tempUrl = [NSMutableString stringWithString:url];
    [tempUrl appendString:@"?"];
    for (NSString * key in allKeys) {
        [tempUrl appendFormat:@"%@=%@",key,param[key]];
    }
    return tempUrl;
}

+ (BOOL)isValidateMobile:(NSString *)mobileNum{
    if (mobileNum.length != 11){
        return NO;
    }
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
     * 电信号段: 133,153,180,181,189,177,1700
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[0678])\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     */
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    /**
     * 中国联通：China Unicom
     * 130,131,132,155,156,185,186,145,176,1709
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    /**
     * 中国电信：China Telecom
     * 133,153,180,181,189,177,1700
     */
    NSString *CT = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES)){
        return YES;
    }
    else{
        return NO;
    }
}

+ (BOOL)isValidEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)isValidateHttp:(NSString *)link {
    if (link.length==0) {
        return NO;
    }
    link = [link lowercaseString];
    NSString *strRegex = @"^http[s]?://.+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strRegex];
    return [predicate evaluateWithObject:link];
}

+ (void)animationWithView:(id)view scale:(CGFloat)scale
{
    UIView *animationView = (UIView *)view;
    [UIView animateWithDuration:0.15 animations:^{
        
        animationView.transform = CGAffineTransformMakeScale(scale, scale);
        
    }completion:^(BOOL finished) {
        if (finished) {    
            [UIView animateWithDuration:0.15 animations:^{
                animationView.transform = CGAffineTransformMakeScale(1.0, 1.0);
            }];
        }
    }];
}

+ (BOOL)passwordlength:(NSString *)text{
    return text.length>=8 ? YES : NO;
}

+ (BOOL)isMatchWithSeatchText:(NSString *)searchText originalText:(NSString *)originalText
{
    BOOL result = YES;
    NSInteger start = 0;
    for (NSInteger i=0; i < searchText.length; i++) {
        if (start == originalText.length) {
            result = NO;
        }
        //
        unichar c = [searchText characterAtIndex:i];
        for (NSInteger k=start; k < originalText.length; k++) {
            if (c == [originalText characterAtIndex:k]) {
                start = k + 1;
                break;
            }
            if (k == originalText.length - 1) {
                result = NO;
            }
        }
        //
    }
    return result;
}


+ (NSMutableAttributedString *)setRangeString:(NSString *)string stringLocation:(int)stringLocation;
{
    return [self setRangeString:string stringLocation:stringLocation part1Color:[UIColor colorWithRed:0.929 green:0.498 blue:0.310 alpha:1.000] part2Color:RGB16(0x7c7c7c)];
}

+ (NSMutableAttributedString *)setRangeString:(NSString *)string stringLocation:(int)stringLocation part1Color:(UIColor *)part1Color part2Color:(UIColor *)part2Color
{
    NSMutableAttributedString *attributtes = [[NSMutableAttributedString alloc] initWithString:string];
    //减去数字之后剩余的内容长度
    int lastStringLocation = (int)string.length;
    NSRange rangePart1 = NSMakeRange(0,stringLocation);
    NSRange rangePart2 = NSMakeRange(stringLocation, lastStringLocation - stringLocation);
    [attributtes addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, stringLocation)];
    [attributtes addAttribute:NSForegroundColorAttributeName value:part1Color range:rangePart1];
    [attributtes addAttribute:NSForegroundColorAttributeName value:part2Color range:rangePart2];
    
    return attributtes;
}

+(void)windowChangeRootViewController:(UIViewController*)controller{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionFade;
    window.rootViewController = controller;
    [window.layer addAnimation:transition forKey:@"animation"];
}

+ (NSString *)chineseToPinyin:(NSString *)chineseString withSpace:(BOOL)withSpace {
    CFStringRef hanzi = (__bridge CFStringRef)chineseString;
    CFMutableStringRef string = CFStringCreateMutableCopy(NULL, 0, hanzi);
    CFStringTransform(string, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform(string, NULL, kCFStringTransformStripDiacritics, NO);
    NSString *pinyin = (NSString *)CFBridgingRelease(string);
    if (!withSpace)
    {
        pinyin = [pinyin stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    return pinyin;
}

+ (void)phoneCallWithNum:(NSString *)phoneNum{
    if (phoneNum.length != 11) {
        return;
    }
    NSMutableString * string = [NSMutableString string];
    [string appendString:[phoneNum substringToIndex:3]];
    [string appendString:@"xxxx"];
    [string appendString:[phoneNum substringFromIndex:7]];
    
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",string];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

+ (UIImage *)avatarPlaceHolderWithGender:(NSInteger)gender{
    NSString * imgTitle ;
    if (gender == 1) {
        imgTitle = @"avatarPlaceholder_m_2";
    }else{
        imgTitle = @"avatarPlaceholder_w_2";
    }
    UIImage * img = [UIImage imageNamed:imgTitle];
    return img;
}


+ (NSString *)fixLineFeedWithString:(NSString *)string{
    NSString * s = [string stringByReplacingOccurrencesOfString:@"\\\n" withString:@"\n"];
    NSString * s_ = [s stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    return s_;
    
}
+ (NSString *)removeLineFeedWithString:(NSString *)string{
    NSString * s = [string stringByReplacingOccurrencesOfString:@"\\\n" withString:@""];
    NSString * s_ = [s stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
    NSString * s__ = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return s__;
}

+(UIImage *)grayImage:(UIImage *)sourceImage
{
    int bitmapInfo =kCGImageAlphaNone;
    int width = sourceImage.size.width;
    int height = sourceImage.size.height;
    CGColorSpaceRef colorSpace =CGColorSpaceCreateDeviceGray();
    CGContextRef context =CGBitmapContextCreate (nil,width,height,8,     // bits per component
                                                 0,colorSpace,bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    if (context ==NULL) {
        return nil;
    }
    CGContextDrawImage(context,CGRectMake(0,0, width, height), sourceImage.CGImage);
    UIImage *grayImage = [UIImage imageWithCGImage:CGBitmapContextCreateImage(context)];
    CGContextRelease(context);
    return grayImage;
    
}

+(UserModel *)getUserFromDataDefault{
    
    NSString * infoStr = [DataDefault objectForKey:DataDefaultUserInfoKey isPrivate:NO];
    if (!infoStr || [infoStr intValue] == 1) {
        return nil;
    }
    NSData *jsonData = [infoStr dataUsingEncoding:NSUTF8StringEncoding];
    
    //    NSData * decode = [jsonData aes256DecryptWithkey:[DataAESKey dataUsingEncoding:NSUTF8StringEncoding] iv:nil];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if (dic) {
        UserModel * model = [[UserModel alloc] init];
        model.cellphone = dic[@"cellPhone"];
        model.uid = [dic[@"id"] integerValue];
        
        model.netToken = dic[@"netToken"];
        return model;
    }else{
        return nil;
    }
}

+ (void)saveUserToDataDefault:(NSDictionary *)infoDic{
    //    NSData * data = [infoDic data];
    NSString *infoJson = [infoDic jsonStringEncoded];
    [DataDefault setObject:infoJson forKey:DataDefaultUserInfoKey isPrivate:NO];
}

+ (void)deleUserFromDataDefault{
    [DataDefault removeWithKey:DataDefaultUserInfoKey isPrivate:NO];
}

+ (NSString *)resumeFolderPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir_Str = [paths objectAtIndex:0];
    NSString *pathStr = [docDir_Str stringByAppendingString:@"/localFile"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:pathStr]){
        //如果不存在,则说明是第一次运行这个程序，那么建立这个文件夹
        NSString *directryPath = [docDir_Str stringByAppendingPathComponent:@"localFile"];
        [fileManager createDirectoryAtPath:directryPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return pathStr;
}


+ (void)thirdResumeFileSave:(NSURL *)filePath{
    
    NSString * folderPath  = [BaseHelper resumeFolderPath];
    NSString * fileName = [filePath lastPathComponent];
    NSString * localPath = [folderPath stringByAppendingPathComponent:fileName];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray *fileList = [NSArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:folderPath error:nil]];
    
    BOOL isFileHad  = NO;
    for (NSString * file in fileList) {
        if ([fileName isEqualToString:file]) {
            isFileHad = YES;
            break;
        }
    }
    if (isFileHad) {
        [fileManager removeItemAtPath:localPath error:nil];
    }
    NSData * data = [NSData dataWithContentsOfURL:filePath];//保存的数据
    if ([data writeToFile:localPath atomically:YES]) {
        [fileManager removeItemAtPath:[filePath absoluteString] error:nil]; //删除Inbox里面的文件
    }
}

+ (NSString *)resumeLocalPahtWithName:(NSString *)fileName{
    NSString * folderPath  = [BaseHelper resumeFolderPath];
    NSString * localPath = [folderPath stringByAppendingPathComponent:fileName];
    return localPath;
}

+ (UIViewController *)windowTopViewController {
    UIViewController *resultVC;
    resultVC = [BaseHelper _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

+ (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [BaseHelper _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [BaseHelper _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}
@end
