//
//  DataDefault.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface DataDefault : NSObject

/**
 *  根据角色获取保存到disk的key
 *
 *  @param role      <#role description#>
 *  @param key       <#key description#>
 *  @param isPrivate <#isPrivate description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)getRoleKeyWithRole:(UserModel *)role key:(NSString *)key isPrivate:(BOOL)isPrivate;

/**
 *  获取沙箱数据
 *
 *  @param key       键名
 *  @param isPrivate 是否私有（如果为TRUE,且是登录状态,表示与角色相关。否则表示公用)
 *
 *  @return 值
 */
+ (id)objectForKey:(NSString *)key isPrivate:(BOOL)isPrivate;

/**
 *  保存数据到沙箱
 *
 *  @param value     要保存的值
 *  @param key       键名
 *  @param isPrivate 是否私有（如果为TRUE,且是登录状态,表示与角色相关。否则表示公用)
 */
+ (void)setObject:(id)value forKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (NSInteger)integerForKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (void)setInteger:(NSInteger)integer forKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (BOOL)boolForKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (void)setBool:(BOOL)integer forKey:(NSString *)key isPrivate:(BOOL)isPrivate;

/**
 *  生成一个带个人信息的key
 *
 *  @param key       <#key description#>
 *
 *  @return <#return value description#>
 */
+ (NSString *)getKeyWithRoleInfo:(NSString *)key;

+ (NSString *)stringForKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (void)setString:(NSString *)string forKey:(NSString *)key isPrivate:(BOOL)isPrivate;

+ (void)removeWithKey:(NSString *)key isPrivate:(BOOL)isPrivate;

/**
 *  今天做某个事件的次数
 *
 *  @param key       <#key description#>
 *  @param isPrivate <#isPrivate description#>
 *
 *  @return <#return value description#>
 */
+ (NSInteger)numberForTodayDoWithKey:(NSString *)key isPrivate:(BOOL)isPrivate;


/**
 *  设置今天做了某个事件的次数+1
 *
 *  @param key       <#key description#>
 *  @param isPrivate <#isPrivate description#>
 *
 */
+ (void)asceNumberForTodayDoWithKey:(NSString *)key isPrivate:(BOOL)isPrivate;


/**
 *  设置今天是否已经做了某个事件
 *
 *  @param key       <#key description#>
 *  @param isPrivate <#isPrivate description#>
 *
 */
+ (void)setBoolForTodayDo:(BOOL)done forKey:(NSString *)key isPrivate:(BOOL)isPrivate;

/**
 *  判断是否第一次进入APP
 *
 *  @return <#return value description#>
 */
+(BOOL)isAPPFirstLogin;

@end
