//
//  CommonDefine.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#ifndef CommonDefine_h
#define CommonDefine_h

//#define DebugWithOutNetWork   @"DebugWithOutNetWork"

#define AutoLoginImgVTag  1100
#define HRLoginImgVTag    1200
#define HRManageLoginImgVTag    1300

#define  DataDefaultUserInfoKey   @"DataDefaultUserInfoKey"
#define  DataDefaultAutologinKey  @"DataDefaultAutologinKey"

#define ListPageCount  10
#define InputBackColor  RGB16(0xfbfbfc)
#define  ViewControllerBkColor RGB16(0x1E2024)
#define  DataSoureTypeKey  @"DataSoureTypeKey"

//#define PhoneDebug  @"PhoneDebug"

#define  ThemeColor RGB16(0xd02326)

#define HSBCSystemfont [UIFont systemFontOfSize:15]
#define  HSBCSegmentationLineColor COLOR(220, 220, 220, 1)

#define COLOR(r,g,b,a) [UIColor colorWithRed:(r/(float)255) green:(g/(float)255) blue:(b/(float)255) alpha:a]
#define color(a,b,c,d) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:d]

#define RGB16(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ISIOS10ORNewer [[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0
#define ISIOS11ORNewer [[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0
#define ISIOS7ORNewer  [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0


#define BatteryH        20.0
#define ToolbarHeight   49
#define TabbarHeight    49
#define NavigationBarHeight 44

#define ScreenWidth  [UIScreen mainScreen].bounds.size.width
#define ScreenHeight  [UIScreen mainScreen].bounds.size.height

// 效果图尺寸为 750x1334

#define HorPxFit(WidthInEffectPic)   ScreenWidth * WidthInEffectPic / 750
#define VerPxFit(HeightInEffectPic) ScreenHeight * HeightInEffectPic / 1334

#define HS_CGRectMake(x, y, width, height) CGRectMake(x * HorPxFit(width), y * HeightEffect(height), width * HorPxFit(width), height * HeightEffect(height))


#define DECLARE_SINGLETON_FOR_CLASS(className) \
+ (className *)shared##className;

#define DEFINE_SINGLETON_FOR_CLASS(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}




//GCD 后台线程异步
#define DispatchBack(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
//CGD 主线程
#define DispatchMain(block) dispatch_async(dispatch_get_main_queue(), block)

#ifndef weakify
#if DEBUG
#if __has_feature(objc_arc)
#define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
#endif
#else
#if __has_feature(objc_arc)
#define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
#endif
#endif
#endif

#ifndef strongify
#if DEBUG
#if __has_feature(objc_arc)
#define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
#endif
#else
#if __has_feature(objc_arc)
#define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
#endif
#endif
#endif


#ifdef DEBUG
#define NSLog(format, ...) do {                                                                          \
fprintf(stderr, "<%s : %d> %s\n",                                           \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "-------\n");                                               \
} while (0)


/*
 　1) __VA_ARGS__ 是一个可变参数的宏，很少人知道这个宏，这个可变参数的宏是新的C99规范中新增的，目前似乎只有gcc支持（VC6.0的编译器不支持）。宏前面加上##的作用在于，当可变参数的个数为0时，这里的##起到把前面多余的","去掉的作用,否则会编译出错, 你可以试试。
 　　2) __FILE__ 宏在预编译时会替换成当前的源文件名
 　　3) __LINE__宏在预编译时会替换成当前的行号
 　　4) __FUNCTION__宏在预编译时会替换成当前的函数名称
 */
//# define DLog(fmt, ...) NSLog((@"[文件名:%s]\n" "[函数名:%s]\n" "[行号:%d] \n" fmt), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);

#define DLog(fmt, ...) NSLog((@"%s[行号:%d] " fmt),__FUNCTION__,__LINE__,##__VA_ARGS__);
//#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define pwd printf("%s %d\n", __PRETTY_FUNCTION__, __LINE__);
#define debug_rect(arg) NSLog( @"CGRect ( %f, %f, %f, %f)", arg.origin.x, arg.origin.y, arg.size.width, arg.size.height );
#define debug_point( arg ) NSLog( @"CGPoint ( %f, %f )", arg.x, arg.y );

#define DLogBool( arg )   NSLog( @"Boolean: %@", ( arg == YES ? @"YES" : @"NO" ) )

#else

#define DLog(...)
#define DLogBool(...)

#endif

#endif /* CommonDefine_h */
