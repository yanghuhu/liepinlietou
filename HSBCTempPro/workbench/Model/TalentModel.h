//
//  TalentModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TalentModel : NSObject

@property (nonatomic ,assign) NSInteger id_;            // id

// 基本信息
@property (nonatomic ,strong) NSString * avatar;        // 头像
@property (nonatomic , strong) NSString * address;   // 地址
@property (nonatomic ,strong) NSString * cellphone;         // 手机号
@property (nonatomic ,strong) NSString * name;          // 姓名
@property (nonatomic ,assign) NSTimeInterval  birthday;      // 生日
@property (nonatomic ,strong) NSString * gender;           // 性别
@property (nonatomic ,strong) NSString * email;         // 邮箱
@property (nonatomic ,strong) NSString * nativePlace;   // 籍贯
@property (nonatomic ,strong) NSString * maritalStatus;  // 婚姻状态
@property (nonatomic ,strong) NSString * education;     // 学历
@property (nonatomic , strong) NSString * expectedSalary;  // 期望月薪
@property (nonatomic , strong) NSString * jobStatus;  //  求职状态
@property (nonatomic , strong) NSString * targetWorkPlace;   // 工作目标地址
@property (nonatomic , strong) NSString * city;  // 所在城市
@property (nonatomic , strong) NSString * jobNature; // 工作性质
@property (nonatomic , assign) NSTimeInterval workStartTime;  // 开始工作时间
@property (nonatomic , assign) NSInteger workYears;  //  工作年限
@property (nonatomic , strong) NSString * profession; //行业
@property (nonatomic , strong) NSString * workExperience;

//自我评价
@property (nonatomic , strong)  NSString * selfEvaluation;  // 自我评价
// 上家公司情况
@property (nonatomic ,strong) NSString * company;    // 上家公司名称
@property (nonatomic ,strong) NSString * position;   // 上家公司职位
//  教育经历
@property (nonatomic , strong) NSString * educationExperience; // 教育经历
@property (nonatomic , strong) NSString * graduateSchool;   // 毕业院校
@property (nonatomic , strong) NSString * major;   // 专业

// 培训经历
@property (nonatomic , strong) NSString * trainingExperience;   // 培训经历

// 项目经验
@property (nonatomic , strong)  NSString * projectExperience; // 项目经验


//语言/技能

@property (nonatomic , strong) NSString * languageAbility;   // 语言
@property (nonatomic , strong) NSString * workSkills;   // 技能


@property (nonatomic , assign) float  matchedDegree; // 匹配度



// pay about
@property (nonatomic , assign) BOOL payStatus;         // 1:未付费；2：已付费
@property (nonatomic , assign) CGFloat price;    //  简历价格
@end
