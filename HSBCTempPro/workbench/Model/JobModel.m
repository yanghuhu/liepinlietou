//
//  JobModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobModel.h"

@implementation JobModel
        + (NSDictionary *)modelCustomPropertyMapper {
            
            return @{@"id_":@"id",
                     @"jobRequireArray":@"requirement",
                     @"jobResponsibitiyArray":@"responsibilities",
                     @"hrModel":@"creator",
                     };
        }

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"jobRequireArray" : [JobRequirementModel class],
             @"jobResponsibitiyArray" : [JobResponsibilityModel class] };
}


- (NSInteger)price{
    return _price/100;
}

- (NSTimeInterval)publishTime{
    return  _publishTime/1000;
}

@end
