//
//  OrderModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/4.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject

@property (nonatomic , assign) NSTimeInterval createTime;  // 创建时间
@property (nonatomic , assign) NSInteger id_;      //订单id
@property (nonatomic , strong) NSString * paymentPlatform; // 支付方式 = ['ALI_PAY', 'WECHAT', 'BANK'],
@property (nonatomic , strong) NSString * state;  // 订单状态 = ['PENDING_PAY', 'PAID'],
@property (nonatomic , assign) NSInteger totalFee; // 价格，单位分
@property (nonatomic , strong) NSString *  serialNumber;  // 订单序列号
@end
