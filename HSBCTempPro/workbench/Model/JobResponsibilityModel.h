//
//  JobResponsibilityModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/4.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobResponsibilityModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSString * description_;

@end
