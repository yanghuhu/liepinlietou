//
//  TaskModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobModel.h"
#import "TalentModel.h"
#import "CompanyModel.h"

@class CandidateModel;
@class PositionOrderModel;

@interface TaskModel : NSObject

@property (nonatomic , strong) CompanyModel * company;
@property (nonatomic , strong) NSArray <PositionOrderModel *>*positionArray;

@end


@interface PositionOrderModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSArray <CandidateModel *>* candidateArray;
@property (nonatomic , strong) JobModel * jobModel;

@end

@interface CandidateModel : NSObject

@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSString * state; // 进度 = ['Recommended', 'Accepted', 'Rejected', 'Wait_Appointment', 'Wait_Interview', 'Interview_Failed', 'Waint_Offer', 'Wait_Register', 'No_Register', 'Wait_Positive', 'No_Positive', 'Positive'],
@property (nonatomic , strong) NSString * status;  // 状态 = ['ON', 'OFF']
@property (nonatomic , assign) BOOL hrStateFlag;   //  hr状态提醒标识
@property (nonatomic , assign) BOOL hhStateFlag;     //  猎头状态提醒标识
@end
