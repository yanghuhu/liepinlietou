//
//  HRModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyModel.h"

@interface HRModel : NSObject

@property (nonatomic , assign) NSInteger id_;        // id
@property (nonatomic , strong) NSString * name;      // 姓名
@property (nonatomic , strong) NSString * headPic;    // 头像
@property (nonatomic , strong) NSString * cellphone;     // 联系方式


@property (nonatomic , strong) CompanyModel * company;


@end
