//
//  SysMessageModel.h
//  HSBCTempPro
//
//  Created by Deve on 2018/3/16.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ContentModel;
@class SortModel;

@interface SysMessageModel : NSObject

@property (nonatomic,strong)NSArray<ContentModel*> *content;
@property (nonatomic,strong)NSArray<SortModel *> *sort;
@property (nonatomic,strong)NSString *first;
@property (nonatomic,strong)NSString *last;
@property (nonatomic,strong)NSString *number;
@property (nonatomic,strong)NSString *numberOfElements;
@property (nonatomic,strong)NSString *size;
@property (nonatomic,strong)NSString *totalElements;
@property (nonatomic,strong)NSString *totalPages;


@end

@interface ContentModel : NSObject

@property (nonatomic,strong)NSString *content;
@property (nonatomic,strong)NSString *id;
@property (nonatomic,strong)NSString *messageLog;
@property (nonatomic,strong)NSString *readFlag;
@property (nonatomic,strong)NSString *receiver;
@property (nonatomic)NSTimeInterval sendTime;


@end

@interface SortModel : NSObject

@property (nonatomic,strong)NSString *ascending;
@property (nonatomic,strong)NSString *descending;
@property (nonatomic,strong)NSString *direction;
@property (nonatomic,strong)NSString *ignoreCase;
@property (nonatomic,strong)NSString *nullHandling;
@property (nonatomic)NSTimeInterval property;

@end

//content =     (
//               {
//                   content = "\U6211\U8fd8\U5728\U6d4b\U8bd5";
//                   id = 3;
//                   messageLog = 740280291719012352;
//                   readFlag = 1;
//                   receiver = 736661756800417792;
//                   sendTime = 1521081806000;
//               },
//               {
//                   content = "\U6d4b\U8bd5\U6d4b\U8bd5";
//                   id = 1;
//                   messageLog = 740280291719012352;
//                   readFlag = 1;
//                   receiver = 736661756800417792;
//                   sendTime = 1521081672000;
//               },
//               {
//                   content = "\U6211\U53c8\U5728\U6d4b\U8bd5";
//                   id = 2;
//                   messageLog = 740280291719012352;
//                   readFlag = 1;
//                   receiver = 736661756800417792;
//                   sendTime = 1520908967000;
//               }
//               );
//first = 1;
//last = 1;
//number = 0;
//numberOfElements = 3;
//size = 10;
//sort =     (
//            {
//                ascending = 0;
//                descending = 1;
//                direction = DESC;
//                ignoreCase = 0;
//                nullHandling = NATIVE;
//                property = sendTime;
//            }
//            );
//totalElements = 3;
//totalPages = 1;

