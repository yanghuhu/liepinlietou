//
//  JobModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HRModel.h"
#import "JobRequirementModel.h"
#import "JobResponsibilityModel.h"
#import "CompanyModel.h"

typedef NS_ENUM(NSInteger,JobState) {
    JobStateJieBang = 1,  //  接榜
    JobStateTuijian,      // 推荐
    JobStateYuyue,        // 预约
    JobStateMianshi,      //  面试
    JobStateOffer,        //  达成offer
    JobStateReward,       //   付酬金
};

@interface JobModel : NSObject

@property (nonatomic , assign) NSInteger id_;               // 职位id
@property (nonatomic , assign) NSInteger price;            // 职位赏金
@property (nonatomic , strong) NSString * level;            //  职务级别
@property (nonatomic , strong) NSString * position;     //  职务  Java软件开发
@property (nonatomic , strong) NSString * profession;   // 行业
@property (nonatomic , assign) NSTimeInterval publishTime;    // 发布时间
@property (nonatomic , assign)  NSInteger recruitingNumber;  // 招聘人数
@property (nonatomic , assign) NSInteger salaryRange;   //  薪资范围
@property (nonatomic , strong) NSString * state;   // ['SUBMIT', 'PUBLISH', 'COMPLETED']
@property (nonatomic , strong) NSString * title;   //  急招Java高级开发
@property (nonatomic , strong)  NSString * updateTime;  //修改时间
@property (nonatomic , strong) NSString * workArea;   //  工作地点
@property (nonatomic , strong) NSString * workExperience;  //  工作年限
@property (nonatomic , strong) NSString * education;
@property (nonatomic , strong) NSString * jobNature;
@property (nonatomic , strong) NSString * additionalInfo;   //  补充说明

@property (nonatomic , strong) HRModel * hrModel;          // hr model
@property (nonatomic , strong) NSArray <JobRequirementModel *>*jobRequireArray;
@property (nonatomic , strong) NSArray <JobResponsibilityModel *>*jobResponsibitiyArray;


@end
