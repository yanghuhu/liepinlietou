//
//  TalentModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentModel.h"

@implementation TalentModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id"
             };
}

- (NSTimeInterval)birthday{
    return _birthday/1000;
}
@end
