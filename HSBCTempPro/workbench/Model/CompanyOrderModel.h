//
//  CompanyOrderModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobModel.h"

@interface CompanyOrderModel : NSObject

@property (nonatomic , strong) JobModel *jobModel;
@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , assign) NSTimeInterval createTime;
@property (nonatomic , assign) BOOL firstFlag;
@end
