//
//  JobRequirementModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/4.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobRequirementModel.h"

@implementation JobRequirementModel

+ (NSDictionary *)modelCustomPropertyMapper {

    return @{@"id_":@"id",
             @"description_":@"description"};

}
@end
