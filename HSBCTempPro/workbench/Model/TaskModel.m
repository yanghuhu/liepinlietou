//
//  TaskModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TaskModel.h"

@implementation TaskModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"positionArray" : [PositionOrderModel class]};
}
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"positionArray":@"orderResponses"};
}
@end


@implementation PositionOrderModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"candidateArray" : [CandidateModel class]};
}

+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id",
             @"jobModel":@"jobDetail",
             @"candidateArray":@"items"
             };
}
@end

@implementation CandidateModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id",
             @"talentModel":@"candidate"
             };
}

@end
