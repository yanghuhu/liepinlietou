//
//  CompanyOrderModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "CompanyOrderModel.h"

@implementation CompanyOrderModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"id_":@"id",
             @"jobModel":@"jobDetail"
             };
}
@end
