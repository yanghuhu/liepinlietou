//
//  JobOrderDetailModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/10.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobOrderDetailModel.h"

@implementation JobOrderDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"talentModel":@"candidate",
             @"orderModel":@"jobTaskOrder",
             @"id_":@"id"
             };
}

- (OrderStep)orderStep{
    if ([_state isEqualToString:PositionCandidateOrderStateRecommended] ||               [_state isEqualToString:PositionCandidateOrderStateRejected] ) {
        _orderStep = OrderStepRecommend;
    }else if ([_state isEqualToString:PositionCandidateOrderStateAccepted] ||
              [_state isEqualToString:PositionCandidateOrderStateWait_Appointment]||
              [_state isEqualToString:PositionCandidateOrderStateWait_Interview] ||
              [_state isEqualToString:PositionCandidateOrderStateInterview_Failed] ||
              [_state isEqualToString:PositionCandidateOrderStateWaint_Offer]){
        _orderStep = OrderStepInterView;
    }else{
        _orderStep = OrderStepPositive;
    }
    return _orderStep;
}

- (NSTimeInterval)reportDutyTime{
    return _reportDutyTime/1000;
}

- (NSTimeInterval)payTime{
    return _payTime/1000;
}

- (NSTimeInterval)interviewTime{
    return  _interviewTime/1000;
}
- (NSTimeInterval)positiveTime{
    return _positiveTime/1000;
}
- (NSTimeInterval)appointmentTime{
    return _appointmentTime/1000;
}
@end

@implementation OfferModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id",
             @"description_":@"description"
             };
}

- (NSTimeInterval)createTime{
    return _createTime/1000;
}
- (NSTimeInterval)reportDutyTime{
    return _reportDutyTime/1000;
}
@end

