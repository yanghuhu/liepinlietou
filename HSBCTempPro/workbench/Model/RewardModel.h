//
//  RewardModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobModel.h"
#import "TalentModel.h"
#import "CompanyOrderModel.h"

@class JobTaskOrder;

@interface RewardModel : NSObject

@property (nonatomic , strong) TalentModel  * candidate;
@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) JobTaskOrder * jobTaskOrder;
@property (nonatomic , strong) NSString * state;  // 状态

@end

@interface JobTaskOrder:NSObject

@property (nonatomic , strong) JobModel * jobModel;

@end

