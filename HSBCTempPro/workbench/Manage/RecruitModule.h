//
//  RecruitModule.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModule.h"
#import "HRModel.h"
#import "JobModel.h"
#import "TalentModel.h"
#import "TaskModel.h"
#import "RewardModel.h"

@interface RecruitModule : BaseModule

/*
 * 获取职位需求列表
 * @param
 *      pageNum  当前页面
 * succBlock
 *       NSArray  [JobModel]
 */
+ (void)getJobDemandListWithPageNum:(NSInteger)pageNum
                          searchKey:(NSString *)searchKey
                            Success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/*
 * 获取职位详细信息
 * @param
 *    jobId: 职位id
 * succBlock
 *     JobModel
 */
+ (void)getJobDetailWithId:(NSInteger)jobId
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;
/*
 * 获取hr详细信息
 * @param
 *     hrId:  hrId
 * succBlock
 *     HRModel
 */
+ (void)getHrInfoWithId:(NSInteger)hrId
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock;


/*
 * 猎头揭榜
 * @param
 *      jobId: 职位id
 * succBlock
 *      BOOL isSuccess
 */
+ (void)acceptJobNeedWithJobId:(NSInteger)jobId
                       success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock;


/*
 *  人才库列表
 *  @param
 *      pageNum  当前页面
 *  succBlock
 *      NSArray  [TalentModel]
 */
+ (void)getTalentListWithPageNum:(NSInteger)pageNum
                       searchKey:(NSString *)searchKey
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;


/*
 *  生成简历购买订单
 *  @param
 *      talentId: 人才id
 *  succBlock
 *      BOOL isSuccess
 */
+ (void)talentOrderCreateWithId:(NSInteger)talentId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;
/*
 *  人才简历支付成功
 *  @param
 *      title:  标题
 *      detail: 描述
 *      totalFee : 金额
 *      serialNumber： 序列号
 *  succBlock
 *      BOOL isSuccess
 */

+ (void)talentPayWithTitle:(NSString *)title
                    detail:(NSString *)detail
                  totalFee:(CGFloat)totalFee
              serialNumber:(NSString *)serialNumber
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;

/*
 *  获取人才简历详细信息
 *  @param
 *      resumeId: 人才id
 *   succBlock
 *      TalentModel
 */
+ (void)getTalentDetailWithId:(NSInteger)talentId
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock;

/*
 * 获取任务表列表
 * @param
 *      pageNum   页码
 * success
 *      NSArray [TaskModel]
 */
+ (void)getTaskListWithPage:(NSInteger)pageNum
                    Success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;

/*
 * 获取酬金库列表
 * success
 *      RewardModel
 */
+ (void)getRewardListWithPage:(NSInteger)pageNum
                     withType:(NSString *)type
                      success:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock;

/*
 * 向职位推荐人才
 * @param
 *    talentId  人才id
 *     positionId  职位id
 */
+ (void)talentRecommendWithTanlentId:(NSInteger)talentId
                          orderId:(NSInteger)orderId
                             success:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock;

/*
 * 获取职务列表
 * @param
 *    pageNum  页码
 *  succBlock
 *       【JobModel】
 */
+ (void)getPositionListWithPage:(NSInteger)pageNum
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 * 获取候选人详情
 * orderId   候选人订单id
 */
+ (void)getCandidateDetailWithId:(NSInteger)orderId
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;

/*
 * 获取我的简历列表
 * @param
 *      page   页面
 */
+ (void)getMyTalentListWithPage:(NSInteger)page
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/*
 * 提交预约时间
 * @param
 *      time  预约时间
 *      id_   订单id
 */
+ (void)submitYuyueTime:(NSTimeInterval)time
                    id_:(NSInteger)id_
                 iphone:(NSString *)iphone
                address:(NSString *)address
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock;
/*  猎头申请佣金
 *  @param
 *      id  订单id
 */
+ (void)requestForReward:(NSInteger)id_
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;

/*   提交评论
 *      sorce  分数
 *      orderId   订单id
 */
+ (void)submitRate:(CGFloat)sorce
            withId:(NSInteger)orderId
           success:(RequestSuccessBlock)succBlock
           failure:(RequestFailureBlock)failBlock;

/*
 *  红点已读
 *      id_   订单id
 */
+ (void)hongdianFlagRead:(NSInteger)id_
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;

/*
 *  获取本周数据
 */
+ (void)getWeekDataStatisticsSuccess:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock;

/*
 *  上传本地简历文件
 */
+ (void)localResumeUploadWithPath:(NSString *)path
                          Success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock;

/*
 *获取未读消息
 */
+ (void)getRetomeServerMessageAccounSuccess:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock;

/*
 *清除未读消息
 */
+ (void)clearNotReaderMessageSuccess:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock;


/*
 *获取服务器消息列表
 */
+ (void)getRetomeSrverMessageList:(NSString *)messageType
                             page:(NSInteger)page
                          Success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock;

/*
 *  获取简历候选人的软电话虚拟号码
 *  param:
 *        talentId  简历id
 */
+ (void)getTalentNetworkPhone:(NSInteger)talentId
                      Success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock;

/*
 *  获取职位推荐中候选人或HR的虚拟号码
 *  param:
 *      orderId  订单id
 *      personType  身份  HR/Talent  
 */
+ (void)getAboutPositionVirtualNumWithOrder:(NSInteger)orderId
                          personType:(NSString *)personType
                             success:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock;


@end



