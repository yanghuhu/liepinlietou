//
//  RecruitMock.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RecruitMock.h"
#import "JobModel.h"
#import "HRModel.h"
#import "TalentModel.h"
#import "TaskModel.h"
#import "RewardModel.h"

//#define NeedMockData  @YES

@implementation RecruitMock

+ (BOOL)isNeedMockData
{
#ifdef NeedMockData
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].needMock) {
        return YES;
    }else{
        return NO;
    }
#else
    return NO;
#endif
}

+ (instancetype)createWithFunctionName:(NSString *)functionName
{
    return [self createWithFunctionName:functionName forceMockData:NO];
}

+ (instancetype)createWithFunctionName:(NSString *)functionName forceMockData:(BOOL)forceMockData
{
#if RELEASE
    forceMockData = NO;
#endif
    if (forceMockData || [self isNeedMockData]) {
        RecruitMock *mockData = [[RecruitMock alloc] init];
        mockData.functionName = functionName;
        return mockData;
    }
    return nil;
}

- (void)getJobDemandListWithPageNum_Success_failure_{
    
    NSString * jsonData = @"{\"status\":\"1\",\"message\":\"\",\"data\":[{\"id\":1,\"reward\":\"500\",\"hrId\":1,\"company\":\"大数据软件科技\",\"position\":\"产品经理\",\"education\":\"本科\",\"compensation\":\"9~12k\",\"experienceReq\":\"3-5年\",\"city\":\"西安\",\"age\":\"30-40岁\",\"jobDesc\":\"端正的工作态度及团队合作意识\",\"companyTag\":\"互联网,大数据,网络安全\"},{\"id\":2,\"reward\":\"1000\",\"hrId\":2,\"company\":\"北京所问数据科技\",\"position\":\"java高级工程师\",\"education\":\"本科\",\"compensation\":\"10~15k\",\"experienceReq\":\"5-8年\",\"city\":\"西安\",\"age\":\"30-40岁\",\"jobDesc\":\"严谨的代码风格及团队合作意识\",\"companyTag\":\"互联网,大数据,网络安全\"},{\"id\":3,\"reward\":\"1000\",\"hrId\":3,\"company\":\"九次方大数据信息集团\",\"position\":\"web前端工程师\",\"education\":\"本科\",\"compensation\":\"7~10k\",\"experienceReq\":\"2-5年\",\"city\":\"西安\",\"age\":\"20-40岁\",\"jobDesc\":\"积极热情的工作态度，有一定审美\",\"companyTag\":\"大数据,智能硬件\"}]}";

    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSArray * data = responseDic[@"data"];
        NSArray * modelArray = [NSArray modelArrayWithClass:[JobModel class] json:[data modelToJSONObject]];
        NSMutableArray * array  = [NSMutableArray arrayWithArray:modelArray];
        static int i = 0;
        if (i%2 ==0) {
            [array addObjectsFromArray:modelArray];
            [array addObjectsFromArray:modelArray];
            [array addObjectsFromArray:modelArray];
            [array addObjectsFromArray:modelArray];
        }else{
            [array addObjectsFromArray:modelArray];
        }
        [self.callbackData addObject:array];
    }
}

- (void)getJobDetailWithId_success_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"uid\":1,\"elfId\":\"4324141243\",\"mobile\":\"15300000000\",\"name\":\"星驰\",\"gender\":1,\"age\":18,\"signature\":\"这个人很懒，什么都没留下\",\"avatar\":\"http://img_path\",\"coin\":\"2345\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        JobModel * model = [JobModel modelWithDictionary:info];
        [self.callbackData addObject:model];
    }
}

- (void)getHrInfoWithId_success_failure_{
    
//    NSString * fir = @"{\"id_\":1,\"name\":\"吴刚\",\"avatar\":\"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2219978258,2981506592&fm=27&gp=0.jpg\",\"sex\":1,\"phone\":\"17791535635\",\"company\":\"大数据软件科技\"}";
//    NSString * sec = @"{\"id_\":2,\"name\":\"谢佳琪\",\"avatar\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1511833819&di=97f3c948cff3ca0a22f48b3765c165e3&imgtype=jpg&er=1&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2Ff3d3572c11dfa9ecc118b78a68d0f703918fc17d.jpg\",\"sex\":2,\"phone\":\"18756327942\",\"company\":\"北京所问数据科技有限公司\"}";
//    NSString * thir = @"{\"id_\":3,\"name\":\"龚伟\",\"avatar\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1511833864&di=6062966752db0849ad31f2cf8af88788&imgtype=jpg&er=1&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimage%2Fc0%253Dshijue1%252C0%252C0%252C294%252C40%2Fsign%3De6bf724a5c4e9258b2398eadf4ebbb2d%2Fd009b3de9c82d1582323c39f8a0a19d8bc3e427f.jpg\",\"sex\":1,\"phone\":\"15539894598\",\"company\":\"九次方大数据信息集团\"}";

    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"id_\":2,\"name\":\"谢佳琪\",\"avatar\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1511833819&di=97f3c948cff3ca0a22f48b3765c165e3&imgtype=jpg&er=1&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2Ff3d3572c11dfa9ecc118b78a68d0f703918fc17d.jpg\",\"sex\":2,\"phone\":\"18756327942\",\"company\":\"北京所问数据科技有限公司\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        HRModel * model = [HRModel modelWithDictionary:info];
        [self.callbackData addObject:model];
    }
}
- (void)acceptJobNeedWithJobId_success_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"uid\":1,\"elfId\":\"4324141243\",\"mobile\":\"15300000000\",\"name\":\"星驰\",\"gender\":1,\"age\":18,\"signature\":\"这个人很懒，什么都没留下\",\"avatar\":\"http://img_path\",\"coin\":\"2345\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        BOOL isSuccess = [info[@"isSuccess"] boolValue];
        [self.callbackData addObject:@(isSuccess)];
    }
}



- (void)getTalentListWithPageNum_success_failure_{
    
    NSString *jsonData = @"{\"status\":\"1\",\"message\":\"\",\"data\":[{\"id_\":1,\"avatar\":\"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=393019269,4033178765&fm=27&gp=0.jpg\",\"phone\":\"17791535635\",\"name\":\"田小娥\",\"sex\":2,\"email\":\"44319qq.com\",\"nativePlace\":\"福建\",\"education\":\"本科\",\"workYears\":4,\"marriageState\":1,\"lastJobCompany\":\"北京新才科技有限公司\",\"lastJobPosition\":\"信息主管\",\"isPay\":1,\"brithday\":\"1980-2-14\"},{\"id_\":2,\"avatar\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1510815265043&di=673b338ecc1d888de38df8c79f2b7190&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2F79f0f736afc3793125cd730ce1c4b74543a91169.jpg\",\"phone\":\"18978654534\",\"name\":\"王百川\",\"sex\":1,\"email\":\"baichuan163.com\",\"nativePlace\":\"江苏\",\"education\":\"研究生\",\"workYears\":2,\"marriageState\":1,\"lastJobCompany\":\"九次方大数据有限公司\",\"lastJobPosition\":\"大数据工程师\",\"isPay\":1,\"brithday\":\"1982-9-10\"},{\"id_\":4,\"avatar\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1510815235058&di=38e53023a723e16f3045d54f3d77424a&imgtype=jpg&src=http%3A%2F%2Fimg2.imgtn.bdimg.com%2Fit%2Fu%3D2317600492%2C3239299417%26fm%3D214%26gp%3D0.jpg\",\"phone\":\"15598383390\",\"name\":\"席婉茹\",\"sex\":2,\"email\":\"73874667qq.com\",\"nativePlace\":\"广西\",\"education\":\"本科\",\"workYears\":5,\"marriageState\":1,\"lastJobCompany\":\"上海森亿科技有限公司\",\"lastJobPosition\":\"高级java工程师\",\"isPay\":2,\"brithday\":\"1990-10-11\"}]}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSArray  * data = responseDic[@"data"];
        NSArray * models = [NSArray modelArrayWithClass:[TalentModel class] json:[data modelToJSONObject]];
        [self.callbackData addObject:models];
    }
}


- (void)talentPayWithId_success_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"uid\":1,\"elfId\":\"4324141243\",\"mobile\":\"15300000000\",\"name\":\"星驰\",\"gender\":1,\"age\":18,\"signature\":\"这个人很懒，什么都没留下\",\"avatar\":\"http://img_path\",\"coin\":\"2345\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        BOOL isSuccess = [info[@"isSuccess"] boolValue];
        [self.callbackData addObject:@(isSuccess)];
    }
}

- (void)getTalentDetailWithId_success_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"uid\":1,\"elfId\":\"4324141243\",\"mobile\":\"15300000000\",\"name\":\"星驰\",\"gender\":1,\"age\":18,\"signature\":\"这个人很懒，什么都没留下\",\"avatar\":\"http://img_path\",\"coin\":\"2345\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        TalentModel * model = [TalentModel modelWithDictionary:info];
        [self.callbackData addObject:model];
    }
}

- (void)getTaskListSuccess_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":{\"uid\":1,\"elfId\":\"4324141243\",\"mobile\":\"15300000000\",\"name\":\"星驰\",\"gender\":1,\"age\":18,\"signature\":\"这个人很懒，什么都没留下\",\"avatar\":\"http://img_path\",\"coin\":\"2345\"}}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSArray  * info = responseDic[@"data"];
        NSArray * taskArray = [NSArray modelArrayWithClass:[TaskModel class] json:[info modelToJSONObject]];
        [self.callbackData addObject:taskArray];
    }
}

- (void)getRewardListWithPage_success_failure_{
    NSString *jsonData =  @"{\"status\":1,\"message\":\"请求成功\",\"data\":[{\"company\":\"某大型IT服务商\",\"position\":[{\"id\":2,\"reward\":\"1000\",\"hrId\":\"1\",\"company\":\"某大型IT服务商\",\"position\":\"技术总监\",\"education\":\"研究生\",\"compensation\":\"20~30k\",\"experienceReq\":\"7-10年\",\"city\":\"西安\",\"age\":\"35-40岁\",\"jobState\":\"1\"},{\"id\":3,\"reward\":\"700\",\"hrId\":\"1\",\"company\":\"某大型IT服务商\",\"position\":\"服务器运营助理\",\"education\":\"本科\",\"compensation\":\"10~12k\",\"experienceReq\":\"3-5年\",\"city\":\"西安\",\"age\":\"25-40岁\",\"jobState\":\"6\"}]},{\"company\":\"大数据软件科技\",\"position\":[{\"id\":1,\"reward\":\"500\",\"hrId\":\"1\",\"company\":\"大数据软件科技\",\"position\":\"产品经理\",\"education\":\"本科\",\"compensation\":\"9~12k\",\"experienceReq\":\"3-5年\",\"city\":\"西安\",\"age\":\"30-40岁\",\"jobState\":\"3\"}]}]}";
    
    NSData *data = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if (responseDic) {
        NSDictionary  * info = responseDic[@"data"];
        NSArray * taskArray = [NSArray modelArrayWithClass:[RewardModel class] json:[info modelToJSONObject]];
        [self.callbackData addObject:taskArray];
    }
}
@end
