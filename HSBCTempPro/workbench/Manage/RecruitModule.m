//
//  RecruitModule.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RecruitModule.h"
#import "RecruitMock.h"
#import "OrderModel.h"
#import "CompanyOrderModel.h"
#import "JobOrderDetailModel.h"
#import "NSString+YYAdd.h"
#import "SysMessageModel.h"
#import <MJExtension/MJExtension.h>

static RecruitMock *mockData;

@implementation RecruitModule

+ (void)getJobDemandListWithPageNum:(NSInteger)pageNum
                          searchKey:(NSString *)searchKey
                            Success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"size" : @(ListPageCount),
                            @"page":@(pageNum),
                            @"sort":@"createTime,desc"
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    if (searchKey) {
        [dic setObject:[searchKey stringByURLEncode] forKey:@"title"];
    }
    
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:@"jobDetail/getJobDetails" mockObj:mockData params:dic handle:^NSArray *(id response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        if (data) {
            NSArray * modelArray = [NSArray modelArrayWithClass:[JobModel class] json:[data modelToJSONObject]];
            return @[@YES,modelArray];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getJobDetailWithId:(NSInteger)jobId
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"cmd" : @(111001)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:nil mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSDictionary * info = response_[@"data"];
        if (info) {
            JobModel * model = [JobModel modelWithDictionary:info];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getHrInfoWithId:(NSInteger)hrId
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"id" : @(hrId)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:nil mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSDictionary * info = response_[@"data"];
        if (info) {
            HRModel * model = [HRModel modelWithDictionary:info];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)acceptJobNeedWithJobId:(NSInteger)jobId
                       success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"id" : @(jobId)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"jobDetailOrder/submitOrder" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            CompanyOrderModel * model = [CompanyOrderModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTalentListWithPageNum:(NSInteger)pageNum
                       searchKey:(NSString *)searchKey
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"page" : @(pageNum),
                            @"size":@(ListPageCount)};
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    if (searchKey) {
        [dic setObject:[searchKey stringByURLEncode] forKey:@"name"];
    }
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:@"human/resumes" mockObj:mockData params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        if (data) {
            NSArray * models = [NSArray modelArrayWithClass:[TalentModel class] json:[data modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}
+ (void)talentOrderCreateWithId:(NSInteger)talentId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"id" : @(talentId)};
//    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"human/resumeOrder" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            OrderModel * model = [OrderModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)talentPayWithTitle:(NSString *)title
                    detail:(NSString *)detail
                  totalFee:(CGFloat)totalFee
           serialNumber:(NSString *)serialNumber
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"orderSerialNumber" : serialNumber,
                            @"paymentPlatform":@"BALANCE",
                            @"account":@([HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.uid),
                            @"detail":detail,
                            @"title":title,
                            @"totalFee":@(totalFee)
                            };
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"payController/payWithoutValidate" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTalentDetailWithId:(NSInteger)talentId
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    
    NSString * api = [NSString stringWithFormat:@"human/resume/%ld",talentId];
    
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:api mockObj:mockData params:nil handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            TalentModel * model = [TalentModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTaskListSuccess:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"cmd" : @(111111)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:nil mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"data"];
        if (data) {
            NSArray * taskArray = [NSArray modelArrayWithClass:[TaskModel class] json:[data modelToJSONObject]];
            return @[@YES,taskArray];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getRewardListWithPage:(NSInteger)pageNum
                     withType:(NSString *)type
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"page" : @(pageNum),
                            @"size":@(10),
                            @"type":type,
                            @"sort":@"createTime,desc"
                            };
    
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:@"jobDetailOrder/getCommissionJobTaskOrders" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        if (data) {
            NSArray * rewardArray = [NSArray modelArrayWithClass:[RewardModel class] json:[data modelToJSONObject]];
            return @[@YES,rewardArray];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)talentRecommendWithTanlentId:(NSInteger)talentId
                          orderId:(NSInteger)orderId
                             success:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"taskId":@(orderId),
                            @"candidateId":@(talentId)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"jobDetailOrder/recommendHuman" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
    
}
+ (void)getPositionListWithPage:(NSInteger)pageNum
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"size" : @(ListPageCount),
                            @"page":@(pageNum),
                            @"sort":@"createTime,desc"
                            };
    
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:@"jobDetailOrder/getJobTaskOrderList" mockObj:mockData params:para handle:^NSArray *(id response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        
        if (data) {
            NSArray * modelArray = [NSArray modelArrayWithClass:[CompanyOrderModel class] json:[data modelToJSONObject]];
            return @[@YES,modelArray];
        }else{
            if (response_) {
                return @[@YES];
            }
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTaskListWithPage:(NSInteger)pageNum
                    Success:(RequestSuccessBlock)succBlock
                    failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"page" : @(pageNum),
                            @"size":@(100)};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    
    [NetworkHandle getRequestForApi:@"jobDetailOrder/getJobTaskOrders" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        if (data) {
            NSArray * models = [NSArray modelArrayWithClass:[TaskModel class] json:[data modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getCandidateDetailWithId:(NSInteger)orderId
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    
    [NetworkHandle getRequestForApi:[NSString stringWithFormat:@"jobDetailOrder/getJobTaskOrderItem/%ld",orderId] mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobOrderDetailModel * model = [JobOrderDetailModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getMyTalentListWithPage:(NSInteger)pageNum
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"page" : @(pageNum),
                            @"sort":@"createTime,desc"};
    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    
    [NetworkHandle getRequestForApi:@"human/myResumes" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * data = response_[@"content"];
        if (data) {
            NSArray * models = [NSArray modelArrayWithClass:[TalentModel class] json:[data modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitYuyueTime:(NSTimeInterval)time
                    id_:(NSInteger)id_
                 iphone:(NSString *)iphone
                address:(NSString *)address
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"appointmentTime" : @(time),
                            @"id":@(id_),
                            @"interviewAddress":address,
                            @"interviewContacts":iphone
                            };
    [NetworkHandle postRequestForApi:@"jobDetailOrder/sendInterviewQRCode" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString * url = response_[@"url"];
        if (url) {
            return @[@YES,url];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}

+ (void)requestForReward:(NSInteger)id_
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"id":@(id_)};
    [NetworkHandle postRequestForApi:@"jobDetailOrder/applyCommission" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}

+ (void)submitRate:(CGFloat)sorce
            withId:(NSInteger)orderId
           success:(RequestSuccessBlock)succBlock
           failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(orderId),@"comment":@"",@"evaluateType":@"HEADHUNTER",@"score":@(sorce)};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/evaluate" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
    
}

+ (void)hongdianFlagRead:(NSInteger)id_
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(id_),@"requestType":@"HEADHUNTER"};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/resetChangeStateFlag" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)getWeekDataStatisticsSuccess:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock{
    
    [NetWorkHelper getRequestForApi:@"statistics/recommendStatistics" params:nil handle:^NSArray *(id response) {
        if (response) {
            return @[@YES,response];
        }else{
            return @[@YES];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)localResumeUploadWithPath:(NSString *)path
                          Success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock{
    
    [NetWorkHelper requestForUploadFileSync:path success:^(id response){
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getRetomeServerMessageAccounSuccess:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    [NetworkHandle getRequestForApi:@"message/getMessageInfo" mockObj:nil params:nil handle:^NSArray *(id response) {
        
        if (response) {
            return @[@YES,response];
        }else{
            return @[@NO];
        }
    }success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)clearNotReaderMessageSuccess:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    [NetworkHandle postRequestForApi:@"message/clearUnreadMessage" mockObj:nil params:nil handle:^NSArray *(id response) {
        return @[@YES];
        
    }success:^(id response){
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getRetomeSrverMessageList:(NSString *)messageType page:(NSInteger)page Success:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    
    NSDictionary *param = @{@"messageType":messageType,
                            @"page":@(page),
                            @"size":@(10),
                            @"sort":@"sendTime,desc"
                            };
    [NetworkHandle getRequestForApi:@"message/getMessages" mockObj:nil params:param handle:^NSArray *(id response) {
        
        NSDictionary *dict = (NSDictionary *)response;
        SysMessageModel *model = [SysMessageModel mj_objectWithKeyValues:dict];
        if (response) {
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    }success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTalentNetworkPhone:(NSInteger)talentId
                      Success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * param = @{@"id":@(talentId)};
    [NetWorkHelper getRequestForApi:@"privacyNumber/getCandidateVNForResume" params:param handle:^NSArray *(id response) {
        NSDictionary * info = (NSDictionary *)response;
        NSString * phoneNum = info[@"virtualNumber"];
        if (phoneNum) {
            return @[@(YES),phoneNum];
        }else{
            return @[@(YES)];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}
    

+ (void)getAboutPositionVirtualNumWithOrder:(NSInteger)orderId
                          personType:(NSString *)personType
                             success:(RequestSuccessBlock)succBlock
                             failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * param = @{@"id":@(orderId)};
    NSString * api;
    if ([personType isEqualToString:@"HR"]) {
        api = @"privacyNumber/getHRVNForOrder";
    }else{
        api = @"privacyNumber/getCandidateVNForOrder";
    }
    [NetWorkHelper getRequestForApi:api params:param handle:^NSArray *(id response) {
        NSDictionary * info = (NSDictionary *)response;
        NSString * phoneNum = info[@"virtualNumber"];
        if (phoneNum) {
            return @[@(YES),phoneNum];
        }else{
            return @[@(YES)];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}
@end
