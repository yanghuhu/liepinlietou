//
//  PayPopView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/3/22.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "PayPopView.h"

@interface PayPopView(){
    UILabel * titleLb;
    UILabel * moneyLb;
}
@end

@implementation PayPopView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR(20, 23, 31, 1);
//        self.backgroundColor = [UIColor yellowColor];
        self.layer.cornerRadius = 10;
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    CGFloat lbH = VerPxFit(80);
    UIFont * textFont =  [UIFont systemFontOfSize:15];
    titleLb = [self lb:nil font:[UIFont systemFontOfSize:19]];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(VerPxFit(15));
        make.left.and.right.equalTo(self);
        make.height.mas_equalTo(lbH);
    }];
    
//    UILabel * moneyTitle = [self lb:@"金额:" font:titleFont];
//    moneyTitle.textAlignment = NSTextAlignmentCenter;
//    [moneyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).mas_offset(HorPxFit(20));
//        make.right.mas_equalTo(self.mas_centerX);
////        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(10));
//        make.centerY.mas_equalTo(self.mas_centerY);
//        make.size.mas_equalTo(CGSizeMake(HorPxFit(150), lbH));
//    }];
    
    moneyLb = [self lb:nil font:textFont];
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.centerY.mas_equalTo(self.mas_centerY).mas_offset(-VerPxFit(15));
        make.height.mas_equalTo(lbH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.titleLabel.font = textFont;
    sureBt.titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self).mas_offset(-VerPxFit(30));
        make.width.mas_equalTo(HorPxFit(320));
        make.height.mas_equalTo(VerPxFit(70));
    }];
}

- (void)didMoveToSuperview{
    if (self.superview) {
        titleLb.text = [NSString stringWithFormat:@"支付%@的简历",_talentModel.name];
        NSString * text = [NSString stringWithFormat:@"金额:  %ld元",_orderModel.totalFee];
        
        moneyLb.attributedText = [BaseHelper setSourceString:text targetString:[NSString stringWithFormat:@"%ld",_orderModel.totalFee] forFont:[UIFont systemFontOfSize:25] color:color(221, 190, 127, 1) customFont:[UIFont systemFontOfSize:16] customColor:[UIColor whiteColor]];
    }
}

- (UILabel *)lb:(NSString *)text font:(UIFont *)font{
    UILabel * lb = [[UILabel alloc] init];
    lb.text = text;
    lb.textColor = [UIColor whiteColor];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = font;
    [self addSubview:lb];
    return lb;
}

- (void)sureAction{
    [self.delegate payAction];
}

@end
