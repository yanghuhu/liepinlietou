//
//  MoneylistCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MoneylistCell.h"
#import "RewardCell.h"
#import "UIView+YYAdd.h"
#import "UIImageView+YYWebImage.h"

#define CellIdentifier  @"PositionCellId"
#define CellHeight 40

@interface MoneylistCell()<UITableViewDelegate,UITableViewDataSource>{
    
    UIImageView * avatarImg;
    UILabel * nameLb;
    UILabel * positionLb;
    UILabel * companyLb;
    UIImageView * rewardImgV;
    UILabel * rewardLb;
    UIView * contentV;
}

@property (nonatomic , strong)  NSArray * positionArray;
@end

@implementation MoneylistCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


- (void)createSubViews{
    
    contentV = [[UIView alloc] init];
    contentV.layer.cornerRadius = HorPxFit(10);
    [self addSubview:contentV];
    [contentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(15));
        make.right.equalTo(self).mas_offset(-HorPxFit(15));
        make.top.equalTo(self).mas_offset(VerPxFit(10));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(1));
    }];
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"talentBg"];
    [contentV addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(contentV);
    }];
    
    CGFloat horPadding = HorPxFit(12);
    CGFloat verPadding = VerPxFit(8);
    CGFloat lbH = VerPxFit(40);
    
    rewardLb = [self lb];
    rewardLb.textAlignment = NSTextAlignmentCenter;
    rewardLb.textColor = COLOR(223, 214, 167, 1);
    rewardLb.frame = CGRectMake(ScreenWidth-HorPxFit(150)-horPadding, CGRectGetHeight(self.frame)+5, HorPxFit(120), lbH);
//    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(self.right).mas_offset(-horPadding);
//        make.top.mas_equalTo(self.mas_centerY).mas_offset(VerPxFit(5));
//        make.size.mas_equalTo(CGSizeMake(HorPxFit(120), lbH));
//    }];
    

    UIImage * img = [UIImage imageNamed:@"rewardChar"];
    CGFloat imgW = HorPxFit(80);
    CGFloat imgH =  imgW*img.size.height / img.size.width;
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
    [contentV addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(rewardLb.mas_centerX);
        make.bottom.mas_equalTo(rewardLb.mas_top).mas_offset(-verPadding);
        make.size.mas_equalTo(CGSizeMake(imgW, imgH));
    }];
//
    avatarImg = [[UIImageView alloc] init];
    avatarImg.layer.cornerRadius = HorPxFit(110)/2.0;
    avatarImg.clipsToBounds = YES;
    avatarImg.backgroundColor = [UIColor redColor];
    avatarImg.contentMode = UIViewContentModeScaleAspectFill;
    [contentV addSubview:avatarImg];
    [avatarImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(HorPxFit(110), HorPxFit(110)));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.mas_equalTo(horPadding*2);
    }];
    
    
    nameLb = [self lb];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(avatarImg.mas_top);
        make.left.mas_equalTo(avatarImg.mas_right).mas_offset(horPadding*2);
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(lbH);
    }];
    
    
    positionLb = [self lb];
    positionLb.textAlignment = NSTextAlignmentLeft;
    positionLb.textColor = COLOR(223, 214, 167, 1);
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(nameLb.mas_centerY);
        make.left.mas_equalTo(nameLb.mas_right).mas_offset(20);
        make.right.mas_equalTo(rewardLb.mas_left).mas_offset(-20);
        make.height.mas_equalTo(nameLb);
    }];
//    [self layoutGrandient:contentV label:positionLb];

    companyLb = [self lb];
    positionLb.textAlignment = NSTextAlignmentLeft;
    [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.height.equalTo(nameLb);
        make.right.mas_equalTo(positionLb.mas_left);
        make.bottom.mas_equalTo(avatarImg.mas_bottom);
    }];
}


- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:17];
    [contentV addSubview:lb];
    return lb;
}


- (void)setModel:(RewardModel *)model{
    _model = model;
    if ([model.candidate.gender isEqualToString:@"男"]) {
        [avatarImg setImageWithURL:[NSURL URLWithString:model.candidate.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1]];
    }else{
        [avatarImg setImageWithURL:[NSURL URLWithString:model.candidate.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:0]];
    }
    nameLb.text = model.candidate.name;
    positionLb.text = model.jobTaskOrder.jobModel.title;
    companyLb.text = model.jobTaskOrder.jobModel.hrModel.company.name;
    UIColor *rewardColor = COLOR(223, 214, 167, 1);
    NSString * rewardText = [NSString stringWithFormat:@"%ld",model.jobTaskOrder.jobModel.price];
    NSString * firChar = [rewardText substringToIndex:1];
    rewardLb.attributedText = [BaseHelper setSourceString:rewardText targetString:firChar forFont:[UIFont systemFontOfSize:23] color:rewardColor customFont:[UIFont systemFontOfSize:17] customColor:rewardColor];
    
}

- (void)layoutGrandient:(UIView *)view label:(UILabel *)label{
    UIColor *color = [UIColor colorWithRed:197/255.0f green:173/255.0f blue:132/255.0f alpha:1];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(id)color.CGColor,  (id)[UIColor whiteColor].CGColor];
    gradientLayer.startPoint = CGPointMake(0, 1);
    gradientLayer.endPoint = CGPointMake(1, 1);
    gradientLayer.frame = label.frame;
    gradientLayer.mask = label.layer;
    label.layer.frame = gradientLayer.bounds;
    [view.layer addSublayer:gradientLayer];
}

@end
