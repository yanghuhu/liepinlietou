//
//  ResumDetailView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalentModel.h"
#import "BasePopView.h"
#import "JobModel.h"

typedef void(^RecommendBlock)(void);

@interface ResumDetailView : BasePopView

@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , strong) RecommendBlock recommendBlock;
@property (nonatomic , strong) JobModel * jobmodel;

- (void)initInfoWithTalent:(TalentModel *)model;

@end
