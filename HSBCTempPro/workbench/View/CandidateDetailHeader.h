//
//  CandidateDetailHeader.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/9.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobOrderDetailModel.h"

@protocol  CandidateDetailHeaderViewDelegate

- (void)getVirtualNumberWithType:(NSString *)type;

@end

@interface CandidateDetailHeader : UIView

@property (nonatomic , weak) id<CandidateDetailHeaderViewDelegate>delegate;
@property (nonatomic , strong) JobOrderDetailModel * model;

@end
