//
//  PositionDemandHeader.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDemandHeader.h"
#import "UIImage+YYAdd.h"
#import "UIButton+Badge.h"

#define padding HorPxFit(60)

@interface PositionDemandHeader()<UITextFieldDelegate>{
    
    UITextField * searchTf;
    UIButton * actionBt;
}

@property (nonatomic ,strong) UIButton * userBt;
@end

@implementation PositionDemandHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    self.backgroundColor = [UIColor blackColor];
    
    
    UIButton * dashboardBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [dashboardBt addTarget:self action:@selector(gotoDashBoardAction) forControlEvents:UIControlEventTouchUpInside];
    [dashboardBt setBackgroundImage:[UIImage imageNamed:@"dashBoard"] forState:UIControlStateNormal];
    [dashboardBt setTitle:@"工作台" forState:UIControlStateNormal];
    dashboardBt.titleLabel.font = [UIFont systemFontOfSize:13];
    dashboardBt.layer.cornerRadius = VerPxFit(70/2.0);
    [self addSubview:dashboardBt];
    [dashboardBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-HorPxFit(30));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(50));
        make.height.mas_equalTo(VerPxFit(75));
        make.width.mas_equalTo(HorPxFit(150));
    }];
    
    UIView * searchView = [[UIView alloc] init];
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.layer.cornerRadius = VerPxFit(56)/2.0;
    [self addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(30));
//        make.bottom.equalTo(self).mas_offset(-VerPxFit(50));
        make.centerY.mas_equalTo(dashboardBt.mas_centerY);
        make.right.mas_equalTo(dashboardBt.mas_left).mas_offset(-HorPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(240), VerPxFit(65)));
    }];
    
    actionBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [actionBt addTarget:self action:@selector(textFilesAction) forControlEvents:UIControlEventTouchUpInside];
    [actionBt setImage:[UIImage imageNamed:@"searchFlag.png"] forState:UIControlStateNormal];
    [searchView addSubview:actionBt];
    [actionBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(searchView).mas_offset(-HorPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(35), HorPxFit(35)));
        make.centerY.equalTo(searchView.mas_centerY);
    }];
    
    
    searchTf = [[UITextField alloc] init];
    searchTf.placeholder = @"输入您想搜索的职位";
    searchTf.returnKeyType = UIReturnKeySearch;
    searchTf.backgroundColor = InputBackColor;
    searchTf.font = [UIFont systemFontOfSize:13];
    searchTf.delegate = self;
    [searchView addSubview:searchTf];
    [searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchView).mas_offset(HorPxFit(25));
        make.top.equalTo(searchView);
        make.bottom.equalTo(searchView);
        make.right.mas_equalTo(actionBt.mas_left).mas_offset(-HorPxFit(20));
    }];
    
//    UILabel * userLb = [[UILabel alloc] init];
//    userLb.font = [UIFont systemFontOfSize:28];
//    userLb.text = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
//    userLb.textColor = [UIColor whiteColor];
//    [self addSubview:userLb];
//    [userLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(searchView);
//        make.right.equalTo(self);
//        make.bottom.mas_equalTo(searchView.mas_top).mas_offset(-VerPxFit(50));
//        make.height.mas_equalTo(35);
//    }];
//
//    UILabel * welcomeLb = [[UILabel alloc] init];
//    welcomeLb.font = [UIFont systemFontOfSize:28];
//    welcomeLb.text = @"欢迎,";
////    welcomeLb.backgroundColor = [UIColor redColor];
//    welcomeLb.textColor = [UIColor whiteColor];
//    [self addSubview:welcomeLb];
//    [welcomeLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(userLb);
//        make.bottom.mas_equalTo(userLb.mas_top).mas_offset(-VerPxFit(40));
//        make.size.mas_equalTo(CGSizeMake(75, 32));
//    }];
//
//    UIImageView * imgV  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"centerIcon"]];
////    imgV.backgroundColor = [UIColor redColor];
//    [self addSubview:imgV];
//    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(welcomeLb.mas_right);
//        make.bottom.equalTo(welcomeLb);
//        make.size.mas_equalTo(CGSizeMake(80, 18));
//    }];
    
    
    UIButton * personCenterBt = [UIButton buttonWithType:UIButtonTypeCustom];
//    personCenterBt.backgroundColor = [UIColor redColor];
    [personCenterBt addTarget:self action:@selector(personHomeAction) forControlEvents:UIControlEventTouchUpInside];
    [personCenterBt setImage:[UIImage imageNamed:@"personCenter"] forState:UIControlStateNormal];
    [self addSubview:personCenterBt];
    [personCenterBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(10));
        make.bottom.mas_equalTo(searchView.mas_top).mas_offset(-VerPxFit(50));
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
    
    UIButton * messageBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageBt addTarget:self action:@selector(messageCheckAction) forControlEvents:UIControlEventTouchUpInside];
    [messageBt setImage:[UIImage imageNamed:@"message_normal"] forState:UIControlStateNormal];
    messageBt.badgeValue = @"";
    messageBt.badgeBGColor = [UIColor redColor];
    [self addSubview:messageBt];
    [messageBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(HorPxFit(-10));
        make.centerY.mas_equalTo(personCenterBt.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
    self.messageButton = messageBt;
    
    UIImageView * titleIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleIcon"]];
    [self addSubview:titleIcon];
    [titleIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.centerY.equalTo(messageBt);
        make.size.mas_equalTo(CGSizeMake(69, 25));
    }];
}

- (void)messageCheckAction{
//    [BaseHelper showProgressHud:@"显示消息页面" showLoading:NO canHide:YES];
    [self.delegate toMessageVCAction];
}

- (void)gotoDashBoardAction{
    [self.delegate gotoDashboardAction];
}

- (void)textFilesAction{
    if (searchTf.text.length >0) {
        searchTf.text = nil;
        [actionBt setImage:[UIImage imageNamed:@"searchFlag.png"] forState:UIControlStateNormal];
        [self.delegate searchCancel];
    }
}

- (void)personHomeAction{
    [self.delegate personCenterAction];
}

- (void)messageAction{
    [self.delegate toMessageVCAction];
}

#pragma mark -- UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [actionBt setImage:[UIImage imageNamed:@"chaFlag"] forState:UIControlStateNormal];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString * searchKey = textField.text;
    if (searchKey && searchKey.length !=0) {
        [self.delegate searchWithKey:searchKey];
    }else{
        [actionBt setImage:[UIImage imageNamed:@"searchFlag.png"] forState:UIControlStateNormal];
        [self.delegate searchCancel];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}




@end
