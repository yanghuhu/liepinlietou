//
//  PositionHeaderView.m
//  HSBCTempPro
//
//  Created by Deve on 2018/3/5.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "PositionHeaderView.h"


@implementation PositionHeaderView
{
    UILabel *_topListLabel;
    UILabel *_rewardLabel;
    UILabel *_personLabel;
    UILabel *_companyLabel;
    UILabel *_positionLabel;
    UILabel *_clockLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addBgImageView];
        [self createSubViews];
    }
    return self;
}

- (void)addBgImageView{
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"topListHeader"];
    [self addSubview:bgImageView];
    
//    图片下方的阴影
    CGFloat shadowH = 35;
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(250+shadowH);
    }];
}

- (void)createSubViews{
    UIView *topList = [self topListView];
    [self addSubview:topList];
    [topList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(20);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.equalTo(@(205));
        make.height.equalTo(@(45));
    }];
    
//    竖线
    UILabel *line_v = [self textLabel:17];
    line_v.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.3];
    [self addSubview:line_v];
    [line_v mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topList.mas_bottom).offset(20);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.width.equalTo(@(1));
        make.height.equalTo(@(50));
    }];
   
//    左侧图片
    UIView *leftPictrue = [self instanceWithRewardPicture:@"detailReward" desc:@"0元/人"];
    [self addSubview:leftPictrue];
    [leftPictrue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topList.mas_left);
        make.right.mas_equalTo(line_v.mas_left);
        make.top.bottom.mas_equalTo(line_v);
    }];
    
//    右侧
    UIView *rightPictrue = [self instanceWithPersonCount:@"detailPerson" desc:@"0人"];
    [self addSubview:rightPictrue];
    [rightPictrue mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(line_v);
        make.right.mas_equalTo(topList.mas_right);
        make.left.mas_equalTo(line_v.mas_right);
    }];

    UILabel *companyLabel = [self textLabel:17];
    companyLabel.text = @"大数据软件公司";
    _companyLabel = companyLabel;
    [self addSubview:companyLabel];
    [companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line_v.mas_bottom).offset(20);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.equalTo(@(20));
    }];

    UILabel *positionLabel = [self textLabel:17];
    positionLabel.text = @"产品经理";
    _positionLabel = positionLabel;
    [self addSubview:positionLabel];
    [positionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companyLabel.mas_bottom).offset(5);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.equalTo(@(20));
    }];

    UILabel *clockLabel = [self textLabel:12];
    clockLabel.text = @"1分钟前发布";
    _clockLabel = clockLabel;
    [self addSubview:clockLabel];
    [clockLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(positionLabel.mas_bottom).offset(25);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.equalTo(@(15));
    }];

    UIImageView *clockImageView = [UIImageView new];
    clockImageView.image = [UIImage imageNamed:@"timeFlag"];
    [self addSubview:clockImageView];
    [clockImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.with.equalTo(@(14));
        make.right.mas_equalTo(clockLabel.mas_left).offset(-3);
        make.centerY.mas_equalTo(clockLabel.mas_centerY);
    }];
}

//榜单
- (UIView *)topListView{
    UIView *topListView = [UIView new];
    topListView.backgroundColor = [UIColor clearColor];
    [self addSubview:topListView];
    
//    虚线框
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"topListBgImage"];
    [topListView addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(topListView);
        make.height.equalTo(@(45));
    }];
    
//    标题
    UILabel *titleLabel = [self textLabel:17];
    titleLabel.text = @"榜单";
    titleLabel.numberOfLines = 3;
    titleLabel.backgroundColor  = [UIColor clearColor];
    [bgImageView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(bgImageView.center);
        make.width.mas_equalTo(bgImageView.mas_width);
        make.height.mas_equalTo(bgImageView.mas_height);
    }];
    
    _topListLabel = titleLabel;
    return topListView;
}


- (UILabel *)textLabel:(CGFloat)font{
    UILabel *textLabel = [UILabel new];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.font = [UIFont systemFontOfSize:font];
    textLabel.textAlignment = NSTextAlignmentCenter;
    [textLabel sizeToFit];
    return textLabel;
}


//酬金
- (UIView *)instanceWithRewardPicture:(NSString *)image desc:(NSString *)desc{
    UIView *bgView = [UIView new];
    [self addSubview:bgView];
    
    UIImageView *picture = [UIImageView new];
    picture.image = [UIImage imageNamed:image];
    [bgView addSubview:picture];
    [picture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_top);
        make.centerX.mas_equalTo(bgView.mas_centerX).offset(0.5);
        make.width.equalTo(@(31.5));
        make.height.equalTo(@(28));
    }];
    
    UILabel *titleLabel = [self textLabel:18];
    titleLabel.text = desc;
    [bgView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picture.mas_bottom).offset(5);
        make.left.right.mas_equalTo(bgView);
        make.height.equalTo(@(20));
    }];
    
    _rewardLabel = titleLabel;
    
    return bgView;
}

//人数
- (UIView *)instanceWithPersonCount:(NSString *)image desc:(NSString *)desc{
    UIView *bgView = [UIView new];
    [self addSubview:bgView];
    
    UIImageView *picture = [UIImageView new];
    picture.image = [UIImage imageNamed:image];
    [bgView addSubview:picture];
    [picture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bgView.mas_top);
        make.centerX.mas_equalTo(bgView.mas_centerX).offset(0.5);
        make.width.equalTo(@(26));
        make.height.equalTo(@(32));
    }];
    
    UILabel *titleLabel = [self textLabel:18];
    titleLabel.text = desc;
    [bgView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(picture.mas_bottom).offset(5);
        make.left.right.mas_equalTo(bgView);
        make.height.equalTo(@(20));
    }];
    
    _personLabel = titleLabel;
    
    return bgView;
}

- (void)setJobModel:(JobModel *)jobModel{
    _jobModel = jobModel;
    _topListLabel.text = jobModel.title;
    _rewardLabel.text = [NSString stringWithFormat:@"%ld元/人",jobModel.price];
    _personLabel.text = [NSString stringWithFormat:@"%ld人",jobModel.recruitingNumber];
    _positionLabel.text = jobModel.position;
    _companyLabel.text = jobModel.hrModel.company.name;
    
    NSString * text = [NSString stringWithFormat:@"%@发布",[BaseHelper stringWithTimeIntevl:jobModel.publishTime format:kDateFormatTypeYYYYMMDD]];
    CGSize size = [BaseHelper getSizeWithString:text font:[UIFont systemFontOfSize:13] contentWidth:MAXFLOAT contentHight:HorPxFit(30)];
    
    _clockLabel.text = text;
    [_clockLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width);
    }];
}

@end
