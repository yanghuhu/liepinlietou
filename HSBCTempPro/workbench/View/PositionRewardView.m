//
//  PositionRewardView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionRewardView.h"


@interface PositionRewardView(){
    UILabel * rewardLb;
    UILabel * personLb;
}
@end
@implementation PositionRewardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionDetailRewardBk"]];
    [self addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    UILabel * sepLine = [[UILabel alloc] init];
    sepLine.backgroundColor = [UIColor whiteColor];
    [self addSubview:sepLine];
    
    [sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(1), VerPxFit(120)));
        make.bottom.mas_equalTo(self).mas_offset(-VerPxFit(80));
    }];
    
    UIView * rewardView = [[UIView alloc] init];
    rewardView.backgroundColor  = [UIColor clearColor];
    [self addSubview:rewardView];
    [rewardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(sepLine.mas_left);
        make.left.equalTo(self).mas_offset(HorPxFit(30));
        make.bottom.equalTo(sepLine);
        make.top.equalTo(sepLine);
    }];
    
    CGFloat imgW = HorPxFit(80);
    CGFloat imgH =  imgW * 56 / 63;
    
  
    
    UIImageView * rewardImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detailReward"]];
    [rewardView addSubview:rewardImgV];
    [rewardImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(rewardView).mas_offset(VerPxFit(0));
        make.centerX.mas_equalTo(rewardView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(imgW, imgH));
    }];
    
    rewardLb = [[UILabel alloc] init];
    rewardLb.text = @"赏金金额";
    rewardLb.textColor = [UIColor whiteColor];
    rewardLb.textAlignment =NSTextAlignmentCenter;
    rewardLb.font = [UIFont systemFontOfSize:19];
    [rewardView addSubview:rewardLb];
    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(rewardImgV.mas_bottom).mas_offset(VerPxFit(10));
        make.left.and.right.equalTo(rewardView);
        make.bottom.equalTo(rewardView);
    }];
    
    UIView * personView = [[UIView alloc] init];
    personView.backgroundColor  = [UIColor clearColor];
    [self addSubview:personView];
    [personView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self).mas_offset(-HorPxFit(30));
        make.left.mas_equalTo(sepLine.mas_right);
        make.bottom.equalTo(sepLine);
        make.top.equalTo(sepLine);
    }];
    UIImageView * personImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detailPerson"]];
    [personView addSubview:personImgV];
    [personImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(personView).mas_offset(VerPxFit(0));
        make.centerX.mas_equalTo(personView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(60), HorPxFit(70)));
    }];
    
    personLb = [[UILabel alloc] init];
    personLb.text = @"招聘人数";
    personLb.textColor = [UIColor whiteColor];
    personLb.textAlignment =NSTextAlignmentCenter;
    personLb.font = [UIFont systemFontOfSize:19];
    [personView addSubview:personLb];
    [personLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(personImgV.mas_bottom).mas_offset(VerPxFit(10));
        make.left.and.right.equalTo(personView);
        make.bottom.equalTo(personView);
    }];
    
   
    UIImage * img = [UIImage imageNamed:@"DetailRewardTitle"];
    CGFloat imgW_ = HorPxFit(120);
    CGFloat imgH_ = imgW_ * 40 / 73.0;
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
//    [self addSubview:imgV];
//    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(sepLine.mas_top).mas_offset(-VerPxFit(40));
//        make.centerX.mas_equalTo(self.mas_centerX);
//        make.size.mas_equalTo(CGSizeMake(imgW_, imgH_));
//    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont systemFontOfSize:25];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.text = @"赏金";
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(sepLine.mas_top).mas_offset(-VerPxFit(40));
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(imgW_, imgH_));
    }];
    
}

-(void)setJobModel:(JobModel *)jobModel{
    _jobModel = jobModel;
    rewardLb.text = [NSString stringWithFormat:@"%ld 元/人",jobModel.price];
    personLb.text = [NSString stringWithFormat:@"%ld 人",jobModel.recruitingNumber];
}

@end
