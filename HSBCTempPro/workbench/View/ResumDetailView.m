//
//  ResumDetailView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "ResumDetailView.h"
#import "TalentModel.h"
#import "UIImageView+YYWebImage.h"
#import "UIView+YYAdd.h"
#import "UIImage+YYadd.h"

#define TitleColor  RGB16(0x2671e6)

@interface ResumDetailView()<UIScrollViewDelegate>{
 
    UIScrollView * contentSV;
    NSArray * baseInfoTitleArray;
    UIFont *textFont;
    UIFont *titleFont;
}

@property (nonatomic , strong) UIButton * resumeBt;
@property (nonatomic , strong) UIView * layoutLastView;

@end

@implementation ResumDetailView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = ViewControllerBkColor;
        textFont = [UIFont systemFontOfSize:15];
        titleFont = [UIFont systemFontOfSize:17];
        
        UILabel * titlelb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 40)];
        titlelb.font = [UIFont boldSystemFontOfSize:20];
        titlelb.textColor = [UIColor whiteColor];
        titlelb.text = @"简历";
        titlelb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titlelb];
        
        contentSV = [[UIScrollView alloc] init];
        contentSV.delegate = self;
        contentSV.backgroundColor = [UIColor clearColor];
        [self addSubview:contentSV];
        [self addTopRightDismissBt];
    }
    return self;
}

- (void)addTopRightDismissBt{
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(VerPxFit(0));
        make.right.equalTo(self).mas_offset(-HorPxFit(6));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(70), 35));
    }];
}

- (void)initInfoWithTalent:(TalentModel *)model{
    
    self.talentModel = model;
     [self createSubViewTalent];
}

- (void)createSubViewTalent{
    [contentSV removeAllSubviews];
    if (!_talentModel) {
        return;
    }

    CGFloat horPadding = HorPxFit(28);
    CGFloat verPadding = VerPxFit(20);
    CGFloat lbH = VerPxFit(35);
    CGFloat lbW = CGRectGetWidth(self.frame) - 2* horPadding;
    
    CGFloat contentW = CGRectGetWidth(self.frame);
    
    CGFloat curY = 0;

    
    UIImageView * avatarImgV = [[UIImageView alloc] initWithFrame:CGRectMake(contentW-HorPxFit(80)-horPadding*2, verPadding*4, HorPxFit(100), HorPxFit(100))];
    if ([_talentModel.gender isEqualToString:@"男"]) {
        [avatarImgV setImageWithURL:[NSURL URLWithString:_talentModel.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1]];
    }else{
        [avatarImgV setImageWithURL:[NSURL URLWithString:_talentModel.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:2]];
    }
    [contentSV addSubview:avatarImgV];
    
    
    UILabel * nameLb= [self lb:CGRectMake(horPadding, curY, CGRectGetMidX(avatarImgV.frame)-2*horPadding, VerPxFit(50))];
    nameLb.text = _talentModel.name;
    nameLb.font = [UIFont boldSystemFontOfSize:19];
    self.layoutLastView = nameLb;
    
    curY +=  (VerPxFit(50)+verPadding);
    
    UILabel * baseInfoLbFir = [self lb:CGRectMake(horPadding, curY, CGRectGetWidth(nameLb.frame), lbH)];
    NSMutableString * baseInfoString = [NSMutableString string];
    if (_talentModel.gender) {
        [baseInfoString appendFormat:@"%@ | ",[BaseHelper removeLineFeedWithString:_talentModel.gender]];
    }
    if (_talentModel.maritalStatus) {
        [baseInfoString appendFormat:@"%@ | ",[BaseHelper removeLineFeedWithString:_talentModel.maritalStatus]];
    }
    if (_talentModel.birthday) {
        [baseInfoString appendFormat:@"%@生 | ",[BaseHelper stringWithTimeIntevl:_talentModel.birthday format:kDateFormatTypeYYYYMMDD]];
    }
    if (_talentModel.nativePlace) {
        [baseInfoString appendFormat:@"籍贯:%@",[BaseHelper removeLineFeedWithString:_talentModel.nativePlace]];
    }
    baseInfoLbFir.text = baseInfoString;
    self.layoutLastView = baseInfoLbFir;
    curY +=  (lbH+verPadding);
    

    UILabel * baseinfoLbSec = [self lb:CGRectMake(horPadding, curY, CGRectGetWidth(nameLb.frame), lbH)];
    NSMutableString * baseInfoStringSec = [NSMutableString string];
    if (_talentModel.education) {
        [baseInfoStringSec appendFormat:@"%@ | ",[BaseHelper removeLineFeedWithString:_talentModel.education]];
    }
    if (_talentModel.city) {
        [baseInfoStringSec appendFormat:@"%@ | ",[BaseHelper removeLineFeedWithString:_talentModel.city]];
    }
    if (_talentModel.workYears) {
        [baseInfoStringSec appendFormat:@"%ld年工作经验",_talentModel.workYears];
    }
    baseinfoLbSec.text = baseInfoStringSec;
    self.layoutLastView = baseinfoLbSec;
    curY +=  (lbH+verPadding);
    
  
//    
//    if (_talentModel.email) {
//        UILabel * emailLb = [self lb:CGRectMake(horPadding, curY, CGRectGetWidth(nameLb.frame), lbH)];
//        emailLb.text = [NSString stringWithFormat:@"Email:%@",_talentModel.email];
//        self.layoutLastView = emailLb;
//        curY += (lbH+verPadding);
//    }
    
    UILabel * yixianglb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    yixianglb.text = @"求职意向";
    yixianglb.textColor = TitleColor;
    self.layoutLastView = yixianglb;
    curY += (lbH+verPadding);
    
    [self addSegLineWithTitle:yixianglb.text];
    
    
    if (_talentModel.jobNature) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        jobNatureLb.text = [NSString stringWithFormat:@"工作性质:%@",_talentModel.jobNature];
        self.layoutLastView = jobNatureLb;
        curY += (lbH+verPadding);

    }
    
    if (_talentModel.profession) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * text = [NSString stringWithFormat:@"期望行业:%@",_talentModel.profession];
        jobNatureLb.text = text;
        self.layoutLastView = jobNatureLb;
        
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
       
        curY += (lbH_+verPadding);

    }
    
    if (_talentModel.targetWorkPlace) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * text = [NSString stringWithFormat:@"期望地区:%@",_talentModel.targetWorkPlace];
        jobNatureLb.text = [NSString stringWithFormat:@"期望地区:%@",_talentModel.targetWorkPlace];
        self.layoutLastView = jobNatureLb;
       
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
       
        curY += (lbH_+verPadding);
    }
    if (_talentModel.expectedSalary) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * text = [NSString stringWithFormat:@"期望月薪:%@",_talentModel.expectedSalary];
        jobNatureLb.text = [NSString stringWithFormat:@"期望月薪:%@",_talentModel.expectedSalary];
        self.layoutLastView = jobNatureLb;
        
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    if (_talentModel.jobStatus) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * text = [NSString stringWithFormat:@"目前状况:%@",_talentModel.jobStatus];
        jobNatureLb.text = [NSString stringWithFormat:@"目前状况:%@",_talentModel.jobStatus];
        self.layoutLastView = jobNatureLb;
        
        
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    
    UILabel * ziwoPingjialb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    ziwoPingjialb.text = @"自我评价";
    ziwoPingjialb.textColor = TitleColor;
    self.layoutLastView = ziwoPingjialb;
    curY += (lbH+verPadding);

    
    [self addSegLineWithTitle:ziwoPingjialb.text];
    
    if (_talentModel.selfEvaluation) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * selfEvaluation = [NSString stringWithFormat:@"  %@",[BaseHelper fixLineFeedWithString:_talentModel.selfEvaluation]];
        CGSize size = [BaseHelper getSizeWithString:selfEvaluation font:textFont contentWidth:CGRectGetWidth(self.frame)-2*horPadding contentHight:MAXFLOAT];
        
        
        jobNatureLb.text = selfEvaluation;
        self.layoutLastView = jobNatureLb;
        
        
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);

    }
    
   
    
    
    UILabel * gongzuojingyanlb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    gongzuojingyanlb.text = @"工作经验";
    gongzuojingyanlb.textColor = TitleColor;
    self.layoutLastView = gongzuojingyanlb;
    curY += (lbH+verPadding);
    
    [self addSegLineWithTitle:gongzuojingyanlb.text];
    if (_talentModel.workExperience) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * projectExperience = [BaseHelper fixLineFeedWithString:_talentModel.workExperience];
        
        
        jobNatureLb.text = projectExperience;
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    
    
    UILabel * xiangmulb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    xiangmulb.text = @"项目经验";
    xiangmulb.textColor = TitleColor;
    self.layoutLastView = xiangmulb;
    curY += (lbH+verPadding);

    [self addSegLineWithTitle:xiangmulb.text];
    
    if (_talentModel.projectExperience) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        NSString * projectExperience = [BaseHelper fixLineFeedWithString:_talentModel.projectExperience];
        
        jobNatureLb.text = projectExperience;
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    
    
    UILabel * jiaoyulb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    jiaoyulb.text = @"教育情况";
    jiaoyulb.textColor = TitleColor;
    self.layoutLastView = jiaoyulb;
    curY += (lbH+verPadding);
    
    
    [self addSegLineWithTitle:jiaoyulb.text];
    
    if (_talentModel.graduateSchool) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        
        jobNatureLb.text = [NSString stringWithFormat:@"毕业学校:%@",_talentModel.graduateSchool];
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
        
        
    }
    
    if (_talentModel.major) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        jobNatureLb.text = [NSString stringWithFormat:@"毕业专业:%@",_talentModel.major];
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    
    UILabel * peixunlb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    peixunlb.text = @"培训经历";
    peixunlb.textColor = TitleColor;
    self.layoutLastView = peixunlb;
    curY += (lbH+verPadding);
    
    [self addSegLineWithTitle:peixunlb.text];
    
    if (_talentModel.trainingExperience) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        jobNatureLb.text = [BaseHelper fixLineFeedWithString:_talentModel.trainingExperience];
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        curY += (lbH_+verPadding);
    }
    
    
    UILabel * yuyanlb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
    yuyanlb.text = @"语言/技能";
    yuyanlb.textColor = TitleColor;
    self.layoutLastView = yuyanlb;
    curY += (lbH+verPadding);
    
    [self addSegLineWithTitle:yuyanlb.text];
    
    if (_talentModel.languageAbility) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        jobNatureLb.text = [NSString stringWithFormat:@"语言:%@",[BaseHelper fixLineFeedWithString:_talentModel.languageAbility]];
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        curY += (lbH_+verPadding);
    }
    
    if (_talentModel.workSkills) {
        UILabel * jobNatureLb = [self lb:CGRectMake(horPadding, curY, lbW, lbH)];
        jobNatureLb.text = [NSString stringWithFormat:@"技能:%@",[BaseHelper fixLineFeedWithString:_talentModel.workSkills]];
        self.layoutLastView = jobNatureLb;
        
        NSString * text = jobNatureLb.text;
        CGSize size = [BaseHelper getSizeWithString:text font:textFont contentWidth:lbW contentHight:MAXFLOAT];
        CGFloat lbH_ = size.height;
        if (lbH_ < lbH) {
            lbH_ = lbH;
        }
        jobNatureLb.height = lbH_;
        
        curY += (lbH_+verPadding);
    }
    
    curY += verPadding;
    
    if (curY + 40 +2*VerPxFit(10)+VerPxFit(70)> ScreenHeight -2* VerPxFit(60)) {
        self.height = ScreenHeight -2* VerPxFit(60);
    }else{
        self.height = curY+40+2*VerPxFit(15)+VerPxFit(60);
    }
    CGFloat contentSVY =HorPxFit(70)+VerPxFit(12);
    contentSV.frame = CGRectMake(0, 40, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) -40 - VerPxFit(70)-2*VerPxFit(10));
    contentSV.contentSize = CGSizeMake(CGRectGetWidth(contentSV.frame), curY);
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(recommendAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bt setTitle:@"推 荐" forState:UIControlStateNormal];
    bt.layer.cornerRadius = VerPxFit(60)/2.0;
    bt.titleLabel.font = [UIFont systemFontOfSize:16];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(horPadding*4);
        make.right.equalTo(self).mas_offset(-horPadding*4);
        make.bottom.equalTo(self).mas_offset(-VerPxFit(10));
        make.bottom.height.mas_equalTo(VerPxFit(70));
    }];
    
}

- (void)addSegLineWithTitle:(NSString *)title{
    UIImageView * yixiangImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"resumeLine"]];
    [contentSV addSubview:yixiangImgV];
    CGSize size = [BaseHelper getSizeWithString:title font:titleFont contentWidth:MAXFLOAT contentHight:VerPxFit(30)];
    [yixiangImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentSV);
        make.top.mas_equalTo(_layoutLastView.bottom);
        make.width.mas_equalTo(size.width+HorPxFit(18));
        make.height.mas_equalTo(VerPxFit(4));
    }];
}


- (void)recommendAction{
    self.recommendBlock();
}

- (UILabel *)lb:(CGRect) frame{
    
    UILabel * lb = [[UILabel alloc] init];
    lb.numberOfLines = 0;
    lb.frame = frame;
    lb.font = textFont;
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    [contentSV addSubview:lb];
    return lb;
}
//#pragma mark -- UIScrollViewDelegate
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [UIView animateWithDuration:0.2 animations:^{
//        _resumeBt.alpha = 0;
//    }];
//}
//
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    [UIView animateWithDuration:0.1 animations:^{
//        _resumeBt.alpha = 1;
//    }];
//}
//
//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    [UIView animateWithDuration:0.1 animations:^{
//        _resumeBt.alpha = 1;
//    }];
//}


@end
