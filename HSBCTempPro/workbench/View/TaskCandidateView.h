//
//  TaskCandidateView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskModel.h"


@protocol TaskCandidateViewDelegate

- (void)candidateSelected:(CandidateModel *)model;

@end

@interface TaskCandidateView : UIView

@property (nonatomic , weak) id<TaskCandidateViewDelegate>delegate;
@property (nonatomic , strong) NSArray *candidateArray;
@property (nonatomic , assign) CGFloat selfW;
- (CGFloat)initSuvViews:(NSArray<CandidateModel*> *)candidateArray withArrowX:(CGFloat )arrowX;
@end
