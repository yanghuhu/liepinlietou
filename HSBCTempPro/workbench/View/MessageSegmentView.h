//
//  MessageSegmentView.h
//  HSBCTempPro
//
//  Created by Deve on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SegmentClickDelegate<NSObject>

- (void)segmentClickChickerData:(NSInteger)tag;

@end


//消息状态选择器
@interface MessageSegmentView : UIView

@property (nonatomic,assign)id <SegmentClickDelegate> delegate;

@end
