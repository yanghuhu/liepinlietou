//
//  MoneyListHeaderView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MoneyListHeaderView.h"

@interface MoneyListHeaderView(){
    
    UILabel * moneyLb;
}

@end

@implementation MoneyListHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    CGFloat padding = HorPxFit(30);
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(padding, 0, 80, CGRectGetHeight(self.frame))];
    titleLb.font = [UIFont systemFontOfSize:18];
    titleLb.text = @"待收款:";
    [self addSubview:titleLb];
    
    moneyLb = [[UILabel alloc] init];
    moneyLb.textAlignment = NSTextAlignmentRight;
    moneyLb.textColor = [UIColor redColor];
    moneyLb.font = [UIFont systemFontOfSize:17];
    moneyLb.text = @"￥2200";
    [self addSubview:moneyLb];
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(self);
        make.right.equalTo(self).mas_offset(-padding);
        make.left.equalTo(titleLb.mas_right).mas_offset(HorPxFit(15));
    }];
    
    UILabel * line = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-1, CGRectGetWidth(self.frame), 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:line];
}
@end
