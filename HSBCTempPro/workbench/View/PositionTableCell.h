//
//  PositionTableCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/4.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"

#define CellW ScreenWidth -2*HorPxFit(30)
#define CellH  VerPxFit(280);

@interface PositionTableCell : UITableViewCell

@property (nonatomic , strong) JobModel * jobModel;

@end
