//
//  TalentPoorSearchView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TalentPoorSearchViewDelegate

- (void)searchWithKey:(NSString *)key;
- (void)searchCancel;
- (void)toResumeCheckAction;
@end

@interface TalentPoorSearchView : UIView

@property (nonatomic , weak) id<TalentPoorSearchViewDelegate>delegate;

@end
