//
//  PositionCollectionCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionCollectionCell.h"

@interface PositionCollectionCell(){
    UILabel * positionNameLb;  //  职位名称
    UILabel * secondLb;        //  待更改
    UILabel * thirdLb;         //  待更改
    UILabel * fouthLb;         //  待更改
}
@end

@implementation PositionCollectionCell

- (void)createSubViews{
    self.isCreatedSubs = YES;
    self.backgroundColor = CellBackColor;
    CGFloat padding = HorPxFit(20);
    CGFloat height = (CGRectGetHeight(self.frame)-padding*2) / 4.0;
    
    UIButton * hrBt = [UIButton buttonWithType:UIButtonTypeCustom];
    hrBt.backgroundColor = [UIColor redColor];
    [hrBt setTitle:@"hr" forState:UIControlStateNormal];
    [hrBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [hrBt addTarget:self action:@selector(hrInfoShowAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:hrBt];
    [hrBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-(padding/2));
        make.top.equalTo(self).mas_offset((padding/2));
        make.size.mas_equalTo(CGSizeMake(height, height));
    }];
    hrBt.layer.cornerRadius = height/2.0;
    
    positionNameLb = [self lb];
    [positionNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self).mas_offset(padding);
        make.height.mas_equalTo(height);
        make.right.equalTo(self);
    }];
    
    secondLb = [self lb];
    [secondLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(positionNameLb.mas_bottom);
        make.left.equalTo(self).offset(padding);
        make.right.equalTo(self).offset(-padding);
        make.height.mas_equalTo(height);
    }];
    thirdLb = [self lb];
    [thirdLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(secondLb.mas_bottom);
        make.left.equalTo(self).offset(padding);
        make.right.equalTo(self).offset(-padding);
        make.height.mas_equalTo(height);
    }];
    fouthLb = [self lb];
    [fouthLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(thirdLb.mas_bottom);
        make.left.equalTo(self).offset(padding);
        make.right.equalTo(self).offset(-padding);
        make.height.mas_equalTo(height);
    }];
}

- (void)setJobModel:(JobModel *)jobModel{
    _jobModel = jobModel;
    positionNameLb.text = jobModel.position;
//    secondLb.text = [NSString stringWithFormat:@"%@/%@/%@",jobModel.experienceReq,jobModel.education,jobModel.compensation];
//    thirdLb.text = [NSString stringWithFormat:@"赏金:￥%ld",jobModel.reward];
//    fouthLb.text = [NSString stringWithFormat:@"%@",jobModel.company];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor blackColor];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lb];
    return lb;
}

- (void)hrInfoShowAction{
    [self.delegate hrInfoShow:self.jobModel];
}
@end
