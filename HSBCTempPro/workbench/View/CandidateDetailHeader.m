//
//  CandidateDetailHeader.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/9.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "CandidateDetailHeader.h"
#import "UIView+YYAdd.h"
#import "AppDelegate.h"
#import "UIImageView+YYWebImage.h"

@interface CandidateDetailHeader(){
    UIImageView *candiAvatar;
    UILabel * candiNamelb;
    UIButton * emailBt;
    UILabel * emailLb;
    
    UILabel * rewardLb;
    UILabel * positionLb;
    UILabel * companyLb;
}

@end

@implementation CandidateDetailHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
        
    }
    return self;
}

- (void)createSubViews{
    
    UIImage * img = [UIImage imageNamed:@"CandidateBk"];
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:img];
    CGFloat imgVH = 565 * ScreenWidth / 750.0;
    bkImgV.frame = CGRectMake(0, 0, ScreenWidth,imgVH);
    [self addSubview:bkImgV];
    self.height = imgVH;
    
    
    
    CGFloat verPadding = VerPxFit(30);
    CGFloat horPadding = HorPxFit(40);
    CGFloat lbH = VerPxFit(30);
    
    candiAvatar = [[UIImageView alloc] init];
    candiAvatar.backgroundColor = [UIColor blueColor];
    candiAvatar.layer.cornerRadius = HorPxFit(160)/2.0;
    candiAvatar.clipsToBounds = YES;
    candiAvatar.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:candiAvatar];
    [candiAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(verPadding);
        make.left.equalTo(self).mas_offset(horPadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(160), HorPxFit(160)));
    }];
    
    candiNamelb = [self lb];
//    candiNamelb.text = @"王大锤";
    candiNamelb.font = [UIFont boldSystemFontOfSize:17];
    [candiNamelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(candiAvatar.mas_right).mas_offset(HorPxFit(40));
        make.top.equalTo(self).mas_offset(2*verPadding);
        make.width.mas_equalTo(HorPxFit(200));
        make.height.mas_equalTo(lbH);
    }];
    
//    emailLb = [self lb];
////    emailLb.text = @"wangdachui@gmail.com";
//    [emailLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.right.and.height.equalTo(candiNamelb);
//        make.top.mas_equalTo(candiNamelb.mas_bottom).mas_offset(verPadding);
//    }];
//
//    emailBt = [UIButton buttonWithType:UIButtonTypeCustom];
//    [emailBt addTarget:self action:@selector(emailAction) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:emailBt];
//    [emailBt mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.right.and.height.and.top.equalTo(emailLb);
//    }];
    
    UIButton * candiPhoneBt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [candiPhoneBt addTarget:self action:@selector(candiPhoneAction) forControlEvents:UIControlEventTouchUpInside];
    [candiPhoneBt setBackgroundImage:[UIImage imageNamed:@"candiPhone"] forState:UIControlStateNormal];
    [self addSubview:candiPhoneBt];
    [candiPhoneBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(candiNamelb.mas_bottom).mas_offset(verPadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(70), HorPxFit(70)));
        make.left.equalTo(candiNamelb);
    }];
    
    
    rewardLb = [self lb];
    rewardLb.textAlignment = NSTextAlignmentRight;
//    rewardLb.text =@"赏金5000";
    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(self.mas_centerY).mas_offset(-VerPxFit(70));
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(lbH);
    }];
   
    positionLb = [self lb];
//    positionLb.text = @"高级产品经理";
    positionLb.textAlignment = NSTextAlignmentRight;
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(rewardLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(lbH);
    }];
   
    companyLb = [self lb];
    companyLb.textAlignment = NSTextAlignmentRight;
//    companyLb.text = @"西安中软国际";
    [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(lbH);
    }];
    
    UIButton * companyBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [companyBt addTarget:self action:@selector(companyPhoneAction) forControlEvents:UIControlEventTouchUpInside];
    [companyBt setBackgroundImage:[UIImage imageNamed:@"companyPhoen"] forState:UIControlStateNormal];
    [self addSubview:companyBt];
    [companyBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-horPadding);
        make.top.mas_equalTo(companyLb.mas_bottom).mas_offset(verPadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(70), HorPxFit(70)));
    }];
}

- (void)setModel:(JobOrderDetailModel *)model{
    _model  = model;
    TalentModel * talent = model.talentModel;
    
    @weakify(candiAvatar);
    [candiAvatar setImageWithURL:[NSURL URLWithString:talent.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1l] options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if ([model.status isEqualToString:@"OFF"]) {
            @strongify(candiAvatar);
            if(image){
                candiAvatar.image = [BaseHelper grayImage:image];
            }
        }
    }];
    candiNamelb.text = talent.name;
    JobModel * jobModel = model.orderModel.jobModel;
    rewardLb.text = [NSString stringWithFormat:@"￥%ld",jobModel.price];
    positionLb.text = jobModel.position;
    companyLb.text = jobModel.hrModel.company.name;
    emailLb.text = model.talentModel.email;
}


- (void)candiPhoneAction{
    [HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount = 0;
    [self.delegate getVirtualNumberWithType:@"Talent"];
}

- (void)companyPhoneAction{
    [HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount = 0;
    [self.delegate getVirtualNumberWithType:@"HR"];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:15];
    [self addSubview:lb];
    return lb;
}

@end
