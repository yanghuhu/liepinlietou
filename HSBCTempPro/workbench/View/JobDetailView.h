//
//  JobDetailView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/17.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"
#import "JobModel.h"

typedef void(^UnderTakeRewardBlcok)(void);

@protocol JobDetailViewDelegate
- (void)jobUndertakeAction:(JobModel *)model;
@end

@interface JobDetailView : BasePopView

@property (nonatomic , strong) JobModel *jobModel;
@property (nonatomic , strong) UnderTakeRewardBlcok  underTakeBlock;
@property (nonatomic , weak) id<JobDetailViewDelegate>delegate;

+ (CGFloat)viewHeight;

@end
