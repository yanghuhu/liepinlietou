//
//  RewardCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"

@interface RewardCell : UITableViewCell

@property (nonatomic , strong) JobModel * model;

- (void)createSubViews;
@end
