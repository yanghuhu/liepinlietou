//
//  PositionDemandHeader.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PositionDemandHeaderDelegate

- (void)searchWithKey:(NSString *)key;
- (void)searchCancel;
- (void)gotoDashboardAction;
- (void)personCenterAction;
- (void)toMessageVCAction;
@end

@interface PositionDemandHeader : UICollectionReusableView

@property (nonatomic , weak) id<PositionDemandHeaderDelegate>delegate;
@property (nonatomic,strong)UIButton * messageButton;

@end
