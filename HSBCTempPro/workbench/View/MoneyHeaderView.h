//
//  MoneyHeaderView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MoneyHeaderViewDelegate

- (void)appearHadPayData;
- (void)appearNotPayData;
- (void)showChatImage;

@end

@interface MoneyHeaderView : UIView{
    UIButton * hadPayBt;
    UIButton * notPaybt;
}

@property (nonatomic , weak) id<MoneyHeaderViewDelegate>delegate;

@end
