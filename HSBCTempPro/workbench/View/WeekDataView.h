//
//  WeekDataView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/2/7.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatView.h"
#import "BasePopView.h"

@interface WeekDataView : BasePopView


- (void)showWithSupView:(UIView *)view withData:(NSDictionary *)data;


@end
