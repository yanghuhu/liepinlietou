//
//  TalentTableHeader.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentTableHeader.h"

@interface TalentTableHeader()<UISearchBarDelegate>{
    
    UISearchBar * searchBar;
    UIButton * resumeBt;
}
@end

@implementation TalentTableHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    resumeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [resumeBt setTitle:@"导入简历" forState:UIControlStateNormal];
    [resumeBt addTarget:self action:@selector(inportResumeAction) forControlEvents:UIControlEventTouchUpInside];
    resumeBt.titleLabel.font = [UIFont systemFontOfSize:13];
    resumeBt.backgroundColor = [UIColor blackColor];
    [self addSubview: resumeBt];
    [resumeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(self);
        make.right.equalTo(self).mas_offset(-HorPxFit(10));
        make.width.mas_equalTo(HorPxFit(140));
    }];
    
    searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    [self addSubview:searchBar];
    [searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.right.equalTo(resumeBt.mas_left).mas_offset(HorPxFit(-20));
    }];
}

//  搜索状态时，为取消搜索功能。正常时为导入简历功能。
- (void)inportResumeAction{
    if (searchBar.isFirstResponder || searchBar.text.length != 0) {
        searchBar.text = nil;
        [searchBar resignFirstResponder];
        [resumeBt setTitle:@"导入简历" forState:UIControlStateNormal];
        [self.delegate searchKey:nil];
        return;
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"导入简历，跳转至简历列表页面" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark -- UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [resumeBt setTitle:@"取消搜索" forState:UIControlStateNormal];
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [resumeBt setTitle:@"导入简历" forState:UIControlStateNormal];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.delegate searchKey:searchBar.text];
    [searchBar resignFirstResponder];
}
@end
