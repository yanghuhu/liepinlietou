//
//  PositionTableCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/4.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionTableCell.h"


@interface PositionTableCell(){
    
    
    UIView * contentView;
    UIView * subInfoContentView;   //   年限，学历，薪资等content
    UIView * tailContentView;    //  地址信息coentnt
    UIView * rewardView;      //  赏金content
    
    
    UIImageView * bkImageView;
    
    
    UILabel * positionNameLb;   //  职位lable
    UILabel * companeyLb;   //  企业lable
    UILabel * experienceLb;  // 工作经历lable
    UILabel * educationLb;   // 学历lable
    UILabel * salaryLb;   // 薪资lable
    
    UILabel * rewardLb;   // 赏金lable
    
    UILabel * timeLb;    // 发布时间lable
    UILabel * locationLb;   //  发布位置
}

@end

@implementation PositionTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)createSubViews{
    self.backgroundColor = [UIColor clearColor];
    UIFont * positionTitleFont = [UIFont systemFontOfSize:16];
    UIFont * infoFont = [UIFont systemFontOfSize:14];
    
    CGFloat contentH = VerPxFit(200);
    CGFloat rewardW = HorPxFit(190);
    
    contentView = [[UIView alloc] init];
    [self addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.and.top.and.right.equalTo(self);
        make.height.mas_equalTo(contentH);
    }];
    bkImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionBk_1"]];
    [contentView addSubview:bkImageView];
    [bkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(contentView).mas_offset(-HorPxFit(20));
        make.right.equalTo(contentView).mas_offset(HorPxFit(20));
        make.bottom.equalTo(contentView).mas_offset(HorPxFit(20));
    }];
    
    rewardView =[[UIView alloc] init];
    rewardView.backgroundColor = [UIColor clearColor];
    [contentView addSubview:rewardView];
    [rewardView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.right.equalTo(contentView);
        make.width.mas_equalTo(rewardW);
    }];
    
    UILabel * rewardTitleLb = [self lb:infoFont];
    rewardTitleLb.text = @"赏 金";
    rewardTitleLb.font = [UIFont boldSystemFontOfSize:19];
    rewardTitleLb.textColor = RGB16(0xEAE1BF);
    rewardTitleLb.backgroundColor = [UIColor clearColor];
    rewardTitleLb.textAlignment = NSTextAlignmentCenter;
    [rewardView addSubview:rewardTitleLb];
    [rewardTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(rewardView);
        make.bottom.equalTo(rewardView).mas_offset(-contentH/2.0);
        make.height.mas_equalTo(HorPxFit(55));
    }];
    
    rewardLb = [self lb:infoFont];
    rewardLb.textAlignment = NSTextAlignmentCenter;
    rewardLb.backgroundColor = [UIColor clearColor];
    rewardLb.textColor = RGB16(0xEAE1BF);
    rewardLb.font = [UIFont boldSystemFontOfSize:22];
//    rewardLb.text = @"500";
    [rewardView addSubview:rewardLb];
    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(rewardView);
        make.top.equalTo(rewardView).mas_offset(contentH/2.0);
        make.height.mas_equalTo(HorPxFit(55));
    }];
    
    CGFloat titleLbH = VerPxFit(40);
    
    positionNameLb = [self lb:positionTitleFont];
    positionNameLb.font = [UIFont systemFontOfSize:16];
    positionNameLb.textColor = [UIColor whiteColor];
//    positionNameLb.text = @"产品经理";
    [contentView addSubview:positionNameLb];
    [positionNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).mas_offset(HorPxFit(40));
        make.top.equalTo(contentView).mas_offset(VerPxFit(10));
        make.right.mas_equalTo(rewardView.mas_left).mas_offset(-HorPxFit(10));
        make.height.mas_equalTo(titleLbH);
    }];
    
    companeyLb = [self lb:infoFont];
//    companeyLb.text = @"红岭经科技网络公司";
    companeyLb.font = [UIFont systemFontOfSize:14];
    companeyLb.textColor = [UIColor whiteColor];
    [contentView addSubview:companeyLb];
    [companeyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(positionNameLb);
        make.top.mas_equalTo(positionNameLb.mas_bottom).mas_offset(VerPxFit(5));
        make.right.mas_equalTo(rewardView.mas_left).mas_offset(-HorPxFit(10));
        make.height.mas_equalTo(titleLbH);
    }];
    
    subInfoContentView = [[UIView alloc] init];
    subInfoContentView.backgroundColor = [UIColor clearColor];
    [contentView addSubview:subInfoContentView];
    [subInfoContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companeyLb.mas_bottom);
        make.left.equalTo(positionNameLb);
        make.bottom.equalTo(contentView).mas_offset(-VerPxFit(10));
        make.right.mas_equalTo(rewardView.mas_left).mas_offset(-HorPxFit(10));
    }];
    
    CGFloat itemW = (CellW - rewardW - HorPxFit(20)) / 3;
    CGFloat itemH = HorPxFit(55);

    UILabel * experienceTitleLb = [[UILabel alloc] init];
    experienceTitleLb.text = @"年限";
    experienceTitleLb.textColor = [UIColor whiteColor];
    experienceTitleLb.backgroundColor = [UIColor clearColor];
    experienceTitleLb.textAlignment = NSTextAlignmentCenter;
    experienceTitleLb.font = [UIFont systemFontOfSize:12];
    [subInfoContentView addSubview:experienceTitleLb];
    [experienceTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(subInfoContentView);
        make.top.equalTo(subInfoContentView);
        make.size.mas_equalTo(CGSizeMake(itemW, itemH));
    }];
    
    experienceLb = [self lb:infoFont];
//    experienceLb.text = @"2~5年";
    experienceLb.textAlignment = NSTextAlignmentCenter;
    experienceLb.font = [UIFont systemFontOfSize:12];
    experienceLb.backgroundColor = [UIColor clearColor];
    [subInfoContentView addSubview:experienceLb];
    [experienceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(experienceTitleLb.mas_bottom);
        make.bottom.mas_equalTo(subInfoContentView).mas_offset(-VerPxFit(7));
        make.left.equalTo(experienceTitleLb);
        make.width.mas_equalTo(itemW);
    }];

    
    UILabel * educationTitleLb = [[UILabel alloc] init];
    educationTitleLb.text = @"学历";
    educationTitleLb.textAlignment = NSTextAlignmentCenter;
    educationTitleLb.font = [UIFont systemFontOfSize:12];
    educationTitleLb.textColor = [UIColor whiteColor];
    [subInfoContentView addSubview:educationTitleLb];
    [educationTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(experienceTitleLb.mas_right);
        make.top.equalTo(subInfoContentView);
        make.size.mas_equalTo(CGSizeMake(itemW, itemH));
    }];
    
    educationLb = [self lb:infoFont];
    educationLb.textAlignment = NSTextAlignmentCenter;
//    educationLb.text = @"本科";
    educationLb.font = [UIFont systemFontOfSize:12];
    [subInfoContentView addSubview:educationLb];
    [educationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(experienceTitleLb.mas_right);
        make.top.mas_equalTo(educationTitleLb.mas_bottom);
        make.bottom.mas_equalTo(subInfoContentView).mas_offset(-VerPxFit(7));
        make.width.mas_equalTo(itemW);
    }];

    UILabel * salaryTitleLb = [[UILabel alloc] init];
    salaryTitleLb.text = @"薪资";
    salaryTitleLb.textColor = [UIColor whiteColor];
    salaryTitleLb.font = [UIFont systemFontOfSize:12];
    salaryTitleLb.textAlignment = NSTextAlignmentCenter;
    [subInfoContentView addSubview:salaryTitleLb];
    [salaryTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(educationTitleLb.mas_right);
        make.top.equalTo(subInfoContentView);
        make.size.mas_equalTo(CGSizeMake(itemW, itemH));
    }];
    
    salaryLb = [self lb:infoFont];
    salaryLb.font = [UIFont systemFontOfSize:12];
    salaryLb.textAlignment = NSTextAlignmentCenter;
//    salaryLb.text = @"3k~5k";
    [subInfoContentView addSubview:salaryLb];
    [salaryLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(educationTitleLb.mas_bottom);
        make.bottom.mas_equalTo(subInfoContentView).mas_offset(-VerPxFit(7));
        make.left.mas_equalTo(educationTitleLb.mas_right);
        make.width.mas_equalTo(itemW);
    }];
   
    tailContentView = [[UIView alloc] init];
    tailContentView.layer.cornerRadius = 10;
    tailContentView.backgroundColor = [UIColor whiteColor];
    [self insertSubview:tailContentView belowSubview:contentView];
    [tailContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).mas_offset(HorPxFit(20));
        make.right.equalTo(contentView).mas_offset(-HorPxFit(20));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(30));
        make.height.mas_equalTo(VerPxFit(120));
    }];
    
    timeLb = [self lb:infoFont];
    timeLb.text = @"1天前发布";
    timeLb.textAlignment = NSTextAlignmentRight;
    timeLb.backgroundColor = [UIColor clearColor];
    timeLb.textColor = RGB16(0x7c7c7c);
    timeLb.font = [UIFont systemFontOfSize:12];
    [tailContentView addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(tailContentView).mas_offset(-HorPxFit(10));
        make.bottom.equalTo(tailContentView).mas_offset(-VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(150), VerPxFit(30)));
    }];

    UIImageView * locationFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locationFlag"]];
    [tailContentView addSubview:locationFlag];
    [locationFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(tailContentView).mas_offset(HorPxFit(15));
        make.bottom.equalTo(tailContentView).mas_offset(-VerPxFit(12));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(30), HorPxFit(30)));
    }];

    locationLb = [self lb:infoFont];
    locationLb.text = @"西安市雁塔区天谷八路中软国际";
    locationLb.backgroundColor = [UIColor clearColor];
    locationLb.textColor = RGB16(0x7c7c7c);
    locationLb.font = [UIFont systemFontOfSize:12];
    [tailContentView addSubview:locationLb];
    [locationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationFlag.mas_right).mas_offset(HorPxFit(9));
        make.right.mas_equalTo(timeLb.mas_left);
        make.bottom.equalTo(tailContentView).mas_offset(-VerPxFit(8));
        make.height.mas_equalTo(HorPxFit(40));
    }];
}

- (void)setJobModel:(JobModel *)jobModel{
    positionNameLb.text = jobModel.title;
    companeyLb.text = jobModel.hrModel.company.name;
    experienceLb.text = jobModel.workExperience;
    educationLb.text = jobModel.education;
    if (jobModel.salaryRange == 0) {
        salaryLb.text = @"面议";
    }else{
        salaryLb.text = [NSString stringWithFormat:@"%ld元",jobModel.salaryRange];
    }
    rewardLb.text = [NSString stringWithFormat:@"￥%ld",jobModel.price];
    locationLb.text = jobModel.workArea;
    timeLb.text = [BaseHelper stringWithTimeIntevl:jobModel.publishTime format:kDateFormatTypeYYYYMMDD];
}

- (UILabel *)lb:(UIFont *)textFont{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    lb.font = textFont;
    return lb;
}

@end
