//
//  RewardCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RewardCell.h"

@interface RewardCell(){
    
    UILabel * positionLb;
    UIButton * requestBt;
}

@end

@implementation RewardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createSubViews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)createSubViews{
    
    self.backgroundColor = [UIColor clearColor];
    CGFloat padding = HorPxFit(30);
    UIImageView * detailFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detail.png"]];
    [self addSubview:detailFlag];
    @weakify(self)
    [detailFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-HorPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(30), VerPxFit(40)));
    }];
    
    requestBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [requestBt setTitle:@"通知支付" forState:UIControlStateNormal];
    [requestBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    requestBt.titleLabel.font = [UIFont systemFontOfSize:13];
    [requestBt setBackgroundColor:[UIColor blackColor]];
    [requestBt addTarget:self action:@selector(requestPay) forControlEvents:UIControlEventTouchUpInside];
    requestBt.layer.cornerRadius = 5;
    [self addSubview:requestBt];
    [requestBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(140), VerPxFit(50)));
        make.right.equalTo(detailFlag.mas_left).mas_offset(-HorPxFit(50));
    }];
    positionLb = [[UILabel alloc] init];
    positionLb.font = [UIFont systemFontOfSize:14];
    positionLb.textColor = [UIColor blackColor];
    positionLb.backgroundColor = [UIColor clearColor];
    [self addSubview:positionLb];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.left.equalTo(self).mas_offset(padding);
        make.right.equalTo(requestBt.mas_left).offset(-padding);
        make.top.and.bottom.equalTo(self);
    }];
}
- (void)requestPay{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"通知企业支付酬金" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)setModel:(JobModel *)model{
    _model = model;
//    if (model.jobState == JobStateReward) {
//        requestBt.enabled = YES;
//        requestBt.backgroundColor = [UIColor blackColor];
//    }else{
//        requestBt.enabled = NO;
//        requestBt.backgroundColor = COLOR(200, 200, 200, 1);
//    }
//    positionLb.text = [NSString stringWithFormat:@"%@: ￥%ld",model.position,model.reward];
}
@end
