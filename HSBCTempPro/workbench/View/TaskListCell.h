//
//  TaskListCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskModel.h"
#import "PGIndexBannerSubiew.h"

@protocol TaskListCellDelegate
- (void)candidateChoosed:(CandidateModel *)candidateModel;
@end

@interface TaskListCell : PGIndexBannerSubiew

//  frame  tableview 的 frame
- (instancetype)initWithTBFrame:(CGRect)frame;
- (void)createSubViews;

@property (nonatomic , weak) id<TaskListCellDelegate>delegate;
@property (nonatomic , strong) TaskModel * taskModel;


@end
