//
//  PositionDetailHeaderView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"

@interface PositionDetailHeaderView : UIView

@property (nonatomic , strong) JobModel * jobModel;

@end
