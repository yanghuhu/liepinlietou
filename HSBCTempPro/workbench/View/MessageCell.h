//
//  MessageCell.h
//  HSBCTempPro
//
//  Created by Deve on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SysMessageModel.h"

@interface MessageCell : UITableViewCell


- (void)setDataModel:(ContentModel *)dict;

@end
