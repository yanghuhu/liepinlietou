//
//  HRInfoView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "HRInfoView.h"
#import "UIImageView+YYWebImage.h"
#import "UIView+YYAdd.h"

@interface HRInfoView(){
    
//    UIControl * bkControl;
    UIImageView * avatarImgV;
    UILabel * namelb;
    UILabel * secondLb;
    UILabel * thirdLb;
    UILabel * fourthLb;
}

@end

@implementation HRInfoView


- (void)createsSubViews{
    self.backgroundColor = [UIColor whiteColor];
//    bkControl = [[UIControl alloc] init];
//    bkControl.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
//    [bkControl addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
//    bkControl.backgroundColor = [UIColor blackColor];
//    bkControl.alpha = 0.4;
//    [self.superview addSubview:bkControl];
    
    CGFloat padding = HorPxFit(20);
    CGFloat avatarHeight = HorPxFit(80);
    CGFloat lbHeight = VerPxFit(40);
    avatarImgV = [[UIImageView alloc] initWithFrame:CGRectMake(padding, padding, avatarHeight, avatarHeight)];
    avatarImgV.contentMode = UIViewContentModeScaleAspectFit;
    avatarImgV.layer.cornerRadius = avatarHeight/2.0;
    [self addSubview:avatarImgV];
    
    namelb = [self lb];
    [namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(avatarImgV.mas_right).mas_offset(padding);
        make.centerY.equalTo(avatarImgV);
        make.right.equalTo(self).mas_offset(-padding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    secondLb = [self lb];
    [secondLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(padding);
        make.top.equalTo(avatarImgV.mas_bottom).offset(padding);
        make.right.equalTo(self).mas_offset(-padding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(phoneCall) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-padding);
        make.centerY.equalTo(secondLb);
        make.size.mas_equalTo(CGSizeMake(avatarHeight, avatarHeight));
    }];
    
//    thirdLb = [self lb];
//    [thirdLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).mas_offset(padding);
//        make.top.equalTo(secondLb.mas_bottom).offset(padding);
//        make.right.equalTo(self).mas_offset(-padding);
//        make.height.mas_equalTo(lbHeight);
//    }];
    fourthLb = [self lb];
    [fourthLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(padding);
        make.top.equalTo(secondLb.mas_bottom).offset(padding);
        make.right.equalTo(self).mas_offset(-padding);
        make.height.mas_equalTo(lbHeight);
    }];
    float a = lbHeight*2+padding*5+avatarHeight;
    self.height = a;
}

- (void)setModel:(HRModel *)model{
    
    _model = model;
//    [avatarImgV setImageWithURL:[NSURL URLWithString:model.avatar] placeholder:[UIImage imageNamed:@"avatarPlaceaholder.png"]];
//    namelb.text = model.name;
//    secondLb.text = model.phone;
////    thirdLb.text = model.domain;
//    fourthLb.text = model.company;
}


- (void)phoneCall{
//    [BaseHelper phoneCallWithNum:_model.phone];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor blackColor];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lb];
    return lb;
}

@end
