//
//  PositionDetailHeaderView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDetailHeaderView.h"

@interface PositionDetailHeaderView(){
    
    UILabel * companyLb;
    UILabel * positionLb;
    UILabel * timeLb;
}
@end

@implementation PositionDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionHeaderDetailBk"]];
    [self addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    positionLb = [self lbWithTextColor:RGB16(0x5272ed)];
    positionLb.text = @"职位名称";
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(VerPxFit(50));
        make.left.mas_equalTo(self).mas_offset(HorPxFit(40));
        make.right.mas_equalTo(self).mas_offset(-HorPxFit(40));
        make.top.mas_equalTo(self.mas_centerY);
    }];
    
    companyLb = [self lbWithTextColor:RGB16(0x5272ed)];
    companyLb.text = @"公司名称";
    [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(VerPxFit(50));
        make.left.mas_equalTo(self).mas_offset(HorPxFit(40));
        make.right.mas_equalTo(self).mas_offset(-HorPxFit(40));
        make.bottom.mas_equalTo(positionLb.mas_top).mas_offset(-VerPxFit(15));
    }];

   
}

- (UILabel *)lbWithTextColor:(UIColor *)color{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = color;
    lb.backgroundColor = [UIColor clearColor];
    lb.textColor = [UIColor whiteColor];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:17];
    [self addSubview:lb];
    return lb;
    
}


- (void)setJobModel:(JobModel *)jobModel{
    _jobModel = jobModel;
    positionLb.text = jobModel.position;
    companyLb.text = jobModel.hrModel.company.name;

    NSString * text = [NSString stringWithFormat:@"%@发布",[BaseHelper stringWithTimeIntevl:jobModel.publishTime format:kDateFormatTypeYYYYMMDD]];
    CGSize size = [BaseHelper getSizeWithString:text font:[UIFont systemFontOfSize:13] contentWidth:MAXFLOAT contentHight:HorPxFit(30)];

    timeLb.text = text;
    [timeLb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width);
    }];
}

@end
