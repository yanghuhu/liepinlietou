//
//  TodayDataShow.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TodayDataShow.h"
@interface TodayDataShow(){
    
    UIImageView * dieImageV;
}
@end

@implementation TodayDataShow

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createsSubViews];
    }
    return self;
}

- (void)createsSubViews{
    dieImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"todayDieImgV"]];
    dieImageV.frame = self.bounds;
    [self addSubview:dieImageV];
}

@end
