//
//  TaskPositionButton.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskModel.h"

@interface TaskPositionButton : UIButton

@property (nonatomic , assign) NSInteger row;
@property (nonatomic , strong) PositionOrderModel * positionModel;

@end
