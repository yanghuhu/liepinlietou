//
//  JobDetailView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/17.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobDetailView.h"

@interface JobDetailView(){
    UILabel * rewardLb;
    UILabel * companyLb;
    UILabel * positionLb;
    UILabel * cityLb;
    UILabel * timeLb;
    UILabel * baseInfoLb;
    UILabel * companyInfoLb;
    UILabel * jobDesLb;
}

@end

@implementation JobDetailView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor  = [UIColor whiteColor];
        [self createSubViews];
        [self addTopRightDismissBt];
    }
    return self;
}

- (void)createSubViews{
    CGFloat horpadding = HorPxFit(28);
    CGFloat verpadding = VerPxFit(30);
    CGFloat lbHeight = VerPxFit(45);
    CGFloat titleW = HorPxFit(160);
    rewardLb = [self lb];
    rewardLb.font  = [UIFont systemFontOfSize:20];
    rewardLb.textColor = [UIColor redColor];
    @weakify(self)
    [rewardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.equalTo(self).mas_equalTo(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UILabel * companyTitle = [self lb];
    companyTitle.text = @"公司名称:";
    [companyTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.equalTo(rewardLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, lbHeight));
    }];
    
    companyLb = [self lb];
    [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.mas_equalTo(companyTitle.mas_right).mas_equalTo(horpadding);
        make.top.equalTo(rewardLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UILabel * positionTitle = [self lb];
    positionTitle.text = @"职位名称:";
    [positionTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.equalTo(companyLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, lbHeight));
    }];
    positionLb = [self lb];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.mas_equalTo(positionTitle.mas_right).mas_equalTo(horpadding);
        make.top.equalTo(companyLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    UIImageView * locationimgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location.png"]];
    [self addSubview:locationimgV];
    [locationimgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(horpadding);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), HorPxFit(50)));
    }];
    cityLb = [self lb];
    [cityLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationimgV.mas_right).mas_offset(horpadding);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), HorPxFit(50)));
    }];
    UIImageView * timeimgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"time.png"]];
    [self addSubview:timeimgV];
    [timeimgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cityLb.mas_right).mas_offset(horpadding);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), HorPxFit(50)));
    }];
    timeLb = [self lb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(timeimgV.mas_right).mas_offset(horpadding);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(150), HorPxFit(50)));
    }];
    
    baseInfoLb = [self lb];
    [baseInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.mas_equalTo(cityLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UILabel * companyInfoTitle = [self lb];
    companyInfoTitle.text = @"公司标签:";
    [companyInfoTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.equalTo(baseInfoLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, lbHeight));
    }];
    companyInfoLb = [self lb];
    [companyInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(companyInfoTitle.mas_right).mas_equalTo(horpadding);
        make.top.equalTo(baseInfoLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UILabel * jobDesTitle = [self lb];
    jobDesTitle.text = @"职位描述:";
    [jobDesTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_equalTo(horpadding);
        make.top.equalTo(companyInfoLb.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, lbHeight));
    }];
    jobDesLb = [self lb];
    [jobDesLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.mas_equalTo(jobDesTitle.mas_right).mas_equalTo(horpadding);
        make.top.equalTo(companyInfoLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_equalTo(-horpadding);
        make.height.mas_equalTo(lbHeight);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(underTakeAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitle:@"猎人接榜" forState:UIControlStateNormal];
    [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bt.backgroundColor = [UIColor blackColor];
    bt.layer.cornerRadius = 5;
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) return;
        make.left.equalTo(self).mas_offset(horpadding);
        make.right.equalTo(self).mas_offset(-horpadding);
        make.top.mas_equalTo(jobDesLb.mas_bottom).mas_offset(verpadding);
        make.height.mas_equalTo(VerPxFit(80));
    }];
}

- (void)setJobModel:(JobModel *)jobModel{
    _jobModel = jobModel;
//    rewardLb.text = [NSString stringWithFormat:@"￥%ld 赏金",jobModel.reward];
//    companyLb.text = jobModel.company;
//    positionLb.text = jobModel.position;
//    cityLb.text = jobModel.city;
//    timeLb.text = @"一天前";
//    baseInfoLb.text = [NSString stringWithFormat:@"%@|%@|%ld岁",jobModel.education,jobModel.experienceReq,jobModel.age];
//    companyInfoLb.text = jobModel.companyTag;
//    jobDesLb.text = jobModel.jobDesc;
}

- (void)underTakeAction{
    [self dismissAction];
    [self.delegate jobUndertakeAction:_jobModel];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = NSTextAlignmentLeft;
    lb.font = [UIFont systemFontOfSize:14];
    [self addSubview:lb];
    return lb;
}

+ (CGFloat)viewHeight{
    CGFloat viewH = 0;
    CGFloat verpadding = VerPxFit(30);
    CGFloat lbHeight = VerPxFit(45);
    
    viewH += (7 *lbHeight);
    viewH+= (8*verpadding);
    viewH +=VerPxFit(80);
    viewH += verpadding;
    return viewH;
}

@end
