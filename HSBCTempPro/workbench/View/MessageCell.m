//
//  MessageCell.m
//  HSBCTempPro
//
//  Created by Deve on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell
{
    UILabel *title;
    UILabel *msContentLabel;
    UILabel *timeLabel;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        [self createSubView];
        
    }
    return self;
}

- (void)createSubView{
    
    CGFloat padding_h = HorPxFit(50);
    UIImageView *headImage = [UIImageView new];
    headImage.image = [UIImage imageNamed:@"messageHead"];
    [self.contentView addSubview:headImage];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(HorPxFit(70), VerPxFit(70)));
        make.bottom.mas_equalTo(self.contentView.mas_centerY);
        make.left.mas_equalTo(self.contentView).offset(padding_h);
    }];
    
    UILabel *titleLabel = [self lb];
    titleLabel.text = @"";
    titleLabel.textColor = [UIColor lightGrayColor];
    titleLabel.font = [UIFont systemFontOfSize:18];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(headImage.mas_centerY);
        make.left.mas_equalTo(headImage.mas_right).offset(HorPxFit(30));
        make.height.mas_equalTo(VerPxFit(22));
        make.right.mas_equalTo(self.contentView);
    }];
    title = titleLabel;

    
    UILabel *messageLabel = [self lb];
    messageLabel.font = [UIFont systemFontOfSize:15];
    messageLabel.text = @"";
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel.mas_left);
        make.right.mas_equalTo(titleLabel.mas_right);
        make.height.mas_equalTo(titleLabel.mas_height);
        make.top.mas_equalTo(titleLabel.mas_bottom).offset(VerPxFit(30));
    }];
    msContentLabel = messageLabel;
    
    UILabel *dateLabel = [self lb];
    dateLabel.text = @"一分钟前";
    dateLabel.font = [UIFont systemFontOfSize:11];
    dateLabel.textColor = [UIColor lightGrayColor];
    [dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.mas_equalTo(titleLabel);
        make.bottom.mas_equalTo(self.contentView).offset(-VerPxFit(20));
    }];
    timeLabel = dateLabel;
    
}

- (UILabel *)lb{
    UILabel *lb = [UILabel new];
    lb.textColor = [UIColor whiteColor];
    [self.contentView addSubview:lb];
    return lb;
}

- (void)setDataModel:(ContentModel *)model{
    
    NSDictionary *modelDict = (NSDictionary *)model;
    title.text = modelDict[@"receiver"];
    msContentLabel.text = modelDict[@"content"];
    
    NSTimeInterval interval = [modelDict[@"sendTime"] doubleValue]/1000;
    NSString * text = [NSString stringWithFormat:@"%@",[BaseHelper stringWithTimeIntevl:interval format:kDateFormatTypeYYYYMMDD]];
    CGSize size = [BaseHelper getSizeWithString:text font:[UIFont systemFontOfSize:13] contentWidth:MAXFLOAT contentHight:HorPxFit(30)];
    
    timeLabel.text = text;
    [timeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width);
    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
