//
//  WeekDataView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/7.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "WeekDataView.h"

@interface WeekDataView(){
 
    UILabel * todayNumLb;
    UILabel * allNumLb;
    UILabel * rewardLb;
    
    ChatView * chatView;
}
@end

@implementation WeekDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    chatView = [[ChatView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), VerPxFit(400))];
    [self addSubview:chatView];
    
    UIView * numContentView = [[UIView alloc] init];
    [self addSubview:numContentView];
    [numContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(self);
        make.top.mas_equalTo(chatView.mas_bottom).mas_offset(-VerPxFit(13));
    }];
    
    UIImageView * imgV = [[UIImageView alloc] init];
    imgV.image = [UIImage imageNamed:@"chatBottom"];
    [numContentView addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.and.top.equalTo(numContentView);
    }];
    
    CGFloat selfW = CGRectGetWidth(self.frame);
    CGFloat itemW = selfW / 3.0;
    NSArray * titles = @[@"今日推荐人数",@"共计推荐人数",@"共获得赏金"];
    for(int i=0;i<titles.count;i++){
        UILabel * titleLb = [[UILabel alloc] init];
        titleLb.text = titles[i];
        titleLb.font = [UIFont systemFontOfSize:12];
        titleLb.textColor = [UIColor whiteColor];
        titleLb.textAlignment = NSTextAlignmentCenter;
        [numContentView addSubview:titleLb];
        [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(numContentView).mas_offset(itemW*i);
//            make.top.equalTo(numContentView);
            make.height.mas_equalTo(VerPxFit(65));
            make.bottom.mas_equalTo(numContentView.mas_centerY);
            make.width.mas_equalTo(itemW);
        }];
        
        UILabel * textLb = [[UILabel alloc] init];
//        textLb.backgroundColor = [UIColor yellowColor];
        textLb.font = [UIFont systemFontOfSize:16];
        textLb.textAlignment = NSTextAlignmentCenter;
        textLb.textColor = [UIColor whiteColor];
        [numContentView addSubview:textLb];
        [textLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.equalTo(titleLb);
            make.top.mas_equalTo(numContentView.mas_centerY);
            make.height.mas_equalTo(VerPxFit(60));
        }];
        if(i != 0){
            UIView * lineView = [[UIView alloc] init];
            lineView.backgroundColor = [UIColor whiteColor];
            [numContentView addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(numContentView).mas_offset(itemW*i);
                make.centerY.mas_equalTo(numContentView.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(HorPxFit(2), VerPxFit(100)));
            }];
        }
        if (i == 0) {
            todayNumLb = textLb;
            todayNumLb.text = @"5人";
        }else if (i == 1){
            allNumLb = textLb;
            allNumLb.text = @"10人";
        }else if (i == 2){
            rewardLb = textLb;
            rewardLb.text = @"￥1200";
        }
    }
}

- (void)showWithSupView:(UIView *)view withData:(NSDictionary *)dic{
    
    todayNumLb.text=  [NSString stringWithFormat:@"%d",[dic[@"todayRecommendCount"] intValue]];
    allNumLb.text =  [NSString stringWithFormat:@"%d",[dic[@"totalRecommendCount"] intValue]];
    rewardLb.text =  [NSString stringWithFormat:@"%d",[dic[@"totalCommission"] intValue]];
    
    
    chatView.dataArray =dic[@"recommendList"];
    [super showWithSupView:view];
}

@end
