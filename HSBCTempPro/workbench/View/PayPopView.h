//
//  PayPopView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/22.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"
#import "OrderModel.h"

@protocol PayPopViewDelegate

- (void)payAction;

@end

@interface PayPopView : BasePopView

@property (nonatomic , strong) OrderModel * orderModel;
@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , weak) id<PayPopViewDelegate>delegate;

@end
