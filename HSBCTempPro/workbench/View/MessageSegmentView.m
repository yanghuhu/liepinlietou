//
//  MessageSegmentView.m
//  HSBCTempPro
//
//  Created by Deve on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "MessageSegmentView.h"

@implementation MessageSegmentView
{
//    线条
    UIImageView *lineImage;
//    系统消息
    UIButton * sysButton;
//    私有消息
    UIButton * privateButton;
//    当前选中的button 默认是系统消息button
    UIButton * currentButton;
}


- (instancetype)init{
    self  = [super init];
    if (self) {
        [self createSubView];
    }
    return self;
}

- (void)createSubView{
    UIButton *systemMessage = [self but];
    [systemMessage setBackgroundImage:[UIImage imageNamed:@"systemMs_High"] forState:UIControlStateNormal];
    [systemMessage addTarget:self action:@selector(systemMsAction:) forControlEvents:UIControlEventTouchUpInside];
    systemMessage.tag = 200;
    [systemMessage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.centerX.mas_equalTo(self.mas_centerX).multipliedBy(0.5);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(60), VerPxFit(30)));
    }];
    currentButton = systemMessage;
    
    UIImageView *line = [self line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX).multipliedBy(0.5);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(60), VerPxFit(4)));
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
    lineImage = line;
    
    UIButton *privateMessage = [self but];
    [privateMessage setBackgroundImage:[UIImage imageNamed:@"privateMs_Normal"] forState:UIControlStateNormal];
    [privateMessage addTarget:self action:@selector(privateMsAction:) forControlEvents:UIControlEventTouchUpInside];
    privateMessage.tag = 201;
    [privateMessage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.centerX.mas_equalTo(self.mas_centerX).multipliedBy(1.5);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(60), VerPxFit(30)));
    }];
    privateButton = privateMessage;
    
}

- (void)systemMsAction:(UIButton *)but{
    if (currentButton==but) {
        return;
    }
    
    [but setBackgroundImage:[UIImage imageNamed:@"systemMs_High"] forState:UIControlStateNormal];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"privateMs_Normal"] forState:UIControlStateNormal];
    [lineImage mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX).offset(-ScreenWidth/4);
    }];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    }];
    currentButton = but;
    if ([self.delegate respondsToSelector:@selector(segmentClickChickerData:)]) {
        [self.delegate segmentClickChickerData:but.tag];
    }
    NSLog(@"systemMsAction");
}

- (void)privateMsAction:(UIButton *)but{
    if (currentButton==but) {
        return;
    }
    
    [but setBackgroundImage:[UIImage imageNamed:@"privateMs_High"] forState:UIControlStateNormal];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"systemMs_Normal"] forState:UIControlStateNormal];
    [lineImage mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX).offset(ScreenWidth/4);
    }];
    
    [UIView animateWithDuration:0.5 animations:^{
       [self layoutIfNeeded];
    }];
    currentButton = but;
    if ([self.delegate respondsToSelector:@selector(segmentClickChickerData:)]) {
        [self.delegate segmentClickChickerData:but.tag];
    }
    NSLog(@"privateMsAction");
}

- (UIButton *)but{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:button];
    return button;
}

- (UIImageView *)line{
    UIImageView *lineImage = [UIImageView new];
    lineImage.image = [UIImage imageNamed:@"messageLine"];
    [self addSubview:lineImage];
    return lineImage;
}

@end
