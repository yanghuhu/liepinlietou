//
//  TalentListTBCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/6.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalentModel.h"

#define CellH  VerPxFit(180)
//#define CellW  ScreenWidth - HorPxFit(40)*2 - HorPxFit(20)*2
#define CellW  ScreenWidth 


@protocol TalentListTBCellDelegate
@optional
- (void)talentPayAction:(TalentModel *)model;
- (void)phoneCall:(TalentModel *)model;
@end

@interface TalentListTBCell : UITableViewCell

@property (nonatomic , weak) id<TalentListTBCellDelegate>delegate;
@property (nonatomic , assign) BOOL showMatch;
@property (nonatomic , strong) TalentModel * talentModel;
- (void)createSbuViews;

@end
