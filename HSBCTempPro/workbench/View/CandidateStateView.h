//
//  CandidateStateView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/9.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyOrderModel.h"
#import "JobOrderDetailModel.h"

@protocol CandidateStateViewDelegate
- (void)dateChoose;
- (void)dateSureAction:(NSString *)iphone address:(NSString *)address;
- (void)requestReward;

- (void)rateAction;
@end

@interface CandidateStateView : UIView

@property (nonatomic , weak) id<CandidateStateViewDelegate>delegate;
@property (nonatomic , strong) JobOrderDetailModel * model;

- (void)updateYueMianTime:(NSDate *)date;
- (void)yumianTimeSumintFinish:(NSString *)url;

@end
