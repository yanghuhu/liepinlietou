//
//  TaskCandidateView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TaskCandidateView.h"
#import "UIImageView+YYWebImage.h"


#define hongdianFlagTag 1000

@interface TaskCandidateView(){
    
    UIImageView * bkImgV;
}

@end

@implementation TaskCandidateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrows"]];
        [self addSubview:bkImgV];
        CGFloat arrawW = HorPxFit(60);
        CGFloat  arrawH =  arrawW * 13 / 25.0;
        [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.mas_top);
            make.left.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(HorPxFit(arrawW), arrawH));
        }];
        self.backgroundColor  = COLOR(242, 242, 242, 1);
        [self addSubview:bkImgV];
    }
    return self;
}


- (CGFloat)initSuvViews:(NSArray<CandidateModel*> *)candidateArray withArrowX:(CGFloat )arrowX{
    [bkImgV mas_updateConstraints:^(MASConstraintMaker *make) {
        CGFloat arrawW = HorPxFit(20);
        make.left.mas_equalTo(arrowX - arrawW/2.0); 
    }];
    self.candidateArray = candidateArray;
    CGFloat horPadding = HorPxFit(25);
    CGFloat verPadding = VerPxFit(15);
    
    CGFloat selfNeedH = 0;
    
    NSInteger rowNum = 4;
    
    CGFloat itemW = (_selfW-(7)*horPadding) / rowNum;
    CGFloat lbH = 20;
    CGFloat itemH = itemW+lbH;
    
    CGFloat curY = verPadding;
    CGFloat curX = verPadding;
    for (int i=0;i<self.candidateArray.count;i++) {
        
        CandidateModel * model = self.candidateArray[i];
        UIImageView * avatarImgv = [[UIImageView alloc] initWithFrame:CGRectMake(curX, curY, itemW, itemW)];
        avatarImgv.clipsToBounds = YES;
        avatarImgv.layer.cornerRadius = itemW/2.0;
        avatarImgv.contentMode = UIViewContentModeScaleAspectFill;
        
        @weakify(avatarImgv);
        [avatarImgv setImageWithURL:[NSURL URLWithString:model.talentModel.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1l] options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            if ([model.status isEqualToString:@"OFF"]) {
                @strongify(avatarImgv);
                if(image){
                    avatarImgv.image = [BaseHelper grayImage:image];
                }
            }
        }];
        [self addSubview:avatarImgv];
        if (model.hhStateFlag) {
            UIView * flagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(18), HorPxFit(18))];
            flagView.tag = hongdianFlagTag+i;
            flagView.layer.cornerRadius = HorPxFit(18)/2.0;
            flagView.backgroundColor = [UIColor redColor];
            flagView.center = CGPointMake(CGRectGetMaxX(avatarImgv.frame)-itemW/6, avatarImgv.center.y - avatarImgv.frame.size.height/2);
            [self addSubview:flagView];
        }
    
        UILabel * nameLb = [[UILabel alloc] initWithFrame:CGRectMake(curX, CGRectGetMaxY(avatarImgv.frame), itemW, lbH)];
        nameLb.text = model.talentModel.name;
        nameLb.font = [UIFont systemFontOfSize:13];
        nameLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:nameLb];
        
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.tag = i;
        [bt addTarget:self action:@selector(talentSelected:) forControlEvents:UIControlEventTouchUpInside];
        bt.frame = CGRectMake(curX, curY, itemW, itemH);
        [self addSubview:bt];
        
        if (((i+1) % rowNum) == 0) {
            curY += (verPadding+itemH);
            curX = horPadding;
        }else{
            curX += (itemW+horPadding);
        }
        selfNeedH = CGRectGetMaxY(nameLb.frame)+verPadding;
    }
    return selfNeedH;
}

- (void)talentSelected:(UIButton *)send{
    [self.delegate candidateSelected:_candidateArray[send.tag]];

    UIView * view = [self viewWithTag:hongdianFlagTag+send.tag];
    if (view) {
        [view removeFromSuperview];
    }
}

@end
