//
//  TalentPoorSearchView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentPoorSearchView.h"

@interface TalentPoorSearchView()<UITextFieldDelegate>{
    UIButton * actionBt;
    UITextField * searchTf;
}
@end

@implementation TalentPoorSearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UIButton * resumeInportBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [resumeInportBt addTarget:self action:@selector(resumeInportAction) forControlEvents:UIControlEventTouchUpInside];
    [resumeInportBt setBackgroundImage:[UIImage imageNamed:@"seacherBt"] forState:UIControlStateNormal];
    [resumeInportBt setTitle:@"我的简历" forState:UIControlStateNormal];
    resumeInportBt.titleLabel.font = [UIFont systemFontOfSize:13];
    [resumeInportBt setTitleColor:[UIColor colorWithRed:250/255.0f green:74/255.0f blue:78/255.0f alpha:1] forState:UIControlStateNormal];
    [self addSubview:resumeInportBt];
    [resumeInportBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-HorPxFit(30));
        make.height.mas_equalTo(VerPxFit(65));
        make.width.mas_equalTo(HorPxFit(150));
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    
    UIImageView * searchView = [[UIImageView alloc] init];
    searchView.image = [UIImage imageNamed:@"talentSearchBg"];
    searchView.userInteractionEnabled = YES;
    [self addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(30));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(resumeInportBt.mas_left).mas_offset(-HorPxFit(20));
        make.height.mas_equalTo(VerPxFit(65));
    }];
    
    actionBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [actionBt addTarget:self action:@selector(textFilesAction) forControlEvents:UIControlEventTouchUpInside];
    [actionBt setImage:[UIImage imageNamed:@"talentSearch"] forState:UIControlStateNormal];
    [searchView addSubview:actionBt];
    [actionBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(searchView).mas_offset(-HorPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), HorPxFit(50)));
        make.centerY.equalTo(searchView.mas_centerY);
    }];
    
    NSString *placeholder = @"输入您想搜索的人员姓名";
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:placeholder];
    [attribute addAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]} range:NSMakeRange(0, placeholder.length)];
    searchTf = [[UITextField alloc] init];
    searchTf.placeholder = placeholder;
    searchTf.attributedPlaceholder = attribute;
    searchTf.returnKeyType = UIReturnKeySearch;
    searchTf.textColor = [UIColor whiteColor];
    searchTf.backgroundColor = [UIColor clearColor];
    searchTf.font = [UIFont systemFontOfSize:13];
    searchTf.delegate = self;
    [searchView addSubview:searchTf];
    [searchTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchView).mas_offset(HorPxFit(20));
        make.top.equalTo(searchView);
        make.bottom.equalTo(searchView);
        make.right.mas_equalTo(actionBt.mas_left).mas_offset(-HorPxFit(20));
    }];
}

- (void)textFilesAction{
    if (searchTf.text.length >0) {
        searchTf.text = nil;
        [actionBt setImage:[UIImage imageNamed:@"searchFlag.png"] forState:UIControlStateNormal];
        [self.delegate searchCancel];
    }
}

- (void)resumeInportAction{
    [self.delegate toResumeCheckAction];
}

#pragma mark -- UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [actionBt setImage:[UIImage imageNamed:@"chaFlag"] forState:UIControlStateNormal];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString * searchKey = textField.text;
    if (searchKey && searchKey.length !=0) {
        [self.delegate searchWithKey:searchKey];
    }else{
        [actionBt setImage:[UIImage imageNamed:@"searchFlag.png"] forState:UIControlStateNormal];
        [self.delegate searchCancel];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
@end
