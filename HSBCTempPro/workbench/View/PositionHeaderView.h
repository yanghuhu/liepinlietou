//
//  PositionHeaderView.h
//  HSBCTempPro
//
//  Created by Deve on 2018/3/5.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"


@interface PositionHeaderView : UIView

@property (nonatomic , strong) JobModel * jobModel;

@end
