//
//  MoneyHeaderView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MoneyHeaderView.h"
#import "Healp.h"

@interface MoneyHeaderView(){
    UIImageView * notpayFlagLine;
    UIImageView * hadpayFlagLine;
    UILabel * notPayLabel;
    UILabel * hadPayLabel;
    UIView * talentPayView;
}
@end
@implementation MoneyHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UIButton *resumeInportBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [resumeInportBt addTarget:self action:@selector(showChatAction) forControlEvents:UIControlEventTouchUpInside];
    [resumeInportBt setBackgroundImage:[UIImage imageNamed:@"seacherBt"] forState:UIControlStateNormal];
    resumeInportBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:resumeInportBt];
    resumeInportBt.frame = CGRectMake(ScreenWidth-HorPxFit(150)-HorPxFit(20), VerPxFit(self.center.y)+4, HorPxFit(150), VerPxFit(65));
    [self layoutGrandient:self label:resumeInportBt.titleLabel];

    UILabel *resumeInportlb = [self lb];
    resumeInportlb.text = @"钱袋";
    resumeInportlb.backgroundColor = [UIColor clearColor];
    resumeInportlb.layer.cornerRadius = VerPxFit(70)/2.0;
    [resumeInportBt addSubview:resumeInportlb];
    resumeInportlb.frame = CGRectMake(0, 0, CGRectGetWidth(resumeInportBt.frame), CGRectGetHeight(resumeInportBt.frame));
    [self layoutGrandient:resumeInportBt label:resumeInportlb];
    
    
    UIImageView * payView = [[UIImageView alloc] init];
    payView.image = [UIImage imageNamed:@"talentSearchBg"];
    payView.userInteractionEnabled = YES;
    talentPayView = payView;
    [self addSubview:payView];
    [payView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(20));
        make.centerY.mas_equalTo(self.mas_centerY).offset(4);
        make.right.mas_equalTo(resumeInportBt.mas_left).mas_offset(-HorPxFit(80));
        make.height.mas_equalTo(VerPxFit(65));
    }];
    
    
    UILabel *notPaylb = [self lb];
    notPaylb.text = @"未支付";
    notPaylb.backgroundColor = [UIColor clearColor];
    notPaylb.layer.cornerRadius = VerPxFit(70)/2.0;
    [payView addSubview:notPaylb];
    notPaylb.frame = CGRectMake(20,payView.center.y-2 , 108, 38);
    notPayLabel = notPaylb;
    notPaylb.textColor = COLOR(223, 214, 167, 1);
//    [self layoutGrandient:payView label:notPaylb];
    
   
    notPaybt = [UIButton buttonWithType:UIButtonTypeCustom];
    notPaybt.layer.cornerRadius = VerPxFit(70)/2.0;
    [notPaybt addTarget:self action:@selector(notPayAction) forControlEvents:UIControlEventTouchUpInside];
    [payView addSubview:notPaybt];
    [notPaybt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(payView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(108, 38));
        make.left.mas_equalTo(self.mas_left).offset(20);
    }];
    
   
    UILabel *hadPayLb = [self lb];
    hadPayLb.text = @"已支付";
    hadPayLb.backgroundColor = [UIColor clearColor];
    hadPayLb.layer.cornerRadius = VerPxFit(70)/2.0;
    [payView addSubview:hadPayLb];
    hadPayLb.frame = CGRectMake(CGRectGetMaxX(notPayLabel.frame)+10,payView.center.y-2 , 108, 38);
    hadPayLabel = hadPayLb;
    
    hadPayBt = [UIButton buttonWithType:UIButtonTypeCustom];
    hadPayBt.layer.cornerRadius = VerPxFit(70)/2.0;
    [hadPayBt addTarget:self action:@selector(hadAction) forControlEvents:UIControlEventTouchUpInside];
    [payView addSubview:hadPayBt];
    [hadPayBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(payView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(108, 38));
        make.left.mas_equalTo(notPaybt.mas_right).offset(10);
    }];
}

- (void)hadAction{
//    [self layoutGrandient:talentPayView label:hadPayLabel];
    hadPayLabel.textColor = COLOR(223, 214, 167, 1);
    notPayLabel.textColor = [UIColor whiteColor];

    [self.delegate appearHadPayData];
}

- (void)notPayAction{
//    [self layoutGrandient:talentPayView label:notPayLabel];
    notPayLabel.textColor = COLOR(223, 214, 167, 1);
    hadPayLabel.textColor = [UIColor whiteColor];
    [self.delegate appearNotPayData];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.backgroundColor = [UIColor clearColor];
    lb.textColor = [UIColor whiteColor];
    return lb;
}

- (void)layoutGrandient:(UIView *)view label:(UILabel *)label{
    UIColor *color = [UIColor colorWithRed:197/255.0f green:173/255.0f blue:132/255.0f alpha:1];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(id)color.CGColor,  (id)[UIColor whiteColor].CGColor];
    gradientLayer.startPoint = CGPointMake(0, 1);
    gradientLayer.endPoint = CGPointMake(1, 1);
    gradientLayer.frame = label.frame;
    gradientLayer.mask = label.layer;
    label.layer.frame = gradientLayer.bounds;
    [view.layer addSublayer:gradientLayer];
}

- (void)layoutGrandient2:(UIView *)view label2:(UILabel *)label{
    UIColor *color = [UIColor whiteColor];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(id)color.CGColor,  (id)[UIColor whiteColor].CGColor];
    gradientLayer.startPoint = CGPointMake(0, 1);
    gradientLayer.endPoint = CGPointMake(1, 1);
    gradientLayer.frame = label.frame;
    gradientLayer.mask = label.layer;
    label.layer.frame = gradientLayer.bounds;
    [view.layer addSublayer:gradientLayer];
}

- (void)showChatAction{
    [self.delegate showChatImage];
}



@end
