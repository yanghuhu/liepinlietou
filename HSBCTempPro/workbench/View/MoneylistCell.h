//
//  MoneylistCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RewardModel.h"
@protocol MoneylistCellDelegate
- (void)positionDetailCheck:(JobModel *)model;
@end

@interface MoneylistCell : UITableViewCell

@property (nonatomic , assign) BOOL isSubCreated;
@property (nonatomic , strong) RewardModel * model;
@property (nonatomic , weak) id<MoneylistCellDelegate>delegate;

@end
