//
//  MoneyChatView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MoneyChatView.h"

@interface MoneyChatView(){
    UIImageView * dieImageV;
    UILabel * hadPayLb;
    UILabel * waitPayLb;
}

@end

@implementation MoneyChatView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    dieImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"backCard"]];
    dieImageV.frame = self.bounds;
    [self addSubview:dieImageV];
    
    CGFloat lbH = VerPxFit(50);
    
    UILabel * hadPayLbTitle = [self lbWithText:@"账户余额" font:[UIFont systemFontOfSize:15]];
    [hadPayLbTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(100));
        make.right.equalTo(self).mas_offset(-HorPxFit(100));
        make.top.equalTo(self).mas_offset(VerPxFit(90));
        make.size.mas_equalTo(CGSizeMake(100, lbH));
    }];
    
    hadPayLb = [self lbWithText:nil font:[UIFont systemFontOfSize:21]];
    hadPayLb.text = [NSString stringWithFormat:@"%ld元",[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.balance];
    hadPayLb.textAlignment = NSTextAlignmentCenter;
    [hadPayLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(hadPayLbTitle);
        make.top.mas_equalTo(hadPayLbTitle.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * waitPayLbTitle = [self lbWithText:@"赠送余额" font:[UIFont systemFontOfSize:15]];
    [waitPayLbTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(hadPayLbTitle);
        make.size.equalTo(hadPayLbTitle);
        make.top.mas_equalTo(hadPayLb.mas_bottom).mas_offset(VerPxFit(10));
    }];
    
    waitPayLb = [self lbWithText:nil font:[UIFont systemFontOfSize:21]];
    waitPayLb.text = [NSString stringWithFormat:@"%ld元",[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.grantBalance];
    waitPayLb.textAlignment = NSTextAlignmentCenter;
    [waitPayLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.height.and.right.equalTo(hadPayLb);
        make.top.mas_equalTo(waitPayLbTitle.mas_bottom).mas_offset(VerPxFit(10));
    }];
}

- (UILabel *)lbWithText:(NSString *)text font:(UIFont *)font {
    
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    if (text) {
        lb.text = text;
    }
    lb.font = font;
    [self addSubview:lb];
    return lb;
}



@end
