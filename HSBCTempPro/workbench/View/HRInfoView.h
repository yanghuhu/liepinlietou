//
//  HRInfoView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"
#import "HRModel.h"

@interface HRInfoView : BasePopView

@property (nonatomic , strong) HRModel * model;

- (void)createsSubViews;
//- (void)showWithSuper:(UIView *)view;

@end
