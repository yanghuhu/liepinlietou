//
//  TaskListCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/7.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TaskListCell.h"
#import "UIView+YYAdd.h"
#import "TaskPositionButton.h"
#import "TaskCandidateView.h"

@interface TaskListCell()<TaskCandidateViewDelegate>{
    
    UIScrollView * scrollView;
    CGFloat SelfH;
    CGFloat SelfW;
    
    UILabel * companyTitlteLb;
    
    CGFloat scrollViewOriginContentH;
}

@property (nonatomic , strong) TaskPositionButton * curSelectedPosition;
@property (nonatomic , strong) TaskCandidateView * personView;

@property (nonatomic , strong) NSDictionary * positionAndKey;
@end

@implementation TaskListCell

- (instancetype)initWithTBFrame:(CGRect)frame{
    self = [super init];
    if (self) {
        SelfH = frame.size.height;
        SelfW = frame.size.width;
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    self.backgroundColor = [UIColor redColor];
    UIImage * img = [UIImage imageNamed:@"companyBk"];
    CGFloat imgW = SelfW;
    CGFloat imgH = img.size.height*imgW/img.size.width;
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
    imgV.backgroundColor = [UIColor blueColor];
    imgV.frame = CGRectMake(0, 0,imgW ,imgH);
    [self addSubview:imgV];

    
    companyTitlteLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(imgV.frame), VerPxFit(40))];
    companyTitlteLb.backgroundColor = [UIColor clearColor];
    companyTitlteLb.font = [UIFont boldSystemFontOfSize:18];
    companyTitlteLb.textColor = [UIColor whiteColor];
    companyTitlteLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:companyTitlteLb];
    companyTitlteLb.centerY = imgV.centerY;
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(imgV.frame), SelfW, SelfH-CGRectGetHeight(imgV.frame))];
    scrollView.backgroundColor = [UIColor whiteColor];
    [self addSubview:scrollView];
}

- (void)setSubviewsWithSuperViewBounds:(CGRect)superViewBounds {
    
    if (CGRectEqualToRect(self.mainImageView.frame, superViewBounds)) {
        return;
    }
//    scrollView.frame = superViewBounds;
//    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height*4)];
//    self.mainImageView.frame = superViewBounds;
    self.coverView.frame = self.bounds;
}

- (void)setTaskModel:(TaskModel *)taskModel{
    
    companyTitlteLb.text = taskModel.company.name;
    [scrollView removeAllSubviews];
    NSMutableArray * array = [NSMutableArray arrayWithArray:taskModel.positionArray];
    
    NSArray * positionArray = array;
    NSMutableDictionary * tempMap = [NSMutableDictionary dictionary];
    CGFloat verPadding = VerPxFit(20);
    CGFloat horPadding = HorPxFit(15);
    CGFloat titileBtPadding = HorPxFit(15);
    CGFloat wHad = SelfW - 2*horPadding;
    CGFloat btH = VerPxFit(50);
    NSInteger rowNum = 0;
    UIButton * lastRowFirBt = nil;
    UIButton * curRowFirBt = nil;
    UIButton * sameRowPriBt = nil;
    for (int i =0; i<positionArray.count; i++) {
        TaskPositionButton * bt = [TaskPositionButton buttonWithType:UIButtonTypeCustom];
        PositionOrderModel * positionModel = positionArray[i];
        bt.positionModel = positionModel;
        NSString * jobString = positionModel.jobModel.title;
        CGSize size = [BaseHelper getSizeWithString:jobString font:[UIFont systemFontOfSize:15] contentWidth:MAXFLOAT contentHight:btH];
        //按钮将要显示的宽度
        CGFloat bt_w = size.width>=HorPxFit(80) ? size.width : HorPxFit(80);
        size = CGSizeMake(bt_w, size.height);
        
        CGFloat wNeed = size.width + horPadding*2 + titileBtPadding*2;
        if (wHad < wNeed && sameRowPriBt) {
            rowNum ++;
            lastRowFirBt = curRowFirBt;
            sameRowPriBt = nil;
            curRowFirBt = bt;
            wHad = SelfW;
            [tempMap setObject:bt forKey:[NSString stringWithFormat:@"%ld",rowNum]];
        }
        if (i == 0) {
            curRowFirBt = bt;
            [tempMap setObject:bt forKey:[NSString stringWithFormat:@"%ld",rowNum]];
        }
        [bt addTarget:self action:@selector(positionChoosed:) forControlEvents:UIControlEventTouchUpInside];
        bt.layer.cornerRadius = btH/2.0;
        
        UIImage *image = [UIImage imageNamed:@"taskPositionButtonNormal"];
        image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10) resizingMode:UIImageResizingModeStretch];
        [bt setBackgroundImage:image forState:UIControlStateNormal];
        bt.row = rowNum;
        [bt setTitle:jobString forState:UIControlStateNormal];
        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        bt.titleLabel.font = [UIFont systemFontOfSize:14];
        [scrollView addSubview:bt];
        
        [bt mas_makeConstraints:^(MASConstraintMaker *make) {
            if (sameRowPriBt) {
                make.left.mas_equalTo(sameRowPriBt.mas_right).mas_offset(horPadding);
            }else{
                make.left.equalTo(scrollView).mas_offset(horPadding);
            }
            if (lastRowFirBt) {
                if (sameRowPriBt) { //   说明不是第一个
                    make.top.equalTo(curRowFirBt);
                }else{
                    make.top.mas_equalTo(lastRowFirBt.mas_bottom).mas_offset(verPadding);
                }
            }else{
                make.top.equalTo(scrollView).mas_offset(verPadding);
            }
            make.size.mas_equalTo(CGSizeMake(size.width+titileBtPadding*2, btH));
        }];
        sameRowPriBt = bt;
        wHad -= (horPadding+size.width+titileBtPadding*2);
    }
    scrollViewOriginContentH = (rowNum+1) * (btH+verPadding) + verPadding;
    self.positionAndKey = tempMap;
    [scrollView setContentSize:CGSizeMake(CGRectGetWidth(scrollView.frame), scrollViewOriginContentH)];
}

- (void)positionChoosed:(TaskPositionButton *)sender{
    
    CGFloat personViewVerPadding = VerPxFit(30);
    if(_curSelectedPosition){
        _curSelectedPosition.backgroundColor = [UIColor clearColor];
        [_curSelectedPosition setBackgroundImage:[UIImage imageNamed:@"taskPositionButtonNormal"] forState:UIControlStateNormal];
        [_curSelectedPosition setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        NSInteger rowNum = _curSelectedPosition.row+1;
        
        TaskPositionButton * postionToMove = _positionAndKey[[NSString stringWithFormat:@"%ld",rowNum]];
        if (postionToMove) {
            TaskPositionButton * position = _positionAndKey[[NSString stringWithFormat:@"%ld",_curSelectedPosition.row]];
            [postionToMove mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(position.mas_bottom).mas_offset(HorPxFit(20));
            }];
            [scrollView updateConstraintsIfNeeded];
        }
        if ([self.personView superview]) {
            [self.personView removeFromSuperview];
        }
        
        if (sender == _curSelectedPosition) {
            self.curSelectedPosition = nil;
            return;
        }
    }
    self.curSelectedPosition = sender;
    [_curSelectedPosition setBackgroundImage:[UIImage imageNamed:@"taskPositionButton_hight"] forState:UIControlStateNormal];
    [_curSelectedPosition setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    NSInteger rowNum = sender.row+1;
    TaskPositionButton * postionToMove = _positionAndKey[[NSString stringWithFormat:@"%ld",rowNum]];
    TaskPositionButton * position = _positionAndKey[[NSString stringWithFormat:@"%ld",sender.row]];
   
    if (!sender.positionModel.candidateArray || sender.positionModel.candidateArray.count == 0) {
        [_curSelectedPosition setBackgroundImage:[UIImage imageNamed:@"taskPositionButtonNormal"] forState:UIControlStateNormal];
        [_curSelectedPosition setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [BaseHelper showProgressHud:@"暂无候选人" showLoading:NO canHide:YES];
        return;
    }
    
    self.personView = [[TaskCandidateView alloc] init];
    self.personView.selfW = SelfW;
    self.personView.delegate = self;
    CGFloat personViewH = [_personView initSuvViews:sender.positionModel.candidateArray withArrowX:CGRectGetMidX(sender.frame)];
    [scrollView insertSubview:self.personView atIndex:0 ];
    [self.personView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(position.mas_bottom).mas_offset(personViewVerPadding);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(SelfW);
        make.height.mas_equalTo(personViewH);
    }];
    if (postionToMove) {
        [postionToMove mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(position.mas_bottom).mas_offset(personViewH+personViewVerPadding*2);
        }];
    }
    [scrollView updateConstraintsIfNeeded];
    [scrollView setContentSize:CGSizeMake(CGRectGetWidth(scrollView.frame), scrollViewOriginContentH+personViewH+personViewVerPadding*2)];
}

#pragma mark -- TaskCandidateViewDelegate

- (void)candidateSelected:(CandidateModel *)model{
    [self.delegate candidateChoosed:model];
}

@end
