//
//  TalentListCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentListCell.h"
#import "UIImageView+YYWebImage.h"

@interface TalentListCell(){
    
    UIImageView * avatarImgV;
    UILabel * nameLb;
    UILabel * baseInfoLb;
    UILabel * historyInfoLb;
    
    UIButton * phoneBt;
    UIButton * payBt;
}

@end

@implementation TalentListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createSbuViews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)talentPayFinished{
    payBt.hidden = YES;
    [self setModel:_model];
}

- (void)createSbuViews{
    CGFloat horpadding = HorPxFit(20);
    CGFloat verpadding = HorPxFit(20);
    CGFloat avatarW = HorPxFit(90);
    @weakify(self)
    avatarImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatarPlaceaholder.png"]];
    avatarImgV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:avatarImgV];
    [avatarImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.left.and.top.equalTo(self).mas_offset(horpadding);
        make.size.mas_equalTo(CGSizeMake(avatarW, avatarW));
    }];
    
    phoneBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [phoneBt addTarget:self action:@selector(phoneCallAction) forControlEvents:UIControlEventTouchUpInside];
    [phoneBt setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
    [self addSubview:phoneBt];
    [phoneBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.centerY.equalTo(self);
        make.right.equalTo(self).mas_offset(-horpadding);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), VerPxFit(100)));
    }];
    
    payBt = [UIButton buttonWithType:UIButtonTypeCustom];
    payBt.backgroundColor = [UIColor orangeColor];
    [payBt setTitle:@"支付" forState:UIControlStateNormal];
    payBt.layer.cornerRadius = 4;
    [payBt addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    payBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:payBt];
    [payBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.right.mas_equalTo(-horpadding);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(120), VerPxFit(73)));
    }];
    
    CGFloat lbH = VerPxFit(40);
    nameLb = [self lb];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.left.equalTo(avatarImgV.mas_right).mas_offset(horpadding);
        make.top.equalTo(self).mas_offset(verpadding);
        make.right.equalTo(phoneBt.mas_left).mas_offset(horpadding);
        make.height.mas_equalTo(lbH);
    }];
    
    baseInfoLb = [self lb];
    [baseInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        if (!self) {
            return ;
        }
        make.left.equalTo(avatarImgV.mas_right).mas_offset(horpadding);
        make.top.equalTo(nameLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_offset(horpadding);
        make.height.mas_equalTo(lbH);
    }];
    
    historyInfoLb = [self lb];
    [historyInfoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(avatarImgV.mas_right).mas_offset(horpadding);
        make.top.equalTo(baseInfoLb.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self).mas_offset(-horpadding);
        make.height.mas_equalTo(lbH);
    }];
}

- (void)phoneCallAction{
    [self.delegate phoneCall:self.model];
}

- (void)payAction{
    [self.delegate payActionWithCell:self];
}

- (void)setModel:(TalentModel *)model{
    _model = model;
    [avatarImgV  setImageWithURL:[NSURL URLWithString:model.avatar] placeholder:[UIImage imageNamed:@"avatarPlaceaholder.png"]];
    nameLb.text = model.name;
//    baseInfoLb.text = [NSString stringWithFormat:@"%@  %@  %@  %@",(model.==1?@"男":@"女"),model.education,model.workExperience,model.city];
    historyInfoLb.text = [NSString stringWithFormat:@"%@/%@",model.company,model.position];
    if (model.payStatus) {
        payBt.hidden = YES;
        phoneBt.hidden = NO;
        [self bringSubviewToFront:phoneBt];
    }else{
        payBt.hidden = NO;
        phoneBt.hidden = YES;
        [self bringSubviewToFront:payBt];
    }
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.font = [UIFont systemFontOfSize:15];
    [self addSubview:lb];
    return lb;
}

+ (CGFloat)cellHeight{
    CGFloat cellH = 0;
    CGFloat lbH = VerPxFit(40);
    cellH += (lbH * 3);
    CGFloat verpadding = HorPxFit(20);
    cellH += (4*verpadding);
    return cellH;
}
@end





