//
//  PositionCollectionCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"

#define CellBackColor COLOR(240, 240, 240, 1)
#define CellHighBackColor  COLOR(250, 250, 250, 1)   //  待更改

@protocol PositionCollectionCellDelegate
- (void)hrInfoShow:(JobModel *)model;
@end

@interface PositionCollectionCell : UICollectionViewCell

@property (nonatomic , strong) JobModel * jobModel;     //  职位信息
@property (nonatomic ,assign) BOOL isCreatedSubs;       //  是否已初始化view组件
@property (nonatomic , weak) id<PositionCollectionCellDelegate>delegate;  

- (void)createSubViews;

@end
