//
//  ChatView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/1/29.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ChatView.h"
#import "UIView+YYAdd.h"

@interface ChatView(){
    
    
}
@property (nonatomic , strong) NSArray * yValueArray;
@property (nonatomic , strong) NSArray * timeValueArray;

@end

@implementation ChatView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray{
    
//    dataArray = @[@{@"recommendDay":@"1518766608",@"recommendCount":@"2"},
//                       @{@"recommendDay":@"1518853008",@"recommendCount":@"8"},
//                       @{@"recommendDay":@"1518939408",@"recommendCount":@"4"},
//                       @{@"recommendDay":@"1519025808",@"recommendCount":@"7"},
//                       @{@"recommendDay":@"1519112208",@"recommendCount":@"1"},
//                       @{@"recommendDay":@"1519198608",@"recommendCount":@"5"},
//                       @{@"recommendDay":@"1519285008",@"recommendCount":@"0"}];
    
    NSMutableArray * yValueArrayTemp = [NSMutableArray array];
    NSMutableArray * timeValueArrayTemp = [NSMutableArray array];
    for (int i=0; i<dataArray.count; i++) {
        NSDictionary * dic = dataArray[i];
        [yValueArrayTemp addObject: [NSNumber numberWithInt:[dic[@"recommendCount"] intValue]]];
        
        NSTimeInterval  timeInterval = [dic[@"recommendDay"] longLongValue];
        
        NSString * weekStr = [BaseHelper weekdayStringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
        [timeValueArrayTemp addObject:weekStr];
    }
    
    self.yValueArray  = yValueArrayTemp;
    self.timeValueArray = timeValueArrayTemp;
    
    _dataArray = dataArray;
    [self removeAllSubviews];
    [self createSubViews];
}

- (void)createSubViews{

    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatTop"]];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.and.left.and.right.equalTo(self);
    }];
//    self.yValueArray = @[@"2",@"8",@"4",@"7",@"1",@"5",@"0"];
//    NSArray * yValueArray = @[@"0",@"0",@"1",@"0",@"0",@"0",@"0"];
    NSMutableArray * _pointsArray = [NSMutableArray array];
    CGFloat verNum = 0;
    for (NSString * valueStr in _yValueArray) {
        CGFloat value = [valueStr integerValue];
        if (value > verNum) {
            verNum = value;
        }
    }
    verNum += 2;
    CGFloat selfW = CGRectGetWidth(self.frame);
    CGFloat selfH = 150;
    
    CGFloat horPadding = HorPxFit(70);
    CGFloat verPadding = VerPxFit(85);
    
//    CGFloat curX = selfW - horPadding;
    CGFloat itemW = (selfW - horPadding*2)/6.5;
    CGFloat itemH = selfH / verNum;
    
//    self.timeValueArray = @[@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN"];
    [_timeValueArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString * string = (NSString *)obj;
        UILabel * lb = [[UILabel alloc] init];
        lb.text = string;
        lb.textColor = [UIColor whiteColor];
        lb.font = [UIFont systemFontOfSize:11];
//        if (idx != 0) {
//            return ;
//        }
        [self addSubview:lb];
        [lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(itemW * idx + horPadding);
            make.bottom.equalTo(self).mas_offset(-VerPxFit(20));
            make.size.mas_equalTo(CGSizeMake(itemW, 30));
        }];
    }];
    
    for (int i=0; i<verNum; i++) {
        UILabel * lb = [[UILabel alloc] init];
        lb.text = [NSString stringWithFormat:@"%d",i];
        lb.textColor = [UIColor whiteColor];
//        lb.backgroundColor = [UIColor redColor];
        lb.font = [UIFont systemFontOfSize:12];
        if (verNum > 7) {
            if (i %2 != 0) {
                continue;
            }
        }
        [self addSubview:lb];
        [lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).mas_offset(horPadding-HorPxFit(30));
            make.bottom.equalTo(self).mas_offset( -(verPadding + i*itemH));
            make.size.mas_equalTo(CGSizeMake(12, 12));
        }];
    }
    
    [_yValueArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSInteger objInter = 1;
        if ([obj respondsToSelector:@selector(integerValue)]) {
            objInter = [obj integerValue];
        }
        CGPoint point = CGPointMake(itemW * idx + horPadding+15, CGRectGetHeight(self.frame) - verPadding - objInter * itemH);
        NSValue *value = [NSValue valueWithCGPoint:CGPointMake(point.x, point.y)];
        [_pointsArray addObject:value];
    }];
    
    NSValue *firstPointValue = [NSValue valueWithCGPoint:CGPointMake(itemW, (CGRectGetHeight(self.frame) - horPadding) / 2)];
    [_pointsArray insertObject:firstPointValue atIndex:0];
    NSValue *endPointValue = [NSValue valueWithCGPoint:CGPointMake(CGRectGetWidth(self.frame), (CGRectGetHeight(self.frame) - itemH) / 2)];
    [_pointsArray addObject:endPointValue];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    for (NSInteger i = 0; i < 6; i++) {
        CGPoint p1 = [[_pointsArray objectAtIndex:i] CGPointValue];
        CGPoint p2 = [[_pointsArray objectAtIndex:i+1] CGPointValue];
        CGPoint p3 = [[_pointsArray objectAtIndex:i+2] CGPointValue];
        CGPoint p4 = [[_pointsArray objectAtIndex:i+3] CGPointValue];
        if (i == 0) {
            [path moveToPoint:p2];
        }
        [self getControlPointx0:p1.x andy0:p1.y x1:p2.x andy1:p2.y x2:p3.x andy2:p3.y x3:p4.x andy3:p4.y path:path];
    }
    CAShapeLayer * _bezierLineLayer = [CAShapeLayer layer];
    _bezierLineLayer.path = path.CGPath;
    _bezierLineLayer.strokeColor = [UIColor redColor].CGColor;
    _bezierLineLayer.fillColor = [[UIColor clearColor] CGColor];
    _bezierLineLayer.shadowColor = COLOR(220, 220, 220, 1).CGColor;
    _bezierLineLayer.shadowOffset = CGSizeMake(1, 2);
    // 默认设置路径宽度为0，使其在起始状态下不显示
    _bezierLineLayer.lineWidth = 3;
    _bezierLineLayer.lineCap = kCALineCapRound;
    _bezierLineLayer.lineJoin = kCALineJoinRound;
    
//    CGContextRef gc = UIGraphicsGetCurrentContext();
//    [self drawLinearGradient:gc path:path.CGPath startColor:[UIColor greenColor].CGColor endColor:[UIColor redColor].CGColor];
//    CGPathRelease(path);

    [self.layer addSublayer:_bezierLineLayer];
}

- (void)getControlPointx0:(CGFloat)x0 andy0:(CGFloat)y0
                       x1:(CGFloat)x1 andy1:(CGFloat)y1
                       x2:(CGFloat)x2 andy2:(CGFloat)y2
                       x3:(CGFloat)x3 andy3:(CGFloat)y3
                     path:(UIBezierPath*) path{
    CGFloat smooth_value =0.6;
    CGFloat ctrl1_x;
    CGFloat ctrl1_y;
    CGFloat ctrl2_x;
    CGFloat ctrl2_y;
    CGFloat xc1 = (x0 + x1) /2.0;
    CGFloat yc1 = (y0 + y1) /2.0;
    CGFloat xc2 = (x1 + x2) /2.0;
    CGFloat yc2 = (y1 + y2) /2.0;
    CGFloat xc3 = (x2 + x3) /2.0;
    CGFloat yc3 = (y2 + y3) /2.0;
    CGFloat len1 = sqrt((x1-x0) * (x1-x0) + (y1-y0) * (y1-y0));
    CGFloat len2 = sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
    CGFloat len3 = sqrt((x3-x2) * (x3-x2) + (y3-y2) * (y3-y2));
    CGFloat k1 = len1 / (len1 + len2);
    CGFloat k2 = len2 / (len2 + len3);
    CGFloat xm1 = xc1 + (xc2 - xc1) * k1;
    CGFloat ym1 = yc1 + (yc2 - yc1) * k1;
    CGFloat xm2 = xc2 + (xc3 - xc2) * k2;
    CGFloat ym2 = yc2 + (yc3 - yc2) * k2;
    ctrl1_x = xm1 + (xc2 - xm1) * smooth_value + x1 - xm1;
    ctrl1_y = ym1 + (yc2 - ym1) * smooth_value + y1 - ym1;
    ctrl2_x = xm2 + (xc2 - xm2) * smooth_value + x2 - xm2;
    ctrl2_y = ym2 + (yc2 - ym2) * smooth_value + y2 - ym2;
    [path addCurveToPoint:CGPointMake(x2, y2) controlPoint1:CGPointMake(ctrl1_x, ctrl1_y) controlPoint2:CGPointMake(ctrl2_x, ctrl2_y)];
}

- (void)drawLinearGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    CGRect pathRect = CGPathGetBoundingBox(path);
    
    //具体方向可根据需求修改
    CGPoint startPoint = CGPointMake(CGRectGetMinX(pathRect), CGRectGetMidY(pathRect));
    CGPoint endPoint = CGPointMake(CGRectGetMaxX(pathRect), CGRectGetMidY(pathRect));
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

@end
