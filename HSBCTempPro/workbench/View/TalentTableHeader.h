//
//  TalentTableHeader.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TalentTableHeaderDelegate
- (void)searchKey:(NSString *)key;
@end

@interface TalentTableHeader : UIView

@property (nonatomic , weak) id<TalentTableHeaderDelegate>delegate;


@end
