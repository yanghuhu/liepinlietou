//
//  CandidateStateView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/9.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "CandidateStateView.h"
#import "UIView+YYAdd.h"
#import "QRCodeCreateManage.h"
#import "RateView.h"
#import "JobOrderDetailModel.h"

#define ActionBtFont  [UIFont systemFontOfSize:14]
#define TextFont [UIFont systemFontOfSize:14]
#define  TitleFont  [UIFont systemFontOfSize:15]

@interface CandidateStateView()<RateViewDelegate>{

    UIButton * recommendBt;
    UIButton * interviewBt;
    UIButton * entryBt;
    
    UIScrollView * contentSV;
    UIImageView * bkImgV;
    UIButton * dateChooseBt;
    UITextField * telField;
    UITextField * addressField;
    
}
@property (nonatomic , strong)  RateView * rateView;

@property (nonatomic , strong) UIButton * rateBt;
@property (nonatomic , assign) UIButton * curSelectBt;

@end

@implementation CandidateStateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    CGFloat horPadding = HorPxFit(130);
    CGFloat itemW = (ScreenWidth - 2*horPadding) / 2;
    
    CGFloat ballW = HorPxFit(180);
    recommendBt = [self bt:[UIImage imageNamed:@"recommendBk"] highImg:[UIImage imageNamed:@"recommendBkHight"]  tag:100 ];
    [recommendBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(ballW, ballW));
        make.top.mas_equalTo(VerPxFit(0));
        make.left.mas_equalTo(horPadding - ballW/2.0);
    }];
    
    UILabel * commendLb = [self lb:@"推荐"];
    [commendLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.centerX.mas_equalTo(recommendBt.mas_centerX);
        make.top.mas_equalTo(recommendBt.mas_bottom).mas_offset(-HorPxFit(40));
    }];
    
    interviewBt = [self bt:[UIImage imageNamed:@"interviewBk"]  highImg:[UIImage imageNamed:@"interviewBkHight"] tag:101 ];
    [interviewBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(ballW, ballW));
        make.top.equalTo(recommendBt);
        make.left.mas_equalTo(horPadding+itemW-ballW/2.0);
    }];
    
    UILabel * interViewLb = [self lb:@"面试"];
    [interViewLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.centerX.mas_equalTo(interviewBt.mas_centerX);
        make.top.mas_equalTo(interviewBt.mas_bottom).mas_offset(-HorPxFit(40));
    }];
   
    entryBt = [self bt:[UIImage imageNamed:@"entryBk"] highImg:[UIImage imageNamed:@"entryBkHight"] tag:102 ];
    [entryBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(ballW, ballW));
        make.top.equalTo(recommendBt);
        make.left.mas_equalTo(horPadding+itemW*2-ballW/2.0);
    }];
    
    UILabel * entryLb = [self lb:@"入职"];
    [entryLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(70);
        make.centerX.mas_equalTo(entryBt.mas_centerX);
        make.top.mas_equalTo(entryBt.mas_bottom).mas_offset(-HorPxFit(40));
    }];
    
    UIImageView * lineFir = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stateLine_"]];
    [self insertSubview:lineFir atIndex:0];
    [lineFir mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(horPadding);
        make.width.mas_equalTo(itemW);
        make.centerY.mas_equalTo(recommendBt.mas_centerY);
        make.height.mas_equalTo(VerPxFit(6));
    }];
    
    UIImageView * lineSec = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stateLine"]];
    [self insertSubview:lineSec atIndex:0];
    [lineSec mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(horPadding+itemW);
        make.width.mas_equalTo(itemW);
        make.centerY.mas_equalTo(recommendBt.mas_centerY);
        make.height.mas_equalTo(VerPxFit(6));
    }];
    
    UIImage * img = [UIImage imageNamed:@"stateRecommend"];
    bkImgV = [[UIImageView alloc] initWithImage:[img stretchableImageWithLeftCapWidth:40 topCapHeight:100]];
    [self addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(horPadding-ballW/2.0);
        make.right.mas_equalTo(-horPadding+ballW/2.0);
        make.top.mas_equalTo(recommendBt.mas_bottom).mas_offset(-VerPxFit(20));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(45));
    }];
    
    contentSV = [[UIScrollView alloc] init];
    [self addSubview:contentSV];
    [contentSV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bkImgV).mas_offset(HorPxFit(48));
        make.top.equalTo(bkImgV).mas_offset(VerPxFit(53));
        make.right.equalTo(bkImgV).mas_offset(-HorPxFit(58));
        make.bottom.equalTo(bkImgV).mas_offset(-VerPxFit(30));
    }];
}

- (UIButton *)bt:(UIImage *)bkImage highImg:(UIImage *)highImg tag:(NSInteger)tag{
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(stateChoosed:) forControlEvents:UIControlEventTouchUpInside];
    bt.tag = tag;
    [bt setBackgroundImage:bkImage forState:UIControlStateNormal];
    [bt setBackgroundImage:highImg forState:UIControlStateHighlighted];
    [self addSubview:bt];
    return bt;
}

- (UILabel *)lb:(NSString *)title{
    UILabel * lb = [[UILabel alloc] init];
    lb.backgroundColor = [UIColor clearColor];
    lb.font = [UIFont systemFontOfSize:15];
    lb.text = title;
    lb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lb];
    return lb;
}

- (UIButton *)rateBt{
    if (!_rateBt) {
        _rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rateBt addTarget:self action:@selector(rateAction) forControlEvents:UIControlEventTouchUpInside];
        _rateBt.titleLabel.font = ActionBtFont;
        [_rateBt setTitle:@"评价" forState:UIControlStateNormal];
        [_rateBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rateBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    }
    return _rateBt;
}


- (void)setModel:(JobOrderDetailModel *)model{
    _model = model;
//
//    if (model.orderStep == OrderStepRecommend) {
//        interviewBt.enabled = NO;
//        entryBt.enabled = NO;
//        [interviewBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
//        [entryBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
//    }else if(model.orderStep == OrderStepInterView){
//        entryBt.enabled = NO;
//        interviewBt.enabled = YES;
//        [entryBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
//    }
//    JobOrderDetailModel * order = [HSBCGlobalInstance sharedHSBCGlobalInstance].orderModel;
    
    JobOrderDetailModel * order = model;
    order.talentModel = model.talentModel;
    order.offer = model.offer;
    if (order.orderStep == OrderStepRecommend) {
        interviewBt.enabled = NO;
        entryBt.enabled = NO;
        [interviewBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
        [entryBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
        
        UIImage * img = [UIImage imageNamed:@"stateRecommend"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
    }else if(order.orderStep == OrderStepInterView){
        entryBt.enabled = NO;
        interviewBt.enabled = YES;
        [entryBt setBackgroundImage:[UIImage imageNamed:@"stateUn"] forState:UIControlStateNormal];
        UIImage * img = [UIImage imageNamed:@"stateInterview"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
    }else{
        UIImage * img = [UIImage imageNamed:@"statePosition"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
    }
    
    [contentSV removeAllSubviews];
//    _model = order;
    if ([_model.state isEqualToString:PositionCandidateOrderStateRecommended]) {  // 已推荐
        [self recommendView];
    }else if([_model.state isEqualToString:PositionCandidateOrderStateAccepted]){
        [self hrAcceptView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateRejected]){
        [self hrResumeDealResult];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]){
        [self interViewInfoView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
        [self interViewInfoView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]){
        [self interViewFinishView:PositionCandidateOrderStateInterview_Failed];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWaint_Offer]){
        [self interViewFinishView:PositionCandidateOrderStateWaint_Offer];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Register]){
        [self waitRegisterView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register]){
        [self waitRegisterView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Positive]){
        [self waitPositiveView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStatePositive]){
        [self positionView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Position]){
        [self notPositiveView];
    }
}

- (void)stateChoosed:(UIButton *)bt{
    if (bt == _curSelectBt) {
        return;
    }
    OrderStep step;
    switch (bt.tag) {
        case 100:{
            step = OrderStepRecommend;
            break;
        }
        case 101:{
            step = OrderStepInterView;
            break;
        }
        case 102:{
            step = OrderStepPositive;
            break;
        }
        default:{
            step = OrderStepRecommend;
            break;
        }
    }
    [self initContentInfoWithStep:step];
}

- (void)initContentInfoWithStep:(OrderStep)orderStep{
    [contentSV removeAllSubviews];
    if (OrderStepPositive == orderStep) {
        UIImage * img = [UIImage imageNamed:@"statePosition"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
        [self setModel:_model];
    }else if(orderStep == OrderStepRecommend){
        UIImage * img = [UIImage imageNamed:@"stateRecommend"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
        [self recommendBackView];
    }else if (orderStep == OrderStepInterView){
        UIImage * img = [UIImage imageNamed:@"stateInterview"];
        bkImgV.image =[img stretchableImageWithLeftCapWidth:40 topCapHeight:100];
        
        if([_model.state isEqualToString:PositionCandidateOrderStateAccepted]){
            [self hrAcceptView];
        }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]) {
            [self interViewInfoView];
        }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
            [self interViewInfoView];
        }else if([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]){
            [self interViewFinishView:PositionCandidateOrderStateInterview_Failed];
        }else if ([_model.state isEqualToString:PositionCandidateOrderStateWaint_Offer]){
            [self interViewFinishView:PositionCandidateOrderStateWaint_Offer];
        }else{
            [self interViewFinishView:PositionCandidateOrderStateWaint_Offer];
        }
//        [self interViewBackView];
    }
}

- (UIView *)recommendView{
    UILabel * lb = [[UILabel alloc] init];
    lb.text = @"已推荐,请耐心等待HR反馈";
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = TextFont;
    [contentSV addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contentSV.mas_centerX);
        make.centerY.mas_equalTo(contentSV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(contentSV.frame), VerPxFit(70)));
    }];
    return lb;
}

- (UIView *)hrAcceptView{
    CGFloat verPadding = VerPxFit(20);
    CGFloat horPadding = HorPxFit(70);
    CGFloat itemH = VerPxFit(60);
    CGFloat needH = 0;
    
    UILabel * dateLb = [[UILabel alloc] init];
    dateLb.font = TitleFont;
    dateLb.text = @"选择约面时间";
    [contentSV addSubview:dateLb];
    [dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentSV).mas_offset(verPadding);
        make.left.equalTo(contentSV).mas_offset(HorPxFit(20));
        make.height.mas_equalTo(25);
        make.right.equalTo(contentSV).mas_offset(-horPadding);
    }];
    
    needH += (verPadding +itemH);
    
    
    dateChooseBt = [UIButton buttonWithType:UIButtonTypeCustom];
    dateChooseBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    dateChooseBt.layer.borderWidth = 1;
//    dateChooseBt.backgroundColor = [UIColor redColor];
    [dateChooseBt setTitle:@"选择日期" forState:UIControlStateNormal];
    dateChooseBt.titleLabel.font  = ActionBtFont;
    [dateChooseBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [dateChooseBt addTarget:self action:@selector(chooseInterViewTime) forControlEvents:UIControlEventTouchUpInside];
    [contentSV addSubview:dateChooseBt];
    [dateChooseBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(itemH);
        make.centerX.mas_equalTo(contentSV.mas_centerX);
        make.left.equalTo(contentSV).mas_offset(horPadding);
        make.right.equalTo(contentSV).mas_offset(-horPadding);
        make.top.mas_equalTo(dateLb.mas_bottom).mas_offset(verPadding*2);
    }];
    needH += (itemH +verPadding*2);
    
    
    telField = [UITextField new];
    telField.text = _model.orderModel.jobModel.hrModel.company.contactNumber;
    telField.font = [UIFont systemFontOfSize:15];
    [contentSV addSubview:telField];
    [telField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dateChooseBt).offset(HorPxFit(20));
        make.right.mas_equalTo(dateChooseBt);
        make.top.mas_equalTo(dateChooseBt.mas_bottom).offset(VerPxFit(15));
        make.height.equalTo(@(VerPxFit(50)));
    }];
    CALayer *fieldLayer = [CALayer new];
    fieldLayer.frame = CGRectMake(0, VerPxFit(50),HorPxFit(400) , 0.5);
    fieldLayer.borderColor = [UIColor lightGrayColor].CGColor;
    fieldLayer.borderWidth = 0.5;
    [telField.layer addSublayer:fieldLayer];
    
    
    UILabel *telLabel = [UILabel new];
    telLabel.text = @"电话:";
    telLabel.font = [UIFont systemFontOfSize:15];
    telLabel.textColor = [UIColor blackColor];
    [telLabel sizeToFit];
    [contentSV addSubview:telLabel];
    [telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(dateChooseBt.mas_left).offset(5);
        make.top.bottom.mas_equalTo(telField);
    }];
    
    needH += VerPxFit(15) + HorPxFit(50);
    
    addressField  = [UITextField new];
    addressField.text = _model.orderModel.jobModel.hrModel.company.address;
    addressField.font = [UIFont systemFontOfSize:15];
    [contentSV addSubview:addressField];
    [addressField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dateChooseBt).offset(HorPxFit(20));
        make.right.mas_equalTo(dateChooseBt);
        make.top.mas_equalTo(telField.mas_bottom).offset(VerPxFit(14));
        make.height.equalTo(@(VerPxFit(50)));
    }];
    CALayer *addressLayer = [CALayer new];
    addressLayer.frame = CGRectMake(0, VerPxFit(50),HorPxFit(400) , 0.5);
    addressLayer.borderColor = [UIColor lightGrayColor].CGColor;
    addressLayer.borderWidth = 0.5;
    [addressField.layer addSublayer:addressLayer];
    
    UILabel *addressLabel = [UILabel new];
    addressLabel.text = @"地址:";
    addressLabel.font = [UIFont systemFontOfSize:15];
    addressLabel.textColor = [UIColor blackColor];
    [addressLabel sizeToFit];
    [contentSV addSubview:addressLabel];
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(dateChooseBt.mas_left).offset(5);
        make.top.bottom.mas_equalTo(addressField);
    }];

    needH += VerPxFit(15) + HorPxFit(50);
    
    UIButton * submitBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBt setTitle:@"确认" forState:UIControlStateNormal];
    submitBt.titleLabel.font  =ActionBtFont;
    [submitBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBt addTarget:self action:@selector(timeSureAction) forControlEvents:UIControlEventTouchUpInside];
    [submitBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [contentSV addSubview:submitBt];
    [submitBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentSV).mas_offset(horPadding);
        make.top.mas_equalTo(addressField.mas_bottom).mas_offset(verPadding*2);
        make.right.equalTo(contentSV).mas_offset(-horPadding);
//        make.width.mas_equalTo(VerPxFit(350));
        make.height.mas_equalTo(VerPxFit(65));
    }];
    contentSV.backgroundColor = [UIColor clearColor];
    needH += (verPadding*2 +VerPxFit(60));
//    view.height = needH;
    
    if (![_model.state isEqualToString:PositionCandidateOrderStateAccepted]) {
        telField.userInteractionEnabled = NO;
        addressField.userInteractionEnabled = NO;
    }
    
    contentSV.contentSize = CGSizeMake(CGRectGetWidth(contentSV.frame), needH+VerPxFit(20));
    return contentSV;
}

- (UIView *)hrResumeDealResult{
    
    CGFloat titleHorPadding = HorPxFit(35);
    CGFloat contentHorPadding = HorPxFit(70);
    CGFloat verPadding = VerPxFit(20);
    CGFloat lbH = VerPxFit(40);
    
    UIView * view = [[UIView alloc] init];
    [contentSV addSubview:view];
    
    UILabel * resultTitleLb = [[UILabel alloc] init];
    resultTitleLb.text = @"简历筛选结果:";
    resultTitleLb.font = TitleFont;
    [view addSubview:resultTitleLb];
    [resultTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(titleHorPadding);
        make.top.equalTo(view).mas_offset(verPadding);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * resultLb = [[UILabel alloc] init];
    resultLb.font = TextFont;
    if([_model.state isEqualToString:PositionCandidateOrderStateRejected]){
        resultLb.text =@"简历未通过";
    }else{
        resultLb.text =@"简历通过";
    }
    
    [view addSubview:resultLb];
    [resultLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(contentHorPadding);
        make.top.mas_equalTo(resultTitleLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(view).mas_offset(-contentHorPadding);
        make.height.mas_equalTo(lbH);
    }];
    UILabel * desTitleLb = [[UILabel alloc] init];

    desTitleLb.text = @"结果原因描述:";
    desTitleLb.font = TitleFont;
    [view addSubview:desTitleLb];
    [desTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(titleHorPadding);
        make.top.mas_equalTo(resultLb.mas_bottom).mas_offset(verPadding);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(lbH);
    }];

    UILabel * desLb = [[UILabel alloc] init];
    CGFloat desLbH = lbH;
    NSString * text;
#ifdef ViewDebug
    text = @"没能通过，很遗憾。年龄太小";
#else
    text = _model.rejectReason;
#endif
    
    CGSize size = [BaseHelper getSizeWithString:text font:TextFont contentWidth:(CGRectGetWidth(contentSV.frame)-2*contentHorPadding) contentHight:MAXFLOAT];
    if (size.height > desLbH) {
        desLbH = size.height;
    }
    desLb.text = text;
    desLb.font = TextFont;
    [view addSubview:desLb];
    [desLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(contentHorPadding);
        make.top.mas_equalTo(desTitleLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(view).mas_offset(-contentHorPadding);
        make.height.mas_equalTo(desLbH);
    }];
    
    view.frame = CGRectMake(0, 0, CGRectGetWidth(contentSV.frame), lbH*3+desLbH+5*verPadding);

    if([_model.state isEqualToString:PositionCandidateOrderStateRejected] &&  !_model.hhEvaluateFlag){
        [self addSubview:self.rateBt];
        [self.rateBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.top.mas_equalTo(desLb.mas_bottom).mas_offset(VerPxFit(15));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(350), VerPxFit(65)));
        }];
        view.frame = CGRectMake(0, 0, CGRectGetWidth(contentSV.frame), lbH*3+desLbH+5*verPadding+VerPxFit(65)+VerPxFit(15));
    }
    return view;
}



- (UIView *)interViewInfoView{
    
    CGFloat verPadding = VerPxFit(30);
    CGFloat horPadding = HorPxFit(40);
    CGFloat lbH = VerPxFit(40);

    UIView * view = [[UIView alloc] init];
    [contentSV addSubview:view];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.font = TitleFont;
    if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]) {
        titleLb.text = @"预约面试时间:";
    }else if([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
        titleLb.text = @"面试时间:";
    }
    [view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(horPadding);
        make.top.equalTo(view).mas_offset(verPadding);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * timeLb  = [[UILabel alloc] init];
    timeLb.font = TextFont;
#ifdef ViewDebug
    timeLb.text = @"2017-10-11 15:00";
#else
    if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]) {
        timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.appointmentTime format:kDateFormatTypeYYYYMMDDHHMM];
    }else if([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
        timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.interviewTime format:kDateFormatTypeYYYYMMDDHHMM];
    }
#endif

    [view addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.mas_equalTo(view.mas_centerX);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verPadding);
        make.height.mas_equalTo(lbH);
        make.left.equalTo(view).mas_offset(horPadding*2);
        make.width.mas_equalTo(200);
    }];
    return view;
}


- (UIView *)interViewFinishView:(NSString *)state{
    CGFloat titleHorPadding = HorPxFit(35);
    CGFloat contentHorPadding = HorPxFit(70);
    CGFloat verPadding = VerPxFit(20);
    CGFloat lbH = VerPxFit(40);
    
    UIView * view = [[UIView alloc] init];
    [contentSV addSubview:view];
    
    UILabel * resultTitleLb = [[UILabel alloc] init];
    resultTitleLb.text = @"面试结果:";
    resultTitleLb.font = TitleFont;
    [view addSubview:resultTitleLb];
    [resultTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(titleHorPadding);
        make.top.equalTo(view).mas_offset(verPadding);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * resultLb = [[UILabel alloc] init];
    resultLb.font = TextFont;
    if ([state isEqualToString: PositionCandidateOrderStateInterview_Failed]) {
        resultLb.text =@"面试未通过";
    }else{
        resultLb.text =@"面试通过";
    }
    
    [view addSubview:resultLb];
    [resultLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(contentHorPadding);
        make.top.mas_equalTo(resultTitleLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(view).mas_offset(-contentHorPadding);
        make.height.mas_equalTo(lbH);
    }];
    UILabel * desTitleLb = [[UILabel alloc] init];
    desTitleLb.font = TitleFont;
    desTitleLb.text = @"结果描述:";
    [view addSubview:desTitleLb];
    [desTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(titleHorPadding);
        make.top.mas_equalTo(resultLb.mas_bottom).mas_offset(verPadding);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(lbH);
    }];
    UILabel * desLb = [[UILabel alloc] init];
    desLb.font = TextFont;
    desLb.numberOfLines = 0;
    CGFloat desLbH = lbH;
    NSString * text;
#ifdef ViewDebug
    text = @"没能通过，很遗憾。技术未达标";
#else
    text =  _model.interviewResultDesc;
#endif
    CGSize size = [BaseHelper getSizeWithString:text font:TextFont contentWidth:(CGRectGetWidth(contentSV.frame)-2*contentHorPadding) contentHight:MAXFLOAT];
    if (size.height > desLbH) {
        desLbH = size.height;
    }
    desLb.text = text;
    desLb.font = TextFont;
    [view addSubview:desLb];
    [desLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(contentHorPadding);
        make.top.mas_equalTo(desTitleLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(view).mas_offset(-contentHorPadding);
        make.height.mas_equalTo(desLbH);
    }];
    
    view.frame = CGRectMake(0, 0, CGRectGetWidth(contentSV.frame), lbH*3+desLbH+5*verPadding);
    
    if ([state isEqualToString: PositionCandidateOrderStateInterview_Failed] && !_model.hhEvaluateFlag) {
        [view addSubview:self.rateBt];
        [self.rateBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(view.mas_centerX);
            make.top.mas_equalTo(desLb.mas_bottom).mas_offset(VerPxFit(15));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(350), VerPxFit(65)));
        }];
        
        view.frame = CGRectMake(0, 0, CGRectGetWidth(contentSV.frame), lbH*3+desLbH+5*verPadding+VerPxFit(65)+VerPxFit(15));
    }
    return view;
}


- (UIView *)waitRegisterView{

    CGFloat verPadding = VerPxFit(40);
    CGFloat horPadding = HorPxFit(40);
    CGFloat lbH = VerPxFit(50);
    
    UIView * view = [[UIView alloc] init];
    [contentSV addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(contentSV);
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register]) {
        titleLb.text = @"Offer入职时间:";
    }else{
        titleLb.text = @"待入职,offer入职时间:";
    }
    
    titleLb.font = TextFont;
    [view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(horPadding);
        make.top.equalTo(view).mas_offset(verPadding);
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * timeLb  = [[UILabel alloc] init];
#ifdef ViewDebug
    timeLb.text = @"2017-10-11 15:00";
#else
    timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.offer.reportDutyTime format:kDateFormatTypeYYYYMMDDHHMM];
    timeLb.font = TextFont;
#endif
    [view addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view).mas_offset(horPadding*2);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(view).mas_offset(-horPadding*2);
        make.height.mas_equalTo(lbH);
    }];
    
    if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register]) {
        UILabel * notRegisterLb = [[UILabel alloc] init];
        notRegisterLb.text = @"未入职";
        notRegisterLb.textColor = [UIColor redColor];
        notRegisterLb.font = TextFont;
        [view addSubview:notRegisterLb];
        [notRegisterLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view).mas_offset(horPadding*2);
            make.top.mas_equalTo(timeLb.mas_bottom).mas_offset(verPadding);
            make.right.equalTo(view).mas_offset(-horPadding*2);
            make.height.mas_equalTo(lbH);
        }];
    }
    return view;
}

- (UIView *)waitPositiveView{
    CGFloat verPadding = VerPxFit(25);
    CGFloat horPadding = HorPxFit(90);
    CGFloat lbH = VerPxFit(50);
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.font = TitleFont;
    titleLb.text = @"到岗时间:";
    [contentSV addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentSV).mas_offset(horPadding/2.0);
        make.top.equalTo(contentSV).mas_offset(verPadding);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * timeLb  = [[UILabel alloc] init];
//    timeLb.backgroundColor = [UIColor redColor];
    timeLb.font = TextFont;
#ifdef ViewDebug
    timeLb.text = @"2017-10-11 15:00";
#else
    timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.reportDutyTime format:kDateFormatTypeYYYYMMDDHHMM];
#endif
    timeLb.textAlignment = NSTextAlignmentCenter;
    [contentSV addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLb).mas_offset(horPadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verPadding);
//        make.right.equalTo(contentSV).mas_offset(-horPadding*2);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(lbH);
    }];
    
    UIButton * submitBt = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBt.titleLabel.font = ActionBtFont;
    if (_model.applyCommission) {
        [submitBt setTitle:@"已申请佣金" forState:UIControlStateNormal];
        submitBt.enabled = NO;
    }else{
        [submitBt setTitle:@"申请佣金" forState:UIControlStateNormal];
    }
    [submitBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitBt.titleLabel.font = ActionBtFont;
    [submitBt addTarget:self action:@selector(rewardApplyAction) forControlEvents:UIControlEventTouchUpInside];
    [submitBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [contentSV addSubview:submitBt];
    
    [submitBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contentSV.mas_centerX);
        make.top.mas_equalTo(timeLb.mas_bottom).mas_offset(VerPxFit(25));
        make.width.equalTo(timeLb).mas_offset(horPadding);
        make.height.mas_equalTo(VerPxFit(65));
    }];
    
    if (_model.itemPayFee > 0) {
        submitBt.hidden = YES;
        
        UILabel * registeredLb = [[UILabel alloc] init];
        registeredLb.text = @"已转正";
        registeredLb.textColor = [UIColor redColor];
        registeredLb.textAlignment = NSTextAlignmentCenter;
        registeredLb.font = TextFont;
        [contentSV addSubview:registeredLb];
        [registeredLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.and.top.and.bottom.equalTo(submitBt);
        }];
        
        if (!_model.hhEvaluateFlag) {
            [contentSV addSubview:self.rateBt];
            [self.rateBt mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(contentSV.mas_centerX);
                make.top.mas_equalTo(registeredLb.mas_bottom).mas_offset(VerPxFit(25));
                make.size.mas_equalTo(CGSizeMake(HorPxFit(380), VerPxFit(65)));
            }];
        }
    }
    return contentSV;
}

- (void)positionView{
    CGFloat verPadding = VerPxFit(25);
    CGFloat horPadding = HorPxFit(90);
    CGFloat lbH = VerPxFit(50);
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.font = TitleFont;
    titleLb.text = @"已转正，转正时间:";
    [contentSV addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentSV).mas_offset(horPadding/2.0);
        make.top.equalTo(contentSV).mas_offset(verPadding);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * timeLb  = [[UILabel alloc] init];
    //    timeLb.backgroundColor = [UIColor redColor];
    timeLb.font = TextFont;
    timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.positiveTime format:kDateFormatTypeYYYYMMDD];
    timeLb.textAlignment = NSTextAlignmentCenter;
    [contentSV addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLb).mas_offset(horPadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verPadding);
        //        make.right.equalTo(contentSV).mas_offset(-horPadding*2);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(lbH);
    }];
    if (!_model.hhEvaluateFlag) {
        [contentSV addSubview:self.rateBt];
        [self.rateBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(contentSV.mas_centerX);
            make.top.mas_equalTo(timeLb.mas_bottom).mas_offset(VerPxFit(25));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(380), VerPxFit(65)));
        }];
    }
}

- (UIView *)notPositiveView{
    CGFloat verPadding = VerPxFit(30);
    CGFloat horPadding = HorPxFit(90);
    CGFloat lbH = VerPxFit(50);
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.font = TitleFont;
    titleLb.text = @"到岗时间:";
    [contentSV addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentSV).mas_offset(horPadding/2.0);
        make.top.equalTo(contentSV).mas_offset(verPadding);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * timeLb  = [[UILabel alloc] init];
    //    timeLb.backgroundColor = [UIColor redColor];
    timeLb.font = TextFont;
#ifdef ViewDebug
    timeLb.text = @"2017-10-11 15:00";
#else
    timeLb.text =  [BaseHelper stringWithTimeIntevl:_model.reportDutyTime format:kDateFormatTypeYYYYMMDDHHMM];
#endif
    timeLb.textAlignment = NSTextAlignmentCenter;
    [contentSV addSubview:timeLb];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(contentSV.mas_centerX);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(verPadding);
        //        make.right.equalTo(contentSV).mas_offset(-horPadding*2);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(lbH);
    }];
    
    UILabel * notRegisterLb = [[UILabel alloc] init];
    notRegisterLb.text = @"未转正";
    notRegisterLb.textColor = [UIColor redColor];
    notRegisterLb.textAlignment = NSTextAlignmentCenter;
    notRegisterLb.font = TextFont;
    [contentSV addSubview:notRegisterLb];
    [notRegisterLb mas_makeConstraints:^(MASConstraintMaker *make) {        
        make.centerX.mas_equalTo(contentSV.mas_centerX);
        make.top.mas_equalTo(timeLb.mas_bottom).mas_offset(verPadding);
        make.width.mas_equalTo(130);
        make.height.mas_equalTo(lbH);
    }];
    
    if (!_model.hhEvaluateFlag) {
        [self addSubview:self.rateBt];
        [self.rateBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(contentSV.mas_centerX);
            make.top.mas_equalTo(notRegisterLb.mas_bottom).mas_offset(VerPxFit(25));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(380), VerPxFit(65)));
        }];
    }
    
    return contentSV;
}

// 推荐状态回看
- (UIView *)recommendBackView{
    if ([_model.state isEqualToString:PositionCandidateOrderStateRecommended]) {
        [self recommendView];
    }else{
        [self hrResumeDealResult];
    }
    return nil;
}

//  面试状态回看
- (UIView *)interViewBackView{
    return [self interViewFinishView:PositionCandidateOrderStateWaint_Offer];
}

- (void)updateYueMianTime:(NSDate *)date{
    NSString * dateString = [BaseHelper stringFromDate:date format:kDateFormatTypeYYYYMMDDHHMM];
    [dateChooseBt setTitle:dateString forState:UIControlStateNormal];
}
- (void)yumianTimeSumintFinish:(NSString *)url{
    [contentSV removeAllSubviews];
    contentSV.contentSize = contentSV.size;
    QRCodeCreateManage * mange = [[QRCodeCreateManage alloc] init];
    UIImage * img = [mange createWithString:url];
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
    imgV.frame = CGRectMake(10, 10, 100, 100);
//    imgV.center = contentSV.center;
    [contentSV addSubview:imgV];
}

- (void)chooseInterViewTime{
    [self.delegate dateChoose];
    
}
- (void)timeSureAction{
    [self.delegate dateSureAction:telField.text address:addressField.text];
}

- (void)rewardApplyAction{
    [self.delegate requestReward];
}

- (void)rateAction{
    
    if (!self.rateView) {
        self.rateView = [[RateView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-HorPxFit(60), HorPxFit(400))];
        self.rateView.delegate  = self;
        self.rateView.model = _model;
    }
    [self.rateView showWithSupView:nil];
}

- (void)rateFinish{
    _rateBt.hidden = YES;
}
@end
