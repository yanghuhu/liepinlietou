//
//  TalentListCell.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalentModel.h"
#import "JobModel.h"

@class TalentListCell;

@protocol TalentListCellDelegate
- (void)payActionWithCell:(TalentListCell *)cell;
- (void)phoneCall:(TalentModel *)model;
@end

@interface TalentListCell : UITableViewCell

@property (nonatomic , strong) TalentModel * model;
@property (nonatomic , weak)  id<TalentListCellDelegate>delegate;

- (void)createSbuViews;
- (void)talentPayFinished;
+ (CGFloat)cellHeight;
@end
