//
//  TalentListTBCell.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/6.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentListTBCell.h"
#import "UIImageView+YYWebImage.h"

@interface TalentListTBCell(){
    UIView * contentView;
    UIImageView * avatarImgV;
    UIButton * payBt;
    UILabel * nameLb;
    UILabel * positionLb;
    UILabel * companylb;
    UILabel * matchLb;
}
@end

@implementation TalentListTBCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
//        [self createSbuViews];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}



- (void)createSbuViews{
    CGFloat horPadding = HorPxFit(30);
    CGFloat verPadding = VerPxFit(20);
    contentView = [[UIView alloc] initWithFrame:CGRectMake(horPadding, 0, CellW-2*horPadding, CellH-verPadding)];
    [self.contentView addSubview:contentView];
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"talentBg"];
    [contentView addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(contentView);
    }];
    
    
    CGFloat avatarH = CGRectGetHeight(contentView.frame)-2*HorPxFit(10);
    avatarImgV = [[UIImageView alloc] initWithFrame:CGRectMake(HorPxFit(10), HorPxFit(10), avatarH, avatarH)];
    avatarImgV.image = [BaseHelper avatarPlaceHolderWithGender:1];
    avatarImgV.layer.cornerRadius = avatarH/2.0;
    avatarImgV.backgroundColor = [UIColor clearColor];
    avatarImgV.contentMode = UIViewContentModeScaleAspectFill;
    avatarImgV.clipsToBounds = YES;
    [contentView addSubview:avatarImgV];
    
    payBt = [UIButton buttonWithType:UIButtonTypeCustom];
    payBt.layer.cornerRadius =  avatarH/2.0;
    payBt.backgroundColor = [UIColor clearColor];
    [payBt setImage:[UIImage imageNamed:@"payIcon"] forState:UIControlStateNormal];
    [payBt addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    payBt.frame =  CGRectMake(CGRectGetWidth(contentView.frame) - avatarH - HorPxFit(10), HorPxFit(10), avatarH, avatarH);
    
    [contentView addSubview:payBt];
    
    
    nameLb = [self lb];
//    nameLb.text = @"人才库";
    [contentView addSubview:nameLb];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(avatarImgV.mas_right).mas_offset(horPadding);
        make.top.equalTo(contentView).mas_offset(VerPxFit(10));
//        make.right.mas_equalTo(payBt.mas_left).mas_offset(horPadding);
        make.height.mas_equalTo(VerPxFit(40));
        make.right.mas_equalTo(payBt.mas_left).mas_offset(-horPadding);
    }];
    
    matchLb = [self lb];
    [contentView addSubview:matchLb];
    matchLb.textColor = [UIColor orangeColor];
    [matchLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(HorPxFit(250));
        make.centerY.mas_equalTo(nameLb.mas_centerY);
        make.right.mas_equalTo(payBt.mas_left);
        make.height.equalTo(nameLb);
    }];
    
    positionLb = [self lb];
    [contentView addSubview:positionLb];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(avatarImgV.mas_right).mas_offset(horPadding);
        make.right.mas_equalTo(payBt.mas_left).mas_offset(-horPadding);
        make.height.mas_equalTo(VerPxFit(40));
        make.centerY.mas_equalTo(avatarImgV.mas_centerY);
    }];

    companylb = [self lb];
    [contentView addSubview:companylb];
    [companylb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(avatarImgV.mas_right).mas_offset(horPadding);
        make.bottom.equalTo(contentView).mas_offset(-VerPxFit(10));
        make.right.mas_equalTo(payBt.mas_left).mas_offset(-horPadding);
        make.height.mas_equalTo(VerPxFit(40));
    }];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.font = [UIFont systemFontOfSize:14];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    return lb;
}

- (void)payAction{
    if (_talentModel.payStatus) {
        [HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount = 0;
        [self.delegate phoneCall:_talentModel];
    }else{
     [self.delegate talentPayAction:_talentModel];
    }
}

-(void)setTalentModel:(TalentModel *)talentModel{
    _talentModel = talentModel;
    if ([talentModel.gender isEqualToString:@"男"]) {
        [avatarImgV setImageWithURL:[NSURL URLWithString:talentModel.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1]];
    }else{
        [avatarImgV setImageWithURL:[NSURL URLWithString:talentModel.avatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:0]];
    }

    companylb.text = talentModel.company;
    positionLb.text = talentModel.position;
    if (_showMatch) {
        matchLb.text = [NSString stringWithFormat:@"匹配度:%.0f%%",talentModel.matchedDegree*100];
        nameLb.text = [NSString stringWithFormat:@"%@",talentModel.name];
    }else{
        nameLb.text = [NSString stringWithFormat:@"%@  %@  %@",talentModel.name,talentModel.gender,talentModel.education];
    }

    if (_talentModel.payStatus) {
        [payBt setImage:[UIImage imageNamed:@"talentPhone"] forState:UIControlStateNormal];
    }else{
        [payBt setImage:[UIImage imageNamed:@"payIcon"] forState:UIControlStateNormal];
    }
    
}

@end
