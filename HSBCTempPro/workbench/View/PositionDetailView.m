//
//  PositionDetailView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDetailView.h"
#import "JobRequirementModel.h"
#import "JobResponsibilityModel.h"
#import "UIView+YYAdd.h"
#define TextFont [UIFont systemFontOfSize:15]

@interface PositionDetailView(){
    UILabel * educationLb;
    UILabel * yearsLb;
    UILabel * locationLb;
    UILabel * typeLb;
    UILabel * wageLb;
    UILabel * noteLb;
    UIView * descriptionView;
    UIView * responsibityView;
    CGFloat selfH;
}
@end

@implementation PositionDetailView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UIImage * originImg = [UIImage imageNamed:@"positionDetailBk"];
    UIImage * img  = [originImg stretchableImageWithLeftCapWidth:originImg.size.width/2.0 topCapHeight:originImg.size.height - 30];
    UIImageView * bkImageV = [[UIImageView alloc] initWithImage:img];
    [self addSubview:bkImageV];
    [bkImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    selfH = 0;
    
    UILabel * titleLable = [[UILabel alloc] init];
    titleLable.text = @"职位介绍";
    titleLable.backgroundColor = [UIColor whiteColor];
    titleLable.textColor = RGB16(0x4077ec);
    titleLable.font = [UIFont systemFontOfSize:18];
    titleLable.clipsToBounds = YES;
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.layer.cornerRadius = VerPxFit(60)/2.0;
    [self addSubview:titleLable];
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self).mas_offset(VerPxFit(50));
        make.width.mas_equalTo(HorPxFit(300));
        make.height.mas_equalTo(VerPxFit(60));
    }];
    selfH+= (VerPxFit(80)+VerPxFit(50));
    
    CGFloat lbH = VerPxFit(50);
    CGSize iconSize = CGSizeMake(HorPxFit(38), HorPxFit(38));
    CGFloat leftRigthPadding = HorPxFit(60);
    CGFloat iconPaddingLb = HorPxFit(20);
    CGFloat rowPadding = VerPxFit(30);
    
    UIImageView * educationIcon = [self imgV:@"educationIcon"];
    [educationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(leftRigthPadding);
        make.top.mas_equalTo(titleLable.mas_bottom).mas_offset(VerPxFit(40));
        make.size.mas_equalTo(iconSize);
    }];
    
    educationLb = [self lb];
    educationLb.text = @"学历";
    [educationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(educationIcon.mas_right).mas_offset(iconPaddingLb);
        make.centerY.mas_equalTo(educationIcon.mas_centerY);
        make.height.mas_equalTo(lbH);
        make.width.mas_equalTo(HorPxFit(230));
    }];
    
    UIImageView * yearsIcon = [self imgV:@"nianxianIcon"];
    [yearsIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(educationLb.mas_right);
        make.centerY.mas_equalTo(educationIcon.mas_centerY);
        make.size.mas_equalTo(iconSize);
    }];
    
    yearsLb = [self lb];
    yearsLb.text = @"工作已有年限";
    [yearsLb mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(yearsIcon.mas_right).mas_offset(iconPaddingLb);
        make.centerY.mas_equalTo(yearsIcon.mas_centerY);
        make.right.equalTo(self).mas_offset(-leftRigthPadding);
        make.height.mas_equalTo(lbH);
    }];
    
    selfH += (lbH+VerPxFit(40));
    
    UIImageView * locationIcon = [self imgV:@"positionLocationIcon"];
    [locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(educationIcon.mas_bottom).mas_offset(rowPadding);
        make.left.equalTo(educationIcon);
        make.size.mas_equalTo(iconSize);
    }];
    
    locationLb = [self lb];
    locationLb.text = @"工作地点";
    [locationLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationIcon.mas_right).mas_offset(iconPaddingLb);
        make.centerY.mas_equalTo(locationIcon.mas_centerY);
        make.right.equalTo(self).mas_offset(-iconPaddingLb);
        make.height.mas_equalTo(lbH);
    }];
    
    selfH += (lbH+rowPadding);
    
    UIImageView * typeIcon = [self imgV:@"positionTypeIcon"];
    [typeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(educationIcon);
        make.top.mas_equalTo(locationIcon.mas_bottom).mas_offset(rowPadding);
        make.size.mas_equalTo(iconSize);
    }];
    
    typeLb = [self lb];
    typeLb.text = @"工作性质";
    [typeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(typeIcon.mas_right).mas_offset(iconPaddingLb);
        make.centerY.mas_equalTo(typeIcon.mas_centerY);
        make.height.mas_equalTo(lbH);
        make.width.mas_equalTo(HorPxFit(230));
    }];
    
    UIImageView * wageIcon = [self imgV:@"gongziIcon"];
    [wageIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(typeLb.mas_right);
        make.centerY.mas_equalTo(typeIcon.mas_centerY);
        make.size.mas_equalTo(iconSize);
    }];
    
    wageLb = [self lb];
    wageLb.text = @"工作薪资";
    [wageLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(wageIcon.mas_right).mas_offset(iconPaddingLb);
        make.centerY.mas_equalTo(wageIcon.mas_centerY);
        make.right.equalTo(self).mas_offset(-leftRigthPadding);
        make.height.mas_equalTo(lbH);
    }];
    
    selfH += (lbH+rowPadding);
    
    UIImageView * responderIcon = [self imgV:@"descriptionIcon"];
    [responderIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(typeIcon.mas_bottom).mas_offset(rowPadding);
        make.size.mas_equalTo(iconSize);
        make.left.equalTo(typeIcon);
    }];
    
    UILabel * responderTitle = [self lb];
    responderTitle.text = @"职位要求";
    [responderTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(responderIcon.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(responderIcon.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    responsibityView = [[UIView alloc] init];
    responsibityView.backgroundColor = [UIColor clearColor];
    [self addSubview:responsibityView];
    [responsibityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(responderIcon.mas_right).mas_offset(iconPaddingLb);
        make.right.equalTo(self).mas_offset(-leftRigthPadding);
        make.top.mas_equalTo(responderIcon.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(0);
    }];
    
    selfH += (rowPadding+iconSize.height+VerPxFit(10));

    UIImageView * descIcon = [self imgV:@"descriptionIcon"];
    [descIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(responsibityView.mas_bottom).mas_offset(rowPadding);
        make.size.mas_equalTo(iconSize);
        make.left.equalTo(responderIcon);
    }];
    
    UILabel * descTitle = [self lb];
    descTitle.text = @"职位描述";
    [descTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(descIcon.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(descIcon.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    descriptionView = [[UIView alloc] init];
    descriptionView.backgroundColor = [UIColor clearColor];
    [self addSubview:descriptionView];
    [descriptionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(responderIcon.mas_right).mas_offset(iconPaddingLb);
        make.right.equalTo(self).mas_offset(-leftRigthPadding);
        make.top.mas_equalTo(descIcon.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(0);
    }];
    
    selfH += (rowPadding+iconSize.height+VerPxFit(10));
    
    UIImageView * noteIcon = [self imgV:@"descriptionIcon"];
    [noteIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(descriptionView.mas_bottom).mas_offset(rowPadding);
        make.size.mas_equalTo(iconSize);
        make.left.equalTo(responderIcon);
    }];
    
    UILabel * noteTitle = [self lb];
    noteTitle.text = @"备注";
    [noteTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(noteIcon.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(noteIcon.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
    noteLb = [self lb];
    noteLb.numberOfLines = 0;
    [noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(responderIcon.mas_right).mas_offset(iconPaddingLb);
        make.right.equalTo(self).mas_offset(-leftRigthPadding);
        make.top.mas_equalTo(noteIcon.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(0);
    }];
    
    selfH += (iconSize.height + VerPxFit(10) + rowPadding);
}


- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.font = TextFont;
    [self addSubview:lb];
    return lb;
}

- (UIImageView *)imgV:(NSString *)imgName{
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    imgV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imgV];
    return imgV;
}

- (CGFloat)insertInfoWithModel:(JobModel *) jobModel{
    
    self.jobModel = jobModel;
    yearsLb.text = _jobModel.workExperience;
    educationLb.text = _jobModel.education;
    locationLb.text = _jobModel.workArea;
    typeLb.text = _jobModel.jobNature;
    
    if (jobModel.salaryRange == 0) {
        wageLb.text = @"面议";
    }else{
        wageLb.text = [NSString stringWithFormat:@"%ld元",jobModel.salaryRange];
    }
    
    CGSize iconSize = CGSizeMake(HorPxFit(38), HorPxFit(38));
    CGFloat leftRigthPadding = HorPxFit(50);
    CGFloat iconPaddingLb = HorPxFit(20);
//    CGFloat selfH = CGRectGetMinY(responsibityView.frame);
    CGFloat curY = 0;
    CGFloat itemW = CGRectGetWidth(self.frame) - leftRigthPadding*2-iconPaddingLb-iconSize.width;
    CGFloat singleH = VerPxFit(50);
    
    NSArray * responsibityArray = jobModel.jobResponsibitiyArray;
    for (int i=0; i<responsibityArray.count; i++) {
        JobResponsibilityModel * model = responsibityArray[i];
        NSString * text = [NSString stringWithFormat:@"%d.%@",i+1,model.description_];
        CGSize size = [BaseHelper getSizeWithString:text font:TextFont contentWidth:itemW contentHight:MAXFLOAT];
        CGFloat H = singleH;
        if (size.height > H) {
            H = size.height;
        }
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, curY, itemW, H)];
        lb.backgroundColor = [UIColor clearColor];
        lb.text = text;
        lb.numberOfLines = 0;
        lb.textColor = [UIColor whiteColor];
        lb.font = TextFont;
        [responsibityView addSubview:lb];
        curY += H;
    }
    selfH += curY ;
    
    [responsibityView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(curY);
    }];
    
    curY = 0;
    NSArray * requireArray = jobModel.jobRequireArray;
    for (int i=0; i<requireArray.count; i++) {
        JobRequirementModel * model = requireArray[i];
        NSString * text = [NSString stringWithFormat:@"%d.%@",i+1,model.description_];
        CGSize size = [BaseHelper getSizeWithString:text font:TextFont contentWidth:itemW contentHight:MAXFLOAT];
        CGFloat H = singleH;
        if (size.height > H) {
            H = size.height;
        }
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, curY, itemW, H)];
        lb.text = text;
        lb.backgroundColor = [UIColor clearColor];
        lb.numberOfLines = 0;
        lb.textColor = [UIColor whiteColor];
        lb.font = TextFont;
        [descriptionView addSubview:lb];
        curY += H;
    }
    [descriptionView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(curY);
    }];
    selfH += curY;
//    jobModel.additionalInfo = @"这个职位只是测试用!这个职位只是测试用!这个职位只是测试用!这个职位只是测试用!这个职位只是测试用!";
    if (jobModel.additionalInfo && jobModel.additionalInfo.length != 0) {
        CGFloat lbW = ScreenWidth-HorPxFit(50)*2 - VerPxFit(10) -iconPaddingLb-leftRigthPadding;
        noteLb.text = jobModel.additionalInfo;
        CGSize size = [BaseHelper getSizeWithString:jobModel.additionalInfo font:TextFont contentWidth:lbW contentHight:MAXFLOAT];
        [noteLb mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(size.height);
        }];
        selfH += size.height;
    }
    self.height = selfH;
    return selfH;
}

@end
