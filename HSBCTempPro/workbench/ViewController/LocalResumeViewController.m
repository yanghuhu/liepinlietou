//
//  LocalResumeViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/7.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "LocalResumeViewController.h"
#import "UIImage+YYAdd.h"

@interface LocalResumeViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITableView * tableView;
}

@property (nonatomic , strong) NSArray * fileArray;

@end

@implementation LocalResumeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR(63, 0, 137, 1);
    self.title = @"我的简历";
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].isThirdResumeToSpecificPage) {
        [HSBCGlobalInstance sharedHSBCGlobalInstance].isThirdResumeToSpecificPage  = NO;
        [HSBCGlobalInstance sharedHSBCGlobalInstance].fileInfoFromThirdApp = nil;
    }
    
    NSArray * vcs = [self.navigationController viewControllers];
    if (vcs.count == 1) {
        UIButton * backBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBt setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
        backBt.frame  = CGRectMake(0, 0, 55, 40);
        [backBt setImage:[[UIImage imageNamed:@"back"]imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:backBt];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBt];
    }
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSString * folderPath  = [BaseHelper resumeFolderPath];
    NSArray *fileList = [NSArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:folderPath error:nil]];
    
    self.fileArray = fileList;
    
    tableView = [[UITableView alloc]initWithFrame:self.view.bounds];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backAction{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _fileArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * contentV = [[UIView alloc] init];
        contentV.backgroundColor = [UIColor whiteColor];
        [cell addSubview:contentV];
        [contentV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.equalTo(cell);
            make.top.equalTo(cell).mas_offset(VerPxFit(28));
            make.bottom.equalTo(cell);
        }];
        
        UILabel * postionLb = [[UILabel alloc] init];
        postionLb.tag = 100;
        postionLb.font = [UIFont systemFontOfSize:15];
        postionLb.textAlignment = NSTextAlignmentCenter;
        [contentV addSubview:postionLb];
        [postionLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentV).mas_offset(HorPxFit(90));
            make.right.equalTo(contentV).mas_offset(-HorPxFit(90));
//            make.centerY.mas_equalTo(contentV.mas_centerY).mas_offset(-VerPxFit(50)/2.0);
            make.centerY.mas_equalTo(contentV.mas_centerY);
            make.height.mas_equalTo(VerPxFit(50));
        }];
        
        UILabel * companyLb = [[UILabel alloc] init];
        companyLb.font = [UIFont systemFontOfSize:15];
        companyLb.textAlignment = NSTextAlignmentCenter;
        companyLb.tag = 101;
        [contentV addSubview:companyLb];
        [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentV).mas_offset(HorPxFit(90));
            make.right.equalTo(contentV).mas_offset(-HorPxFit(90));
            make.centerY.mas_equalTo(contentV.mas_centerY).mas_offset(VerPxFit(50)/2.0);
            make.height.mas_equalTo(VerPxFit(50));
        }];
        
//        UIImageView * flagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionUnselect"]];
//        flagImgV.tag = 102;
//        [contentV addSubview:flagImgV];
//        [flagImgV mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(contentV).mas_offset(HorPxFit(50));
//            make.centerY.mas_equalTo(contentV.mas_centerY);
//            make.size.mas_equalTo(CGSizeMake(HorPxFit(30), HorPxFit(30)));
//        }];
        
    }
    UILabel * positionLb = [cell viewWithTag:100];
    UILabel * companylb = [cell viewWithTag:101];
    
    positionLb.text =  _fileArray[indexPath.row];
//    companylb.text = model.jobModel.hrModel.company.name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return VerPxFit(150);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
