//
//  UserDashboardViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CompanyOrderModel.h"


typedef NS_ENUM(NSInteger, PageIndex) {
    PageIndexTalentPoor,
    PageIndexTaskPage,
};
@interface UserDashboardViewController : BaseViewController

@property (nonatomic , strong) CompanyOrderModel * jobOrderModel;
@property (nonatomic , assign) PageIndex pageIndex;

@end
