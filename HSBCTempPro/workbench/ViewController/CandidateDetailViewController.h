//
//  CandidateDetailViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TaskModel.h"

@interface CandidateDetailViewController : BaseViewController

@property (nonatomic , strong) CandidateModel* model;
@property (nonatomic , assign) NSInteger orderId;
@end
