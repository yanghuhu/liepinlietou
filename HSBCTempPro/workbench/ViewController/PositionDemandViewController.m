//
//  PositionDemandViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDemandViewController.h"
#import "PositionTableCell.h"
#import "PositionDemandHeader.h"
#import "UserDashboardViewController.h"
#import "HRInfoView.h"
#import "HRModel.h"
#import "JobDetailView.h"
#import "RecruitModule.h"
#import "PositionDetailViewController.h"
#import "UIScrollView+PSRefresh.h"
#import "MJRefreshHeader.h"
#import "UserHomeViewController.h"
#import "MessageViewController.h"
#import "AppDelegate.h"
#import "UIButton+Badge.h"

static NSString *const cellId = @"cellId";
static NSString *const headerId = @"headerId";

#define CellPadding HorPxFit(20)
#define HeaderHeight HorPxFit(40)



@interface PositionDemandViewController ()<PositionDemandHeaderDelegate,UITableViewDelegate,UITableViewDataSource,JobDetailViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,PositionDemandHeaderDelegate>

@property (nonatomic , strong) UITableView * tableView;

@property (nonatomic , strong) NSMutableArray * dataSource;

@property (nonatomic , strong) HRInfoView * hrInfoView;
@property (nonatomic , strong) JobDetailView * jobDetailView;
@property (nonatomic , strong) JobModel *jobSelected;
@property (nonatomic , assign) NSInteger  pageNum;

@property (nonatomic , strong) PositionDemandHeader * headerView;

@property (nonatomic , assign) BOOL isSearching;
@property (nonatomic , strong) NSString * searchKey;
@property (nonatomic , assign) NSInteger  searchpageNum;
@property (nonatomic , strong) NSMutableArray * serachDataSource;

@property (nonatomic , strong) JobModel * curSelectJobModel;

@end

@implementation PositionDemandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ViewControllerBkColor;
    _isSearching = NO;
    _headerView = [[PositionDemandHeader alloc] init];
    _headerView.delegate = self;
    [self.view addSubview:_headerView];
    [_headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.and.right.equalTo(self.view);
        make.height.mas_equalTo(VerPxFit(330));
    }];
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(30));
        make.right.mas_equalTo(-HorPxFit(30));
        make.top.equalTo(_headerView.mas_bottom);
        make.bottom.equalTo(self.view);
    }];
    
    @weakify(self)
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 0;
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    
    self.tableView.mj_footer =[MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, HorPxFit(20))];
    _tableView.tableHeaderView = view;

    self.pageNum = 0;
    [BaseHelper showProgressLoadingInView:self.view];
    [self getDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    [self getRetomeSeverMessage];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].isThirdResumeToSpecificPage) {
        NSURL  * url =  [HSBCGlobalInstance sharedHSBCGlobalInstance].fileInfoFromThirdApp[@"url"];
        [BaseHelper thirdResumeFileSave:url];

        AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate showLocalResumeVC];
    }
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        if (_isSearching) {
            _searchpageNum ++;
        }else{
            _pageNum ++;
        }
    }else{
        // 下拉刷新
        if (_isSearching) {
            _searchpageNum = 0;
        }else{
            _pageNum = 0;
        }
    }
    [self getDataSource];
}


- (void)getDataSource{
    if (!_dataSource) {
        self.dataSource = [NSMutableArray array];
    }
  
    [RecruitModule getJobDemandListWithPageNum:_isSearching?_searchpageNum:_pageNum searchKey:_searchKey  Success:^(NSArray * dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        if (_isSearching) {
            if (_searchpageNum == 0) {
                [self.serachDataSource removeAllObjects];
                [_tableView.mj_header endRefreshing];
            }else{
                [_tableView.mj_footer endRefreshing];
            }
        }else{
            if (_pageNum == 0) {  // 下拉刷新
                [self.dataSource removeAllObjects];
                [_tableView.mj_header endRefreshing];
            }else{
                [_tableView.mj_footer endRefreshing];
            }
        }
        if (dataArray && dataArray.count != 0) {
            if (_isSearching) {
                [self.serachDataSource addObjectsFromArray:dataArray];
            }else{
                [self.dataSource addObjectsFromArray:dataArray];
            }
        }
        [self.tableView reloadData];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (HRInfoView *)hrInfoView{
    if (!_hrInfoView) {
        CGFloat horPadding = HorPxFit(70);
        _hrInfoView = [[HRInfoView alloc] initWithFrame:CGRectMake(0,0, ScreenWidth - horPadding*2, 0)];
        [_hrInfoView createsSubViews];
    }
    return _hrInfoView;
}

- (JobDetailView *)jobDetailView{
    if (!_jobDetailView) {
        _jobDetailView = [[JobDetailView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - 2*HorPxFit(50), [JobDetailView viewHeight])];
        _jobDetailView.delegate = self;
    }
    return _jobDetailView;
}

- (void)toGongzuotai{
    UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_isSearching) {
        return _serachDataSource.count;
    }else{
        return _dataSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    PositionTableCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[PositionTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    JobModel * model;
    if (_isSearching) {
        model = _serachDataSource[indexPath.row];
    }else{
        model = _dataSource[indexPath.row];
    }
    cell.jobModel = model;
    cell.textLabel.text = model.title;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellH;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    JobModel * jobModel;
    
    if (_isSearching) {
        jobModel = _serachDataSource[indexPath.row];
    }else{
        jobModel = _dataSource[indexPath.row];
    }
    
    self.curSelectJobModel = jobModel;
    PositionDetailViewController * vc = [[PositionDetailViewController alloc] init];
    vc.jobModel = jobModel;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- PositionDemandHeaderDelegate
- (void)gotoDashboardAction{
    UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
    vc.pageIndex = PageIndexTalentPoor;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)toMessageVCAction{
    MessageViewController * vc = [[MessageViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)personCenterAction{
    UserHomeViewController * vc = [[UserHomeViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchWithKey:(NSString *)key{
    if (!key || key.length ==0) {
        return;
    }
    _isSearching = YES;
    self.searchKey = key;
    if(!_serachDataSource){
        _serachDataSource = [NSMutableArray array];
    }else{
        [_serachDataSource removeAllObjects];
    }
    self.searchpageNum = 0;
    [self getDataSource];
}
- (void)searchCancel{
    _isSearching = NO;
    self.searchKey = nil;
    [self.serachDataSource removeAllObjects];
    [_tableView reloadData];
}

#pragma mark --JobDetailViewDelegate
- (void)jobUndertakeAction:(JobModel *)model{
    UIActionSheet * sheetView = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"去任务栏查看",@"去推荐人才", nil];
    [sheetView showInView:self.view];
    self.jobSelected = model;
}

#pragma mark -- UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 2){
        return;
    }
//    UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
//    if (buttonIndex == 1) {
//        vc.pageIndex = PageIndexTalentPoor;
//        vc.jobModel  = self.jobSelected;
//    }else if(buttonIndex == 0){
//        vc.pageIndex = PageIndexTaskPage;
//    }
//    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- PositionCollectionCellDelegate

- (void)hrInfoShow:(JobModel *)model{
    [BaseHelper showProgressLoadingInView:self.view];
}

#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        // 取消
    }else if (buttonIndex == 1){
        //  确定
        [BaseHelper showProgressLoadingInView:self.view];
        [RecruitModule acceptJobNeedWithJobId:_curSelectJobModel.id_ success:^(JobModel *model){
            [BaseHelper hideProgressHudInView:self.view];
            [BaseHelper showProgressHud:@"揭榜成功" showLoading:NO canHide:YES];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
        }];
    }
}

- (void)getRetomeSeverMessage{
    [RecruitModule getRetomeServerMessageAccounSuccess:^(id resopnse){
        NSDictionary *dict = (NSDictionary *)resopnse;
        NSString *hasUnreadMessage = dict[@"hasUnreadMessage"];
        if ([hasUnreadMessage boolValue]) {
            _headerView.messageButton.badgeValue = @"1" ;

        }else{
            _headerView.messageButton.badgeValue = @"" ;
        }
    } failure:^(NSString *error, ResponseType responseType) {
        NSLog(@"获取未读消息===%@",error);
    }];
}

@end
