//
//  MoneyViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MoneyViewController.h"
#import "MoneylistCell.h"
#import "MoneyListHeaderView.h"
#import "RecruitModule.h"
#import "MoneyHeaderView.h"
#import "MoneyChatView.h"
#import "CandidateDetailViewController.h"
#import "AccountModule.h"

#define  CellIdentifier @"CellIdentifier"

typedef enum {
    DataTypePaid,
    DataTypeNotPay
}DataType;

@interface MoneyViewController ()<UITableViewDelegate,UITableViewDataSource,MoneyHeaderViewDelegate>{
    
    UITableView * tableView;
}

@property (nonatomic , strong) NSMutableArray<RewardModel *> * rewardArray;

@property (nonatomic , strong) MoneyChatView *chatView;
@property (nonatomic , strong) NSMutableArray * dataPayed;
@property (nonatomic , strong) NSMutableArray * dataNotPay;
@property (nonatomic , assign) NSInteger paiedPageNum;
@property (nonatomic , assign) NSInteger notpayPageNum;
@property (nonatomic , assign)  DataType dataType;

@end

@implementation MoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    _notpayPageNum = 0;
    _paiedPageNum = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!tableView) {
        [self createSubViews];
#ifdef DebugWithOutNetWork
#else
        [self getDataPaidForFistComeIn];
        self.dataType = DataTypeNotPay;
        [BaseHelper showProgressLoadingInView:self.view];
        [self getDataSource];
#endif
    }
}

- (void)createSubViews{
    MoneyHeaderView * headerView = [[MoneyHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, VerPxFit(140))];
    headerView.delegate = self;
    headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerView];

    
    tableView  = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerView.frame), ScreenWidth, self.view.frame.size.height-CGRectGetMaxY(headerView.frame))];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    [BaseHelper setExtraCellLineHidden:tableView];
    
    @weakify(self)
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
   
    
    self.dataPayed = [NSMutableArray array];
    self.dataNotPay = [NSMutableArray array];
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        if (_dataType == DataTypePaid) {
            _paiedPageNum ++;
        }else{
            _notpayPageNum ++;
        }
    }else{
        // 下拉刷新
        if (_dataType == DataTypePaid) {
            _paiedPageNum = 0;
        }else{
            _notpayPageNum = 0;
        }    }
    [self getDataSource];
}


- (void)getDataSource{
    if (!_rewardArray) {
        _rewardArray = [NSMutableArray array];
    }
    
    [RecruitModule getRewardListWithPage:_dataType ==DataTypePaid?_paiedPageNum:_notpayPageNum
                                withType:_dataType ==DataTypePaid?@"Paid":@"PENDING_PAY"
                                 success:^(NSArray *sourArray){
        [BaseHelper hideProgressHudInView:self.view];
        if (sourArray && sourArray.count != 0) {
            if (_dataType == DataTypePaid) {
                if (_paiedPageNum == 0) {
                    [_dataPayed removeAllObjects];
                }
                [_dataPayed addObjectsFromArray:sourArray];
            }else{
                if (_notpayPageNum == 0) {
                    [_dataNotPay removeAllObjects];
                }
                [_dataNotPay addObjectsFromArray:sourArray];
            }
            [tableView reloadData];
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getDataPaidForFistComeIn{
    [RecruitModule getRewardListWithPage:_paiedPageNum
                                withType:@"Paid"
                                 success:^(NSArray *sourArray){
                                     [BaseHelper hideProgressHudInView:self.view];
                                     if (sourArray && sourArray.count != 0) {
                                         [_dataPayed addObjectsFromArray:sourArray];
                                     }
                                 } failure:^(NSString *error, ResponseType responseType) {
                                     [BaseHelper hideProgressHudInView:self.view];
                                     if (error) {
                                         [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
                                     }
                                 }];
}

- (void)willMoveToParentViewController:(UIViewController *)parent{
}
- (void)didMoveToParentViewController:(UIViewController *)parent{
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_dataType == DataTypePaid) {
        return  _dataPayed.count;
    }else{
        return _dataNotPay.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MoneylistCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        cell = [[MoneylistCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
//        cell.delegate = self;
    }
    RewardModel * model = nil;
    if (_dataType == DataTypePaid) {
        model = _dataPayed[indexPath.row];
    }else{
        model = _dataNotPay[indexPath.row];
    }
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return VerPxFit(170);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_dataType == DataTypeNotPay) {
        RewardModel * model = _dataNotPay[indexPath.row];
        CandidateDetailViewController * vc = [[CandidateDetailViewController alloc] init];
        vc.orderId = model.id_;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark -- MoneylistCellDelegate
- (void)appearNotPayData{
    self.dataType = DataTypeNotPay;
    [tableView reloadData];
}

- (void)appearHadPayData{
    self.dataType = DataTypePaid;
    [tableView reloadData];
}

- (void)showChatImage{
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule getAccountBlanceSuccess:^{
        [BaseHelper hideProgressHudInView:self.view];
        if (!_chatView) {
            CGFloat w = ScreenWidth;
            CGFloat h = 360*w/550;
            _chatView = [[MoneyChatView alloc] initWithFrame:CGRectMake(0, 0, w, h)];
        }
        [self.chatView showWithSupView:nil];
    } failure:^(NSString *error, ResponseType responseType) {
       [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}
@end
