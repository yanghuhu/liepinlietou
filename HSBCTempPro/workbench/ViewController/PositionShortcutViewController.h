//
//  PositionShortcutViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TalentModel.h"

@interface PositionShortcutViewController : BaseViewController
@property (nonatomic , strong) TalentModel * talentModel;
@end
