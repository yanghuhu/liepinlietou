//
//  PositionShortcutViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionShortcutViewController.h"
#import "CompanyOrderModel.h"
#import "RecruitModule.h"
#import "JobModel.h"

#define CellIdentifier @"CellIdentifier"

@interface PositionShortcutViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITableView * tableView;
    
}
@property (nonatomic , strong) NSIndexPath * indexPathSelected;
@property (nonatomic , strong) NSMutableArray * positionArray;
@property (nonatomic , strong) CompanyOrderModel * orderModel;
@property (nonatomic , assign) NSInteger pageNum;
@end

@implementation PositionShortcutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"职务列表";
    self.view.backgroundColor = COLOR(63, 0, 137, 1);
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight-64)];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableView];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 25)];
    [bkBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    bkBt.frame = CGRectMake(0, 0, 60, 45);
    [bkBt setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    
    UIBarButtonItem * backItem = [[UIBarButtonItem alloc] initWithCustomView:bkBt];
    self.navigationItem.leftBarButtonItem = backItem;
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.frame = CGRectMake(0, 0, 60, 40);
    [sureBt setImageEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    [sureBt setImage:[UIImage imageNamed:@"suerFlag"] forState:UIControlStateNormal];
    
    UIBarButtonItem * sureItem = [[UIBarButtonItem alloc] initWithCustomView:sureBt];
    self.navigationItem.rightBarButtonItem = sureItem;
    _pageNum = 0;
    [BaseHelper showProgressLoadingInView:self.view];
    [self getDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)getDataSource{
    if (!_positionArray) {
        self.positionArray = [NSMutableArray array];
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getPositionListWithPage:_pageNum success:^(NSArray * dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        if (dataArray && dataArray.count != 0) {
            [self.positionArray addObjectsFromArray:dataArray];

            [tableView reloadData];
        }else{
            [BaseHelper showProgressHud:@"暂无数据" showLoading:NO canHide:YES];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)backAction{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)sureAction{
    if (_indexPathSelected) {
        if (_indexPathSelected.row < _positionArray.count) {
            CompanyOrderModel * model = _positionArray[_indexPathSelected.row];
            self.orderModel = model;
        }
    }
    if(!_orderModel){
        [BaseHelper showProgressHud:@"请选择职务" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule talentRecommendWithTanlentId:_talentModel.id_ orderId:_orderModel.id_ success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"推荐成功" showLoading:NO canHide:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _positionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * contentV = [[UIView alloc] init];
        contentV.backgroundColor = [UIColor whiteColor];
        [cell addSubview:contentV];
        [contentV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.equalTo(cell);
            make.top.equalTo(cell).mas_offset(VerPxFit(28));
            make.bottom.equalTo(cell);
        }];
        
        UILabel * postionLb = [[UILabel alloc] init];
        postionLb.tag = 100;
        postionLb.font = [UIFont systemFontOfSize:15];
        postionLb.textAlignment = NSTextAlignmentCenter;
        [contentV addSubview:postionLb];
        [postionLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentV).mas_offset(HorPxFit(90));
            make.right.equalTo(contentV).mas_offset(-HorPxFit(90));
            make.centerY.mas_equalTo(contentV.mas_centerY).mas_offset(-VerPxFit(50)/2.0);
            make.height.mas_equalTo(VerPxFit(50));
        }];
        
        UILabel * companyLb = [[UILabel alloc] init];
        companyLb.font = [UIFont systemFontOfSize:15];
        companyLb.textAlignment = NSTextAlignmentCenter;
        companyLb.tag = 101;
        [contentV addSubview:companyLb];
        [companyLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentV).mas_offset(HorPxFit(90));
            make.right.equalTo(contentV).mas_offset(-HorPxFit(90));
            make.centerY.mas_equalTo(contentV.mas_centerY).mas_offset(VerPxFit(50)/2.0);
            make.height.mas_equalTo(VerPxFit(50));
        }];
        
        UIImageView * flagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionUnselect"]];
        flagImgV.tag = 102;
        [contentV addSubview:flagImgV];
        [flagImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(contentV).mas_offset(HorPxFit(50));
            make.centerY.mas_equalTo(contentV.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(HorPxFit(30), HorPxFit(30)));
        }];
    }
    
    UILabel * positionLb = [cell viewWithTag:100];
    UILabel * companylb = [cell viewWithTag:101];
    
    CompanyOrderModel *model = _positionArray[indexPath.row];
    positionLb.text =  model.jobModel.title;
    companylb.text = model.jobModel.hrModel.company.name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return VerPxFit(150);
}

-(void)viewDidLayoutSubviews{
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];//此处的self是指tableView，在Controller中是：self.tableView
    }
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.indexPathSelected) {
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:_indexPathSelected];
        UIImageView * flagImgV = [cell viewWithTag:102];
        [flagImgV setImage:[UIImage imageNamed:@"positionUnselect"]];
    }
    UITableViewCell * cell_ = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView * flagImgV = [cell_ viewWithTag:102];
    [flagImgV setImage:[UIImage imageNamed:@"positionSelect"]];
    
    self.indexPathSelected = indexPath;
}

@end
