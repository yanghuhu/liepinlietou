//
//  CandidateDetailViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/8.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "CandidateDetailViewController.h"
#import "RecruitModule.h"
#import "CandidateDetailHeader.h"
#import "CandidateStateView.h"


@interface CandidateDetailViewController ()<CandidateStateViewDelegate,CandidateDetailHeaderViewDelegate>{
    CandidateDetailHeader * headerView;
    CandidateStateView * stateContentV;
}
@property (nonatomic , strong) JobOrderDetailModel* orderModel;
@property (nonatomic , strong) UIView * datePickerContentV;
@property (nonatomic , strong) UIDatePicker * datePicker;
@property (nonatomic , strong) NSDate * yuyueTime;
@end

@implementation CandidateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.navigationItem.title = @"候选人详情";
    self.view.backgroundColor = [UIColor whiteColor];
    [self createSubViews];
    if (_model.hrStateFlag) {
        [self submitHongdianYidu];
    }
    [self getDataSoure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    headerView = [[CandidateDetailHeader alloc] initWithFrame:CGRectMake(0,0, ScreenWidth, 0)];
    headerView.delegate = self;
    [self.view addSubview:headerView];
    stateContentV = [[CandidateStateView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerView.frame), ScreenWidth, ScreenHeight-CGRectGetMaxY(headerView.frame))];
    stateContentV.hidden = YES;
    stateContentV.delegate = self;
    [self.view addSubview:stateContentV];
}

- (void)getDataSoure{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getCandidateDetailWithId:_model?_model.id_:_orderId success:^(JobOrderDetailModel * model){
        [BaseHelper hideProgressHudInView:self.view];
        self.orderModel = model;
        [self initViewInfo];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)initViewInfo{
    if (!_orderModel) {
        return;
    }
    headerView.model = _orderModel;
    stateContentV.hidden = NO;
    stateContentV.model = _orderModel;
}

- (UIView *)datePickerContentV{
    if (!_datePickerContentV) {
        _datePickerContentV= [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-265, ScreenWidth, 261)];
        _datePickerContentV.backgroundColor = COLOR(240, 240, 240, 1);
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 261-216, ScreenWidth, 216)];
        _datePicker.backgroundColor = COLOR(230, 230, 230, 1);
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        _datePicker.minimumDate = [NSDate date];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文 默认为英文
        _datePicker.locale = locale;
        [_datePickerContentV addSubview:_datePicker];
        
        UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [sureBt setTitle:@"确定" forState:UIControlStateNormal];
        [sureBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sureBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [sureBt addTarget:self action:@selector(datePickerSure) forControlEvents:UIControlEventTouchUpInside];
//        sureBt.backgroundColor = [UIColor redColor];
        sureBt.frame = CGRectMake(ScreenWidth-70, 5, 60, 30);
        [_datePickerContentV addSubview:sureBt];
        
        UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBt setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        cancelBt.backgroundColor = [UIColor redColor];
        cancelBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [cancelBt addTarget:self action:@selector(datePickerCancel) forControlEvents:UIControlEventTouchUpInside];
        cancelBt.frame = CGRectMake(5, 5, 60, 30);
        [_datePickerContentV addSubview:cancelBt];
    }
    return _datePickerContentV;
}

- (void)datePickerSure{
    NSDate * date = [_datePicker date];
    self.yuyueTime = date;
    [stateContentV updateYueMianTime:date];
    [self datePickerCancel];
}

- (void)datePickerCancel{
    [self.datePickerContentV removeFromSuperview];
}

- (void)submitHongdianYidu{
    [RecruitModule hongdianFlagRead:_model.id_ success:^{
        _model.hrStateFlag = NO;
    } failure:^(NSString *error, ResponseType responseType) {
    }];
}

#pragma mark -- CandidateStateViewDelegate

- (void)dateChoose{
    [self.view addSubview:self.datePickerContentV];
    self.datePicker.date = [NSDate date];
}

- (void)dateSureAction:(NSString *)iphone address:(NSString *)address{
    [BaseHelper showProgressLoadingInView:self.view];
    
    [RecruitModule submitYuyueTime:([self.yuyueTime timeIntervalSince1970]*1000) id_:_model.id_ iphone:iphone address:address success:^(NSString *url){
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"时间提交成功" showLoading:NO canHide:YES];
        [stateContentV yumianTimeSumintFinish:url];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)requestReward{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule requestForReward:_model?_model.id_:_orderId success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"申请提交成功" showLoading:NO canHide:YES];
        [self getDataSoure];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)rateAction:(CGFloat)rate{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule submitRate:rate withId:self.orderModel.orderModel.id_ success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"申请提交成功" showLoading:NO canHide:YES];
        [self getDataSoure];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}


#pragma mark -- CandidateDetailHeaderViewDelegate
- (void)getVirtualNumberWithType:(NSString *)type_{
    int   setNum = 2;
    NSString  * type = type_;
    NSLog(@"requestCount :%d",[HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount);
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getAboutPositionVirtualNumWithOrder:_orderModel.id_ personType:type success:^(NSString *phoneNum){
        
        [HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount ++;
        if (!phoneNum || [phoneNum isKindOfClass:[NSNull class]]) {
            if ([HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount == setNum) {
                [BaseHelper hideProgressHudInView:self.view];
                [BaseHelper showProgressHud:@"获取虚拟号码失败,请稍后再试" showLoading:NO canHide:YES];
            }else{
                // 后台对接阿里获取虚拟号码，未避免阿里延迟，故失败后重复请求
                sleep(15);
                [self getVirtualNumberWithType:type];
            }
        }else{
            [BaseHelper hideProgressHudInView:self.view];
            [BaseHelper phoneCallWithNum:phoneNum];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}
@end
