//
//  TalentpoolViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TalentpoolViewController.h"
#import "TalentTableHeader.h"
#import "ResumDetailView.h"
#import "PositionShortcutViewController.h"
#import "BaseNavigationController.h"
#import "RecruitModule.h"
#import "OrderModel.h"
#import "TalentListTBCell.h"
#import "TalentPoorSearchView.h"
#import "UIView+YYAdd.h"
#import "LocalResumeViewController.h"
#import "PayPopView.h"

#define CellIdentifier @"CellInentify"

@interface TalentpoolViewController ()<TalentListTBCellDelegate,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIAlertViewDelegate,TalentPoorSearchViewDelegate,PayPopViewDelegate>{
    UITableView * tableView;
    TalentTableHeader * tableHeader;
}

@property (nonatomic , strong) NSMutableArray * talentsArray;
@property (nonatomic , strong) ResumDetailView * resumDetailView;
@property (nonatomic , assign) BOOL isSearching;
@property (nonatomic , assign) NSInteger pageNum;
@property (nonatomic , strong) OrderModel * orderModer;
@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , strong) NSString * searchKey;
@property (nonatomic , strong) NSMutableArray * serachDataSource;
@property (nonatomic , strong) PayPopView * payPopView;
@end

@implementation TalentpoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!tableView) {
        [self createSubViews];
    }
}

- (void)createSubViews{
    
    tableView  = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, self.view.frame.size.height) style:UITableViewStyleGrouped];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    [BaseHelper setExtraCellLineHidden:tableView];
  
    @weakify(self)
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 0;
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    tableView.mj_footer =[MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
    _pageNum = 0;
#ifdef DebugWithOutNetWork
#else
    [BaseHelper showProgressLoadingInView:self.view];
    [self getDataSource];
#endif
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        _pageNum ++;
    }else{
        // 下拉刷新
        _pageNum = 0;
    }
    [self getDataSource];
}

- (void)getDataSource{
    if (!_talentsArray) {
        self.talentsArray = [NSMutableArray array];
    }
    [RecruitModule getTalentListWithPageNum:_pageNum searchKey:_searchKey success:^(NSArray *dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        if(dataArray && dataArray.count != 0){
            if (_isSearching) {
                if (_pageNum == 0) {
                    [self.serachDataSource removeAllObjects];
                }
                [self.serachDataSource addObjectsFromArray:dataArray];
            }else{
                if (_pageNum == 0) {
                    [self.talentsArray removeAllObjects];
                }
                [self.talentsArray addObjectsFromArray:dataArray];
            }
        }
        [tableView reloadData];
        if (!dataArray || dataArray.count < 10) {
            // remove add more
        }
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (ResumDetailView *)resumDetailView{
    if (!_resumDetailView) {
        CGFloat horpadding = HorPxFit(30);
        _resumDetailView = [[ResumDetailView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-2*horpadding, 0)];
        @weakify(self);
        @weakify(_resumDetailView);
        _resumDetailView.recommendBlock = ^(void){
            @strongify(self);
            @strongify(_resumDetailView)
            if (!self || !_resumDetailView) {
                return ;
            }
            [_resumDetailView dismissAction];
            if (!self.jobOrdermodel) {
                PositionShortcutViewController * vc = [[PositionShortcutViewController alloc] init];
                vc.talentModel  =_resumDetailView.talentModel;
                BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
                [self presentViewController:nav animated:YES completion:^{
                }];
            }else{
                [BaseHelper showProgressLoadingInView:self.view];
                [RecruitModule talentRecommendWithTanlentId:self.talentModel.id_ orderId:self.jobOrdermodel.id_ success:^{
                    [BaseHelper hideProgressHudInView:self.view];
                    [BaseHelper showProgressHud:@"推荐成功" showLoading:NO canHide:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                } failure:^(NSString *error, ResponseType responseType) {
                    [BaseHelper hideProgressHudInView:self.view];
                    if (error) {
                        [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
                    }
                }];
            }
        };
    }
    return _resumDetailView;
}

- (PayPopView *)payPopView{
    if (!_payPopView) {
        _payPopView = [[PayPopView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth - HorPxFit(90), VerPxFit(350))];
        _payPopView.delegate = self;
    }
    return _payPopView;
}

- (void)willMoveToParentViewController:(UIViewController *)parent{
}
- (void)didMoveToParentViewController:(UIViewController *)parent{
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_isSearching) {
        return _serachDataSource.count;
    }
    return self.talentsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    TalentPoorSearchView * searchView = [[TalentPoorSearchView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(25)*2, 70)];
    searchView.delegate = self;
    return searchView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TalentListTBCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[TalentListTBCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        [cell createSbuViews];
        if(_jobOrdermodel){
            cell.showMatch = YES;
        }else{
            cell.showMatch = NO;
        }
    }
    TalentModel * model = nil;
    if (_isSearching) {
        model = _serachDataSource[indexPath.row];
    }else{
         model = _talentsArray[indexPath.row];
    }
    cell.talentModel = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellH;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TalentModel * model = nil;
    if (_isSearching) {
        model = _serachDataSource[indexPath.row];
    }else{
        model = _talentsArray[indexPath.row];
    }
    
    if (model.payStatus) {
        [BaseHelper showProgressLoadingInView:self.view];
        @weakify(self)
        [RecruitModule getTalentDetailWithId:model.id_ success:^(TalentModel * model){
            [BaseHelper hideProgressHudInView:self.view];
            [weak_self.resumDetailView initInfoWithTalent:model];
            [weak_self.resumDetailView showWithSupView:nil];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            [self netFailWihtError:error andStatusCode:responseType];
        }];
        
        self.resumDetailView.jobmodel = self.jobOrdermodel.jobModel;
        self.talentModel  = model;
    }else{
        [self talentPayAction:model];
    }
}

//- (void)searchKey:(NSString *)key{
//    if (!key) {
//        _isSearching = NO;
//        [tableView reloadData];
//        return;
//    }
//    NSMutableArray * tempArray = [NSMutableArray array];
//    for (TalentModel * model in _talentsArray) {
//        if ([model.name containsString:key]) {
//            [tempArray addObject:model];
//        }
//    }
//    self.searchResult = tempArray;
//    _isSearching = YES;
//    [tableView reloadData];
//}

- (void)phoneCall:(TalentModel *)model_{
    int   setNum = 2;
    TalentModel * model = model_;
    NSLog(@"requestCount :%d",[HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount);
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getTalentNetworkPhone:model.id_ Success:^(NSString *phoneNum){
        [HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount ++;
        if (!phoneNum || [phoneNum isKindOfClass:[NSNull class]]) {
            if ([HSBCGlobalInstance sharedHSBCGlobalInstance].getVirtualNumberCount == setNum) {
                [BaseHelper hideProgressHudInView:self.view];
                [BaseHelper showProgressHud:@"获取虚拟号码失败,请稍后再试" showLoading:NO canHide:YES];
            }else{
                // 后台对接阿里获取虚拟号码，未避免阿里延迟，故失败后重复请求
                sleep(15);
                [self phoneCall:model];
            }
        }else{
            [BaseHelper hideProgressHudInView:self.view];
            [BaseHelper phoneCallWithNum:phoneNum];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)talentPayAction:(TalentModel *)talentModel{
    self.talentModel = talentModel;
    [self createOrderWithTalent];
}

- (void)createOrderWithTalent{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule talentOrderCreateWithId:_talentModel.id_ success:^(OrderModel *model){
        [BaseHelper hideProgressHudInView:self.view];
        self.orderModer = model;
        
        self.payPopView.talentModel = self.talentModel;
        self.payPopView.orderModel = model;
        [self.payPopView showWithSupView:self.view.superview];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)payAction{
    [BaseHelper showProgressLoading];
    [RecruitModule talentPayWithTitle:@"简历支付" detail:@"简历支付" totalFee:self.orderModer.totalFee serialNumber:self.orderModer.serialNumber success:^{
        [_payPopView dismissAction];
        [BaseHelper hideProgressHud];
        [BaseHelper showProgressHud:@"支付成功" showLoading:NO canHide:YES];
        _talentModel.payStatus = YES;
        [tableView reloadData];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHud];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- TalentPoorSearchViewDelegate

- (void)searchWithKey:(NSString *)key{
    if (!key || key.length ==0) {
        return;
    }
    _isSearching = YES;
    self.searchKey = key;
    if(!_serachDataSource){
        _serachDataSource = [NSMutableArray array];
    }else{
        [_serachDataSource removeAllObjects];
    }
    [self getDataSource];
}

- (void)searchCancel{
    _isSearching = NO;
    [self.serachDataSource removeAllObjects];
    [tableView reloadData];
}

- (void)toResumeCheckAction{
//    [BaseHelper showProgressHud:@"导入简历" showLoading:NO canHide:YES];
    LocalResumeViewController * vc = [[LocalResumeViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
