//
//  TalentpoolViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CompanyOrderModel.h"

@interface TalentpoolViewController : BaseViewController

@property (nonatomic , strong) CompanyOrderModel * jobOrdermodel;
@property (nonatomic , assign) CGFloat ViewHeight;

@end
