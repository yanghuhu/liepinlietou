//
//  MyTalentViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CompanyOrderModel.h"

@interface MyTalentViewController : BaseViewController

@property (nonatomic , strong) CompanyOrderModel * jobOrderModel;

@end
