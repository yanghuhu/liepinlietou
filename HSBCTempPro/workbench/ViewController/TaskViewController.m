//
//  TaskViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "TaskViewController.h"
#import "TaskListCell.h"
#import "RecruitModule.h"
#import "NewPagedFlowView.h"
#import "TaskListCell.h"
#import "CandidateDetailViewController.h"
#import "UIScrollView+PSRefresh.h"

#define CellIdentifier  @"CellIdentifier"


@interface TaskViewController ()<TaskListCellDelegate,NewPagedFlowViewDelegate,NewPagedFlowViewDataSource>{
    
    NSArray * talentArrayFir;
    NSArray * talentArraySec;
    NSArray * talentArrayThird;
}

@property (nonatomic , assign) NSInteger pageNum;
@property (nonatomic , strong) NSMutableArray * taskDataSource;
@property (nonatomic , strong) NewPagedFlowView * flowPageView;

@end

@implementation TaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!self.flowPageView) {
        [self createSubViews];
    }
}

- (void)createSubViews{
    
    self.flowPageView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, VerPxFit(60), ScreenWidth, self.view.frame.size.height-VerPxFit(80)-VerPxFit(30))];
    self.flowPageView.backgroundColor = [UIColor clearColor];
    self.flowPageView.delegate = self;
    self.flowPageView.dataSource = self;
    self.flowPageView.minimumPageAlpha = 0.4;
    self.flowPageView.needsReload = NO;
    self.flowPageView.isOpenAutoScroll = NO;
    self.flowPageView.isCarousel = NO;
    self.flowPageView.leftRightMargin = HorPxFit(70);
    self.flowPageView.topBottomMargin = VerPxFit(70);
    self.flowPageView.isOpenAutoScroll = YES;
    [self.view addSubview:self.flowPageView];
    
    @weakify(self)
    [self.flowPageView.scrollView addRefreshHeaderWithClosure:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    
    [self.flowPageView.scrollView addRefreshFooterWithClosure:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
    
    [self.flowPageView reloadData];
#ifdef DebugWithOutNetWork
#else
    [BaseHelper showProgressLoadingInView:self.view];
    [self getDataSource];
#endif
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)willMoveToParentViewController:(UIViewController *)parent{

}

- (void)didMoveToParentViewController:(UIViewController *)parent{

}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
//        _pageNum ++;
        _pageNum = 0;
    }else{
        // 下拉刷新
        _pageNum = 0;
    }
    [self getDataSource];
}


- (void)getDataSource{
    if (!_taskDataSource) {
        self.taskDataSource = [NSMutableArray array];
    }
    [RecruitModule getTaskListWithPage:_pageNum Success:^(NSArray *dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        [self.flowPageView.scrollView endRefreshing];

        if(dataArray && dataArray.count != 0){
            [self.taskDataSource addObjectsFromArray:dataArray];
            [self.flowPageView reloadData];
        }
        if (!dataArray || dataArray.count < 10) {
            // remove add more
        }        
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self.flowPageView.scrollView endRefreshing];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return self.taskDataSource.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    TaskListCell *bannerView = (TaskListCell *)[flowView dequeueReusableCell];
    bannerView.backgroundColor = [UIColor orangeColor];
    if (!bannerView) {
        bannerView = [[TaskListCell alloc] initWithTBFrame:CGRectMake(0, 0, self.view.frame.size.width - HorPxFit(40)*2, self.view.frame.size.height-VerPxFit(80)-VerPxFit(30))];
        bannerView.delegate =self;
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
    }
    bannerView.taskModel = _taskDataSource[index];
    //在这里下载网络图片
    
    return bannerView;
}

- (void)candidateChoosed:(CandidateModel *)candidateModel{
    CandidateDetailViewController * vc = [[CandidateDetailViewController alloc] init];
    vc.model = candidateModel;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
