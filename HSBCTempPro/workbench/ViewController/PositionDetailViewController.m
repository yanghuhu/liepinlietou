//
//  PositionDetailViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDetailViewController.h"
#import "PositionDetailHeaderView.h"
#import "PositionRewardView.h"
#import "RecruitModule.h"
#import "PositionDetailView.h"
#import "RecruitModule.h"
#import "UserDashboardViewController.h"
#import "MyTalentViewController.h"
#import "PositionHeaderView.h"

@interface PositionDetailViewController ()<UIActionSheetDelegate>{
    
    UIScrollView * contentSV;
    PositionDetailHeaderView * headerView;
    PositionRewardView * rewardView;
    PositionDetailView * detailView;
    PositionHeaderView * positionHeader;
}

@property (nonatomic , strong) CompanyOrderModel * orderModel;
@end

@implementation PositionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = ViewControllerBkColor;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"职位详情";
    
    contentSV = [[UIScrollView alloc] init];
    contentSV.backgroundColor = [UIColor clearColor];
    [self.view addSubview:contentSV];
    
    @weakify(self)
    [contentSV mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth, ScreenHeight));
//        make.centerX.mas_equalTo(self.view.mas_centerX);
//        make.centerY.mas_equalTo(self.view.mas_centerY);
        make.edges.mas_equalTo(self.view);
    }];
//    [self getJobDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self createSubViews];
    [self insertInfoWithModel];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)getJobDetail{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getJobDetailWithId:_jobModel.id_ success:^(JobModel *model){
        [BaseHelper hideProgressHudInView:self.view];
        self.jobModel = model;
        [self insertInfoWithModel];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)createSubViews{
    
    CGFloat leftRightPadding = HorPxFit(90);
   
//    rewardView = [[PositionRewardView alloc] initWithFrame:CGRectMake(leftRightPadding,VerPxFit(40), ScreenWidth-leftRightPadding*2, VerPxFit(360))];
//    [contentSV addSubview:rewardView];
//
//    headerView = [[PositionDetailHeaderView alloc] initWithFrame:CGRectMake(HorPxFit(50), CGRectGetMaxY(rewardView.frame)-VerPxFit(100) , ScreenWidth-HorPxFit(50)*2, VerPxFit(340))];
//    [contentSV insertSubview:headerView belowSubview:rewardView];
    
    positionHeader = [[PositionHeaderView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 250)];
    [contentSV addSubview:positionHeader];

    detailView = [[PositionDetailView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(positionHeader.frame), ScreenWidth, VerPxFit(1000))];
    [contentSV addSubview:detailView];
    contentSV.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(detailView.frame));
    
    UIButton * jiebangBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [jiebangBt addTarget:self action:@selector(jiabangAction) forControlEvents:UIControlEventTouchUpInside];
    [jiebangBt setBackgroundImage:[UIImage imageNamed:@"jiebangBt"] forState:UIControlStateNormal];
    [jiebangBt setTitle:@"猎人揭榜" forState:UIControlStateNormal];
    [self.view addSubview:jiebangBt];
    [jiebangBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).mas_offset(-VerPxFit(20));
        make.left.equalTo(self.view).mas_offset(HorPxFit(80));
        make.right.equalTo(self.view).mas_offset(-HorPxFit(80));
        make.height.mas_equalTo(VerPxFit(70));
    }];
}

- (void)insertInfoWithModel{
    headerView.jobModel = self.jobModel;
    rewardView.jobModel = self.jobModel;
    positionHeader.jobModel = self.jobModel;
    CGFloat heightAdd = [detailView insertInfoWithModel:self.jobModel];
    CGFloat bottomPadding = 90;
    
    contentSV.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(positionHeader.frame)+heightAdd+bottomPadding);
}

- (void)jiabangAction{
    
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitModule acceptJobNeedWithJobId:_jobModel.id_ success:^(CompanyOrderModel *model){
        self.orderModel = model;
        @strongify(self)
        if (!self) {
            return ;
        }
        [BaseHelper hideProgressHudInView:self.view];
        if (model.firstFlag) {
            [BaseHelper showProgressHud:@"揭榜成功" showLoading:NO canHide:YES];
            [self acceptSuccess];
        }else{
            UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
            vc.jobOrderModel = _orderModel;
            vc.pageIndex = PageIndexTalentPoor;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)acceptSuccess{
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:@"揭榜成功" delegate:self cancelButtonTitle:@"返回" destructiveButtonTitle:nil otherButtonTitles:@"去推荐人才",@"去任务表", nil];
    [sheet showInView:self.view];
}

#pragma mark -- UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        //  去人才库
//        MyTalentViewController * vc = [[MyTalentViewController alloc] init];
//        vc.jobOrderModel = _orderModel;
        UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
        vc.jobOrderModel = _orderModel;
        vc.pageIndex = PageIndexTalentPoor;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (buttonIndex == 1){
        // 去任务表
        UserDashboardViewController * vc = [[UserDashboardViewController alloc] init];
        vc.pageIndex = PageIndexTaskPage;
        vc.jobOrderModel = _orderModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
