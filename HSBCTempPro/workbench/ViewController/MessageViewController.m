//
//  MessageViewController.m
//  HSBCTempPro
//
//  Created by Deve on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageCell.h"
#import "MessageSegmentView.h"
#import "RecruitModule.h"
#import "SysMessageModel.h"
#import "MJRefreshHeader.h"


static NSString *const messageCellId = @"messageCellId";
NSString *const SYS_MS = @"SYS_MESSAGE";
NSString *const INFORM_MS = @"INFORM_MESSAGE";


@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource,SegmentClickDelegate>

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)MessageSegmentView *messageSegment;
@property (nonatomic,strong)SysMessageModel * model;
@property (nonatomic ,assign)NSInteger  pageNum;
@property (nonatomic,strong)NSString *notificationType;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"消息";
    
    _notificationType = SYS_MS;
    [self layoutBgView];
    [self getRetomeMsList:_notificationType];
    [self clearNotReaderMs];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)layoutBgView{
    UIImageView *headImageView = [UIImageView new];
    headImageView.userInteractionEnabled = YES;
    headImageView.image  = [UIImage imageNamed:@"dashboardBg"];
    [self.view addSubview:headImageView];
    
    [headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(VerPxFit(220));
    }];
    
    MessageSegmentView *message = [self messageSegment];
    [headImageView addSubview:message];
    [message mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(headImageView);
        make.height.mas_equalTo(VerPxFit(60));
    }];
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image  = [UIImage imageNamed:@"UIEdgeInsetsZero"];
    [self.view addSubview:bgImageView];
    
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headImageView.mas_bottom);
        make.bottom.left.right.mas_equalTo(self.view);
    }];
    
}

- (MessageSegmentView *)messageSegment{
    if (!_messageSegment) {
        _messageSegment = [MessageSegmentView new];
        _messageSegment.delegate = self;
        
    }
    return _messageSegment;
}


- (UITableView *)tableView{
    if (!_tableView ) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerClass:[MessageCell class] forCellReuseIdentifier:messageCellId];
        _tableView.layoutMargins =  UIEdgeInsetsZero;
        _tableView.separatorInset = UIEdgeInsetsZero;
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        
        @weakify(self)
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            _pageNum = 0;
            [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
        }];
        
        _tableView.mj_footer =[MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weak_self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
        }];
        
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(VerPxFit(220));
            make.left.right.bottom.mas_equalTo(self.view);
        }];
    }
    return _tableView;
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        _pageNum ++;
        
    }else{
        // 下拉刷新
        _pageNum = 0;
        [self.dataSource removeAllObjects];
    }
    [self getRetomeMsList:_notificationType];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  VerPxFit(200);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count ? self.dataSource.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:messageCellId];
    cell.layoutMargins = UIEdgeInsetsZero;
    [cell setDataModel:self.dataSource[indexPath.row]];
    return cell;
}

#pragma mark -----SegmentDelegate-----
- (void)segmentClickChickerData:(NSInteger)tag{
    switch (tag) {
        case 200:
        {
            [self.dataSource removeAllObjects];
            [self getRetomeMsList:SYS_MS];
            _notificationType = SYS_MS;
        }
            break;
        case 201:
        {
            [self.dataSource removeAllObjects];
            [self getRetomeMsList:INFORM_MS];
            _notificationType = INFORM_MS;

        }
            break;
        default:
            break;
    }
}


- (void)clearNotReaderMs{
    [RecruitModule clearNotReaderMessageSuccess:^{
         NSLog(@"消息清除成功===");
    } failure:^(NSString *error, ResponseType responseType) {
        NSLog(@"消息清除失败===%@",error);
    }];
}

- (void)getRetomeMsList:(NSString *)messageType{
    [RecruitModule getRetomeSrverMessageList:messageType page:_pageNum Success:^(SysMessageModel *model){
        [BaseHelper hideProgressHudInView:self.view];
        
        for (ContentModel *contentModel in model.content) {
            [self.dataSource addObject:contentModel];
        }
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
        
    } failure:^(NSString *error, ResponseType responseType) {
        [_tableView.mj_header endRefreshing];
        [_tableView.mj_footer endRefreshing];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
