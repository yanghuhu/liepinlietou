//
//  UserDashboardViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "UserDashboardViewController.h"
#import "TalentpoolViewController.h"
#import "MoneyViewController.h"
#import "TaskViewController.h"
#import "UIView+YYAdd.h"
#import "TodayDataShow.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "WeekDataView.h"
#import "RecruitModule.h"

#define navCotentHeight VerPxFit(150)
#define headerBgImageViewHeight VerPxFit(389)

@interface UserDashboardViewController ()

@property (nonatomic ,strong) TalentpoolViewController * talentpoolViewController;
@property (nonatomic ,strong) MoneyViewController * moneyViewController;
@property (nonatomic ,strong) TaskViewController * taskViewController;
@property (nonatomic ,strong) UIViewController * currentVC;

@property (nonatomic ,strong) UIView * navScrollView;
@property (nonatomic , strong) UIButton * curSelectedBt;
@property (nonatomic , strong) TodayDataShow *todayDataShow;
@property (nonatomic , strong) UIImageView * bkCireImgV;

@property (nonatomic , strong) UIImageView * headerBkImg;
@property (nonatomic, strong) UIImageView *headerBgImageView;


@property (nonatomic , strong) WeekDataView * weekChatView;
@end

@implementation UserDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"工作台";
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    
    
    [self layoutBgView];
    [self initTitleCotent];
    [self addSubChildVC];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
     self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
     self.navigationController.navigationBar.translucent = NO;
}

- (void)layoutBgView{
    _headerBgImageView = [UIImageView new];
    _headerBgImageView.image  = [UIImage imageNamed:@"dashboardHeader"];
    [self.view addSubview:_headerBgImageView];
    [_headerBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(headerBgImageViewHeight);
    }];
    
    UIImageView *bgImageView = [UIImageView new];
    bgImageView.image  = [UIImage imageNamed:@"dashboardBg"];
    [self.view addSubview:bgImageView];
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_headerBgImageView.mas_bottom);
        make.bottom.left.right.mas_equalTo(self.view);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)initTitleCotent{

    UIButton * dataShowBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [dataShowBt addTarget:self action:@selector(showTodyData) forControlEvents:UIControlEventTouchUpInside];
    [dataShowBt setImage:[UIImage imageNamed:@"todyData"] forState:UIControlStateNormal];
    dataShowBt.backgroundColor = [UIColor clearColor];
    [self.view addSubview:dataShowBt];
    [dataShowBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).mas_offset(-HorPxFit(20));
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+BatteryH+VerPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(90), VerPxFit(40)));
    }];
    
    UILabel * dataTitle = [[UILabel alloc] init];
    dataTitle.font = [UIFont systemFontOfSize:15];
    dataTitle.textColor = [UIColor whiteColor];
    dataTitle.backgroundColor = [UIColor clearColor];
    dataTitle.text = @"本周数据";
    [self.view addSubview:dataTitle];
    [dataTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(dataShowBt.mas_left);
        make.centerY.mas_equalTo(dataShowBt.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70,VerPxFit(40)));
    }];
    
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = [NSString stringWithFormat:@"%@,您好",[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone];
    titleLb.font = [UIFont boldSystemFontOfSize:19];
    titleLb.textColor = [UIColor whiteColor];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(20));
        make.centerY.mas_equalTo(dataTitle.mas_centerY);
        make.right.mas_equalTo(dataTitle.mas_left).mas_offset(HorPxFit(20));
        make.height.mas_equalTo(VerPxFit(40));
    }];
    
//    self.navScrollView = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(40), NavigationBarHeight+BatteryH+VerPxFit(30)+VerPxFit(40)+VerPxFit(40), ScreenWidth-HorPxFit(40)*2, navCotentHeight)];
    self.navScrollView = [UIView new];
    self.navScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.navScrollView];
    [_navScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(HorPxFit(40));
        make.right.mas_equalTo(self.view).offset(-HorPxFit(40));
        make.bottom.mas_equalTo(_headerBgImageView.mas_bottom).offset(-VerPxFit(20));
        make.height.mas_equalTo(navCotentHeight);
    }];
    
    NSArray * titles = @[@"人才库",@"任务表",@"赏金库"];
    NSArray * images = @[@"telentIcon",@"taskIcon",@"rewardIcon"];
//    CGFloat itemW = CGRectGetWidth(_navScrollView.frame)/titles.count;
//    CGFloat itemH = CGRectGetHeight(_navScrollView.frame);
    CGFloat itemW = (ScreenWidth-HorPxFit(40)*2)/titles.count;
    CGFloat itemH = navCotentHeight;
    
    for (int i =0; i<titles.count; i++) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(itemW*i, 0, itemW, itemH)];
        view.backgroundColor = [UIColor clearColor];
        [_navScrollView addSubview:view];
        
        UIImageView * flag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:images[i]]];
        flag.tag = 1000;
        flag.contentMode = UIViewContentModeScaleAspectFit;
        [view addSubview:flag];
        [flag mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.left.and.right.equalTo(view).mas_offset(VerPxFit(10));
            make.height.mas_equalTo(VerPxFit(65));
        }];
        
        UILabel * titleLb = [[UILabel alloc] init];
        titleLb.textAlignment = NSTextAlignmentCenter;
        titleLb.text = titles[i];
        titleLb.textColor = [UIColor whiteColor];
        titleLb.font = [UIFont systemFontOfSize:15];
        [view addSubview:titleLb];
        [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(VerPxFit(30));
            make.width.equalTo(view);
            make.centerX.mas_equalTo(flag.mas_centerX);
            make.bottom.equalTo(view).mas_offset(VerPxFit(-20));
        }];
        
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.frame = view.bounds;
        bt.tag = 100+i;
        bt.alpha = 0.3;
        [bt addTarget:self action:@selector(itemShift:) forControlEvents:UIControlEventTouchUpInside];
        [view insertSubview:bt atIndex:0];
        [bt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.top.and.bottom.equalTo(view);
        }];
        
        if ((_pageIndex == PageIndexTalentPoor && i == 0) || (_pageIndex == PageIndexTaskPage && i == 1)) {
            if(!_bkCireImgV){
                self.bkCireImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dashboardItembk"]];
            }
            [view insertSubview:self.bkCireImgV atIndex:0];
            [self.bkCireImgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(flag.mas_centerX).mas_offset(HorPxFit(26));
                make.bottom.mas_equalTo(flag);
                make.width.mas_equalTo(VerPxFit(80));
                make.height.mas_equalTo(VerPxFit(80)/3*2);
            }];
        }
    }
    
}

- (void)showTodyData{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitModule getWeekDataStatisticsSuccess:^(NSDictionary * data){
        [BaseHelper hideProgressHudInView:self.view];
        
        if (!_weekChatView) {
            _weekChatView = [[WeekDataView alloc] initWithFrame:CGRectMake(HorPxFit(20), 0, ScreenWidth - 2*HorPxFit(40), VerPxFit(560))];
        }
        [_weekChatView showWithSupView:self.view withData:data];
        
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)itemShift:(UIButton *)bt{
    if (bt == _curSelectedBt) {
        return;
    }else{
        
        UIView * flag = [bt.superview viewWithTag:1000];
        [self.bkCireImgV removeFromSuperview];
        [bt.superview insertSubview:self.bkCireImgV atIndex:0];
        [self.bkCireImgV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(flag.mas_centerX).mas_offset(HorPxFit(26));
            make.bottom.mas_equalTo(flag);
            make.width.mas_equalTo(VerPxFit(80));
            make.height.mas_equalTo(VerPxFit(80)/3*2);
        }];
        self.curSelectedBt = bt;
    }
    UIViewController * vc = nil;
    switch (bt.tag) {
        case 100:{
            vc = _talentpoolViewController;
            break;
        }
        case 101:{
            vc = _taskViewController;
            break;
        }
        case 102:{
            vc = _moneyViewController;
            break;
        }
        default:
            break;
    }
    if (vc != self.currentVC) {
        [self changeControllerFromOldController:_currentVC toNewController:vc];
    }
}

- (void)addSubChildVC{
    
//    CGFloat vcY =  CGRectGetMaxY(_navScrollView.frame);
    CGRect vcFrame = CGRectMake(0,headerBgImageViewHeight, ScreenWidth, ScreenHeight -headerBgImageViewHeight);
    
    if (!_headerBkImg) {
        CGFloat imgW = self.view.frame.size.width - 2*HorPxFit(28);
        CGFloat imgH = 31* imgW / 636;
        self.headerBkImg = [[UIImageView alloc] init];
//        [self.view addSubview:self.headerBkImg];
//        [_headerBkImg mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.view).mas_offset(HorPxFit(55));
//            make.right.equalTo(self.view).mas_offset(-HorPxFit(55));
//            make.bottom.equalTo(self.view.mas_top).mas_offset(vcY+VerPxFit(40));
//            make.height.mas_equalTo(imgH);
//        }];
    }
    
    self.talentpoolViewController = [[TalentpoolViewController alloc] init];
    
    self.moneyViewController = [[MoneyViewController alloc] init];
    self.taskViewController = [[TaskViewController alloc] init];
    
    self.talentpoolViewController.view.frame = vcFrame;
    self.moneyViewController.view.frame = vcFrame;
    self.taskViewController.view.frame = vcFrame;
    
    self.talentpoolViewController.ViewHeight = ScreenHeight -headerBgImageViewHeight-VerPxFit(50);
    
    if(_pageIndex == PageIndexTaskPage){
        [self addChildViewController:self.talentpoolViewController];
        [self changeControllerFromOldController:self.talentpoolViewController toNewController:_taskViewController];
//        self.navigationItem.title = @"任务表";
    }else{
        self.talentpoolViewController.jobOrdermodel  = _jobOrderModel;
        [self addChildViewController:self.taskViewController];
        [self changeControllerFromOldController:self.taskViewController toNewController:_talentpoolViewController];
//        self.navigationItem.title = @"人才库";
    }
}


#pragma mark - 切换viewController
- (void)changeControllerFromOldController:(UIViewController *)oldController toNewController:(UIViewController *)newController
{
    [self addChildViewController:newController];
    [newController didMoveToParentViewController:self];

    [self.view addSubview:newController.view];
    if (oldController) {
        [oldController willMoveToParentViewController:nil];
        [oldController removeFromParentViewController];
    }
    self.currentVC = newController;
    
    if (newController == _talentpoolViewController) {
        self.headerBkImg.image = [UIImage imageNamed:@"talentTitleBk"];
        _talentpoolViewController.view.alpha = 1;
        _taskViewController.view.alpha = 0;
        _moneyViewController.view.alpha = 0;
    }else if (newController == _taskViewController){
        self.headerBkImg.image = [UIImage imageNamed:@"taskTitleBk"];
        _talentpoolViewController.view.alpha = 0;
        _taskViewController.view.alpha = 1;
        _moneyViewController.view.alpha = 0;
    }else if (newController == _moneyViewController){
        self.headerBkImg.image = [UIImage imageNamed:@"rewardTitleBk"];
        _talentpoolViewController.view.alpha = 0;
        _taskViewController.view.alpha = 0;
        _moneyViewController.view.alpha = 1;
    }
}

@end
