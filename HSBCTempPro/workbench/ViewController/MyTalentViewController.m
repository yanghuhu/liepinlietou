//
//  MyTalentViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MyTalentViewController.h"
#import "TalentListTBCell.h"
#import "RecruitModule.h"
#import "ResumDetailView.h"
#import "JobModel.h"

@interface MyTalentViewController ()<UITableViewDelegate,UITableViewDataSource,TalentListTBCellDelegate>{

    UITableView * tableView;
}
@property (nonatomic , assign) NSInteger pageNum;
@property (nonatomic , strong) NSMutableArray * talentsArray;
@property (nonatomic , strong) ResumDetailView * resumDetailView;
@property (nonatomic , strong) TalentModel * talentModel;
@end

@implementation MyTalentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的人才库";
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationBarHeight+BatteryH, ScreenWidth, ScreenHeight-NavigationBarHeight+BatteryH)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    @weakify(self)
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum = 0;
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    [BaseHelper showProgressLoadingInView:self.view];
    [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        _pageNum ++;
    }else{
        // 下拉刷新
        _pageNum = 0;
    }
    [self getDataSource];
}

- (void)getDataSource{
    if (!_talentsArray) {
        self.talentsArray = [NSMutableArray array];
    }
    [RecruitModule getMyTalentListWithPage:_pageNum  success:^(NSArray *dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        if(dataArray && dataArray.count != 0){
            [self.talentsArray addObjectsFromArray:dataArray];
        }
        [tableView reloadData];
        if (!dataArray || dataArray.count < 10) {
            // remove add more
        }
        [tableView.mj_header endRefreshing];
    } failure:^(NSString *error, ResponseType responseType) {
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}


- (ResumDetailView *)resumDetailView{
    if (!_resumDetailView) {
        CGFloat horpadding = HorPxFit(50);
        _resumDetailView = [[ResumDetailView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-2*horpadding, 0)];
        @weakify(self);
        @weakify(_resumDetailView);
        _resumDetailView.recommendBlock = ^(void){
            @strongify(self);
            @strongify(_resumDetailView)
            if (!self || !_resumDetailView) {
                return ;
            }
            [_resumDetailView dismissAction];
            [BaseHelper showProgressLoadingInView:self.view];
            [RecruitModule talentRecommendWithTanlentId:self.talentModel.id_ orderId:self.jobOrderModel.id_ success:^{
                [BaseHelper hideProgressHudInView:self.view];
                [BaseHelper showProgressHud:@"推荐成功" showLoading:NO canHide:YES];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSString *error, ResponseType responseType) {
                [BaseHelper hideProgressHudInView:self.view];
                if (error) {
                    [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
                }
            }];
        };
    }
    return _resumDetailView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _talentsArray.count;
}

- (TalentListTBCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    TalentListTBCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[TalentListTBCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.contentView.frame = CGRectMake(0, 0, CellW, CellH);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        [cell createSbuViews];
    }
    TalentModel * model = nil;
    model = _talentsArray[indexPath.row];
    cell.talentModel = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellH;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   
   TalentModel * model = _talentsArray[indexPath.row];
   
//        [BaseHelper showProgressLoadingInView:self.view];
//        [RecruitModule getTalentDetailWithId:model.id_ success:^(TalentModel * model){
//            [BaseHelper hideProgressHudInView:self.view];
//        } failure:^(NSString *error, ResponseType responseType) {
//            [BaseHelper hideProgressHudInView:self.view];
//            [self netFailWihtError:error andStatusCode:responseType];
//        }];
    
    self.resumDetailView.jobmodel = self.jobOrderModel.jobModel;
//    [self.resumDetailView initInfoWithTalent:[HSBCGlobalInstance sharedHSBCGlobalInstance].talentModel];
                [self.resumDetailView initInfoWithTalent:model];
    [self.resumDetailView showWithSupView:nil];
   
}


- (void)phoneCall:(TalentModel *)model{
    [BaseHelper phoneCallWithNum:model.cellphone];
}


@end
