//
//  NSSafeMutableDictionary.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "NSSafeMutableDictionary.h"

@interface NSSafeMutableDictionary()

@property (strong,nonatomic) NSMutableDictionary* dict;

@end

@implementation NSSafeMutableDictionary

- (id) init{
    if (self = [super init]){
        self.dict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void) dealloc{
    self.dict = nil;
}

- (void) setObject:(id)anObject forKey:(id<NSCopying>)aKey{
    if (anObject!=nil){
        [_dict setObject:anObject forKey:aKey];
    }
    else{
        
    }
}

- (id) objectForKey:(id<NSCopying>)aKey{
    return [_dict objectForKey:aKey];
}

- (NSArray*) allKeys{
    return [_dict allKeys];
}

- (NSArray*) allValues{
    return [_dict allValues];
}

- (void) removeAllObjects{
    [_dict removeAllObjects];
}

- (void) removeObjectForKey:(NSString *)key {
    [_dict removeObjectForKey:key];
}

- (void)addEntriesFromDictionary:(NSDictionary *)dic {
    [_dict addEntriesFromDictionary:dic];
}

- (NSInteger)count
{
    return self.dict.count;
}

+ (NSSafeMutableDictionary*) dictionary{
    return [[NSSafeMutableDictionary alloc] init];
}

@end
