//
//  MapHomeViewController.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MapHomeViewController.h"
#import "MapView.h"
#import "MapFilterPopView.h"
#import "MapManage.h"
#import "MapDataPopShowView.h"

#define FilterOriginY  BatteryH

@interface MapHomeViewController ()<MapFilterPopViewDelegate,MapManageDelegate,MapViewDelegate>

@property (nonatomic , strong) MapView * mapView;
@property (nonatomic , strong) MapFilterPopView * mapFilterPopView;
@property (nonatomic , strong) MapManage * mapManage;
@property (nonatomic , strong) NSString * searchKey;
@property (nonatomic , strong) MapDataPopShowView * popShowView;


@end

@implementation MapHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"地图";
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    bt.frame = CGRectMake(0, 2, 50, 40);
    [bt setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];

    [bt addTarget:self action:@selector(showFilterPage) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:bt];
    self.navigationItem.rightBarButtonItem = item;
    self.mapView =({
        MapView * view = [[MapView alloc] initWithFrame:CGRectMake(0, NavigationBarHeight+BatteryH, ScreenWidth, ScreenHeight - ToolbarHeight - NavigationBarHeight - BatteryH)];
        view.delegate = self;
        view;
        });
    [self.view addSubview:_mapView];
    
    self.mapFilterPopView = [[MapFilterPopView alloc] initWithFrame:CGRectMake(0, FilterOriginY, ScreenWidth, NavigationBarHeight)];
    self.mapFilterPopView.delegate = self;
    [self.view addSubview:self.mapFilterPopView];
    
    self.searchKey = self.mapFilterPopView.optionArray[0];
    
    self.mapManage = [[MapManage alloc] init];
    self.mapManage.delegate = self;
    
    [BaseHelper showProgressLoadingInView:self.view];
#ifdef PhoneDebug
    // 模拟器不可定位，用户位置需指定
    [self.mapManage startSerialLocation];
#else
    [self.mapManage searchWithKey:self.searchKey];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


// 通过上下移动view来显示隐藏。隐藏于navigationbar下面
- (void)showFilterPage{
    
    CGFloat toY = 0;
    if (CGRectGetMinY(self.mapFilterPopView.frame) == FilterOriginY) {
        toY =  NavigationBarHeight+BatteryH;
    }else{
        toY = FilterOriginY;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.mapFilterPopView.frame;
        frame.origin.y = toY;
        self.mapFilterPopView.frame = frame;
    } completion:^(BOOL finished) {

    }];
}

#pragma mark -- MapFilterPopViewDelegate
// 筛选条件更改
- (void)filterOptionChoosed:(NSString *)searchKey{
    [self showFilterPage];
    self.searchKey = searchKey;
    [BaseHelper showProgressLoadingInView:self.view];
    [self.mapManage searchWithKey:self.searchKey];
}

#pragma mark -- MapManageDelegate
// map poi检索完成
- (void)poiSearchFinished:(NSArray<AMapPOI *> *)pois{
    [BaseHelper hideProgressHudInView:self.view];
    // 刷新map上的显示
    [self.mapView updateMapDataFlag:pois withType:self.searchKey];
}

// 定位完成
- (void)locationFinished:(BOOL)isLocationSucces{
    if (isLocationSucces) {
        [self.mapManage searchWithKey:self.searchKey];
    }else{
        [BaseHelper hideProgressHudInView:self.view];
    }
}

// 定位用户位置
- (void)updateUserLocation{
    if (!self.mapManage.isLocationing) {
        [BaseHelper showProgressLoadingInView:self.view];
        [self.mapManage startSerialLocation];
    }
}

#pragma maek -- MapViewDelegate
// 地图单条数据详细显示
- (void)mapDataPopShow:(MapDataModel *)model{
    self.popShowView = [[MapDataPopShowView alloc] initWithFrame:self.view.bounds withData:model];
    [self.view addSubview:self.popShowView];
    [BaseHelper animationWithView:_popShowView scale:1.1];
}


@end
