//
//  MapManage.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

@protocol MapManageDelegate <NSObject>

- (void)poiSearchFinished:(NSArray<AMapPOI *> *)pois;
- (void)locationFinished:(BOOL)isLocationSucces;

@end

@interface MapManage : NSObject


@property (nonatomic , weak) id<MapManageDelegate>delegate;
@property (nonatomic) BOOL isLocationing;

/**
 *  地图poi检索
 *  key：检索关键字，如：KTV，银行等
 */
- (void)searchWithKey:(NSString *)key;

/**
 *  开始定位
 */
-(void)startSerialLocation;

/**
 *  停止定位
 */
- (void)stopSerialLocation;

@end
