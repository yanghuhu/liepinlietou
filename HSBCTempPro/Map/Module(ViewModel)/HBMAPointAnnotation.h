//
//  HBMAPointAnnotation.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MAMapKit/MAMapKit.h>
#import "MapDataModel.h"

@interface HBMAPointAnnotation : MAPointAnnotation

// 用户自己
@property (nonatomic, assign) BOOL userAnnotation;

@property (nonatomic , strong) MapDataModel * model;

@end
