//
//  MapManage.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MapManage.h"

@interface MapManage()<AMapSearchDelegate,AMapLocationManagerDelegate>

@property (nonatomic , strong) AMapSearchAPI * mapSearchAPI;
@property (nonatomic , strong)  AMapLocationManager * locationManager;
@end

@implementation MapManage

- (void)searchWithKey:(NSString *)key{
    if (self.mapSearchAPI == nil) {
        self.mapSearchAPI = [[AMapSearchAPI alloc] init];
        self.mapSearchAPI.delegate = self;
    }
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = [AMapGeoPoint locationWithLatitude:[HSBCGlobalInstance sharedHSBCGlobalInstance].latitude longitude:[HSBCGlobalInstance sharedHSBCGlobalInstance].longitude];
    request.keywords = key;
    request.requireExtension = YES;
    request.offset = 50;
    request.radius = 5000;
    [self.mapSearchAPI AMapPOIAroundSearch:request];
}

- (void)startSerialLocation{

    if (self.locationManager == nil) {
        self.locationManager = [[AMapLocationManager alloc] init];
        [self.locationManager setDelegate:self];
        [self.locationManager setPausesLocationUpdatesAutomatically:NO];
        [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    }
    self.isLocationing = YES;
    [self.locationManager startUpdatingLocation];
}

- (void)stopSerialLocation
{
    self.isLocationing = NO;
    [self.locationManager stopUpdatingLocation];
}

#pragma mark --AMapSearchDelegate

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    NSArray * pois = response.pois;
    if (self.delegate && [self.delegate respondsToSelector:@selector(poiSearchFinished:)]) {
        [self.delegate poiSearchFinished:pois.count==0?nil:pois];
    }
}

#pragma mark --AMapLocationManagerDelegate

- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error
{
    self.isLocationing = NO;
    //定位错误
    NSLog(@"%s, amapLocationManager = %@, error = %@", __func__, [manager class], error);
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationManager)]) {
        [self.delegate locationFinished:NO];
    }
}

- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location
{
    self.isLocationing = NO;
    //定位结果
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    [HSBCGlobalInstance sharedHSBCGlobalInstance].latitude = location.coordinate.latitude;
    [HSBCGlobalInstance sharedHSBCGlobalInstance].longitude = location.coordinate.longitude;
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationFinished:)]) {
        [self.delegate locationFinished:YES];
    }
}

@end
