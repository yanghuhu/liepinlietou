//
//  MapFilterPopView.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MapFilterPopViewDelegate <NSObject>

- (void)filterOptionChoosed:(NSString *)searchKey;

@end

@interface MapFilterPopView : UIView

@property (nonatomic, strong) NSArray * optionArray;
@property (nonatomic , weak) id<MapFilterPopViewDelegate>delegate;

@end
