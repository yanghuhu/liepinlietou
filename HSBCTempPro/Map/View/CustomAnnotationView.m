//
//  CustomAnnotationView.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "CustomAnnotationView.h"

@implementation CustomAnnotationView

- (id)initWithAnnotation:(id <MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] init];
        [tap addTarget:self action:@selector(annotationTap)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)annotationTap{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapDataChoosed:)]) {
        [self.delegate mapDataChoosed:self.model];
    }
}

- (void)setModel:(MapDataModel *)model{
    _model = model;
    if ([model.type isEqualToString:@"公交站"]) {
        self.image = [UIImage imageNamed:@"bus"];
    }else if ([model.type isEqualToString:@"地铁站"]){
        self.image = [UIImage imageNamed:@"ditie"];
    }else if ([model.type isEqualToString:@"KTV"]){
        self.image = [UIImage imageNamed:@"ktv"];
    }else if ([model.type isEqualToString:@"加油站"]){
        self.image = [UIImage imageNamed:@"oil"];
    }else if ([model.type isEqualToString:@"银行"]){
        self.image = [UIImage imageNamed:@"bank"];
    }else if ([model.type isEqualToString:@"酒店"]){
        self.image = [UIImage imageNamed:@"hotel"];
    }
}

@end
