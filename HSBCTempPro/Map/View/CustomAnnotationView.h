//
//  CustomAnnotationView.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
#import "MapDataModel.h"

@protocol CustomAnnotationViewDelegate <NSObject>

- (void)mapDataChoosed:(MapDataModel *)model;

@end

@interface CustomAnnotationView : MAAnnotationView

@property (nonatomic , strong) MapDataModel * model;
@property (nonatomic , weak) id<CustomAnnotationViewDelegate>delegate;

@end
