//
//  MapFilterPopView.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MapFilterPopView.h"

#define OptionBtBaseTag 100

@interface MapFilterPopView()

@property (nonatomic, strong) UIScrollView * contentView;
@property (nonatomic) NSInteger indexSelect;


@end

@implementation MapFilterPopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView = [[UIScrollView alloc] initWithFrame:self.bounds];
        self.contentView.backgroundColor = COLOR(220, 220, 220, 1);
        self.contentView.showsHorizontalScrollIndicator = NO;
        [self addSubview:self.contentView];
        self.optionArray = @[@"公交站",@"地铁站",@"KTV",@"加油站",@"银行",@"酒店"];
        [self addFilterOptions];
    }
    return self;
}

- (void)addFilterOptions{

    CGFloat buttonW = 70;
    CGFloat paddingHor = 10;
    CGFloat paddingVer = 6;
    for (int i=0; i<self.optionArray.count; i++) {
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.frame = CGRectMake(paddingHor + (buttonW+paddingHor)*i,paddingVer, buttonW, CGRectGetHeight(self.contentView.frame)-2*paddingVer);
        bt.layer.cornerRadius = 8;
        [bt addTarget:self action:@selector(optionChoosed:) forControlEvents:UIControlEventTouchUpInside];
        bt.layer.borderWidth = 0.6;
        bt.tag = i + OptionBtBaseTag;
        bt.layer.borderColor = [UIColor grayColor].CGColor;
        bt.titleLabel.font = [UIFont systemFontOfSize:14];
        bt.titleLabel.textAlignment = NSTextAlignmentCenter;
        [bt setTitle:self.optionArray[i] forState:UIControlStateNormal];
        bt.backgroundColor = [UIColor lightGrayColor];
        [_contentView addSubview:bt];
        
        if (i == 0) {
            bt.backgroundColor = ThemeColor;
            self.indexSelect = bt.tag;
        }
    }
    CGFloat cotentW = self.optionArray.count*(buttonW+paddingHor)+paddingHor;
    self.contentView.contentSize = CGSizeMake(cotentW, CGRectGetHeight(self.contentView.frame));
}

- (void)optionChoosed:(UIButton *)bt{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(filterOptionChoosed:)]) {
        UIButton * bt_ = [_contentView viewWithTag:self.indexSelect];
        if (bt_) {
            bt_.backgroundColor = [UIColor lightGrayColor];
        }
        bt.backgroundColor = ThemeColor;
        self.indexSelect = bt.tag;
        [self.delegate filterOptionChoosed:self.optionArray[bt.tag - OptionBtBaseTag]];
    }
}

@end
