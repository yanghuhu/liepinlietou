//
//  MapDataPopShowView.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MapDataPopShowView.h"
#import "MapDataModel.h"

@interface MapDataPopShowView()

@property (nonatomic , strong) UILabel * titleLb;
@property (nonatomic , strong) MapDataModel * model;
@property (nonatomic , strong) UIControl * bkControl;
@end

@implementation MapDataPopShowView

- (instancetype)initWithFrame:(CGRect)frame withData:(MapDataModel *)model{

    self = [super initWithFrame:frame];
    if (self) {
        self.model = model;
        self.bkControl = [[UIControl alloc] initWithFrame:self.bounds];
        [self.bkControl addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.bkControl];
        [self initTitleLb];
    }
    return self;
}

- (void)initTitleLb{
    self.titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame)-60, 100)];
    self.titleLb.textColor = [UIColor blackColor];
    self.titleLb.textAlignment = NSTextAlignmentCenter;
    self.titleLb.layer.borderColor = [UIColor grayColor].CGColor;
    self.titleLb.layer.borderWidth = 0.4;
    self.titleLb.layer.cornerRadius = 6;
    self.titleLb.backgroundColor = [UIColor whiteColor];
    self.titleLb.numberOfLines = 0;
    self.titleLb.text = self.model.name;
    [self addSubview:self.titleLb];
    self.titleLb.center = self.center;
}

- (void)dismiss{
    [self removeFromSuperview];
}




@end
