//
//  MapView.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "MapView.h"
#import "HBMAPointAnnotation.h"
#import "CustomAnnotationView.h"

@interface MapView()<CustomAnnotationViewDelegate>


@end

@implementation MapView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.maMapView];
        [self createSubViews];
        
    }
    return self;
}
- (void)createSubViews{
    _maMapView = ({
        MAMapView *maMapView = [[MAMapView alloc] initWithFrame:self.bounds];
        maMapView.delegate = self;
        maMapView.showsUserLocation = YES;
        maMapView.userTrackingMode = MAUserTrackingModeFollowWithHeading;
        maMapView.showsCompass = NO;
        maMapView.showsScale = NO;
        
        [maMapView setZoomLevel:15.f animated:YES];
        maMapView.customizeUserLocationAccuracyCircleRepresentation = YES;
        maMapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:maMapView];
        maMapView;
    });
    
    {
        UIButton * _GPSButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40.f, 40.f)];
        _GPSButton.center = CGPointMake(CGRectGetMidX(_GPSButton.bounds) + 10, self.bounds.size.height -  CGRectGetMidY(_GPSButton.bounds) - 10.f);
        [_GPSButton setImage:[UIImage imageNamed:@"gpsIcon"] forState:UIControlStateNormal];
        [_GPSButton setImage:[UIImage imageNamed:@"gpsIcon_hight"] forState:UIControlStateHighlighted];
        [_GPSButton addTarget:self action:@selector(gpsAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_GPSButton];
    };

}

-(MAMapView *)maMapView{

    if (_maMapView == nil) {
            }
    return _maMapView;
}


- (void)updateMapDataFlag:(NSArray<AMapPOI *> *)pois withType:(NSString *)type{
    
    _maMapView.centerCoordinate = CLLocationCoordinate2DMake([HSBCGlobalInstance sharedHSBCGlobalInstance].latitude, [HSBCGlobalInstance sharedHSBCGlobalInstance].longitude);
    
    [_maMapView removeAnnotations:[_maMapView annotations]];
    
    HBMAPointAnnotation * annotation = [[HBMAPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake([HSBCGlobalInstance sharedHSBCGlobalInstance].latitude, [HSBCGlobalInstance sharedHSBCGlobalInstance].longitude);
    annotation.userAnnotation = YES;
    [_maMapView addAnnotation:annotation];
    
    for (int i=0; i<pois.count; i++) {
        AMapPOI * poi = pois[i];
        
        MapDataModel * model = [[MapDataModel alloc] init];
        model.name = poi.name;
        model.type = type;
        HBMAPointAnnotation * annotation = [[HBMAPointAnnotation alloc] init];
        annotation.model = model;
        annotation.userAnnotation = NO;
        annotation.coordinate = CLLocationCoordinate2DMake(poi.location.latitude, poi.location.longitude);
        [_maMapView addAnnotation:annotation];
    }
}


#pragma mark --
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[HBMAPointAnnotation class]]) {
        
        
        HBMAPointAnnotation *anno = (HBMAPointAnnotation *)annotation;
        if (anno.userAnnotation) {
            static NSString *userLocationStyleReuseIndetifier = @"userLocationStyleReuseIndetifier";
            MAAnnotationView *annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
            if (annotationView == nil)
            {
                annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
//                annotationView.image = [UIImage imageNamed:@"userLocation"];
            }
            annotationView.draggable = YES;
            annotationView.canShowCallout = NO;
            return annotationView;
        }else{
            static NSString *userLocationStyleReuseIndetifier = @"dataLocationStyleReuseIndetifier";
            CustomAnnotationView *annotationView = (CustomAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:userLocationStyleReuseIndetifier];
            if (annotationView == nil)
            {
                annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:userLocationStyleReuseIndetifier];
                annotationView.model = anno.model;
                annotationView.delegate = self;
    //            annotationView.image = [UIImage imageNamed:@"mapDataFlag"];
            }
            annotationView.draggable = YES;
            annotationView.canShowCallout = NO;

            return annotationView;
        }
    }
    return nil;
}


//#pragma mark - ==定位按钮==

- (void)addGPSButton{

      }
//
- (void)gpsAction{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateUserLocation)]) {
        [self.delegate updateUserLocation];
    }
}

#pragma mark -- CustomAnnotationViewDelegate
- (void)mapDataChoosed:(MapDataModel *)model{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapDataPopShow:)]) {
        [self.delegate mapDataPopShow:model];
    }
}

@end
