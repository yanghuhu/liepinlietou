//
//  MapView.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "MapDataModel.h"

@protocol MapViewDelegate <NSObject>

- (void)mapDataPopShow:(MapDataModel *)model;
- (void)updateUserLocation;
@end

@interface MapView : UIView<MAMapViewDelegate>

@property (nonatomic , strong) MAMapView * maMapView;
@property (nonatomic , weak) id<MapViewDelegate>delegate;

- (void)updateMapDataFlag:(NSArray<AMapPOI *> *)pois withType:(NSString *)type;

@end
