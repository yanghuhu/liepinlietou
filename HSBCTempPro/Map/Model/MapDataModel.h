//
//  MapDataModel.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapDataModel : NSObject

@property (nonatomic , strong) NSString * name;
@property (nonatomic , strong) NSString * type;

@end
