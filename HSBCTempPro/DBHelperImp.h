//
//  DBHelperImp.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "NSSafeMutableDictionary.h"

typedef enum {
    kDBSortByNormal,
    kDBSortByAsc,
    kDBSortByDesc,
}DBSortByType;


@interface DBHelperImp : NSObject

@property (nonatomic, strong) UserModel *role;


- (id)initWithDataBase:(FMDatabase *)db andDBQueue:(FMDatabaseQueue *)dbQueue;

/**
 *  登录角色时初始化
 *
 *  @param role 当前角色
 *
 */
- (id)initWithRole:(UserModel *)role;

/**
 *  没有登录时初始化
 */
- (id)initWithoutRole;

- (BOOL)createTable:(NSString *)tableName para:(NSSafeMutableDictionary *)dict;

- (BOOL)dropTable:(NSString *)tableName;

- (NSString*) whereStrByAttrAndValue:(NSSafeMutableDictionary *)attrAndValue;

/**
 *  是否存在该记录
 *
 *  @param tableName             表名
 *  @param whereColumnNameAndVal 条件字典
 *
 *  @return YES：存在
 */
- (BOOL)isExistDataTableName:(NSString *)tableName whereColumnNameAndVal:(NSSafeMutableDictionary *)whereColumnNameAndVal;

- (NSArray*) fetchDataFromTable:(NSString *)tableName
                   attrAndValue:(NSSafeMutableDictionary *)attrAndValue
                      sortField:(NSString *)sortField
                         sortBy:(DBSortByType)sortByType
               convertDataBlock:(NSArray *(^)(FMResultSet *fs))convertDataBlock;

- (NSArray*) fetchDataFromTable:(NSString *)tableName
                   attrAndValue:(NSSafeMutableDictionary *)attrAndValue
                      sortField:(NSString *)sortField
                         sortBy:(DBSortByType)sortByType
                          limit:(NSInteger)limit
               convertDataBlock:(NSArray *(^)(FMResultSet *fs))convertDataBlock;

- (NSArray*) fetchDataFromTableStr:(NSString *)tableStr
                          whereSQL:(NSString *)whereSQL
                         sortField:(NSString *) sortField
                            sortBy:(DBSortByType)sortByType
                         converter:(NSArray *(^)(FMResultSet *fs))converter;

- (NSArray*) fetchDataFromTableStr:(NSString *)tableStr
                          whereSQL:(NSString *)whereSQL
                         sortField:(NSString *) sortField
                            sortBy:(DBSortByType)sortByType
                             limit:(NSInteger)limit
                         converter:(NSArray *(^)(FMResultSet *fs))converter;

- (NSArray*) fetchDataWithSql:(NSString *)sql
                    converter:(NSArray *(^)(FMResultSet *fmResultSet))converter;

- (BOOL) delectDataFromTable:(NSString *)tableName
                attrAndValue:(NSSafeMutableDictionary *)attrAndValue;

- (BOOL) delectDataFromTable:(NSString *)tableName
                    whereStr:(NSString *)whereStr;

- (BOOL) deleteAllDataFromTable:(NSString *)tableName;

- (BOOL) updateDataToTable:(NSString *)tableName attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions;

- (BOOL) updateDataToTable:(NSString *)tableName attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions addCountRows:(NSArray *)addCountRows;

- (BOOL)updateDataToTable:(NSString *)tableName fmdb:(FMDatabase *)db attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions  addCountRows:(NSArray *)addCountRows;

/**
 *  执行SQL语句
 *
 *  @param sql       可参数化的sql语句
 *  @param arguments 参数化的sql语句中的参数值，注意其顺序要与?这种参数化符号一致
 */
- (void)excuteSql:(NSString *)sql withArgumentsInArray:(NSArray *)arguments;

- (void) addRowTo:(NSString *)tableName
         withName:(NSString *)rowName
          andAttr:(NSString *)rowAttr;

- (BOOL)insertDataToTable:(NSString *)tableName
             attrAndValue:(NSSafeMutableDictionary *)attrAndValue
          restrictAttrArr:(NSArray *)rAttrStrArr
                overwrite:(BOOL)overwrite;

- (void)batchInserDataToTable:(NSString *)tableName
                attrAndValues:(NSArray *)attrAndValues;

/**
 *  批量插入或保存数据
 *
 *  @param tableName               表名
 *  @param keyFieldNames           表唯一键名数组(注意：当该值为空是不会执行更新操作)
 *  @param attrAndValues           该实体的数组 或者 表的所有字段的名称与其值的字典数组
 *  @param excludeUpdateFieldNames 要排除更新的字段名称
 */
- (void)batchSaveWithTableName:(NSString *)tableName
                 keyFieldNames:(NSArray *)keyFieldNames
                 attrAndValues:(NSArray *)attrAndValues
       excludeUpdateFieldNames:(NSArray *)excludeUpdateFieldNames;

- (void)batchSaveWithTableName:(NSString *)tableName
                 keyFieldNames:(NSArray *)keyFieldNames
                 attrAndValues:(NSArray *)attrAndValues
                  addCountRows:(NSArray *)addCounts
       excludeUpdateFieldNames:(NSArray *)excludeUpdateFieldNames;

- (NSDate*) topUpdateTimeInTable:(NSString *)tableName
                    dateAttrName:(NSString*) dateAttrName
               whereAttrAndValue:(NSSafeMutableDictionary *)attrAndValue
                     isAscending:(BOOL)isAscending;

- (BOOL)excuteSql:(NSString *)sql;

- (NSInteger)countRowToTable:(NSString *)tableName
                    andWhere:(NSString *)where;

- (NSInteger)sumRowToTable:(NSString *)tableName
                       row:(NSString *)row
                     where:(NSString *)where;

@end
