//
//  UserModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/10/26.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

+ (NSDictionary *)modelCustomPropertyMapper {

    return @{@"uid":@"id"};
}

@end
