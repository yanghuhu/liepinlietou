//
//  UserModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/10/26.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic , strong) NSString * nickName;
@property (nonatomic, assign) NSInteger  uid;   // 用户id
@property (nonatomic , strong) NSString * cellphone;  // 手机号
@property (nonatomic, strong) NSString * headPic;     //用户头像
@property (nonatomic , assign) NSInteger evaluateCount;  // 总评价次数
@property (nonatomic , assign) CGFloat score;

@property (nonatomic , strong) NSString * netToken;  //  当前登录用户的网络请求的token

@property (nonatomic , assign) NSInteger grantBalance;   //  赠送余额
@property (nonatomic , assign) NSInteger balance;  // 账户余额

@end
