//
//  BaseTable.h
//  JLGProject
//
//  Created by Michael on 2017/3/18.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "DBHelper.h"
#import "BaseHelper.h"
#import "NSObject+YYModel.h"

#define PRIME_KEY_FLAG(ATTR_TYPE) [ATTR_TYPE stringByAppendingString:@" PRIMARY KEY"]
#define AUTO_PRIME_KEY_FLAG(ATTR_TYPE) [ATTR_TYPE stringByAppendingString:@" PRIMARY KEY AUTOINCREMENT"]

#define kByteType                   @"BYTE"
#define kVarCharType(LENGTH)        [NSString stringWithFormat:@"VARCHAR(%d)",LENGTH]
#define kNVarCharType(LENGTH)       [NSString stringWithFormat:@"NVARCHAR(%d)",LENGTH]
#define kAutoIntType                @"INTEGER PRIMARY KEY AUTOINCREMENT"
#define kIntType                    @"INTEGER"
#define kUrlType                    kVarCharType(256)
#define kIsReadedType               [kIntType stringByAppendingString:@" DEFAULT(0)"]
#define kDateType                   kVarCharType(25)
#define kTextType                   @"TEXT"


#import <Foundation/Foundation.h>

@protocol BaseTableProtocol

- (BOOL)createTableIfNotExist;
- (void)updateTableAccordingToDbVersion;

@end

@interface BaseTable : NSObject

@property (nonatomic, strong) NSString *tableName;

- (BOOL)cleanTable;

- (BOOL)cleanTableImp:(DBHelperImp *)imp;

- (NSInteger)currentTableVersion;

- (void)setCurrentTableVersion:(NSInteger)version;

- (void)createAndUpgradeTable;

/**
 *  获取公共数据库
 *
 *  @return DBHelperImp
 */
- (DBHelperImp *)getPublicDb;

- (NSString *)whereContainWithDictionary:(NSDictionary *)dictionary;
@end
