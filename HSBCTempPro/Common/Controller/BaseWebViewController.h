//
//  BaseWebViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/2.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
typedef NS_ENUM(NSUInteger, JsCallAppUrlTypeEnum){
    
    /**
     *  不支持这个功能
     */
    JsCallAppUrlTypeEnum_NotFound,
    /**
     *  调用相册
     */
    JsCallAppUrlTypeEnum_CallPhotoFromAlbum,
    /**
     *  调用相机或者相册
     */
    JsCallAppUrlTypeEnum_CallPhotoFromAlbumOrCamera,
    /**
     *  只有相机
     */
    JsCallAppUrlTypeEnum_OnlyCamera,
    /**
     **
     *  弹出框
     */
    JsCallAppUrlTypeEnum_AlertView,
    /**
     *  分享
     */
    JsCallAppUrlTypeEnum_Share,
    /**
     *  改变导航栏右边的按钮文本
     */
    JSCallNavigationItemText,
    /**
     *  跳转到某个页面
     */
    JsCallNavigationToSomeViewControllerUrl,
};

@interface BaseWebViewController : UIViewController

@property (nonatomic, strong) UIWebView *webViewController;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, assign) CGSize webViewSize;

/**
 *  强制显示这个title，若提供这个属性的话
 */
@property (nonatomic, strong) NSString *forceTitle;
//@property (nonatomic ,assign) EnumWebViewType webViewType;
@property (nonatomic, strong) JSContext *context;

@end
