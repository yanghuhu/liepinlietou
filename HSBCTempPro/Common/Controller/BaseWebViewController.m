//
//  BaseWebViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/2.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "BaseWebViewController.h"

@interface BaseWebViewController ()<UIWebViewDelegate>

@end

@implementation BaseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    if (_urlString) {
        [self.webViewController loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
    }
    self.webViewSize = CGSizeZero;
    [self.view addSubview:self.webViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIWebView *)webViewController{
    if (!_webViewController) {
        self.webViewSize = CGSizeEqualToSize(self.webViewSize,CGSizeZero)?CGSizeMake(ScreenWidth, ScreenHeight-BatteryH-NavigationBarHeight) : self.webViewSize;
        _webViewController = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.webViewSize.width, self.webViewSize.height)];
        _webViewController.scalesPageToFit = YES;
        _webViewController.delegate = self;
        _webViewController.backgroundColor = [UIColor clearColor];
    }
    return _webViewController;
}

- (void)reFreshWebview{
    if (_urlString) {
        [self.webViewController loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
    }
}

@end
