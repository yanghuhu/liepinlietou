//
//  BaseViewController.h
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+YYModel.h"
#import "MJRefresh.h"

typedef enum {
    TableViewDataSourceChangeForRefresh = 0,
    TableViewDataSourceChangeForGetMore,
}TableViewDataSourceChange;   // tableview数据源变化的两种情况

@interface BaseViewController : UIViewController

- (void)createViews;
- (void)loadData;
// 网络错误处理
- (void)netFailWihtError:(NSString *)error andStatusCode:(NSInteger)statusCode;
@end
