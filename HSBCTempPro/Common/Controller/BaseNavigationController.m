//
//  BaseNavigationController.m
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

// 第一次初始化类之前调用且只调用一次
+ (void)initialize {
    [self setupNavigationBarTheme];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
 * 1.隐藏底部tabbar
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (animated) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

// 个性化tabbar样式
+ (void)setupNavigationBarTheme
{
    UINavigationBar *bar = [UINavigationBar appearance];
    bar.barTintColor = ViewControllerBkColor;
    bar.backgroundColor = ViewControllerBkColor;
    NSDictionary *titleAtrr = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],NSForegroundColorAttributeName:RGB16(0Xffffff)};
    [bar setTitleTextAttributes:titleAtrr]; 
}

@end
