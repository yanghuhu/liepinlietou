//
//  PlaceholdeViewController.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PlaceholdeViewController.h"
#import "DBHelper.h"
#import "SettingViewController.h"
#import "ThirdLoginManage.h"

@interface PlaceholdeViewController ()

@end

@implementation PlaceholdeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    bt.frame =CGRectMake(100, 100, 60, 40);
    [bt addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    bt.backgroundColor = [UIColor redColor];
    [self.view addSubview:bt];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)action{
    SettingViewController * vc  = [[SettingViewController alloc] init];
    [BaseHelper windowChangeRootViewController:vc];
}

- (void)login{
//    [ThirdLoginManage loginWithType:ThirdLoginTypeSinaWeibo];
}

@end
