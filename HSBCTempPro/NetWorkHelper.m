
//  NetWorkHelper.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/8.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "NetWorkHelper.h"
#import <AFNetworking/AFNetworking.h>
#import "Reachability.h"
#import "NSDictionary+YYAdd.h"
#import "NSObject+YYModel.h"

#define kAlertLogout 1
#define kAlertExperenceExpired 2

#define Net_Work_Lost_Text                  @"The network connection was lost"
#define Net_Work_Lost_Text_                 @"似乎已断开与互联网的连接"
#define Net_Work_Lost_Text_CN               @"网络异常，请检查网络连接或稍后再试"

#define Net_Work_Timeout_Text               @"The request timed out"
#define Net_Work_Timeout_Text_              @"请求超时"
#define Net_Work_Timeout_Text_CN            @"请求超时，请稍后再试"

#define Net_Work_Not_Completed_Text         @"The operation couldn’t be completed"
#define Net_Work_Not_Completed_CN_Text      @"未能完成操作"

@interface NetWorkHelper() < UIAlertViewDelegate >
{
    //    RequestSuccessBlock _checkNetworkAvailabeCompleteBlock;  // 检查网络完成block
    //    BaseAlertView *_alertView;
}

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) NetworkHandle *networkHandle;
@property (nonatomic, strong) Reachability *reach;
@property (nonatomic, assign) NetworkStatus oldReachStatus;

@end

@implementation NetWorkHelper

+ (NetWorkHelper *)sharedNetworkHelper {
    static NetWorkHelper *sharedNetworkHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedNetworkHelper = [[self alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:sharedNetworkHelper
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        
        sharedNetworkHelper.reach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
        [sharedNetworkHelper.reach startNotifier];
    });
    return sharedNetworkHelper;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _sessionManager = [AFHTTPSessionManager manager];
        _networkHandle = [[NetworkHandle alloc] init];
    }
    return self;
}

- (BOOL)isNetworkValid{
    NetworkStatus  status = [self.reach currentReachabilityStatus];
    if (status == NotReachable) {
        return NO;
    }
    return  YES;
}


- (void)reachabilityChanged:(NSNotification *)note
{
    if (!self.reach) {
        return;
    }
    NetworkStatus status = [self.reach currentReachabilityStatus];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    if (status == NotReachable) {
        if (self.oldReachStatus != NotReachable) {
            [BaseHelper showProgressHud:@"您的网络已经断开" showLoading:NO canHide:YES];
        }
    } else if (status == ReachableViaWWAN) {
        if (_oldReachStatus != ReachableViaWWAN) {
            [BaseHelper showMultiLineProgressHub:@"您正在使用蜂窝数据上网，请注意流量使用"];
        }
    } else {
        if (self.oldReachStatus == ReachableViaWWAN) {
            [BaseHelper showProgressHud:@"您已切换到WiFi网络" showLoading:NO canHide:YES];
        }
    }
    self.oldReachStatus = status;
}

+ (NSString *)urlForModule:(NSString *)module {
    return nil;
//    return [NSString stringWithFormat:@"%@%@%@", [DataConfig GET_BASE_URL], [DataConfig GET_BASE_PATH], module];
}

+ (NSURLSessionDataTask *)getRequestForApi:(NSString *)Api
                                    params:(NSDictionary *)params
                                    handle:(NetworkHandleBlock)handle
                                   success:(RequestSuccessBlock)success
                                   failure:(RequestFailureBlock)failure{
    
    NSMutableString * url = [NSMutableString stringWithFormat:@"%@%@",SeverUrl,Api];
    if (params && [params allKeys].count != 0) {
        [url appendString:@"?"];
    }
    NSArray * allKey = [params allKeys];
    for (int i=0;i<allKey.count;i++) {
        NSString * key = allKey[i];
        [url appendFormat:@"%@=%@",key,params[key]];
        if (i != allKey.count-1) {
            [url appendString:@"&"];
        }
    }
    return [[NetWorkHelper sharedNetworkHelper] getRequestForUrl:url params:params handle:handle success:success failure:failure];
}

- (NSURLSessionDataTask *)getRequestForUrl:(NSString *)url
                                    params:(NSDictionary *)params
                                    handle:(NetworkHandleBlock)handle
                                   success:(RequestSuccessBlock)success
                                   failure:(RequestFailureBlock)failure{
    
//    NSString *url = SeverUrl;
    if (![self isNetworkValid]) {
        [_networkHandle ExcuteOnMainTreadWithBlock:^{
            if (failure) {
                failure(@"当前网络不可用", kResponseTypeNetWorkError);
            }
        }];
        return nil;
    }
#ifdef DEBUG
    DLog(@"\n 请求地址:%@ \n=====================\n", url);
    DLog(@"\n 请求字段:%@ \n=====================\n", [params jsonStringEncoded]);
#endif
    
    AFHTTPSessionManager * sessionManage = self.sessionManager;
    sessionManage.requestSerializer = [AFHTTPRequestSerializer serializer];
    sessionManage.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    sessionManage.requestSerializer.timeoutInterval = 30.0f;
    sessionManage.responseSerializer = [AFHTTPResponseSerializer serializer];
    sessionManage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"text/html",@"text/txt", @"application/json",@"application/octet-stream",@"application/x-www-form-urlencoded",nil];
    
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken) {
        [sessionManage.requestSerializer setValue:[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken forHTTPHeaderField:@"X-UserToken"];
    }
    NSURLSessionDataTask * task = [sessionManage GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(_networkHandle.isMock && !responseObject)
        {
            return;
        }
        NSDictionary *responseDic =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [_networkHandle ExcuteOnBlackGroundWithBlock:^{
#ifdef DEBUG
            if ([responseDic isKindOfClass:[NSDictionary class]]) {
                DLog(@"\n 响应数据（转成Josn字符串）:%@ \n=====================\n", [responseDic jsonStringEncoded]);
            }else{
                DLog(@"\n 响应数据:%@ \n=====================\n", responseDic);
            }
#endif
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                if (!responseDic) {  // 某些操作只需要知道是否成功的情况下，server不返回数据。
                    success(@(YES));
                    return ;
                }
            }];
            
            NSString *errorMsgFromHandle = nil;
            if (handle) {
                NSArray *result;
                @try {
                    result = handle(responseDic);
                }
                @catch (NSException *exception) {
                    DLog(@"exception:%@", exception.description);
                    if (failure) {
                        [_networkHandle ExcuteOnMainTreadWithBlock:^{
                            failure(@"读取数据异常", kResponseTypeDataParseError);
                        }];
                    }
                    return;
                }
                BOOL isSuccess = [result count] > 0 && [[result objectAtIndex:0] boolValue];
                if (isSuccess) {
                    if (success && [result count] >= 2) {
                        [_networkHandle ExcuteOnMainTreadWithBlock:^{
                            @try {
                                switch (result.count) {
                                    case 3:
                                        success([result objectAtIndex:1],[result objectAtIndex:2]);
                                        break;
                                    case 4:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3]);
                                        break;
                                    case 5:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3],[result objectAtIndex:4]);
                                        break;
                                    case 6:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3],[result objectAtIndex:4],[result objectAtIndex:5]);
                                        break;
                                    default:
                                        success([result objectAtIndex:1],nil);
                                        break;
                                }
                            }
                            @catch (NSException *exception) {
                                DLog(@"exception: %@",exception);
                                //不能直接调failure
                                //                                if (failure) {
                                //                                    failure(@"读取数据异常", kResponseTypeDataError);
                                //                                }
                                return;
                            }
                        }];
                        return;
                    }
                }else {
                    errorMsgFromHandle = [result count] >= 2 ? [result objectAtIndex:1] : @"数据加载失败";
                }
            }
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                if (failure) {
                    failure([errorMsgFromHandle length] > 0 ? errorMsgFromHandle : nil, kResponseTypeDataParseError);
                }
            }];
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"%@",[error description]);
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        int statusCode = (int)response.statusCode;
        NSDictionary * dic = response.allHeaderFields;
        DLog(@"respose headers:%@",dic);
        NSString * errorMsg = dic[@"X-alert"];
        NSString * errorMsg_ = [errorMsg stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if (errorMsg_) {
            if (failure) {
                [_networkHandle ExcuteOnMainTreadWithBlock:^{
                    failure(errorMsg_,statusCode);
                }];
                return ;
            }
        }else if(statusCode > 399){
            errorMsg_ = [NSString stringWithFormat:@"Network error: %d",statusCode];
            if (failure) {
                [_networkHandle ExcuteOnMainTreadWithBlock:^{
                    failure(errorMsg_,statusCode);
                }];
                return ;
            }
        }
        
        NSString * errorDesc = [error description];
        if ([errorDesc containsString:@"请求超时"] || [errorDesc containsString:@"似乎已断开与互联网的连接"] || [errorDesc containsString:@"time out"] || [errorDesc containsString:@"connection was lost"] || [errorDesc containsString:@"1009"]) {
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                [BaseHelper showProgressHudWithText:[self handleErrorResponse:errorDesc]];
                if (failure) {
                    failure(@"网络请求超时，请稍后重试", kResponseTypeNetWorkTimeout);
                }
            }];
            return;
        }
    }];
    return task;
}

+ (NSURLSessionDataTask *)postRequestForApi:(NSString *)Api
                                      params:(NSDictionary *)params
                                      handle:(NetworkHandleBlock)handle
                                     success:(RequestSuccessBlock)success
                                     failure:(RequestFailureBlock)failure {
    return [[NetWorkHelper sharedNetworkHelper] postRequestForApi:Api params:params handle:handle success:success failure:failure];
}

- (NSURLSessionDataTask *)postRequestForApi:(NSString *)api
                                    params:(NSDictionary *)params
                                    handle:(NetworkHandleBlock)handle
                                   success:(RequestSuccessBlock)success
                                   failure:(RequestFailureBlock)failure{

    NSString *url = [NSString stringWithFormat:@"%@%@",SeverUrl,api];
    if (![self isNetworkValid]) {
        [_networkHandle ExcuteOnMainTreadWithBlock:^{
            if (failure) {
                failure(@"当前网络不可用", kResponseTypeNetWorkError);
            }
        }];
        return nil;
    }
#ifdef DEBUG
        DLog(@"\n 请求地址:%@ \n=====================\n", url);
        DLog(@"\n 请求字段:%@ \n=====================\n", [params jsonStringEncoded]);
#endif
    
    AFHTTPSessionManager * sessionManage = self.sessionManager;
    sessionManage.requestSerializer = [AFJSONRequestSerializer serializer];
    sessionManage.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    sessionManage.requestSerializer.timeoutInterval = 30.0f;
    sessionManage.responseSerializer = [AFHTTPResponseSerializer serializer];
    sessionManage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"text/html",@"text/txt", @"application/json",@"application/octet-stream",@"application/x-www-form-urlencoded",nil];
    
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken) {
        [sessionManage.requestSerializer setValue:[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken forHTTPHeaderField:@"X-UserToken"];
    }
    
    NSURLSessionDataTask * task = [sessionManage POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //当responseObject为nil时直接return。
        if(_networkHandle.isMock && !responseObject)
        {
            return;
        }
        NSDictionary *responseDic =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        [_networkHandle ExcuteOnBlackGroundWithBlock:^{
#ifdef DEBUG
            if ([responseDic isKindOfClass:[NSDictionary class]]) {
                DLog(@"\n 响应数据（转成Josn字符串）:%@ \n=====================\n", [responseDic jsonStringEncoded]);
            }else{
                DLog(@"\n 响应数据:%@ \n=====================\n", responseDic);
            }
#endif
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                if (!responseDic) {  // 某些操作只需要知道是否成功的情况下，server不返回数据。
                    success(@(YES));
                    return ;
                }
            }];
            if (!responseDic) {  // 某些操作只需要知道是否成功的情况下，server不返回数据。
                return ;
            }
            NSString *errorMsgFromHandle = nil;
            if (handle) {
                NSArray *result;
                @try {
                    result = handle(responseDic);
                }
                @catch (NSException *exception) {
                    DLog(@"exception:%@", exception.description);
                    if (failure) {
                        [_networkHandle ExcuteOnMainTreadWithBlock:^{
                            failure(@"读取数据异常", kResponseTypeDataParseError);
                        }];
                    }
                    return;
                }
                BOOL isSuccess = [result count] > 0 && [[result objectAtIndex:0] boolValue];
                if (isSuccess) {
                        [_networkHandle ExcuteOnMainTreadWithBlock:^{
                            @try {
                                switch (result.count) {
                                    case 2:
                                        success([result objectAtIndex:1]);
                                        break;
                                    case 3:
                                        success([result objectAtIndex:1],[result objectAtIndex:2]);
                                        break;
                                    case 4:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3]);
                                        break;
                                    case 5:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3],[result objectAtIndex:4]);
                                        break;
                                    case 6:
                                        success([result objectAtIndex:1],[result objectAtIndex:2],[result objectAtIndex:3],[result objectAtIndex:4],[result objectAtIndex:5]);
                                        break;
                                    default:
                                        success(nil);
                                        break;
                                }
                            }
                            @catch (NSException *exception) {
                                DLog(@"exception: %@",exception);
//不能直接调failure
//                                if (failure) {
//                                    failure(@"读取数据异常", kResponseTypeDataError);
//                                }
                                return;
                            }
                        }];
                        return;
                }else {
                    errorMsgFromHandle = [result count] >= 2 ? [result objectAtIndex:1] : @"数据加载失败";
                }
            }
            
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                if (failure) {
                    failure([errorMsgFromHandle length] > 0 ? errorMsgFromHandle : nil, kResponseTypeDataParseError);
                }
            }];
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"%@",[error description]);
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        int statusCode = (int)response.statusCode;
        NSDictionary * dic = response.allHeaderFields;
        DLog(@"respose headers:%@",dic);
        NSString * errorMsg = dic[@"X-alert"];
        NSString * errorMsg_ = [errorMsg stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if (errorMsg_) {
            if (failure) {
                [_networkHandle ExcuteOnMainTreadWithBlock:^{
                    failure(errorMsg_,statusCode);
                }];
                return ;
            }
        }else if(statusCode > 399){
            errorMsg_ = [NSString stringWithFormat:@"Network error: %d",statusCode];
            if (failure) {
                [_networkHandle ExcuteOnMainTreadWithBlock:^{
                    failure(errorMsg_,statusCode);
                }];
                return ;
            }
        }else{
            errorMsg_ = [NSString stringWithFormat:@"Network error: %d",statusCode];
            if (failure) {
                [_networkHandle ExcuteOnMainTreadWithBlock:^{
                    failure(errorMsg_,statusCode);
                }];
                return ;
            }
        }
        
        NSString * errorDesc = [error description];
        if ([errorDesc containsString:@"请求超时"] || [errorDesc containsString:@"似乎已断开与互联网的连接"] || [errorDesc containsString:@"time out"] || [errorDesc containsString:@"connection was lost"] || [errorDesc containsString:@"1009"] ) {
            [_networkHandle ExcuteOnMainTreadWithBlock:^{
                [BaseHelper showProgressHudWithText:[self handleErrorResponse:errorDesc]];
                if (failure) {
                    failure(@"网络请求超时，请稍后重试", kResponseTypeNetWorkTimeout);
                }
            }];
            return;
        }
    }];
    
    return task;
}

- (NSString *)handleErrorResponse:(NSString *)errorStr
{
    if ([errorStr containsString:Net_Work_Lost_Text] || [errorStr containsString:Net_Work_Lost_Text_] ) {
        return Net_Work_Lost_Text_CN;
    }else if ([errorStr containsString:Net_Work_Timeout_Text] || [errorStr containsString:Net_Work_Timeout_Text_]) {
        return Net_Work_Timeout_Text_CN;
    }
    return errorStr;
}

+ (void)requestForUploadImageSync:(UIImage *)image
                               withModule:(NSString *)module
                                  success:(RequestSuccessBlock)success
                                  failure:(RequestFailureBlock)failure{
    
    [[NetWorkHelper sharedNetworkHelper] requestForUploadImageSync:image withModule:module success:success failure:failure];
}

+ (void)requestForUploadFileSync:(NSString *)path
                         success:(RequestSuccessBlock)success
                         failure:(RequestFailureBlock)failure{
    
    [[NetWorkHelper sharedNetworkHelper] requestForUploadFileSync:path withApi:nil success:success failure:failure];
}

+ (void)updateVideoWithvideo:(NSURL *)video
                      module:(NSString *)module
                     Success:(RequestSuccessBlock)successBlock
                     Failure:(RequestFailureBlock)failBlock
{
     [[NetWorkHelper sharedNetworkHelper] updateVideoWithvideo:video module:module Success:successBlock Failure:failBlock];
}

- (void)updateVideoWithvideo:(NSURL *)video
                    module:(NSString *)module
                    Success:(RequestSuccessBlock)successBlock
                    Failure:(RequestFailureBlock)failBlock{

    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 100000;
    
    if (![self isNetworkValid]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [BaseHelper showProgressHud:@"当前网络不可用" showLoading:NO canHide:YES];
        });
        return;
    }
    
    NSData *videoData = [NSData dataWithContentsOfURL:video];
;
    while ([videoData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.1;
        videoData =  [NSData dataWithContentsOfURL:video];
    }

    DLog(@"videoData %@",videoData);

    NSString *url =SeverUrl;
    
#ifdef DEBUG
    DLog(@"\n 请求地址:%@ \n=====================\n", url);
#endif
    
    AFHTTPSessionManager * sessionManage = self.sessionManager;
    sessionManage.requestSerializer = [AFJSONRequestSerializer serializer];
    [sessionManage.requestSerializer setValue:@"IOS" forHTTPHeaderField:@"device"];
 
    sessionManage.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    sessionManage.requestSerializer.timeoutInterval = 30.0f;
    sessionManage.responseSerializer = [AFJSONResponseSerializer serializer];
    sessionManage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"text/html",@"text/txt", @"application/json",nil];//,
    
    NSDictionary * dic = @{@"cmd":@"100003",@"module":module,@"device":@"IOS"};
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param addEntriesFromDictionary:dic];
    DLog(@"\n 请求字段:%@ \n=====================\n", param);
    
    [sessionManage POST:url parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:videoData name:@"video" fileName:@"video.mp4" mimeType:@"video/mp4"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *responseDic = responseObject;
        DLog(@"上传视频返回：%@",responseDic);
        if([responseDic[@"status"] intValue] != 1){
                // 上传失败
        }else {
            NSDictionary * info = responseDic[@"data"];
            DLog(@"info %@",info);
                // 上传成功
        }
       successBlock(nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        DLog(@"url:%@url \n error:%@", url, error);
        NSString *errorDesc = error.localizedDescription;
        if (failBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([error code] != NSURLErrorCancelled) {
                    failBlock(errorDesc, 0);
                }
            });
        }
    }];
}

- (void)requestForUploadImageSync:(UIImage *)image
                       withModule:(NSString *)module
                          success:(RequestSuccessBlock)success
                          failure:(RequestFailureBlock)failure{
    
    if (![self isNetworkValid]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [BaseHelper showProgressHud:@"当前网络不可用" showLoading:NO canHide:YES];
        });
        return;
    }
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.7);
    NSString *url = @"http://120.78.184.120:9002/ImageUpload/ImageUp";

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    //formData: 专门用于拼接需要上传的数据,在此位置生成一个要上传的数据体
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/png"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功 %@", responseObject);
        [_networkHandle ExcuteOnMainTreadWithBlock:^{
            success(responseObject);
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"url:%@url \n error:%@", url, error);
        NSString *errorDesc = error.localizedDescription;
        if (failure) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([error code] != NSURLErrorCancelled) {
                    failure(errorDesc, 0);
                }
            });
        }
        NSLog(@"上传失败 %@", error);
    }];
}

- (void)requestForUploadFileSync:(NSString *)filePath
                       withApi:(NSString *)api
                          success:(RequestSuccessBlock)success
                          failure:(RequestFailureBlock)failure{
    
    if (![self isNetworkValid]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [BaseHelper showProgressHud:@"当前网络不可用" showLoading:NO canHide:YES];
        });
        return;
    }
    NSData *fileData  = [NSData dataWithContentsOfFile:filePath];
    NSString *url = [NSString stringWithFormat:@"%@%@",SeverUrl,@"human/importResume"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken) {
        [manager.requestSerializer setValue:[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken forHTTPHeaderField:@"X-UserToken"];
    }
    
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSString *  fileName = [filePath lastPathComponent];
//        fileName = [fileName stringByDeletingPathExtension];
        NSString * fileType = [NSString stringWithFormat:@"application/%@",[filePath pathExtension]];
        [formData appendPartWithFileData:fileData name:@"file" fileName:fileName?fileName:@"a" mimeType:fileType];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"上传成功 %@", responseObject);
        [_networkHandle ExcuteOnMainTreadWithBlock:^{
            success(responseObject);
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        DLog(@"url:%@url \n error:%@", url, error);
        NSString *errorDesc = error.localizedDescription;
        if (failure) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([error code] != NSURLErrorCancelled) {
                    failure(errorDesc, 0);
                }
            });
        }
        NSLog(@"上传失败 %@", error);
    }];
}



- (void)dealloc {
    [self.reach stopNotifier];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
