//
//  AppDelegate.h
//  HSBCTempPro
//
//  Created by Michael on 2017/10/26.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showLocalResumeVC;

@end

