//
//  GradeView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradeView : UIView

@property (nonatomic , assign) CGFloat source;

- (instancetype)initWithHeight:(CGFloat)height sourece:(CGFloat)source;

- (void)gradeWithScore:(CGFloat)score canOperate:(BOOL)canOperate;

@end
