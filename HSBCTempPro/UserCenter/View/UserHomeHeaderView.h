//
//  UserHomeHeaderView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  UserHomeHeaderViewDelegate
- (void)toUserInfoEditVC;
@end

@interface UserHomeHeaderView : UIView

@property (nonatomic , weak) id<UserHomeHeaderViewDelegate>delegate;

- (void)updateInfoAppear;

@end
