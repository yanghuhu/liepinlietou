//
//  UserInfoEditView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserInfoEditView.h"

@interface UserInfoEditView()<UITextFieldDelegate>{
 
    NSString * nickName;
    
    UITextField * nickTextField;
}
@end

@implementation UserInfoEditView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}


- (void)createSubViews{
    
    UILabel * nickNameLb = [[UILabel alloc] init];
    nickNameLb.font = [UIFont systemFontOfSize:15];
    nickNameLb.text = @"昵称:";
//    nickNameLb.backgroundColor = [UIColor orangeColor];
    [self addSubview:nickNameLb];
    [nickNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(60));
        make.top.equalTo(self).mas_offset(VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(50, VerPxFit(60)));
    }];
    
    nickTextField = [self nickTextField];
    NSString * nickName = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.nickName;
    nickTextField.text = nickName?nickName:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    [nickTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nickNameLb.mas_right);
        make.centerY.mas_equalTo(nickNameLb.mas_centerY).mas_offset(VerPxFit(-5));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(450), VerPxFit(50)));
    }];
}

- (NSString *)nickName{
    return nickTextField.text;
}

- (UITextField *)nickTextField{
    UITextField * tf = [UITextField new] ;
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    [self addSubview:tf];
//    
    UIView * bottomLine = [[UIView alloc] init];
    bottomLine.backgroundColor = COLOR(220, 200, 200, 1);
    [self addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(tf);
        make.top.mas_equalTo(tf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    return tf;
}

@end
