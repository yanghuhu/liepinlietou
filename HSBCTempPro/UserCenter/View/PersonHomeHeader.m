//
//  PersonHomeHeader.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PersonHomeHeader.h"
#import "UIView+YYAdd.h"
#import "UIImage+YYAdd.h"
#import "GradeView.h"
#import "UserModel.h"

@interface PersonHomeHeader(){
    
    UILabel * namelb;
    UIImageView * avatarImgV;
    UILabel * orderDescLb;
}
@end

@implementation PersonHomeHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatSubViews];
    }
    return self;
}

- (void)creatSubViews{

    UIImage * img = [UIImage imageNamed:@"avatar_placeholder"];
    UIImage * img_ = [img imageByRoundCornerRadius:HorPxFit(240)/2.0];
    avatarImgV  = [[UIImageView alloc] initWithImage:img_];
    avatarImgV.backgroundColor = [UIColor clearColor];

    [avatarImgV setLayerShadow:COLOR(230, 230, 230, 1) offset:CGSizeMake(0,2) radius:HorPxFit(70)/2.0];
    [self addSubview:avatarImgV];
    [avatarImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(-HorPxFit(20));
        make.top.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(160), VerPxFit(160)));
    }];
    avatarImgV.layer.cornerRadius = HorPxFit(160)/2.0;
    
    namelb = [[UILabel alloc] init];
    namelb.font = [UIFont systemFontOfSize:25];
    namelb.textColor = [UIColor whiteColor];
    namelb.text = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    [self addSubview:namelb];
    [namelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self);
        make.right.mas_equalTo(avatarImgV.mas_left).mas_offset(HorPxFit(20));
        make.height.mas_equalTo(40);
    }];
    
    UILabel * gradeTitle = [[UILabel alloc] init];
    gradeTitle.font = [UIFont systemFontOfSize:17];
    gradeTitle.text = @"评分:";
    gradeTitle.textColor = [UIColor whiteColor];
    [self addSubview:gradeTitle];
    [gradeTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(namelb);
        make.top.mas_equalTo(namelb.mas_bottom).mas_offset(VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(45, 30));
    }];
    
    UserModel * model = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel;

    GradeView * gradeView = [[GradeView alloc] initWithHeight:24 sourece:5];
//    gradeView.backgroundColor = [UIColor whiteColor];
    gradeView.frame = CGRectMake(45, 40+VerPxFit(36), CGRectGetWidth(gradeView.frame), 24);
    [gradeView gradeWithScore:model.score canOperate:NO];
    [self addSubview:gradeView];
    
//    [gradeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(gradeTitle.mas_right).mas_offset(HorPxFit(0));
//        make.centerY.mas_equalTo(gradeTitle.mas_centerY);
//        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(gradeView.frame), 20));
//    }];
    
    orderDescLb = [[UILabel alloc] init];
    orderDescLb.font = [UIFont systemFontOfSize:15];
    orderDescLb.textColor = [UIColor whiteColor];
    orderDescLb.text = [NSString stringWithFormat:@"被评价%ld单，已收藏15家企业",model.evaluateCount];
    
    [self addSubview:orderDescLb];
    [orderDescLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(namelb);
        make.top.mas_equalTo(gradeTitle.mas_bottom).mas_offset(VerPxFit(20));
        make.right.mas_equalTo(avatarImgV.mas_left);
        make.height.mas_equalTo(20);
    }];
    
//    UIButton * setBt = [UIButton buttonWithType:UIButtonTypeCustom];
//    [setBt addTarget:self action:@selector(setAction) forControlEvents:UIControlEventTouchUpInside];
//    [setBt setImage:[UIImage imageNamed:@"set"] forState:UIControlStateNormal];
//    [setBt setImage:[UIImage imageNamed:@"set_hight"] forState:UIControlStateHighlighted];
//    [self addSubview:setBt];
//    
//    [setBt mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self).with.mas_offset(-20);
//        make.top.mas_equalTo(HorPxFit(20));
//        make.size.mas_equalTo(CGSizeMake(HorPxFit(50),HorPxFit(50)));
//    }];
}

- (void)setAction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(toUserSetPage)]) {
        [self.delegate toUserSetPage];
    }
}

@end
