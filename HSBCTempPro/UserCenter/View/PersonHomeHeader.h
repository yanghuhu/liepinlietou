//
//  PersonHomeHeader.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PersonHomeHeaderDelegate <NSObject>

- (void)toUserSetPage;

@end

@interface PersonHomeHeader : UIView

@property (nonatomic , weak) id<PersonHomeHeaderDelegate>delegate;

@end
