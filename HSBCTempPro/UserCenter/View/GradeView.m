//
//  GradeView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "GradeView.h"
#import "UIView+YYAdd.h"

@interface GradeView(){
    UIImageView * lightImgV;
    UIImageView * grayImgV;
    UIScrollView * scrollView;
}
@end

@implementation GradeView

- (instancetype)initWithHeight:(CGFloat)height sourece:(CGFloat)source{
    
    self  = [super init];
    if (self) {
        UIImage * img = [UIImage imageNamed:@"starGray"];
        CGFloat width = img.size.width * height / img.size.height;
        self.frame = CGRectMake(0, 0, width, height);
        
        UIImage * imgGray = [UIImage imageNamed:@"starGray"];
        grayImgV = [[UIImageView alloc] initWithImage:imgGray];
        grayImgV.frame = CGRectMake(0, 0, width, height);
        [self addSubview:grayImgV];
        
        scrollView = [[UIScrollView alloc] init];
        scrollView.clipsToBounds = YES;
        scrollView.userInteractionEnabled = NO;
        scrollView.frame = CGRectMake(0, 0, width/5*source, height);
        [self addSubview:scrollView];
        
        UIImage * imgLight = [UIImage imageNamed:@"starLight"];
        lightImgV = [[UIImageView alloc] initWithImage:imgLight];
        lightImgV.frame = CGRectMake(0, 0, width, height);
        lightImgV.contentMode = UIViewContentModeScaleToFill;
        [scrollView addSubview:lightImgV];
    }
    return self;
}

- (void)setSource:(CGFloat)source{
    source = source;
    CGFloat height = CGRectGetHeight(scrollView.frame);
    UIImage * img = [UIImage imageNamed:@"starGray"];
    CGFloat width = img.size.width * height / img.size.height;
    scrollView.frame = CGRectMake(0, 0, width/5*source, CGRectGetHeight(scrollView.frame));
}

- (CGFloat)source{
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat scrollviewW = CGRectGetWidth(scrollView.frame);
    return  scrollviewW * 5 / width;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    lightImgV.autoresizingMask = UIViewAutoresizingNone;
    UITouch * touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    CGRect fram = scrollView.frame;
    fram.size.width = point.x;
    scrollView.frame = fram;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch * touch = [touches anyObject];
    CGPoint point = [touch locationInView:scrollView];
    
    if (point.x > 10 && point.x < self.frame.size.width && point.y <self.frame.size.height && point.y>0) {
        CGRect fram = scrollView.frame;
        fram.size.width = point.x;
        scrollView.frame = fram;
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    lightImgV.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

- (void)gradeWithScore:(CGFloat)score canOperate:(BOOL)canOperate{
    if (!canOperate) {
        self.userInteractionEnabled = NO;
    }
    NSLog(@"%f",CGRectGetWidth(lightImgV.frame));
    CGFloat width = CGRectGetWidth(lightImgV.frame) / 5 * score;
    scrollView.width = width;
}

@end
