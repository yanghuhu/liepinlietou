//
//  UserModule.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserModule.h"
#import "NetworkHandle.h"
#import "UIApplication+YYAdd.h"

@implementation UserModule
+ (void)resetPasswordWithOldPwd:(NSString *)oldPwd
                         newPwd:(NSString *)newPwd
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"newPassword" : newPwd,
                            @"oldPassword":oldPwd,
                            };
    [NetworkHandle postRequestForApi:@"auth/changePassword" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getUserInfoSuccess:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"userToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
    
    [NetworkHandle getRequestForApi:@"auth/getLoggedUser" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        if (response) {
            UserModel * model = [UserModel modelWithDictionary:response];
            if (model) {
                [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
                return @[@YES,model];
            }else{
                [BaseHelper showProgressHud:@"用户数据解析失败" showLoading:NO canHide:YES];
                return @[@NO];
            }
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)resetUserInfoWithAvatarImg:(NSString *)imgUrl
                          nickName:(NSString *)nickName
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSMutableDictionary * infoDic = [NSMutableDictionary dictionary];
    if (imgUrl) {
        [infoDic setObject:imgUrl forKey:@"headPic"];
    }
    if (nickName) {
        [infoDic setObject:nickName forKey:@"nickName"];
    }
    [NetworkHandle postRequestForApi:@"auth/changeUserInfo" mockObj:nil params:infoDic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}


+ (void)getCurrentVersionSuccess:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    
    NSString * version = [UIApplication sharedApplication].appVersion;
    NSDictionary * dic = @{@"platform":@"HEADHUNTER",@"version":version};
    [NetworkHandle getRequestForApi:@"appVersion/getLastVersion" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        if (response) {
            return @[@YES,response];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)logoutSuccess:(RequestSuccessBlock)succBlock
              failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle getRequestForApi:@"auth/signOut" mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

@end
