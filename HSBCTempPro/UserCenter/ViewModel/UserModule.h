//
//  UserModule.h
//  HSBCTempPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModule : NSObject

/*
 * 重设密码
 * @param
 *      username: 用户名
 * succBlock
 *      bool isSuccess
 */
+ (void)resetPasswordWithOldPwd:(NSString *)username
                         newPwd:(NSString *)newPwd
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;


/*
 *  获取用户信息
 *
 */
+ (void)getUserInfoSuccess:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;

/*
 *  编辑用户信息
 */
+ (void)resetUserInfoWithAvatarImg:(NSString *)imgUrl
                          nickName:(NSString *)nickName
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;
/*
 *  获取版本信息
 */
+ (void)getCurrentVersionSuccess:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;


/*
 *  登出
 */
+ (void)logoutSuccess:(RequestSuccessBlock)succBlock
              failure:(RequestFailureBlock)failBlock;

@end
