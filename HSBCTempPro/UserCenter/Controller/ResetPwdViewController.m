//
//  ResetPwdViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ResetPwdViewController.h"
#import "UserModule.h"

@interface ResetPwdViewController (){
    UITextField * oldPwdTf;
    UITextField * newPwdTf;
    UITextField * newPwdComfirTf;
}

@end

@implementation ResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"找回密码";
    self.view.backgroundColor = [UIColor blackColor];
    [self createSubViews];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)createSubViews{
    
    CGFloat titleW = 80;
    CGFloat cellH = VerPxFit(65);
    CGFloat horpadding = HorPxFit(50);
    CGFloat verpadding = VerPxFit(45);
    
    UILabel * oldPwdTitle = [self lb:@"原密码"];
    [oldPwdTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(50));
        make.top.equalTo(self.view).mas_offset(VerPxFit(190));
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    UIView * oldPwdView = [self tf];
    oldPwdTf = [oldPwdView viewWithTag:100];
    oldPwdTf.secureTextEntry = YES;
    [oldPwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(oldPwdTitle.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(oldPwdTitle.mas_centerY).mas_offset(-VerPxFit(15));;
        make.right.equalTo(self.view).mas_offset(-horpadding);
        make.height.mas_equalTo(cellH);
    }];
    
    UILabel * newPwdTitle = [self lb:@"新密码"];
    [newPwdTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(oldPwdTitle);
        make.top.mas_equalTo(oldPwdTitle.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    UIView * newPwdView = [self tf];
    newPwdTf = [newPwdView viewWithTag:100];
    newPwdTf.secureTextEntry = YES;
    [newPwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(oldPwdTitle.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(newPwdTitle.mas_centerY).mas_offset(-VerPxFit(15));
        make.right.equalTo(self.view).mas_offset(-horpadding);
        make.height.mas_equalTo(cellH);
    }];
  
    UILabel * newPwdComfirTitle = [self lb:@"确认新密码"];
    [newPwdComfirTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(oldPwdTitle);
        make.top.mas_equalTo(newPwdTitle.mas_bottom).mas_offset(verpadding);
        make.size.mas_equalTo(CGSizeMake(titleW, cellH));
    }];
    
    UIView * newPwdComfirView = [self tf];
    newPwdComfirTf = [newPwdComfirView viewWithTag:100];
    newPwdComfirTf.secureTextEntry = YES;
    [newPwdComfirView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(newPwdComfirTitle.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(newPwdComfirTitle.mas_centerY);
        make.right.equalTo(self.view).mas_offset(-horpadding);
        make.height.mas_equalTo(cellH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBt.layer.cornerRadius = VerPxFit(90)/2.0;
    sureBt.backgroundColor = [UIColor clearColor];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [sureBt setTitle: @"确定" forState:UIControlStateNormal];
    [sureBt addTarget:self  action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.layer.cornerRadius =5;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(newPwdComfirView.mas_bottom).offset(VerPxFit(80));
        make.height.mas_equalTo(VerPxFit(80));
        make.left.mas_equalTo(newPwdComfirTitle.mas_left).mas_offset(HorPxFit(30));
        make.right.mas_equalTo(newPwdComfirView.mas_right).mas_offset(-HorPxFit(30));
    }];
}

- (void)sureAction{
    if (!oldPwdTf.text ||  oldPwdTf.text.length == 0 ) {
        [BaseHelper showProgressHud:@"请输入旧密码" showLoading:NO canHide:YES];
        return ;
    }
    if (!newPwdTf.text || newPwdTf.text.length ==0) {
        [BaseHelper showProgressHud:@"请输入新密码" showLoading:NO canHide:YES];
        return ;
    }
    if (!newPwdComfirTf.text || newPwdComfirTf.text.length == 0) {
        [BaseHelper showProgressHud:@"请确认新密码" showLoading:NO canHide:YES];
        return ;
    }
    if (![newPwdComfirTf.text isEqualToString:newPwdTf.text]) {
        [BaseHelper showProgressHud:@"新密码两次输入不同，请重新输入" showLoading:NO canHide:YES];
        return;
    }
    
    if(![BaseHelper passwordlength:newPwdTf.text]){
        [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
        return;
    }
    
    
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule resetPasswordWithOldPwd:oldPwdTf.text newPwd:newPwdTf.text success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"重置成功" showLoading:NO canHide:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.text = text;
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    [self.view addSubview:lb];
    return lb;
}

- (UIView *)tf{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view];
    
    UITextField * tf = [[UITextField alloc] init];
    tf.textColor = [UIColor whiteColor];
    tf.tag = 100;
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    tf.leftView = leftView;
    tf.leftViewMode = UITextFieldViewModeAlways;
    [view addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(view);
    }];
    
    UIImageView * imgV = [[UIImageView alloc] init];
    imgV.image = [UIImage imageNamed:@"AccountBottom"];
    [view addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tf.mas_bottom);
        make.left.and.right.equalTo(tf);
        make.height.mas_equalTo(2);
    }];
    
    return view;
    
}

@end
