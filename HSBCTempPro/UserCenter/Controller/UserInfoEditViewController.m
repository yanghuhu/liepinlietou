//
//  UserInfoEditViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/8.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserInfoEditViewController.h"
#import "UIImage+YYAdd.h"
#import "UIView+YYAdd.h"
#import "UserInfoEditView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NetWorkHelper.h"
#import "UserModule.h"
#import "UIImageView+YYWebImage.h"

@interface UserInfoEditViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    UIImageView * avatarImgV;
    UILabel * phoneLb;
    UserInfoEditView * userInfoEditView;
    
}
@property (nonatomic , strong) UIImage * avatarImage;
@property (nonatomic , strong) UIImagePickerController * imagePickerController;
@property (nonatomic , strong) NSString * avatarUrl;
@end

@implementation UserInfoEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"信息编辑";
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    self.view.backgroundColor = [UIColor blackColor];
    UIImage * bkImage = [UIImage imageNamed:@"userInfoEditBk"];
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[bkImage stretchableImageWithLeftCapWidth:0 topCapHeight:400]];
    bkImgV.userInteractionEnabled = YES;
    [self.view addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(30));
        make.right.equalTo(self.view).mas_offset(-HorPxFit(30));
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+BatteryH+20);
        make.bottom.equalTo(self.view).mas_offset(-VerPxFit(20));
    }];
    
    UIImage * img = [UIImage imageNamed:@"avatar_placeholder"];
    UIImage * img_ = [img imageByRoundCornerRadius:HorPxFit(240)/2.0];
    avatarImgV  = [[UIImageView alloc] initWithImage:img_];
    avatarImgV.backgroundColor = [UIColor clearColor];
    avatarImgV.userInteractionEnabled = YES;
    //    [avatarImgV setLayerShadow:COLOR(230, 230, 230, 1) offset:CGSizeMake(0,2) radius:HorPxFit(70)/2.0];
    [bkImgV addSubview:avatarImgV];
    [avatarImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bkImgV.mas_centerX);
        make.top.equalTo(self.view).mas_offset(VerPxFit(210));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(200), VerPxFit(200)));
    }];
    avatarImgV.layer.cornerRadius = HorPxFit(200)/2.0;
    avatarImgV.clipsToBounds = YES;
    [avatarImgV setImageWithURL:[NSURL URLWithString:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.headPic] placeholder:[UIImage imageNamed:@"avatar_placeholder"]];
    
    
    UIView  * avatarBkView = [[UIView alloc] init];
    avatarBkView.layer.cornerRadius = VerPxFit(210)/2;
    avatarBkView.backgroundColor = [UIColor whiteColor];
    [bkImgV insertSubview:avatarBkView belowSubview:avatarImgV];
    [avatarBkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(avatarImgV.mas_centerX);
        make.centerY.mas_equalTo(avatarImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(210), VerPxFit(210)));
    }];
    
    UITapGestureRecognizer * userInfoEditGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userAvatarEdit)];
    [avatarImgV addGestureRecognizer:userInfoEditGesture];
    
    phoneLb = [[UILabel alloc] init];
    phoneLb.textColor = [UIColor whiteColor];
    phoneLb.font = [UIFont systemFontOfSize:15];
    phoneLb.text = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    phoneLb.textAlignment = NSTextAlignmentCenter;
    [bkImgV addSubview:phoneLb];
    [phoneLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(bkImgV);
        make.top.equalTo(avatarImgV.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(30);
    }];

    UIButton * sureAction = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureAction addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    [sureAction setTitle:@"保存" forState:UIControlStateNormal];
    [sureAction setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    sureAction.titleLabel.font = [UIFont systemFontOfSize:15];
    [bkImgV addSubview:sureAction];
    [sureAction mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(bkImgV.mas_centerX);
        make.bottom.equalTo(bkImgV).mas_offset(-VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(450), VerPxFit(73)));
    }];
    
    userInfoEditView = [[UserInfoEditView alloc] init];
    [self.view addSubview:userInfoEditView];
    [userInfoEditView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(40));
        make.right.equalTo(self.view).mas_offset(-HorPxFit(40));
        make.top.mas_equalTo(phoneLb.mas_bottom).mas_offset(VerPxFit(90));
        make.bottom.mas_equalTo(sureAction.mas_top).mas_offset(-VerPxFit(20));
    }];
}

- (void)userAvatarEdit{
    @weakify(self)
    UIAlertController * alert  =[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * camerAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self selectImageFromCamera];
    }];
    UIAlertAction * photoesAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self selectImageFromAlbum];
    }];
    UIAlertAction * cancelAction =  [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:camerAction];
    [alert addAction:photoesAction];
    [self presentViewController:alert animated:YES completion:^{
    }];
}

- (void)selectImageFromCamera
{
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //设置摄像头模式（拍照，录制视频）为录像模式
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)selectImageFromAlbum
{
    //NSLog(@"相册");
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}


- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        _imagePickerController.allowsEditing = YES;
    }
    return _imagePickerController;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    NSString *mediaType=[info objectForKey:UIImagePickerControllerMediaType];
    //判断资源类型
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        //如果是图片
        UIImage * img_ = info[UIImagePickerControllerEditedImage];
        //        UIImage * img_ = [self fixOrientation:img];
        //        UIImage* img_ =[info objectForKey:UIImagePickerControllerOriginalImage];
        
        if(img_.size.height < 300){
            self.avatarImage = img_;
        }else{
            CGFloat h = 300;
            CGFloat w = img_.size.width * h / img_.size.height;
            UIImage * img = [img_ imageByResizeToSize:CGSizeMake(w, h)];
            self.avatarImage = img;
        }
    }
    if (_avatarImage) {
        avatarImgV.image = self.avatarImage;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)submitAction{
    if (_avatarImage) {
        [BaseHelper showProgressLoadingInView:self.view];
        [NetWorkHelper requestForUploadImageSync:_avatarImage withModule:nil success:^(NSDictionary * info){
            [BaseHelper hideProgressHudInView:self.view];
            if (info) {
                NSString * url = info[@"url"];
                self.avatarUrl = url;
            }
            [self submitUserInfo];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
            [self submitUserInfo];
        }];
    }else{
        [self submitUserInfo];
    }
}

- (void)submitUserInfo{
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule resetUserInfoWithAvatarImg:self.avatarUrl nickName:userInfoEditView.nickName success:^{
        [BaseHelper hideProgressHudInView:self.view];
            [BaseHelper showProgressHud:@"提交成功" showLoading:NO canHide:YES];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

@end
