//
//  VersionViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "VersionViewController.h"
#import "UserModule.h"
#import "UIApplication+YYAdd.h"

@interface VersionViewController (){
    UILabel * curVersionLb;
}
@property (nonatomic , strong) NSDictionary * versionInfo;
@end

@implementation VersionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"版本检测";
    self.view.backgroundColor = COLOR(220, 220, 220, 1);
    [self createSubViews];
    [self getCurVersion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createSubViews{
    UIImageView * logoImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    [self.view addSubview:logoImgView];
    [logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+BatteryH+ VerPxFit(90));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(160), HorPxFit(160)));
    }];
    curVersionLb = [[UILabel alloc] init];
    curVersionLb.font  = [UIFont systemFontOfSize:17];
    curVersionLb.text =  [NSString stringWithFormat:@"猎场猎头端 %@",[UIApplication sharedApplication].appVersion];
    curVersionLb.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:curVersionLb];
    [curVersionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view);
        make.top.mas_equalTo(logoImgView.mas_bottom).mas_offset(VerPxFit(20));
        make.height.mas_equalTo(35);
    }];
    
    UILabel * copyRightLb = [[UILabel alloc] init];
    copyRightLb.textAlignment = NSTextAlignmentCenter;
    copyRightLb.font = [UIFont systemFontOfSize:14];
    copyRightLb.textColor = [UIColor lightGrayColor];
    copyRightLb.text = [NSString stringWithFormat:@"Copyright © 2017-2018 TianTian.\n All Right Reserved."];
    copyRightLb.numberOfLines = 0 ;
    [self.view addSubview:copyRightLb];
    [copyRightLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view);
        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-VerPxFit(15));
        make.height.mas_equalTo(55);
    }];
}


- (void)updateInfoAppear{

    BOOL isHadNew = [self.versionInfo[@"newVersion"] boolValue];
    if (isHadNew) {
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        [bt setTitle:@"更新版本" forState:UIControlStateNormal];
        [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        bt.titleLabel.font = [UIFont systemFontOfSize:15];
        [bt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
        [self.view addSubview:bt];
        [bt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view.mas_centerX);
//            make.top.mas_equalTo(curVersionLb.mas_bottom).mas_offset(VerPxFit(60));
            make.centerY.mas_equalTo(self.view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(HorPxFit(350), VerPxFit(65)));
        }];
    }

}

- (void)getCurVersion{
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule getCurrentVersionSuccess:^(NSDictionary * info){
        [BaseHelper hideProgressHudInView:self.view];
        self.versionInfo = info;
        [self updateInfoAppear];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
