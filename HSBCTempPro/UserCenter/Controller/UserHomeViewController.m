//
//  UserHomeViewController.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "UserHomeViewController.h"
#import "PersonHomeHeader.h"
#import "SettingViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "UIImage+YYAdd.h"
#import "ResetPwdViewController.h"
#import "UIView+YYAdd.h"
#import "GradeView.h"
#import "UserInfoEditViewController.h"
#import "UserModule.h"
#import "UserModel.h"
#import "UserHomeHeaderView.h"
#import "LangeSetViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "ResetPwdViewController.h"
#import "VersionViewController.h"
#import "UserModule.h"


#define KEYUITableViewCell  @"UITableViewCell"

@interface UserHomeViewController ()<UITableViewDelegate,UITableViewDataSource,UserHomeHeaderViewDelegate>{
    UserHomeHeaderView * headerView;
}

@property (nonatomic, strong) UITableView * tableView_;
@property (nonatomic , strong) NSArray * titleArray;
@property (nonatomic , strong) NSArray * iconArray;
@end

@implementation UserHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.title = @"个人中心";
    self.view.backgroundColor = ViewControllerBkColor;
    self.titleArray = @[@[@"我的收藏",@"我的简历",@"我的记录"],
                        @[@"语言设置"],
                        @[@"修改密码"],
                        @[@"意见反馈",@"关于猎聘",@"版本检测"]];
    self.iconArray= @[@[@"like",@"set",@"record"]];
    [self createSubViews];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [bt setImage:[[UIImage imageNamed:@"back"]imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(30));
        make.top.equalTo(self.view).mas_offset(VerPxFit(45));
        make.size.mas_equalTo(CGSizeMake(50, 35));
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"个人中心";
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.centerY.mas_equalTo(bt.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(90, 30));
    }];
    
    
    [self getUserInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUserInfo{
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule getUserInfoSuccess:^(UserModel * usermodel){
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = usermodel;
        [headerView updateInfoAppear];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)createSubViews{
    
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userHomeBk"]];
    bkImgV.userInteractionEnabled = YES;
    [self.view addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view);
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+BatteryH);
        make.bottom.equalTo(self.view);
    }];
    
    headerView = [[UserHomeHeaderView alloc] init];
    headerView.delegate = self;
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view);
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+BatteryH+VerPxFit(10));
        make.height.mas_equalTo(VerPxFit(330));
    }];
    
    [headerView updateInfoAppear];
    
    self.tableView_ = [[UITableView alloc] init];
    self.tableView_.separatorInset = UIEdgeInsetsZero;
    self.tableView_.layoutMargins = UIEdgeInsetsZero;
    self.tableView_.delegate = self;
//    self.tableView_.backgroundColor = [UIColor clearColor];
    self.tableView_.dataSource = self;
    self.tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView_.showsVerticalScrollIndicator = NO;
    self.tableView_.backgroundColor = [UIColor clearColor];
    [self.tableView_ registerClass:[UITableViewCell class] forCellReuseIdentifier:KEYUITableViewCell];
    [self.view addSubview:self.tableView_];
    [self.tableView_ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headerView.mas_bottom).mas_offset(VerPxFit(120));
        make.left.and.right.equalTo(self.view);
        make.bottom.equalTo(self.view).mas_offset(-VerPxFit(20));
    }];
    
    [BaseHelper setExtraCellLineHidden:self.tableView_];
    [headerView updateInfoAppear];
}

#pragma mark -- UserHomeHeaderViewDelegate
- (void)toUserInfoEditVC{
    UserInfoEditViewController * vc = [[UserInfoEditViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.titleArray.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section==self.titleArray.count ? 1 : [self.titleArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.section!=self.titleArray.count) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:KEYUITableViewCell forIndexPath:indexPath];
        cell.separatorInset = UIEdgeInsetsZero;
        cell.textLabel.text = self.titleArray[indexPath.section][indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        return cell;
        
    }else{
        static NSString * CellIdentifier = @"CellIdentifier_";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        [bt addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
        bt.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 44);
        [bt setTitle:@"退出账号" forState:UIControlStateNormal];
        bt.titleLabel.font = [UIFont systemFontOfSize:15];
        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell addSubview:bt];
        return cell;
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    }
    return VerPxFit(40);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1){
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), VerPxFit(50))];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }else{
        return nil;
    }
}

#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            LangeSetViewController * vc = [[LangeSetViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            ResetPwdViewController * vc = [[ResetPwdViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
        {
            if (indexPath.row == 0) {
                
            }else if (indexPath.row == 1){
                
            }else if (indexPath.row == 2){
                VersionViewController * vc = [[VersionViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -- PersonHomeHeaderDelegate

- (void)toUserSetPage{
    SettingViewController * vc = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)logoutAction{
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule logoutSuccess:^{
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = nil;
        LoginViewController * vc = [[LoginViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        [DataDefault removeWithKey:DataDefaultUserInfoKey isPrivate:NO];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

@end
