//
//  BasePopView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "BasePopView.h"
#import "AppDelegate.h"

@implementation BasePopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        bkControl = [[UIControl alloc] init];
        bkControl.backgroundColor = [UIColor blackColor];
        bkControl.alpha = 0.6;
        [bkControl addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)addTopRightDismissBt{
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
//    [bt setImage:[UIImage imageNamed:@"closeHight"] forState:UIControlStateHighlighted];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(VerPxFit(12));
        make.right.equalTo(self).mas_offset(-HorPxFit(20));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(70), HorPxFit(70)));
    }];
}

- (UIView *)supView:(UIView *)view{
    UIView * view_ = view;
    if (!view) {
        AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
         view_ = appdelegate.window;
    }
    bkControl.frame = view_.bounds;
    return view_;
}

- (void)showWithSupView:(UIView *)view{
    // rewrite me
    UIView * supView = [self supView:view];
    self.alpha = 0;
    bkControl.alpha = 0;
    self.center = supView.center;
    [supView addSubview:bkControl];
    [supView addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        bkControl.alpha = 0.8;
        self.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
        }
    }];
}

- (void)dismissAction{
    
    [UIView animateWithDuration:0.3 animations:^{
        bkControl.alpha = 0;
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [bkControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}
@end
