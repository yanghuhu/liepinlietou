//
//  RateView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RateView.h"
#import "GradeView.h"
#import "RecruitModule.h"

@interface RateView(){
    GradeView * gradeView;
}
@end
@implementation RateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR(20, 23, 31, 1);
        self.layer.cornerRadius = 10;
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"订单已完成";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:16];
    titleLb.textColor = [UIColor whiteColor];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(20));
        make.left.and.right.equalTo(self);
    }];
    
    UILabel * subTitleLb = [[UILabel alloc] init];
    subTitleLb.text = @"请评价一下和您沟通的HR吧";
    subTitleLb.textAlignment = NSTextAlignmentCenter;
    subTitleLb.font = [UIFont systemFontOfSize:14];
    subTitleLb.textColor = [UIColor whiteColor];
    [self addSubview:subTitleLb];
    [subTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.mas_equalTo(titleLb.mas_bottom).offset(VerPxFit(25));
        make.height.mas_equalTo(30);
    }];
    
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(40));
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.top.mas_equalTo(subTitleLb.mas_bottom).mas_offset(VerPxFit(15));
        make.height.mas_equalTo(1);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    bt.titleLabel.font = [UIFont systemFontOfSize:15];
    [bt setTitle:@"提交" forState:UIControlStateNormal];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self).mas_offset(-VerPxFit(30));
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(300), VerPxFit(60)));
    }];
    
    GradeView * gradeView = [[GradeView alloc] initWithHeight:HorPxFit(45) sourece:5];
    [self addSubview:gradeView];
    [gradeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.bottom.mas_equalTo(bt.mas_top).offset(-VerPxFit(55));
        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(gradeView.frame), 30));
    }];
}

- (void)sureAction{
    [BaseHelper showProgressLoading];
    [RecruitModule submitRate:gradeView.source withId:_model.id_ success:^{
        [BaseHelper hideProgressHud];
        [BaseHelper showProgressHud:@"提交成功" showLoading:NO canHide:YES];
        [self.delegate rateFinish];
        [self dismissAction];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHud];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

@end
