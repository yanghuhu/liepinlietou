//
//  BasePopView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasePopView : UIView{
    UIControl * bkControl;    
}
// 当前popview的supview
- (UIView *)supView:(UIView *)view;
//  popview的展示
- (void)showWithSupView:(UIView *)view;
// 移除popview
- (void)dismissAction;
// 添加右上角的按钮
- (void)addTopRightDismissBt;

@end
