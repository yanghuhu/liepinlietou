//
//  ThirdLoginManage.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "ThirdLoginManage.h"
#import <ShareSDKExtension/SSEThirdPartyLoginHelper.h>
#import "ThirdUserModel.h"

@implementation ThirdLoginManage

+ (void)loginWithType:(ThirdLoginType)type callBack:(thirdLoginBackBlock)blcok{
    
    // temp
    type = ThirdLoginTypeSinaWeibo;
    
    SSDKPlatformType platformType;
    switch (type) {
        case ThirdLoginTypeQQ:{
            platformType = SSDKPlatformTypeQQ;
            break;
        }
        case ThirdLoginTypeWechat:{
            platformType = SSDKPlatformTypeWechat;
            break;
        }
        case ThirdLoginTypeSinaWeibo:{
            platformType = SSDKPlatformTypeSinaWeibo;
            break;
        }
        case ThirdLoginTypeFacebook:{
            platformType = SSDKPlatformTypeFacebook;
            break;
        }
        case ThirdLoginTypeTwitter:{
            platformType = SSDKPlatformTypeTwitter;
            break;
        }
        case ThirdLoginTypeGooglePlus:{
            platformType = SSDKPlatformTypeGooglePlus;
            break;
        }
        default:
            break;
    }
    [BaseHelper showProgressLoading];
    @weakify(self)
    [SSEThirdPartyLoginHelper loginByPlatform:platformType onUserSync:^(SSDKUser *user, SSEUserAssociateHandler associateHandler) {
        [BaseHelper hideProgressHud];
        @strongify(self);
        if(!self) return;
        ThirdUserModel * model = [[ThirdUserModel alloc] init];
        model.platformId = user.uid;
        model.headPic = user.icon;
        model.gender = user.gender==1?@"男":@"女";
        model.nickName = user.nickname;
        model.state = 1;
        blcok(model);
    } onLoginResult:^(SSDKResponseState state, SSEBaseUser *user, NSError *error) {
        [BaseHelper hideProgressHud];
        @strongify(self);
        if(!self) return;
        
        ThirdUserModel * model = [[ThirdUserModel alloc] init];
        if(state == SSDKResponseStateCancel){
            model.state = -1;
        }else{
            model.state = 0;
        }
        blcok(model);
//        if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginFinish:userInfo:)]) {
//            [self.delegate thirdLoginFinish:state==SSDKResponseStateFail?ThirdLoginStateFail:ThirdLoginStateCancel userInfo:nil];
//        }
    }];
    
}

@end
