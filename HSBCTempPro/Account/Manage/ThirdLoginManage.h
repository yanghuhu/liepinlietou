//
//  ThirdLoginManage.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^thirdLoginBackBlock)();

typedef  NS_ENUM(NSInteger, ThirdLoginState){
    ThirdLoginStateSuccess,
    ThirdLoginStateCancel,
    ThirdLoginStateFail,
};

@protocol ThirdLoginManageDelegate <NSObject>

- (void)thirdLoginFinish:(ThirdLoginState)state userInfo:(UserModel *)model;

@end

typedef enum : NSUInteger {
    ThirdLoginTypeQQ = 0,
    ThirdLoginTypeWechat,
    ThirdLoginTypeSinaWeibo,
    ThirdLoginTypeFacebook,
    ThirdLoginTypeTwitter,
    ThirdLoginTypeGooglePlus,
} ThirdLoginType;

@interface ThirdLoginManage : NSObject

//@property (nonatomic, weak) id<ThirdLoginManageDelegate>delegate;

+ (void)loginWithType:(ThirdLoginType)type callBack:(thirdLoginBackBlock)blcok;

@end
