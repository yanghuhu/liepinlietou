//
//  AccountMock.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AccountMock.h"

@implementation AccountMock

//#define  NeedMockData

#pragma mark - override super function
+ (BOOL)isNeedMockData
{
#ifdef NeedMockData
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].needMock) {
        return YES;
    }else{
        return NO;
    }
#else
    return NO;
#endif
}

+ (instancetype)createWithFunctionName:(NSString *)functionName
{
    return [self createWithFunctionName:functionName forceMockData:NO];
}

+ (instancetype)createWithFunctionName:(NSString *)functionName forceMockData:(BOOL)forceMockData
{
#if RELEASE
    forceMockData = NO;
#endif
    if (forceMockData || [self isNeedMockData]) {
        AccountMock *mockData = [[AccountMock alloc] init];
        mockData.functionName = functionName;
        return mockData;
    }
    return nil;
}

- (void)loginWithUserName_password_success_failure_{
    
    UserModel * userModel = [[UserModel alloc] init];
    userModel.uid = 1;
    [self.callbackData addObject:userModel];
}

- (void)registerWithUsername_password_success_failure_{
    
    UserModel * userModel  = [[UserModel alloc] init];
    userModel.uid = 1;
    [self.callbackData addObject:userModel];
}

- (void)resetPasswordWithUsername_success_failure_{
    
    [self.callbackData addObject:@(YES)];
}



@end

