//
//  AccountModule.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModule.h"

@interface AccountModule : BaseModule

/*
 * @param
 *      username:  用户名
 *      password:  密码
 * succBlock
 *      UserModel
 */
+ (void)loginWithUserName:(NSString *)username
                 password:(NSString *)password
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock;


/*
 * 第三方登录
 */
+ (void)thirdLoginWihtId:(NSInteger)platformId
        thirdPlatformType:(NSString *)platform
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;

/*
 * @param
 *      username: 用户名
 *      password: 密码
 * succBlock
 *      UserModel
 */
+ (void)registerWithUsername:(NSString *)username
                    password:(NSString *)password
                   verfyCode:(NSString *)verfyCode
                     success:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock;

/*
 * 找回密码
 * @param
 *      username: 用户名
 * succBlock
 *      bool isSuccess
 */
+ (void)resetPasswordWithUsername:(NSString *)username
                          
                        verfyCode:(NSString *)verfyCode
                          success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock;

/**
 *找回密码下一步
 *@param
 *     pwd_ 新密码
 *     newPwd  确认新密码
 *     token   token
 */
+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd
                              token:(NSString *)token
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock;

/*
 *  当前用户登出
 */
+ (void)curUserLogoutSuccess:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock;


/*
 *  获取当前内提供的第三方登录方式
 */
+ (void)getThirdLoginTypesSuccess:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock;

/*
 *  获取短信验证码
 */
+ (void)getSmsVerifyCode:(NSString *)phone
                    type:(NSString *)type
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;


/*
 *  第三方账号绑定用户
 *  param
 *      phone:手机号
 *      gender: 性别
 *      headPic: 头像
 *      nickName: 昵称
 *      password: 密码
 *      platformId:第三方平台id
 *      verificationCode: 验证码
 */
+ (void)accountBingWithCellphone:(NSString *)phone
                          gender:(NSString *)gender
                         headPic:(NSString *)headPic
                        nickName:(NSString *)nickName
                        password:(NSString *)password
                      platformId:(NSString *)platformId
               thirdPlatformType:(NSString *)thirdPlatformType
                verificationCode:(NSString *)verificationCode
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;
/*
 *  获取账户信息
 */
+ (void)getAccountBlanceSuccess:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;
@end
