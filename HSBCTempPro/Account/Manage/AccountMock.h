//
//  AccountMock.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseMockData.h"

@interface AccountMock : BaseMockData

@end
