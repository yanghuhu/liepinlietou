//
//  AccountModule.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/24.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AccountModule.h"
#import "UserModel.h"
#import "AccountMock.h"
#import "ThirdLoginTypeModel.h"
#import "NSArray+YYAdd.h"

static AccountMock *mockData;


@implementation AccountModule

+ (void)loginWithUserName:(NSString *)username
                 password:(NSString *)password
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"cellphone" : username,
                            @"password" : password,
                            @"plateformType":@"HEADHUNTER",
                            @"verificationCode":@"aaa"
                            };
    
    mockData = [AccountMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"auth/login" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString * token = response_[@"token"];
        if (!token || token.length == 0) {
            [BaseHelper showProgressHud:@"token 返回异常" showLoading:NO canHide:YES];
        }else{
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * info = response_[@"user"];
        if (info) {
            UserModel * model = [UserModel modelWithDictionary:info];
            if (model) {
                [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
                return @[@YES,model];
            }else{
                [BaseHelper showProgressHud:@"用户数据解析失败" showLoading:NO canHide:YES];
                return @[@NO];
            }
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)thirdLoginWihtId:(NSInteger)platformId
       thirdPlatformType:(NSString *)platform
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"platformId" : @(platformId),
                            @"platformType" : @"HEADHUNTER",
                            @"thirdPlatformType":platform
                            };
    
    [NetworkHandle postRequestForApi:@"auth/loginByPlatform" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString * token = response_[@"token"];
        if (!token || token.length == 0) {
            [BaseHelper showProgressHud:@"token 返回异常" showLoading:NO canHide:YES];
        }else{
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * info = response_[@"user"];
        if (info) {
            UserModel * model = [UserModel modelWithDictionary:info];
            if (model) {
                [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
                return @[@YES,model];
            }else{
                [BaseHelper showProgressHud:@"用户数据解析失败" showLoading:NO canHide:YES];
                return @[@NO];
            }
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}

+ (void)registerWithUsername:(NSString *)username
                    password:(NSString *)password
                   verfyCode:(NSString *)verfyCode
                     success:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"cellphone" : username,
                            @"password" : password,
                            @"verificationCode":verfyCode
                            };
    
    mockData = [AccountMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"auth/register" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        
        NSString * token = response_[@"token"];
        if (!token || token.length == 0) {
            [BaseHelper showProgressHud:@"token 返回异常" showLoading:NO canHide:YES];
        }else{
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * info = response_[@"user"];
        if (info) {
            UserModel * model = [UserModel modelWithDictionary:info];
            if (model) {
                [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
                return @[@YES,model];
            }else{
                [BaseHelper showProgressHud:@"用户数据解析失败" showLoading:NO canHide:YES];
                return @[@NO];
            }
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)resetPasswordWithUsername:(NSString *)username
                        verfyCode:(NSString *)verfyCode
                          success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"cellphone" : username,
                            @"platformType":@"HEADHUNTER",
                            @"verificationCode":verfyCode
                            };
    
    mockData = [AccountMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle postRequestForApi:@"auth/setPassword" mockObj:mockData params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary *dict = (NSDictionary *)response;
        NSString *token = dict[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
            return @[@YES,token];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd token:(NSString *)token success:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"password":newPwd,
                            @"platformType":@"HEADHUNTER",
                            @"token":token,
                            };
    [NetworkHandle postRequestForApi:@"auth/setPasswordStep2" mockObj:nil params:para handle:^NSArray *(id response) {
        
        return @[@YES];
        
    } success:^(id response){
        if (succBlock) {
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)curUserLogoutSuccess:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle postRequestForApi:@"auth/setPassword" mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getThirdLoginTypesSuccess:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBlock{
    [NetworkHandle getRequestForApi:@"thirdPlatformConfig/getConfigs" mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        NSArray *types = (NSArray *)response;
        if (types) {
            NSArray * typeModels = [NSArray modelArrayWithClass:[ThirdLoginTypeModel class] json:[types jsonStringEncoded]];
            return @[@YES,typeModels];
        }
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getSmsVerifyCode:(NSString *)phone
                    type:(NSString *)type
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"cellphone":phone,@"type":type,@"platformType":@"HEADHUNTER"};
    [NetWorkHelper postRequestForApi:@"auth/sendCode" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)accountBingWithCellphone:(NSString *)phone
                          gender:(NSString *)gender
                         headPic:(NSString *)headPic
                        nickName:(NSString *)nickName
                        password:(NSString *)password
                      platformId:(NSString *)platformId
               thirdPlatformType:(NSString *)thirdPlatformType
                verificationCode:(NSString *)verificationCode
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    if (phone) {
        [param setObject:phone forKey:@"cellphone"];
    }
    if (gender) {
        [param setObject:gender forKey:@"gender"];
    }
    if (headPic) {
        [param setObject:headPic forKey:@"headPic"];
    }if (nickName) {
        [param setObject:nickName forKey:@"nickName"];
    }
    if (password) {
        [param setObject:password forKey:@"password"];
    }
    if (platformId) {
        [param setObject:platformId forKey:@"platformId"];
    }
    if (thirdPlatformType) {
        [param setObject:thirdPlatformType forKey:@"thirdPlatformType"];
    }
    if (verificationCode) {
        [param setObject:verificationCode forKey:@"verificationCode"];
    }
    [param setObject:@"HEADHUNTER" forKey:@"platformType"];

    [NetWorkHelper postRequestForApi:@"auth/platformBind" params:param handle:^NSArray *(id response) {
        NSDictionary * response_ = (NSDictionary *)response;
        
        NSString * token = response_[@"token"];
        if (!token || token.length == 0) {
            [BaseHelper showProgressHud:@"token 返回异常" showLoading:NO canHide:YES];
        }else{
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * info = response_[@"user"];
        if (info) {
            UserModel * model = [UserModel modelWithDictionary:info];
            if (model) {
                [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
                return @[@YES,model];
            }else{
                [BaseHelper showProgressHud:@"用户数据解析失败" showLoading:NO canHide:YES];
                return @[@NO];
            }
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getAccountBlanceSuccess:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    [NetWorkHelper getRequestForApi:@"account/getAccount" params:nil handle:^NSArray *(id response) {
        NSDictionary * info = (NSDictionary *)response;
        NSInteger balance = [info[@"balance"] integerValue];
        NSInteger grantBalance = [info[@"grantBalance"] integerValue];
        UserModel * userModel = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel;
        userModel.grantBalance = grantBalance;
        userModel.balance  = balance;
        return @[@YES,userModel];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}
@end

