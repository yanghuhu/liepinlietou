//
//  RegisterContentView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RegisterContentView.h"

@interface RegisterContentView()<UITextFieldDelegate>{

    UITextField * phoneTf;
    UITextField * pwdTf;
    UITextField * verifyCodeTf;
    UIButton * verifyCodeBt;
    
}
@end

@implementation RegisterContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}


- (void)createSubViews{
    CGFloat horpadding = HorPxFit(20);
    CGFloat verpadding = VerPxFit(36);
    CGFloat tfH = VerPxFit(80);
  
    phoneTf = [self tfWihtPlaceHolder:@"手机号"];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.equalTo(self);
        make.height.mas_equalTo(tfH);
    }];

    pwdTf = [self tfWihtPlaceHolder:@"请输入至少8位有效密码"];
    pwdTf.secureTextEntry =YES;
    [self addSubview:pwdTf];
    [pwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verpadding);
        make.height.mas_equalTo(tfH);
    }];

    verifyCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeBt addTarget:self action:@selector(getVerifyCodeAction) forControlEvents:UIControlEventTouchUpInside];
    [verifyCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verifyCodeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    verifyCodeBt.backgroundColor = [UIColor blackColor];
    verifyCodeBt.layer.cornerRadius = tfH/2.0;
    verifyCodeBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [verifyCodeBt setBackgroundImage:[UIImage imageNamed:@"verifyCodeBk"] forState:UIControlStateNormal];
    [self addSubview:verifyCodeBt];
    [verifyCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdTf.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self);
        make.width.mas_equalTo(HorPxFit(200));
        make.height.mas_equalTo(tfH);
    }];

    verifyCodeTf = [self tfWihtPlaceHolder:@"验证码"];
    verifyCodeTf.returnKeyType = UIReturnKeyDone;
    verifyCodeTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.mas_equalTo(pwdTf.mas_bottom).mas_offset(verpadding);
        make.right.mas_equalTo(verifyCodeBt.mas_left).mas_offset(-horpadding);
        make.height.mas_equalTo(tfH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBt.layer.cornerRadius = VerPxFit(90)/2.0;
    sureBt.backgroundColor = [UIColor clearColor];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [sureBt setTitle: @"注册" forState:UIControlStateNormal];
    [sureBt addTarget:self  action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.layer.cornerRadius =5;
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(verifyCodeTf.mas_bottom).offset(VerPxFit(60));
        make.height.mas_equalTo(VerPxFit(90));
         make.left.equalTo(self);
         make.right.equalTo(self);
    }];
}

- (UITextField *)tfWihtPlaceHolder:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.layer.cornerRadius = VerPxFit(80)/2.0;
    tf.delegate = self;
    tf.borderStyle = UITextBorderStyleNone;
    tf.font = [UIFont systemFontOfSize:15];
    tf.placeholder = placeHolder;
    tf.backgroundColor = [UIColor whiteColor];
    tf.returnKeyType = UIReturnKeyNext;
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
    return tf;
}

- (NSString *)userName{
    return phoneTf.text;
}
- (NSString *)password{
    return pwdTf.text;
}
- (NSString *)verifyCode{
    return verifyCodeTf.text;
}

- (void)getVerifyCodeAction{
    
    if ([BaseHelper isValidateMobile:phoneTf.text]) {
        [self.delegate getVerifyCode:phoneTf.text];
    }else{
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
    }
}
- (void)getVerifyCodeSuccess{
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = YES;
                [verifyCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = NO;
                [verifyCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)sureAction{
    [self.delegate registerAction];
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == phoneTf) {
        [pwdTf becomeFirstResponder];
    }else if (textField == pwdTf){
        [verifyCodeTf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * validString = @"0123456789";
    if (textField == phoneTf) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                return NO;
            }
        }
        
    }
    return YES;
}
@end
