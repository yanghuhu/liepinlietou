//
//  RegisterContentView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterContentViewDelegate

-(void)getVerifyCode:(NSString *)phone;
- (void)registerAction;

@end


@interface RegisterContentView : UIView

@property (nonatomic , weak) id<RegisterContentViewDelegate>delegate;

@property (nonatomic , strong) NSString * userName;
@property (nonatomic , strong) NSString * password;
@property (nonatomic , strong) NSString *verifyCode;

- (void)getVerifyCodeSuccess;
@end
