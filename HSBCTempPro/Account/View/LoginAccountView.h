//
//  LoginAccountView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardReturnKeyHandler.h"

@protocol LoginAccountViewDelegate
- (void)loginAction;
- (void)forgetPasswordAction;
@end

@interface LoginAccountView : UIView<UITextFieldDelegate>{

    UITextField * userNameTf;
    UITextField * passwordTf;
}

@property (nonatomic , weak) id<LoginAccountViewDelegate>delegate;
@property (nonatomic , strong) NSString * userName;
@property (nonatomic , strong) NSString * password;

@property (nonatomic , assign) BOOL isAutoLogin;

@end
