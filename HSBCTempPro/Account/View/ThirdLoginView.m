//
//  ThirdLoginView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "ThirdLoginView.h"
#import "UIView+YYAdd.h"
@interface ThirdLoginView()

@property (nonatomic , strong) UIControl *bkControl;

@end

@implementation ThirdLoginView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatSubViews];
    }
    return self;
}

- (void)creatSubViews{
    containerView = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:containerView];
    
    NSArray * loginBtInfo = @[@{@"image":@"sina",@"select":@"sinaWeiboLogin"},
                         @{@"image":@"wechat",@"select":@"wechatLogin"},
                         @{@"image":@"qq",@"select":@"QQLogin"},
                         @{@"image":@"facebook",@"select":@"facebookLogin"},
                         @{@"image":@"google",@"select":@"googlePlusLogin"},
                         @{@"image":@"twitter",@"select":@"twitterLogin"}];
    
    CGFloat verPading = VerPxFit(60);
    CGFloat btW = (ScreenWidth - verPading * 5) / 4;
    CGFloat horPading = VerPxFit(30);
    NSInteger numberHor = 4;
    for (int i=0; i<loginBtInfo.count; i++) {
        NSDictionary * btInfo = loginBtInfo[i];
        NSInteger row = i / numberHor;
        CGRect  frame = CGRectMake(verPading + (i-row*numberHor)*(btW + verPading), horPading + (horPading+btW)* row, btW, btW);
        
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.frame = frame;
        [bt setImage:[UIImage imageNamed:btInfo[@"image"]] forState:UIControlStateNormal];
        SEL select = NSSelectorFromString(btInfo[@"select"]);
        [bt addTarget:self action:select forControlEvents:UIControlEventTouchUpInside];
        [containerView addSubview:bt];
    }
    NSInteger row = loginBtInfo.count / 4;
    containerView.height = (row+1)*(btW+horPading)+horPading;
    self.height = containerView.height;
}

- (void)toShow:(BOOL)toShow{
    CGFloat toY = 0;
    if (toShow) {
        [self.superview insertSubview:self.bkControl belowSubview:self];
        toY = ScreenHeight - CGRectGetHeight(self.frame);
    }else{
        toY = ScreenHeight;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.top = toY;
    }];
}

- (UIControl *)bkControl{
    if (!_bkControl) {
        _bkControl = [[UIControl alloc] init];
        [_bkControl addTarget:self action:@selector(hideMe) forControlEvents:UIControlEventTouchUpInside];
        _bkControl.frame = [UIScreen mainScreen].bounds;
        _bkControl.backgroundColor = [UIColor clearColor];
    }
    return _bkControl;
}

- (void)hideMe{
    [self.bkControl removeFromSuperview];
    [self toShow:NO];
}

- (void)sinaWeiboLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeSinaWeibo];
    }
}
- (void)wechatLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeWechat];
    }
}
- (void)QQLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeQQ];
    }
}
- (void)facebookLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeFacebook];
    }
}
- (void)googlePlusLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeGooglePlus];
    }
}
- (void)twitterLogin{
    [self hideMe];
    if (self.delegate && [self.delegate respondsToSelector:@selector(thirdLoginWithType:)]) {
        [self.delegate thirdLoginWithType:ThirdLoginTypeTwitter];
    }
}

@end
