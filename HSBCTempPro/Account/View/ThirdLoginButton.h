//
//  ThirdLoginButton.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThirdLoginTypeModel.h"

@interface ThirdLoginButton : UIButton

@property (nonatomic , strong)  ThirdLoginTypeModel * model;

@end
