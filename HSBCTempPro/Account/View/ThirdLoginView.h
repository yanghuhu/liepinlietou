//
//  ThirdLoginView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThirdLoginManage.h"

@protocol  ThirdLoginViewDelegate <NSObject>

- (void)thirdLoginWithType:(ThirdLoginType)type;

@end

@interface ThirdLoginView : UIView{

    UIView * containerView;
    
}

@property (nonatomic , weak) id<ThirdLoginViewDelegate>delegate;
- (void)toShow:(BOOL)toShow;

@end
