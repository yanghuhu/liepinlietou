//
//  LoginAccountView.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginAccountView.h"
#import "YYTextKeyboardManager.h"

@interface LoginAccountView(){
 
    UIButton * eyeBt;
    UIButton * autoLoginBt;
}
@end

@implementation LoginAccountView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    CGFloat height = VerPxFit(80);
    CGFloat padding = VerPxFit(36);
    
    userNameTf = ({
        UITextField * tf = [[UITextField alloc] init];
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
        tf.leftView = view;
        tf.leftViewMode = UITextFieldViewModeAlways;
        tf.backgroundColor = InputBackColor;
        tf.layer.cornerRadius = height/2.0;
        tf.font = HSBCSystemfont;
        tf.borderStyle = UITextBorderStyleNone;
        tf.returnKeyType = UIReturnKeyNext;
        tf.font = [UIFont systemFontOfSize:15];
        tf.delegate = self;
        tf.placeholder = @"请输入手机号";
        tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [self addSubview:tf];
        [tf mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.and.right.equalTo(self);
            make.height.mas_equalTo(height);
        }];
        tf;
    });
    passwordTf = ({
        UITextField * tf = [[UITextField alloc] init];
        tf.font = HSBCSystemfont;
        tf.layer.cornerRadius = height/2.0;
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
        tf.leftView = view;
        tf.leftViewMode = UITextFieldViewModeAlways;
        tf.backgroundColor = InputBackColor;
        tf.returnKeyType = UIReturnKeyDone;
        tf.font = [UIFont systemFontOfSize:15];
        tf.secureTextEntry = YES;
        tf.borderStyle = UITextBorderStyleNone;
        tf.placeholder = LoginPasswordPlaceholder;
        tf.delegate = self;
        tf.placeholder = @"请输入至少8位有效密码";
        [self addSubview:tf];
        [tf mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(userNameTf);
            make.top.mas_equalTo(userNameTf.mas_bottom).mas_offset(padding);
            make.right.equalTo(self);
            make.height.mas_equalTo(height);
        }];
        eyeBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [eyeBt addTarget:self action:@selector(textFiledSecurityShift) forControlEvents:UIControlEventTouchUpInside];
//        eyeBt.backgroundColor = [UIColor redColor];
        eyeBt.frame = CGRectMake(0, 0, 60, height);
//        eyeBt.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, height/2.0);
        [eyeBt setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
        tf.rightView = eyeBt;
        tf.rightViewMode = UITextFieldViewModeAlways;
        tf;
    });
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBt.layer.cornerRadius = VerPxFit(90)/2.0;
    sureBt.backgroundColor = [UIColor clearColor];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"] forState:UIControlStateNormal];
    [sureBt setTitle: LoginButtonAndViewTitle forState:UIControlStateNormal];
    [sureBt addTarget:self  action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:16];
    sureBt.layer.cornerRadius =5;
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passwordTf.mas_bottom).offset(VerPxFit(60));
        make.height.mas_equalTo(VerPxFit(90));
        make.left.and.right.equalTo(self);
        
    }];

    UIImageView * autologinImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FuXuan_sel"]];
    autologinImgV.tag = AutoLoginImgVTag;
    //    imgV.backgroundColor = [UIColor redColor];
    [self addSubview:autologinImgV];
    [autologinImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sureBt).mas_offset(HorPxFit(70));
        make.top.mas_equalTo(sureBt.mas_bottom).mas_offset(VerPxFit(28));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(35), HorPxFit(35)));
    }];
    UILabel * autoLogin = [[UILabel alloc] init];
    autoLogin.font = [UIFont systemFontOfSize:15];
    autoLogin.text = @"自动登录";
    autoLogin.textColor = [UIColor whiteColor];
    [self addSubview:autoLogin];
    [autoLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(autologinImgV.mas_right);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    autoLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [autoLoginBt addTarget:self action:@selector(autologinShift:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:autoLoginBt];
    [autoLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(autologinImgV);
        make.right.equalTo(autoLogin);
        make.height.mas_equalTo(VerPxFit(40));
        make.centerY.equalTo(autologinImgV.mas_centerY);
    }];
    id obj = [DataDefault objectForKey:DataDefaultAutologinKey isPrivate:NO];
    BOOL isAutoLogin = NO;
    if (!obj) {
        [DataDefault setObject:@(YES) forKey:DataDefaultAutologinKey isPrivate:NO];
        isAutoLogin = YES;
    }else{
        isAutoLogin = [obj boolValue];
    }
    if (isAutoLogin) {
        autoLoginBt.selected = YES;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autoLoginBt.selected = NO;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    
    
    UIButton * findPwdBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [findPwdBt addTarget:self action:@selector(findPwdAction) forControlEvents:UIControlEventTouchUpInside];
    [findPwdBt setTitle:@"忘记密码" forState:UIControlStateNormal];
    findPwdBt.titleLabel.textAlignment = NSTextAlignmentRight;
    findPwdBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [findPwdBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:findPwdBt];
    [findPwdBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(sureBt).mas_offset(-HorPxFit(70));
        make.top.mas_equalTo(sureBt.mas_bottom).offset(VerPxFit(25));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(150), VerPxFit(50)));
    }];

}

- (void)textFiledSecurityShift{
    if (passwordTf.secureTextEntry) {
        passwordTf.secureTextEntry = NO;
        [eyeBt setImage:[UIImage imageNamed:@"eyeOpen"] forState:UIControlStateNormal];
    }else{
        passwordTf.secureTextEntry = YES;
        [eyeBt setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
    }
}

- (void)autologinShift:(UIButton *)bt{
    UIImageView * autologinImgV = [self viewWithTag:AutoLoginImgVTag];
    bt.selected = !bt.selected;
    if (bt.selected) {
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    [DataDefault setObject:@(bt.selected) forKey:DataDefaultAutologinKey isPrivate:NO];
}

- (BOOL)isAutoLogin{
    return  autoLoginBt.selected;
}

- (NSString *)userName{
    _userName = userNameTf.text;
    return _userName;
}

- (NSString *)password{
    _password = passwordTf.text;
    return _password;
}

- (void)sureAction{
    [self.delegate loginAction];
}

- (void)findPwdAction{
    [self.delegate forgetPasswordAction];
}

#pragma mark -- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == userNameTf) {
        [passwordTf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == userNameTf) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        NSString * validString = @"0123456789";
        if ([validString rangeOfString:string].location != NSNotFound) {
            return YES;
        }else if ([string isEqualToString:@""]){
            return YES;
        }
    }
    
    return YES;
}
@end
