//
//  PhoneBindingViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ThirdUserModel.h"

@interface PhoneBindingViewController : BaseViewController


@property (nonatomic , assign) BOOL isAutoLoginCheck;
@property (nonatomic , strong) ThirdUserModel * thirdUserModel;

@end
