//
//  UserBindingViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2018/3/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserBindingViewController.h"
#import "AccountModule.h"
#import "PhoneBindingViewController.h"
#import "PositionDemandViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"

@interface UserBindingViewController (){
 
    UIButton * verifyBt;
}

@property (nonatomic , strong) UITextField * phoneTf;
@property (nonatomic , strong) UITextField * verifyTf;
@property (nonatomic , strong) UITextField * pwdTf;

@end

@implementation UserBindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"快速注册";
    self.view.backgroundColor = color(24, 26, 30, 1);
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)createSubViews{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(40);
    CGFloat tfHeight = VerPxFit(100);
    _phoneTf = [self tf:@"输入手机号"];
    _phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _verifyTf = [self tf:@"输入验证码"];
    _pwdTf = [self tf:@"请输入至少8位的密码"];
    _pwdTf.secureTextEntry = YES;
    
    [_phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.equalTo(self.view).mas_offset(VerPxFit(220));
        make.height.mas_equalTo(tfHeight);
    }];
    
    verifyBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [verifyBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    verifyBt.layer.cornerRadius = 6;
    verifyBt.backgroundColor = color(228, 17, 33, 1);
    verifyBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:verifyBt];
    [verifyBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.mas_equalTo(_phoneTf.mas_bottom).mas_offset(verPadding);
        make.height.mas_equalTo(tfHeight);
        make.width.mas_equalTo(HorPxFit(200));
    }];
    
    [_verifyTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.height.equalTo(_phoneTf);
        make.right.mas_equalTo(verifyBt.mas_left).mas_offset(-HorPxFit(20));
        make.top.mas_equalTo(_phoneTf.mas_bottom).mas_offset(verPadding);
    }];
    
    [_pwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.height.equalTo(_phoneTf);
        make.top.mas_equalTo(_verifyTf.mas_bottom).mas_offset(verPadding);
    }];
    
    UILabel * msgLb = [[UILabel alloc] init];
    msgLb.text =[NSString stringWithFormat: @"注册后您的猎场账号和%@账号都可登陆",_thirdUserModel.platformType];
    msgLb.textColor = color(78, 87, 100, 1);
    msgLb.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:msgLb];
    [msgLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(_pwdTf);
        make.height.mas_equalTo(VerPxFit(30));
        make.top.mas_equalTo(_pwdTf.mas_bottom).mas_offset(VerPxFit(90));
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt setTitle:@"注册" forState:UIControlStateNormal];
    [sureBt addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:17];
    sureBt.backgroundColor = color(228, 17, 33, 1);
    sureBt.layer.cornerRadius = 8;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(msgLb);
        make.top.mas_equalTo(msgLb.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(VerPxFit(100));
    }];
    
    UIButton * toLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [toLoginBt setTitle:@"已有账号,立即登陆" forState:UIControlStateNormal];
    [toLoginBt addTarget:self action:@selector(toLoginAction) forControlEvents:UIControlEventTouchUpInside];
    toLoginBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [toLoginBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    toLoginBt.layer.cornerRadius = 8;
    [self.view addSubview:toLoginBt];
    [toLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(sureBt);
        make.top.mas_equalTo(sureBt.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(VerPxFit(70));
    }];
}

- (UITextField *)tf:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.backgroundColor = color(45, 47, 50, 1);
    tf.layer.cornerRadius = 5;
    tf.attributedPlaceholder =  [[NSAttributedString alloc] initWithString:placeHolder attributes:
                                 @{NSForegroundColorAttributeName:[UIColor whiteColor],
                                   NSFontAttributeName:[UIFont systemFontOfSize:15]
                                   }];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.textColor = [UIColor whiteColor];
    tf.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:tf];
    return  tf;
}

- (void)toLoginAction{
    PhoneBindingViewController * vc = [[PhoneBindingViewController alloc] init];
    vc.thirdUserModel = _thirdUserModel;
    vc.isAutoLoginCheck = _isAutoLoginCheck;
    [self.navigationController pushViewController:vc animated:YES];
}

//  注册
- (void)registerAction{
    NSString * userName = _phoneTf.text;
    NSString * password = _pwdTf.text;
    NSString * verifyCode = _verifyTf.text;
    
    if (!userName || ![BaseHelper isValidateMobile:userName]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    if(!verifyCode || verifyCode.length == 0){
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }
    if(!password || password.length == 0 ||password.length<8){
        [BaseHelper showProgressHud:@"请输入有效密码" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule registerWithUsername:userName password:password verfyCode:verifyCode success:^(UserModel * model){
        [BaseHelper hideProgressHudInView:self.view];
        [self bingAction];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

// 绑定
- (void)bingAction{
    NSString * userName = _phoneTf.text;
    NSString * password = _pwdTf.text;
    NSString * verifyCode = _verifyTf.text;
    
    if (!userName || ![BaseHelper isValidateMobile:userName]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    if(!verifyCode || verifyCode.length == 0){
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }
    if(!password || password.length == 0){
        [BaseHelper showProgressHud:@"请输入密码" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule accountBingWithCellphone:_phoneTf.text gender:_thirdUserModel.gender headPic:_thirdUserModel.headPic nickName:_thirdUserModel.nickName password:_pwdTf.text platformId:_thirdUserModel.platformId thirdPlatformType:_thirdUserModel.platformIdentifying verificationCode:nil success:^(UserModel * model){
        [BaseHelper hideProgressHudInView:self.view];
        
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
        PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        nav.navigationBar.hidden = YES;
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        if (_isAutoLoginCheck) {
            NSDictionary * info = @{@"cellPhone":_phoneTf.text,@"id":@(model.uid),@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
            [BaseHelper saveUserToDataDefault:info];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)getVerifyCode{
    if (!_phoneTf.text || ![BaseHelper isValidateMobile:_phoneTf.text]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule getSmsVerifyCode:_phoneTf.text type:@"Register" success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [self getVerifyCodeSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)getVerifyCodeSuccess{
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyBt.enabled = YES;
                [verifyBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyBt.enabled = NO;
                [verifyBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}




@end
