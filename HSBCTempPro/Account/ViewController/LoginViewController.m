//
//  LoginViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/1.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginViewController.h"
#import "ThirdLoginView.h"
#import "UIView+YYAdd.h"
#import "LoginAccountView.h"
#import "ThirdLoginManage.h"
#import "RegisterViewController.h"
#import "PositionDemandViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "AccountModule.h"
#import "PasswordResetViewController.h"
#import "UIView+YYAdd.h"
#import "RegisterContentView.h"
#import "WeekDataView.h"
#import "ThirdLoginTypeModel.h"
#import "UserBindingViewController.h"
#import "ThirdLoginButton.h"
#import "AlipayManage.h"
#import "ThirdUserModel.h"

#define TitleSelectedColor [UIColor whiteColor]
#define TitleUnSelectedColor [UIColor lightGrayColor]

#define TitleSelectFont  [UIFont systemFontOfSize:16]
#define TitleUnSelectFont [UIFont systemFontOfSize:13]

typedef enum {
    AppearTypeLogin,
    AppearTypeRegister
}AppearType;

@interface LoginViewController ()<ThirdLoginViewDelegate,ThirdLoginManageDelegate,RegisterContentViewDelegate,LoginAccountViewDelegate>{
    LoginAccountView * loginAccountView;
    RegisterContentView * registerAccountView;
    UIButton * loginBt;
    UIButton * registerBt;
    UIImageView * flagImagVLogin;
    UIImageView * flagImagVRegister;
    
    AlipayManage * alipayManage;
}

@property (nonatomic , strong) ThirdLoginManage * thirdLoginManage;
@property (nonatomic , strong) ThirdLoginView * thirdLoginView;
@property (nonatomic , assign) AppearType appearType;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginBk"]];
    bkImgV.frame = self.view.bounds;
    [self.view addSubview:bkImgV];

    [super viewDidLoad];
    self.navigationItem.title = LoginButtonAndViewTitle;
    self.view.backgroundColor = ViewControllerBkColor;
    
    self.appearType = AppearTypeLogin;
    
    alipayManage = [[AlipayManage alloc] init];
    alipayManage.viewcontroller  = self;
    alipayManage.orderId = @"837857568364758";
    alipayManage.price = 0.01;
    
    [AccountModule getThirdLoginTypesSuccess:^(NSArray * array){
        if (array) {
            [self updateThirdLoginAppear:array];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)createViews{
    
    CGFloat contentPadiing = HorPxFit(50);
    CGFloat contentViewW = ScreenWidth-2*contentPadiing;
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleIcon"]];
    [self.view addSubview:imgV];
    CGFloat titleFlagW = HorPxFit(90);
    CGFloat titleFlagH = titleFlagW*80/180;
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view).mas_offset(VerPxFit(220));
        make.size.mas_equalTo(CGSizeMake(titleFlagW, titleFlagH));
    }];
    
    CGFloat btH = VerPxFit(40);
    CGFloat btW = contentViewW /2.0;
    loginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBt setTitle:@"登录" forState:UIControlStateNormal];
    [loginBt setTitleColor:TitleSelectedColor forState:UIControlStateNormal];
    loginBt.titleLabel.font = TitleSelectFont;
    [loginBt addTarget:self action:@selector(shiftAppearType:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBt];
    
    [loginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(contentPadiing);
        make.top.equalTo(self.view).mas_offset(VerPxFit(380));
        make.width.mas_equalTo(btW);
        make.height.mas_equalTo(btH);
    }];
    
    UILabel * sepline = [[UILabel alloc] init];
    sepline.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:sepline];
    [sepline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(loginBt.mas_right);
        make.top.equalTo(loginBt);
        make.width.mas_equalTo(1);
        make.height.equalTo(loginBt);
    }];
    
    registerBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerBt setTitle:@"注册" forState:UIControlStateNormal];
    registerBt.titleLabel.font = TitleUnSelectFont;
    [registerBt setTitleColor:TitleUnSelectedColor forState:UIControlStateNormal];
    [registerBt addTarget:self action:@selector(shiftAppearType:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBt];
    [registerBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(sepline.mas_right);
        make.top.equalTo(loginBt);
        make.width.mas_equalTo(btW);
        make.height.mas_equalTo(btH);
    }];

    flagImagVLogin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"accountFlag"]];
    [self.view addSubview:flagImagVLogin];
    [flagImagVLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loginBt.mas_bottom).mas_offset(VerPxFit(16));
        make.centerX.mas_equalTo(loginBt.mas_centerX);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(2);
    }];
    flagImagVRegister = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"accountFlag"]];
    flagImagVRegister.hidden = YES;
    [self.view addSubview:flagImagVRegister];
    [flagImagVRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(registerBt.mas_bottom).mas_offset(VerPxFit(16));
        make.centerX.mas_equalTo(registerBt.mas_centerX);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(2);
    }];
    
    loginAccountView = [[LoginAccountView alloc] init];
    loginAccountView.delegate = self;
    [self.view addSubview:loginAccountView];
    [loginAccountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(contentPadiing);
        make.top.mas_equalTo(loginBt.mas_bottom).mas_offset(VerPxFit(50));
        make.right.equalTo(self.view).mas_offset(-contentPadiing);
        make.height.mas_equalTo(VerPxFit(450));
    }];
    
    registerAccountView = [[RegisterContentView alloc] init];
    registerAccountView.delegate = self;
    registerAccountView.hidden = YES;
    [self.view insertSubview:registerAccountView belowSubview:loginAccountView];
    [registerAccountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(contentPadiing);
        make.top.equalTo(loginAccountView);
        make.right.equalTo(self.view).mas_offset(-contentPadiing);
        make.height.mas_equalTo(VerPxFit(520));
    }];
}

- (void)updateThirdLoginAppear:(NSArray *)thirdLoginTypes{
    if(!thirdLoginTypes || thirdLoginTypes.count == 0){
        return;
    }
    NSMutableArray * types = [NSMutableArray arrayWithCapacity:thirdLoginTypes.count];
    for (ThirdLoginTypeModel * model in thirdLoginTypes) {
        if ([model.state isEqualToString:@"ON"]) {
            [types addObject:model];
        }
    }
    NSInteger rows = types.count /3 + (types.count %3 >0?1:0);
    CGFloat verPadding = VerPxFit(35);
    CGFloat titleH = VerPxFit(40);
    CGFloat itemHW = VerPxFit(80);
    CGFloat contentH = (rows+1)*verPadding + titleH + itemHW*rows;
    CGFloat horPadding = (ScreenWidth - 3*itemHW) / 4.0;
    
    UIView * thirdLoginView = [[UIView alloc] init];
    thirdLoginView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:thirdLoginView];
    [thirdLoginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(self.view);
        make.height.mas_equalTo(contentH);
    }];
    
    UIImageView * titleFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lineForThirdLogin"]];
    [thirdLoginView addSubview:titleFlag];
    [titleFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(thirdLoginView).mas_offset(HorPxFit(40));
        make.right.equalTo(thirdLoginView).mas_offset(-HorPxFit(40));
        make.top.equalTo(thirdLoginView).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(1);
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"第三方登陆";
    titleLb.textColor = RGB16(0xE1E1E1);
    titleLb.font = [UIFont systemFontOfSize:15];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [thirdLoginView addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(thirdLoginView.centerX);
        make.top.equalTo(thirdLoginView);
        //        make.centerY.mas_equalTo(titleFlag.centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(200), VerPxFit(25)));
    }];
    
    CGFloat curX = horPadding;
    CGFloat curY = verPadding+titleH;
    for (int i=0; i<types.count; i++) {
        ThirdLoginTypeModel * model = types[i];
        NSString * key = model.thirdPlatformType;
        ThirdLoginButton * bt = [ThirdLoginButton buttonWithType:UIButtonTypeCustom];
        bt.tag = i;
        bt.model = model;
        [bt addTarget:self action:@selector(thirdLogin:) forControlEvents:UIControlEventTouchUpInside];
        [bt setImage:[UIImage imageNamed:key] forState:UIControlStateNormal];
        bt.frame = CGRectMake(curX, curY, itemHW, itemHW);
        bt.backgroundColor = [UIColor clearColor];
        [thirdLoginView addSubview:bt];
        if (i%3 < 2) {
            curX+= (horPadding+itemHW);
        }else{
            curX = horPadding;
            curY += (verPadding + itemHW);
        }
    }
}

- (void)thirdLogin:(ThirdLoginButton *)bt{
    
    NSInteger thirdLoginType;
    
    if ([bt.model.thirdPlatformType isEqualToString:@"QQ"]) {
        thirdLoginType = ThirdLoginTypeQQ;
    }else if ([bt.model.thirdPlatformType isEqualToString:@"Wechat"]){
        thirdLoginType = ThirdLoginTypeWechat;
    }else if ([bt.model.thirdPlatformType isEqualToString:@"FaceBook"]){
        thirdLoginType = ThirdLoginTypeFacebook;
    }else if ([bt.model.thirdPlatformType isEqualToString:@"Google"]){
        thirdLoginType = ThirdLoginTypeGooglePlus;
    }else if ([bt.model.thirdPlatformType isEqualToString:@"Twitter"]){
        thirdLoginType = ThirdLoginTypeTwitter;
    }
 
    [ThirdLoginManage loginWithType:thirdLoginType callBack:^(ThirdUserModel * model){
        // 登录成功
        if(model.state == 1){
            [self loginWithThird:model platform:ThirdLoginTypeWechat];
        }else if(model.state == 0){  // 登录 失败
            [BaseHelper showProgressHud:@"第三方授权失败" showLoading:NO canHide:YES];
        }
    }];
}

- (void)loginWithThird:(ThirdUserModel *)model platform:(ThirdLoginType)thirdLoginType{
    NSString * platFormString;
    if (thirdLoginType == ThirdLoginTypeQQ) {
        platFormString = @"QQ";
    }else if (thirdLoginType == ThirdLoginTypeWechat){
        platFormString = @"Wechat";
    }else if (thirdLoginType == ThirdLoginTypeFacebook){
        platFormString = @"FaceBook";
    }else if (thirdLoginType == ThirdLoginTypeGooglePlus){
        platFormString = @"Google";
    }else if (thirdLoginType == ThirdLoginTypeTwitter){
        platFormString = @"Twitter";
    }
    model.platformIdentifying = thirdLoginType == ThirdLoginTypeWechat?@"Wechat":platFormString;
    model.platformType =  (thirdLoginType == ThirdLoginTypeWechat?@"微信":platFormString);
    @weakify(self)
    [AccountModule thirdLoginWihtId:[model.platformId integerValue] thirdPlatformType:platFormString success:^(UserModel * userModel){
        [BaseHelper hideProgressHudInView:self.view];
        PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        if (loginAccountView.isAutoLogin) {
            NSDictionary * info = @{@"cellPhone":userModel.cellphone,@"id":@(userModel.uid),@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
            [BaseHelper saveUserToDataDefault:info];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        if (responseType == kResponseTypeNotFound) {
            //  跳转绑定用户
            UserBindingViewController * vc = [[UserBindingViewController alloc] init];
            vc.thirdUserModel = model;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
}


- (void)shiftAppearType:(UIButton *)bt{
    if (bt == loginBt) {
        if (_appearType == AppearTypeLogin) {
            return;
        }
        [loginBt setTitleColor:TitleSelectedColor forState:UIControlStateNormal];
        [registerBt setTitleColor:TitleUnSelectedColor forState:UIControlStateNormal];
        [self.view bringSubviewToFront:loginAccountView];
        loginAccountView.hidden = NO;
        flagImagVLogin.hidden = NO;
        flagImagVRegister.hidden = YES;
        registerAccountView.hidden = YES;
        loginBt.titleLabel.font = TitleSelectFont;
        registerBt.titleLabel.font = TitleUnSelectFont;
        self.appearType = AppearTypeLogin;
    }
    if (bt == registerBt) {
        if (_appearType == AppearTypeRegister) {
            return;
        }
        [registerBt setTitleColor:TitleSelectedColor forState:UIControlStateNormal];
        [loginBt setTitleColor:TitleUnSelectedColor forState:UIControlStateNormal];
        [self.view bringSubviewToFront:registerAccountView];
        loginAccountView.hidden = YES;
        flagImagVLogin.hidden = YES;
        flagImagVRegister.hidden = NO;
        registerBt.titleLabel.font = TitleSelectFont;
        loginBt.titleLabel.font = TitleUnSelectFont;
        registerAccountView.hidden = NO;
        self.appearType = AppearTypeRegister;
    }
}

- (void)sureAction{
    [self.view resignFirstResponder];
// test
//    [alipayManage doAPPay];
//    return;
    if (_appearType == AppearTypeLogin) {
        NSString * userName =  loginAccountView.userName;
        NSString * password = loginAccountView.password;
        if (!userName || ![BaseHelper isValidateMobile:userName]) {
            [BaseHelper showProgressHud:@"手机号码有误，请重新输入" showLoading:NO canHide:YES];
            return;
        }
        if(!password || password.length == 0 ||password.length<8){
            [BaseHelper showProgressHud:@"登录密码有误,请重新输入" showLoading:NO canHide:YES];
            return;
        }
//        if(![BaseHelper passwordlength:password]){
//            [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
//            return;
//        }
        [BaseHelper showProgressLoadingInView:self.view];
        [AccountModule loginWithUserName:userName password:password success:^(UserModel * userModel){
            [BaseHelper hideProgressHudInView:self.view];
            PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
            BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
            AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
            appdelete.window.rootViewController = nav;
            
            if (loginAccountView.isAutoLogin) {
                NSDictionary * info = @{@"cellPhone":userName,@"id":@(userModel.uid),@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
                [BaseHelper saveUserToDataDefault:info];
            }
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
        }];
    }else{
        NSString * userName = registerAccountView.userName;
        NSString * password = registerAccountView.password;
        NSString * verifyCode = registerAccountView.verifyCode;
        
        if (!userName || ![BaseHelper isValidateMobile:userName]) {
            [BaseHelper showProgressHud:@"手机号码有误，请重新输入" showLoading:NO canHide:YES];
            return;
        }
        if(!password || password.length == 0){
            [BaseHelper showProgressHud:@"请输入密码" showLoading:NO canHide:YES];
            return;
        }
        if(![BaseHelper passwordlength:password]){
            [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
            return;
        }
        if(!verifyCode || verifyCode.length == 0){
            [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
            return;
        }
        [BaseHelper showProgressLoadingInView:self.view];
        [AccountModule registerWithUsername:userName password:password verfyCode:verifyCode success:^(UserModel * model){
            [BaseHelper hideProgressHudInView:self.view];
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
            BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
            nav.navigationBar.hidden = YES;
            AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
            appdelete.window.rootViewController = nav;
            
            if (loginAccountView.isAutoLogin) {
                NSDictionary * info = @{@"cellPhone":userName,@"id":@(model.uid),@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
                [BaseHelper saveUserToDataDefault:info];
            }
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            if (error) {
                [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
            }
        }];
    }
}

- (void)showThirdLoginView{
    if (!self.thirdLoginView.superview) {
        [self.view addSubview:self.thirdLoginView];
    }
    [self.thirdLoginView toShow:YES];
}

- (ThirdLoginView *)thirdLoginView{
    if (!_thirdLoginView) {
        _thirdLoginView = [[ThirdLoginView alloc] initWithFrame:CGRectMake(0, ScreenHeight, ScreenWidth, 100)];
        _thirdLoginView.delegate = self;
        _thirdLoginView.backgroundColor = [UIColor whiteColor];
    }
    return _thirdLoginView;
}

#pragma mark --RegisterContentViewDelegate
- (void)registerAction{
    [self sureAction];
}

- (void)getVerifyCode:(NSString *)phone{
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule getSmsVerifyCode:phone type:@"Register" success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [registerAccountView getVerifyCodeSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- LoginAccountViewDelegate

- (void)loginAction{
//
//    WeekDataView * chatView = [[WeekDataView alloc] initWithFrame:CGRectMake(HorPxFit(20), 0, ScreenWidth - 2*HorPxFit(20), VerPxFit(560))];
//    chatView.center = self.view.center;
//    [self.view addSubview:chatView];
//    return;
//    RateView * rateView = [[RateView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-HorPxFit(40), HorPxFit(400))];
//    rateView.center = self.view.center;
//    [self.view addSubview:rateView];

    [self sureAction];
}

- (void)forgetPasswordAction{
    PasswordResetViewController * vc = [[PasswordResetViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end

