//
//  RegisterViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/2.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RegisterViewController.h"
#import "PositionDemandViewController.h"
#import "AppDelegate.h"
#import "BaseNavigationController.h"
#import "AccountModule.h"

@interface RegisterViewController ()<UITextFieldDelegate>{
    
    UIView * contentV;
    UITextField * phoneTf;
    UITextField * pwdTf;
    UITextField * verifyCodeTf;
    UIButton * verifyCodeBt;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR(240, 240, 240, 1);
    self.navigationItem.title = RegisterButtonAndPageTitle;
    [self createViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createViews{
    CGFloat iconW = HorPxFit(50);
    CGFloat horpadding = HorPxFit(20);
    CGFloat verpadding = VerPxFit(15);
    CGFloat tfH = VerPxFit(100);
    
    contentV = [[UIView alloc] init];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 5;
    [self.view addSubview:contentV];
    [contentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(50));
        make.right.equalTo(self.view).mas_equalTo(-HorPxFit(50));
        make.top.mas_equalTo(VerPxFit(250));
        make.height.mas_equalTo(tfH*3+3);
    }];
    
    UIImageView * phoneIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nameIcon.png"]];
    phoneIcon.contentMode = UIViewContentModeScaleAspectFit;
    [contentV addSubview:phoneIcon];
    [phoneIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentV).mas_offset((tfH-iconW)/2.0);
        make.left.equalTo(contentV).mas_offset(horpadding);
        make.size.mas_equalTo(CGSizeMake(iconW, iconW));
    }];
    
    phoneTf = [self tfWihtPlaceHolder:@"手机号"];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [contentV addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(phoneIcon.mas_right).mas_offset(horpadding);
        make.top.equalTo(contentV);
        make.right.equalTo(contentV).mas_offset(-horpadding);
        make.height.mas_equalTo(tfH);
    }];
    
    UILabel * phoneSeqPwd = [self lb];
    [phoneSeqPwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(phoneTf.mas_bottom);
        make.left.and.right.equalTo(contentV);
        make.height.mas_equalTo(1);
    }];
    
    UIImageView * pwdIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password.png"]];
    pwdIcon.contentMode = UIViewContentModeScaleAspectFit;
    [contentV addSubview:pwdIcon];
    [pwdIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(phoneSeqPwd.mas_bottom).mas_offset((tfH-iconW)/2.0);
        make.left.equalTo(contentV).mas_offset(horpadding);
        make.size.mas_equalTo(CGSizeMake(iconW, iconW));
    }];
    
    pwdTf = [self tfWihtPlaceHolder:@"密码"];
    pwdTf.secureTextEntry = YES;
    [contentV addSubview:pwdTf];
    [pwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdIcon.mas_right).mas_offset(horpadding);
        make.top.mas_equalTo(phoneSeqPwd.mas_bottom);
        make.right.equalTo(contentV).mas_offset(-horpadding);
        make.height.mas_equalTo(tfH);
    }];
    
    UILabel * pwdSeqCode = [self lb];
    [pwdSeqCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdTf.mas_bottom);
        make.left.and.right.equalTo(contentV);
        make.height.mas_equalTo(1);
    }];
    
    UIImageView * codeIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"verifyCode.png"]];
    codeIcon.contentMode = UIViewContentModeScaleAspectFit;
    [contentV addSubview:codeIcon];
    [codeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdSeqCode.mas_bottom).mas_offset((tfH-iconW)/2.0);
        make.left.equalTo(contentV).mas_offset(horpadding);
        make.size.mas_equalTo(CGSizeMake(iconW, iconW));
    }];
    
    verifyCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [verifyCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verifyCodeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    verifyCodeBt.backgroundColor = [UIColor blackColor];
    verifyCodeBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [contentV addSubview:verifyCodeBt];
    [verifyCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(pwdSeqCode.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(contentV).mas_offset(-horpadding);
        make.width.mas_equalTo(HorPxFit(200));
        make.bottom.equalTo(contentV).offset(-verpadding);
    }];
    
    verifyCodeTf = [self tfWihtPlaceHolder:@"验证码"];
    verifyCodeTf.returnKeyType = UIReturnKeyDone;
    verifyCodeTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [contentV addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(pwdIcon.mas_right).mas_offset(horpadding);
        make.top.mas_equalTo(pwdSeqCode.mas_bottom);
        make.right.mas_equalTo(verifyCodeBt.mas_left).mas_offset(-horpadding);
        make.height.mas_equalTo(tfH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(registerSureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    sureBt.backgroundColor = [UIColor clearColor];
    sureBt.layer.cornerRadius = 5;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(contentV);
        make.top.mas_equalTo(contentV.mas_bottom).mas_offset(VerPxFit(70));
        make.height.mas_equalTo(VerPxFit(90));
    }];
}

- (void)getVerifyCode{
    if ([BaseHelper isValidateMobile:phoneTf.text]) {
        [BaseHelper showProgressLoadingInView:self.view];

        [AccountModule getSmsVerifyCode:phoneTf.text type:@"Register" success:^{
            [BaseHelper hideProgressHudInView:self.view];
            [self getVerifyCodeAction];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
         [self netFailWihtError:error andStatusCode:responseType];
        }];
        
    }else{
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
    }
}

- (void)getVerifyCodeAction{
    
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];

    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = YES;
                [verifyCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = NO;
                [verifyCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)registerSureAction{
    
    
    [self.view resignFirstResponder];
    NSString * userName = phoneTf.text;
    NSString * password = pwdTf.text;
    NSString * verify = verifyCodeTf.text;
    
    if (!userName || ![BaseHelper isValidateMobile:userName]) {
        [BaseHelper showProgressHud:@"手机号码有误，请重新输入" showLoading:NO canHide:YES];
        return;
    }
    if(!password || password.length == 0){
        [BaseHelper showProgressHud:@"请输入密码" showLoading:NO canHide:YES];
        return;
    }
    if (!verify || verify.length == 0) {
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule registerWithUsername:userName password:password verfyCode:verify success:^(UserModel * model){
        
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
        PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [self netFailWihtError:error andStatusCode:responseType];
        }
    }];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.backgroundColor = COLOR(240, 240, 240, 1);
    [contentV addSubview:lb];
    return lb;
}


- (UITextField *)tfWihtPlaceHolder:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.delegate = self;
    tf.borderStyle = UITextBorderStyleNone;
    tf.font = [UIFont systemFontOfSize:15];
    tf.placeholder = placeHolder;
    tf.returnKeyType = UIReturnKeyNext;
    return tf;
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == phoneTf) {
        [pwdTf becomeFirstResponder];
    }else if (textField == pwdTf){
        [verifyCodeTf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

@end
