//
//  PhoneBindingViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "PhoneBindingViewController.h"
#import "AccountModule.h"
#import "BaseNavigationController.h"
#import "PositionDemandViewController.h"
#import "AppDelegate.h"

@interface PhoneBindingViewController ()

@property (nonatomic , strong) UITextField * phoneTf;
@property (nonatomic , strong) UITextField * pwdTf;

@end

@implementation PhoneBindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"关联账号";
    self.view.backgroundColor = color(24, 26, 30, 1);
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(40);
    CGFloat tfHeight = VerPxFit(100);
    
    UILabel * msgLb = [[UILabel alloc] init];
    msgLb.text = [NSString stringWithFormat:@"关联%@账号",_thirdUserModel.platformType];
    msgLb.font = [UIFont systemFontOfSize:13];
    msgLb.textColor = [UIColor whiteColor];
    [self.view addSubview:msgLb];
    [msgLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.equalTo(self.view).mas_offset(VerPxFit(200));
        make.height.mas_equalTo(40);
    }];
    
    _phoneTf = [self tf:@"手机号"];
    _phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _pwdTf = [self tf:@"密码"];
    _pwdTf.secureTextEntry = YES;

    [_phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.mas_equalTo(msgLb.mas_bottom).mas_offset(0);
        make.height.mas_equalTo(tfHeight);
    }];
    
    [_pwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.mas_equalTo(_phoneTf.mas_bottom).mas_offset(verPadding);
        make.height.mas_equalTo(tfHeight);
    }];
    
    
    UILabel * bingMsgLb = [[UILabel alloc] init];
    bingMsgLb.text = [NSString stringWithFormat:@"关联后您的%@账号和猎场账号都可登陆",_thirdUserModel.platformType];
    bingMsgLb.font = [UIFont systemFontOfSize:13];
    bingMsgLb.textColor = [UIColor whiteColor];
    [self.view addSubview:bingMsgLb];
    [bingMsgLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(horPadding);
        make.right.equalTo(self.view).mas_offset(-horPadding);
        make.top.mas_equalTo(_pwdTf.mas_bottom).mas_offset(VerPxFit(100));
        make.height.mas_equalTo(20);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt setTitle:@"登陆" forState:UIControlStateNormal];
    [sureBt addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:17];
    sureBt.backgroundColor = color(228, 17, 33, 1);
    sureBt.layer.cornerRadius = 8;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(msgLb);
        make.top.mas_equalTo(bingMsgLb.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(VerPxFit(100));
    }];
}

- (UITextField *)tf:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.backgroundColor = color(45, 47, 50, 1);
    tf.layer.cornerRadius = 5;
    tf.attributedPlaceholder =  [[NSAttributedString alloc] initWithString:placeHolder attributes:
                                 @{NSForegroundColorAttributeName:[UIColor whiteColor],
                                   NSFontAttributeName:[UIFont systemFontOfSize:15]
                                   }];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.textColor = [UIColor whiteColor];
    tf.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:tf];
    return  tf;
}

- (void)loginAction{
    [_phoneTf resignFirstResponder];
    [_pwdTf resignFirstResponder];
    NSString * userName = _phoneTf.text;
    NSString * password = _pwdTf.text;
    
    if (!userName || ![BaseHelper isValidateMobile:userName]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    if(!password || password.length == 0){
        [BaseHelper showProgressHud:@"请输入密码" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule accountBingWithCellphone:_phoneTf.text gender:_thirdUserModel.gender headPic:_thirdUserModel.headPic nickName:_thirdUserModel.nickName password:_pwdTf.text platformId:_thirdUserModel.platformId thirdPlatformType:_thirdUserModel.platformIdentifying verificationCode:nil success:^(UserModel * model){
        [BaseHelper hideProgressHudInView:self.view];
        
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
        PositionDemandViewController * vc = [[PositionDemandViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        nav.navigationBar.hidden = YES;
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        if (_isAutoLoginCheck) {
            NSDictionary * info = @{@"cellPhone":_phoneTf.text,@"id":@(model.uid),@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
            [BaseHelper saveUserToDataDefault:info];
        }
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

@end
