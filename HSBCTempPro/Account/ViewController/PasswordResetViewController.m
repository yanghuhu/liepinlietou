//
//  PasswordResetViewController.m
//  HSBCTempPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PasswordResetViewController.h"
#import "AccountModule.h"
#import "NextResetPwdViewController.h"

@interface PasswordResetViewController ()<UITextFieldDelegate>{
    UITextField * phoneTf;
    UITextField * pwdTf;
    UITextField * verifyCodeTf;
    UIButton * verifyCodeBt;
}

@end

@implementation PasswordResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden  = NO;
    self.view.backgroundColor = ViewControllerBkColor;
    self.navigationItem.title = @"身份验证";
    [self createViews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createViews{
    CGFloat horpadding = HorPxFit(20);
    CGFloat verpadding = VerPxFit(36);
    CGFloat tfH = VerPxFit(80);
    
    phoneTf = [self tfWihtPlaceHolder:@"手机号"];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self.view addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(50));
        make.top.equalTo(self.view).mas_offset(VerPxFit(200));
        make.right.equalTo(self.view).mas_offset(-HorPxFit(50));
        make.height.mas_equalTo(tfH);
    }];
    
    verifyCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [verifyCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verifyCodeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [verifyCodeBt setBackgroundImage:[UIImage imageNamed:@"verifyCodeBk"] forState:UIControlStateNormal];
    verifyCodeBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:verifyCodeBt];
    [verifyCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(phoneTf);
        make.width.mas_equalTo(HorPxFit(200));
        make.height.mas_equalTo(tfH);
    }];
    
    verifyCodeTf = [self tfWihtPlaceHolder:@"验证码"];
    verifyCodeTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self.view addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneTf);
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verpadding);
        make.right.mas_equalTo(verifyCodeBt.mas_left).mas_offset(-horpadding);
        make.height.mas_equalTo(tfH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"下一步" forState:UIControlStateNormal];
    [sureBt setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:16];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"""] forState:UIControlStateNormal];
    sureBt.layer.cornerRadius = 5;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(phoneTf);
        make.top.mas_equalTo(verifyCodeTf.mas_bottom).mas_offset(VerPxFit(60));
        make.height.mas_equalTo(VerPxFit(90));
    }];
}

- (void)getVerifyCode{
    if ([BaseHelper isValidateMobile:phoneTf.text]) {
        [BaseHelper showProgressLoadingInView:self.view];
        [AccountModule getSmsVerifyCode:phoneTf.text type:@"Forget" success:^{
            [BaseHelper hideProgressHudInView:self.view];
            [self getVerifyCodeAction];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            [self netFailWihtError:error andStatusCode:responseType];
        }];
        
    }else{
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
    }
}


- (void)getVerifyCodeAction{
    
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];
    
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = YES;
                [verifyCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                verifyCodeBt.enabled = NO;
                [verifyCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateNormal)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)resetAction{
    
    [self.view resignFirstResponder];
    NSString * iphone = phoneTf.text;
    NSString * verify = verifyCodeTf.text;
    
    if (!iphone || ![BaseHelper isValidateMobile:iphone]) {
        [BaseHelper showProgressHud:@"手机号码有误，请重新输入" showLoading:NO canHide:YES];
        return;
    }
    
    if (verify.length!=6) {
        [BaseHelper showProgressHud:@"验证码输入有误，请重新输入" showLoading:NO canHide:YES];
    }

    @weakify(self)
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule resetPasswordWithUsername:iphone  verfyCode:verify success:^{
        [BaseHelper hideProgressHudInView:self.view];
        @strongify(self)
        if (!self) {
            return ;
        }
        NextResetPwdViewController *nextVC = [NextResetPwdViewController new];
        [self.navigationController pushViewController:nextVC animated:YES];
        
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [self netFailWihtError:error andStatusCode:responseType];
        }
    }];
}


- (UITextField *)tfWihtPlaceHolder:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.delegate = self;
    tf.layer.cornerRadius = HorPxFit(80)/2.0;
    tf.borderStyle = UITextBorderStyleNone;
    tf.backgroundColor = [UIColor whiteColor];
    tf.font = [UIFont systemFontOfSize:15];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.placeholder = placeHolder;
    return tf;
}


#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == phoneTf) {
        [verifyCodeTf becomeFirstResponder];
    }else if (textField == verifyCodeTf){
        [pwdTf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * validString = @"0123456789";
    if (textField == phoneTf) {
        if (textField.text.length == 11 && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound) {
            if ([string isEqualToString:@""]){
                return YES;
            }else{
                return NO;
            }
        }
    }
    return YES;
}
@end
