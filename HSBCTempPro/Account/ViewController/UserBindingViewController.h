//
//  UserBindingViewController.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ThirdUserModel.h"

@interface UserBindingViewController : BaseViewController

@property (nonatomic , strong) NSString * thirdLoginType;
@property (nonatomic , strong) ThirdUserModel  * thirdUserModel;
@property (nonatomic , assign) BOOL isAutoLoginCheck;


@end
