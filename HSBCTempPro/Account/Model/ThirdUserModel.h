//
//  ThirdUserModel.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThirdUserModel : NSObject

@property (nonatomic , strong) NSString * nickName;
@property (nonatomic , strong) NSString *  headPic;
@property (nonatomic , strong) NSString * gender;
@property (nonatomic , strong) NSString * platformId;
@property (nonatomic , strong) NSString * platformType;     //  '微信', 'QQ', 'Google', 'FaceBook', 'Twitter'
@property (nonatomic , strong) NSString * platformIdentifying;   //  'Wechat', 'QQ', 'Google', 'FaceBook', 'Twitter'
@property (nonatomic , assign) NSInteger state;   // 1:成功  0：失败    -1:取消
@end
