//
//  ThirdLoginTypeModel.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThirdLoginTypeModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSString * state; //  ON  /  OFF
@property (nonatomic , strong) NSString * thirdPlatformType;

@end
