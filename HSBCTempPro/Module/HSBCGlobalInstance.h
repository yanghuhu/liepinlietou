//
//  HSBCGlobalInstance.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Commondefine.h"
#import "TalentModel.h"
#import "JobOrderDetailModel.h"

@interface HSBCGlobalInstance : NSObject

DECLARE_SINGLETON_FOR_CLASS(HSBCGlobalInstance)

@property (nonatomic,strong) UserModel * curUserModel;  // 当前登录的用户

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;

@property (nonatomic , strong) NSString * netWorkToken;  // 网络请求token
@property (nonatomic , assign)  BOOL needMock;

@property (nonatomic , strong) NSDictionary * fileInfoFromThirdApp;   // 第三方跳转查看简历相关信息
@property (nonatomic , assign) BOOL isThirdResumeToSpecificPage;  //是否需要展示我的简历查看第三方简历


// 后台对接阿里获取虚拟号码，未避免阿里延迟，故失败后重复请求
@property (nonatomic , assign) int  getVirtualNumberCount;

@end
