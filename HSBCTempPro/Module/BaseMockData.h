//
//  BaseMockData.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+YYModel.h"
#import "NSDictionary+YYAdd.h"

@interface BaseMockData : NSObject

@property (nonatomic, strong) NSMutableDictionary *callbackDicData;
/**
 *  要返回的模拟数据
 */
@property (nonatomic, strong) NSMutableArray *callbackData;

@property (nonatomic, copy) NSString *functionName;

@property (nonatomic, copy) NSDictionary *parameters;

/**
 *  创建模拟数据对象
 *
 *  @param functionName NSStringFromSelector(_cmd)
 *
 *  @return BaseMockData的子类
 */
+ (instancetype)createWithFunctionName:(NSString *)functionName;

/**
 *  创建模拟数据对象
 *
 *  @param functionName  NSStringFromSelector(_cmd)
 *  @param forceMockData YES:表示强制生成模拟数据，忽略isNeedMockData全局设置方法
 *
 *  @return BaseMockData的子类
 */
+ (instancetype)createWithFunctionName:(NSString *)functionName forceMockData:(BOOL)forceMockData;

/**
 *  是否需要模拟数据
 *
 *  @return YES:需要
 */
+ (BOOL)isNeedMockData;

///**
// *  子类负责模拟数据
// */
//- (void)mockData;

- (BaseMockData *)Excute;



@end
