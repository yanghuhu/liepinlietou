//
//  BaseModule.h
//  JLGProject
//
//  Created by yang on 2017/3/1.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+YYModel.h"
#import "NetWorkHelper.h"
#import "NetworkHandle.h"

@interface BaseModule : NSObject

+ (NSDictionary *)reqestParam:(NSDictionary *)info;

@end


