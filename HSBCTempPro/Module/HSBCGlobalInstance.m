//
//  HSBCGlobalInstance.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "HSBCGlobalInstance.h"

@implementation HSBCGlobalInstance

DEFINE_SINGLETON_FOR_CLASS(HSBCGlobalInstance)

#ifdef PhoneDebug

#else

- (CLLocationDegrees)latitude{
    return 34.2475679573;
}

- (CLLocationDegrees)longitude{
    return 108.9206886292;
}

#endif

@end
