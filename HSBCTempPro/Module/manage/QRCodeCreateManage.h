//
//  QRCodeCreateManage.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface QRCodeCreateManage : NSObject

- (UIImage *)createWithString:(NSString *)string;

@end
