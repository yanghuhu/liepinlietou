//
//  NetworkHandle.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/9.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseMockData.h"

@interface NetworkHandle : NSObject

@property (nonatomic, assign) BOOL isMock;

- (void)ExcuteOnBlackGroundWithBlock:(void(^)())block;
- (void)ExcuteOnMainTreadWithBlock:(void (^)())block;


/**
 *  模拟数据还是真实数据
 *
 *  @param module       <#module description#>
 *  @param mockObj      不提供该实例时走的是请求服务端的数据
 *  @param params       <#params description#>
 *  @param handle       <#handle description#>
 *  @param successBlock <#successBlock description#>
 *  @param failBlock    <#failBlock description#>
 */
+ (void)postRequestForApi:(NSString *)module mockObj:(BaseMockData *)mockObj
                  params:(NSDictionary *)params
                  handle:(NetworkHandleBlock)handle
                 success:(RequestSuccessBlock)successBlock
                 failure:(RequestFailureBlock)failBlock;


+ (void)getRequestForApi:(NSString *)api  mockObj:(BaseMockData *)mockObj
                  params:(NSDictionary *)params
                  handle:(NetworkHandleBlock)handle
                 success:(RequestSuccessBlock)successBlock
                 failure:(RequestFailureBlock)failBlock;
@end
