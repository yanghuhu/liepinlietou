//
//  DBHelperImp.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "DBHelperImp.h"
#import <SQLCipher/sqlite3.h>
#import "DataDefault.h"
#import "NSObject+YYModel.h"
#import "NSString+YYAdd.h"

#define DATE_FORMAT @"yyyy-MM-dd HH:mm:ss.SSS"

@interface DBHelperImp(){
    
}

@property (nonatomic, strong) FMDatabaseQueue *dbQueue;

@end


@implementation DBHelperImp


- (id)initWithDataBase:(FMDatabase *)db andDBQueue:(FMDatabaseQueue *)dbQueue {
    self = [super init];
    if (self) {
        _dbQueue = dbQueue;
    }
    return self;
}

- (id)initWithRole:(UserModel *)role{
    self = [super init];
    if (self) {
        self.role = role;
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask,YES);
        NSString *ourDocumentPath = [documentPaths objectAtIndex:0];
        NSString *uid = [NSString stringWithFormat:@"%ld", (long)role.uid];
        
//        NSString * dbKeyOri = @"secrect";
//        NSString *dbKey = dbKeyOri;
        NSString *filename = [NSString stringWithFormat:@"HSBC_%@",uid];
        NSString *filenameEnc = [NSString stringWithFormat:@"%@_enc", filename];
        NSString *fullPathEnc = [ourDocumentPath stringByAppendingFormat:@"/%@.db",filenameEnc];
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        formater.dateFormat = DATE_FORMAT;
        _dbQueue = [FMDatabaseQueue databaseQueueWithPath:fullPathEnc];
        DLog(@"db path:%@", fullPathEnc);
        //这里初始化数据库，虽然和之后的操作可以多线程同步，但是还是初始化完成吧，而且后面还有补救逻辑（非必要）。不会卡着的。下同
        __block BOOL rs = NO;
        [_dbQueue inDatabase:^(FMDatabase *db) {
            [db setDateFormat:formater];
            rs = [db open] && [db goodConnection];
//            rs = [db open] && [db setKey:dbKey] && [db goodConnection];
        }];
    }
    return self;
}


- (id)initWithoutRole {
    self = [super init];
    if (self) {
        self.role = nil;
        NSArray *tempPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,  NSUserDomainMask,YES);
        NSString *ourTempPath =[tempPaths objectAtIndex:0];
        NSString *filename = @"public";
        NSString *fullPath = [ourTempPath stringByAppendingFormat:@"/%@.db",filename];
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        formater.dateFormat = DATE_FORMAT;
        _dbQueue = [FMDatabaseQueue databaseQueueWithPath:fullPath];
        DLog(@"db path:%@", fullPath);
        NSString * dbKey = @"secrect";
        [_dbQueue inDatabase:^(FMDatabase *db) {
            [db setDateFormat:formater];
            BOOL res = [db open] && [db setKey:dbKey];
            DLog(@"res:%@", @(res));
        }];
    }
    return self;
}

- (void)dealloc{
    [_dbQueue inDatabase:^(FMDatabase *db) {
        [db close];
    }];
}

- (BOOL)createTable:(NSString *)tableName para:(NSSafeMutableDictionary *)dict
{
    NSMutableString *sqlCreateTable = [NSMutableString stringWithFormat:@"CREATE TABLE IF NOT EXISTS '%@' (",tableName] ;
    bool firstKey = YES;
    for (NSString* key in [dict allKeys]){
        NSString* obj = [dict objectForKey:key];
        if (!firstKey){
            [sqlCreateTable appendString:@","];
        }
        [sqlCreateTable appendFormat:@" '%@' %@ ",key,obj];
        firstKey = NO;
    }
    [sqlCreateTable appendString:@")"];
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:sqlCreateTable];
    }];
    return rs;
}

- (BOOL)dropTable:(NSString *)tableName
{
    if (![tableName isNotBlank]) {
        return NO;
    }
    __block BOOL isSuccess = NO;
    NSMutableString *sqlCreateTable = [NSMutableString stringWithFormat:@"DROP TABLE IF EXISTS %@",tableName] ;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        isSuccess = [db executeUpdate:sqlCreateTable];
    }];
    return isSuccess;
}

- (NSString *)addQuote:(id)value{
    if ([value isKindOfClass:[NSDate class]]) {
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        formater.dateFormat = DATE_FORMAT;
        return [NSString stringWithFormat:@"'%@'",[formater stringFromDate:value]];
    }else if ([value isKindOfClass:[NSString class]]){
        return [NSString stringWithFormat:@"'%@'",[value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
    }
    return [NSString stringWithFormat:@"%@",value];
}

- (NSString *)whereStrByAttrAndValue:(NSSafeMutableDictionary *)attrAndValue{
    if (!attrAndValue || attrAndValue.allKeys.count == 0)
        return nil;
    //    NSInteger count  = [attrAndValue allKeys].count;
    NSMutableArray* whereComponentArr = [NSMutableArray array];
    //    for (NSInteger i = 0 ; i < count ; i++){
    //        NSString* whereComponent = [NSString stringWithFormat:@" %@ = '%@'",[[attrAndValue allKeys] objectAtIndex:i],[[attrAndValue allValues] objectAtIndex:i]];
    //        [whereComponentArr addObject:whereComponent];
    //    }
    for (NSString *key in [attrAndValue allKeys]) {
        id value = [attrAndValue objectForKey:key];
        NSString *str;
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSDate class]]) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                formater.dateFormat = DATE_FORMAT;
                str = [NSString stringWithFormat:@" %@='%@'", key, [formater stringFromDate:value]];
            }else {
                str = [NSString stringWithFormat:@" %@='%@'", key, [value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
            }
        } else {
            str = [NSString stringWithFormat:@" %@=%d", key, [value intValue]];
        }
        [whereComponentArr addObject:str];
    }
    
    return [whereComponentArr componentsJoinedByString:@" AND "];
}

- (BOOL)isExistsData:(FMDatabase *)dbObj tableName:(NSString *)tableName whereColumnNameAndVal:(NSSafeMutableDictionary *)whereColumnNameAndVal
{
    if (whereColumnNameAndVal.count==0) {
        return false;
    }
    
    NSString *whereSql = [self whereStrByAttrAndValue:whereColumnNameAndVal];
    whereSql = whereSql.length>0 ? [NSString stringWithFormat:@" where %@",whereSql]:@"";
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) as c FROM %@ %@",tableName,whereSql];
    FMResultSet * rs =[dbObj executeQuery:sql withArgumentsInArray:whereColumnNameAndVal.allValues];
    int count = 0;
    while ([rs next]){
        count = [[rs stringForColumn:@"c"] intValue];
    }
    [rs close];
    //    DLog(@"isExistsData sql:%@",sql);
    return count>0;
}

- (BOOL)isExistDataTableName:(NSString *)tableName whereColumnNameAndVal:(NSSafeMutableDictionary *)whereColumnNameAndVal
{
    __block BOOL isExistData = NO;
    @weakify(self)
    [_dbQueue inDatabase:^(FMDatabase *db) {
        isExistData = [weak_self isExistsData:db tableName:tableName whereColumnNameAndVal:whereColumnNameAndVal];
    }];
    return isExistData;
}

- (NSArray *)fetchDataFromTable:(NSString *)tableName
                   attrAndValue:(NSSafeMutableDictionary *)attrAndValue
                      sortField:(NSString *)sortField
                         sortBy:(DBSortByType)sortByType
               convertDataBlock:(NSArray *(^)(FMResultSet *fmResultSet))convertDataBlock {
    NSString* whereStr = [self whereStrByAttrAndValue:attrAndValue];
    return [self fetchDataFromTableStr:tableName whereSQL:whereStr sortField:sortField sortBy:sortByType limit:-1 converter:convertDataBlock];
}

- (NSArray *)fetchDataFromTable:(NSString *)tableName
                   attrAndValue:(NSSafeMutableDictionary *)attrAndValue
                      sortField:(NSString *)sortField
                         sortBy:(DBSortByType)sortByType
                          limit:(NSInteger)limit
               convertDataBlock:(NSArray *(^)(FMResultSet *fmResultSet))convertDataBlock {
    NSString *whereStr = [self whereStrByAttrAndValue:attrAndValue];
    return [self fetchDataFromTableStr:tableName whereSQL:whereStr sortField:sortField sortBy:sortByType limit:limit converter:convertDataBlock];
}

- (NSArray *)fetchDataFromTableStr:(NSString *)tableStr
                          whereSQL:(NSString *)whereSQL
                         sortField:(NSString *) sortField
                            sortBy:(DBSortByType)sortByType
                         converter:(NSArray *(^)(FMResultSet *fmResultSet))converter{
    return [self fetchDataFromTableStr:tableStr whereSQL:whereSQL sortField:sortField sortBy:sortByType limit:-1 converter:converter];
}

- (NSArray *)fetchDataFromTableStr:(NSString *)tableStr
                          whereSQL:(NSString *)whereSQL
                         sortField:(NSString *) sortField
                            sortBy:(DBSortByType)sortByType
                             limit:(NSInteger)limit
                         converter:(NSArray *(^)(FMResultSet *fmResultSet))converter{
    NSMutableString* sql = [NSMutableString stringWithFormat:@"SELECT * FROM %@",tableStr];
    if ([whereSQL length] !=0)
        [sql appendFormat:@" WHERE %@", whereSQL];
    if (sortField) {
        if (sortByType == kDBSortByAsc) {
            [sql appendFormat:@" order by %@ asc ", sortField];
        } else if (sortByType == kDBSortByDesc) {
            [sql appendFormat:@" order by %@ desc ", sortField];
        }
    }
    
    if (limit > 0) {
        [sql appendFormat:@" limit %ld ", (long)limit];
    }
    
    
    //    DLog(@"sql :%@ ",sql);
    
    //当有一个地方调用了这个方法后，而在converter这个block里再调用这个方法就会
    //造成死锁。fmdb会抛异常而导致APP崩溃
    
    __block NSArray* rs;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet* ptr = [db executeQuery:sql];
        rs = converter(ptr);
        [ptr close];
    }];
    
    return rs;
}

- (NSArray *)fetchDataWithSql:(NSString *)sql
                    converter:(NSArray *(^)(FMResultSet *fmResultSet))converter {
    __block NSArray* rs;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet* ptr = [db executeQuery:sql];
        rs = converter(ptr);
        [ptr close];
    }];
    return rs;
}

- (BOOL)delectDataFromTable:(NSString *)tableName
               attrAndValue:(NSSafeMutableDictionary *)attrAndValue{
    NSString* whereStr = [self whereStrByAttrAndValue:attrAndValue];
    return [self delectDataFromTable:tableName
                            whereStr:whereStr];
}

- (BOOL)delectDataFromTable:(NSString *)tableName
                   whereStr:(NSString *)whereStr{
    NSMutableString *sqlStr = [NSMutableString stringWithFormat:@"DELETE FROM %@",tableName];
    if (whereStr!=nil){
        [sqlStr appendFormat:@" WHERE %@",whereStr];
    }
        DLog(@"delete sql str:%@", sqlStr);
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:sqlStr];
    }];
   
    return rs;
}

- (BOOL)deleteAllDataFromTable:(NSString *)tableName {
    NSString *sqlStr = [NSString stringWithFormat:@"DELETE FROM %@",tableName];
    //DLog(@"delete sql str:%@", sqlStr);
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:sqlStr];
    }];
    
    return rs;
}

- (BOOL)updateDataToTable:(NSString *)tableName attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions {
    NSMutableString *attrStr = [NSMutableString string];
    for (NSString *key in [attrAndValue allKeys]) {
        id value = [attrAndValue objectForKey:key];
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSDate class]]) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                formater.dateFormat = DATE_FORMAT;
                [attrStr appendFormat:@"%@='%@',", key, [formater stringFromDate:value]];
            }else {
                [attrStr appendFormat:@"%@='%@',", key, [value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
            }
        } else {
            [attrStr appendFormat:@"%@=%d,", key, [value intValue]];
        }
    }
    if (attrStr.length>0) {
        [attrStr deleteCharactersInRange:NSMakeRange(attrStr.length-1, 1)];
    }
    NSString *conditionStr = [self whereStrByAttrAndValue:conditions];
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@" , tableName , attrStr , conditionStr];
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:updateSQL];
    }];
    return rs;
}

- (BOOL)updateDataToTable:(NSString *)tableName attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions addCountRows:(NSArray *)addCountRows {
    NSMutableString *attrStr = [NSMutableString string];
    for (NSString *key in [attrAndValue allKeys]) {
        id value = [attrAndValue objectForKey:key];
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSDate class]]) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                formater.dateFormat = DATE_FORMAT;
                [attrStr appendFormat:@"%@='%@',", key, [formater stringFromDate:value]];
            }else {
                [attrStr appendFormat:@"%@='%@',", key, [value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
            }
        } else {
            [attrStr appendFormat:@"%@=%d,", key, [value intValue]];
        }
    }
    for (NSString *addCountRow in addCountRows) {
        [attrStr appendFormat:@"%@=%@+1,", addCountRow, addCountRow];
    }
    if (attrStr.length>0) {
        [attrStr deleteCharactersInRange:NSMakeRange(attrStr.length-1, 1)];
    }
    NSString *conditionStr = [self whereStrByAttrAndValue:conditions];
    NSString* updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@" , tableName , attrStr , conditionStr];
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:updateSQL];
    }];
    
    return rs;
}

- (BOOL)updateDataToTable:(NSString *)tableName fmdb:(FMDatabase *)db attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions  addCountRows:(NSArray *)addCountRows {
    NSMutableString *attrStr = [NSMutableString string];
    for (NSString *key in [attrAndValue allKeys]) {
        id value = [attrAndValue objectForKey:key];
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSDate class]]) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                formater.dateFormat = DATE_FORMAT;
                [attrStr appendFormat:@"%@='%@',", key, [formater stringFromDate:value]];
            }else {
                [attrStr appendFormat:@"%@='%@',", key, [value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
            }
        } else {
            [attrStr appendFormat:@"%@=%d,", key, [value intValue]];
        }
    }
    for (NSString *addCountRow in addCountRows) {
        [attrStr appendFormat:@"%@=%@+1,", addCountRow, addCountRow];
    }
    if (attrStr.length>0) {
        [attrStr deleteCharactersInRange:NSMakeRange(attrStr.length-1, 1)];
    }
    NSString *conditionStr = [self whereStrByAttrAndValue:conditions];
    NSString* updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@" , tableName , attrStr , conditionStr];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: DATE_FORMAT];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [db setDateFormat:dateFormatter];
    return  [db executeUpdate:updateSQL];
}

- (BOOL)updateDataToTable:(NSString *)tableName fmdb:(FMDatabase *)db attrAndValue:(NSSafeMutableDictionary *)attrAndValue conditions:(NSSafeMutableDictionary *)conditions {
    NSMutableString *attrStr = [NSMutableString string];
    for (NSString *key in [attrAndValue allKeys]) {
        id value = [attrAndValue objectForKey:key];
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSDate class]]) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *formater = [[NSDateFormatter alloc] init];
                formater.dateFormat = DATE_FORMAT;
                [attrStr appendFormat:@"%@='%@',", key, [formater stringFromDate:value]];
            }else {
                [attrStr appendFormat:@"%@='%@',", key, [value stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
            }
        } else {
            [attrStr appendFormat:@"%@=%d,", key, [value intValue]];
        }
    }
    if (attrStr.length>0) {
        [attrStr deleteCharactersInRange:NSMakeRange(attrStr.length-1, 1)];
    }
    NSString *conditionStr = [self whereStrByAttrAndValue:conditions];
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@" , tableName , attrStr , conditionStr];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: DATE_FORMAT];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [db setDateFormat:dateFormatter];
    BOOL isSuccess = [db executeUpdate:updateSQL];
    if (!isSuccess) {
        DLog(@"更新失败: %@", updateSQL);
    }
    return isSuccess;
}


- (BOOL)excuteSql:(NSString *)sql {
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:sql];
    }];
    return rs;
}

- (void)excuteSql:(NSString *)sql withArgumentsInArray:(NSArray *)arguments
{
    [_dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:sql withArgumentsInArray:arguments];
    }];
}

- (void)addRowTo:(NSString *)tableName
        withName:(NSString *)rowName
         andAttr:(NSString *)rowAttr {
    [self excuteSql:[NSString stringWithFormat:@"ALTER TABLE %@ ADD %@ %@", tableName, rowName, rowAttr]];
}

- (BOOL)insertDataToTable:(NSString *)tableName
             attrAndValue:(NSSafeMutableDictionary *)attrAndValue
          restrictAttrArr:(NSArray *)rAttrStrArr
                overwrite:(BOOL)overwrite{
    NSSafeMutableDictionary* rAttrAndValue = [NSSafeMutableDictionary dictionary];
    //    [[NSDictionary dictionaryWithObjects:rValueStrArr forKeys:rAttrStrArr]]
    for (NSString* rAttr in rAttrStrArr){
        [rAttrAndValue setObject:[attrAndValue objectForKey:rAttr] forKey:rAttr];
    }
    if (rAttrStrArr.count > 0){
        NSArray* checkResult = [self fetchDataFromTable:tableName
                                           attrAndValue:rAttrAndValue
                                              sortField:nil
                                                 sortBy:kDBSortByNormal
                                       convertDataBlock:^(FMResultSet* rs){
                                           NSMutableArray* arr = [NSMutableArray array];
                                           while ([rs next]) {
                                               [arr addObject:rs];
                                           }
                                           return arr;
                                       }];
        if ([checkResult count] > 0 && overwrite == NO) {
            return NO;
        }
        if ([checkResult count] > 0){
            //            return NO;
            // 如果数据存在就不作处理，不要去删除数据，而是要调用updateDataToTable接口
            /*[self delectDataFromTable:tableName
             attrAndValue:rAttrAndValue];*/
            return [self updateDataToTable:tableName
                              attrAndValue:attrAndValue
                                conditions:rAttrAndValue];
        }
    }
    
    NSMutableArray* quoteAttrStrArr = [NSMutableArray array];
    for (NSString* attrStr in [attrAndValue allKeys]){
        NSString* quoteAttrStr = [self addQuote:attrStr];
        [quoteAttrStrArr addObject:quoteAttrStr];
    }
    NSMutableArray* quoteValueStrArr = [NSMutableArray array];
    for (NSString* valueStr in [attrAndValue allValues]){
        NSString* quoteValueStr = [self addQuote:valueStr];
        [quoteValueStrArr addObject:quoteValueStr];
    }
    NSString* attrStr = [quoteAttrStrArr componentsJoinedByString:@","];
    NSString* valueStr = [quoteValueStrArr componentsJoinedByString:@","];
    
    NSString* insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)" , tableName , attrStr , valueStr];
    __block BOOL rs = NO;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeUpdate:insertSQL];
    }];
    return rs;
}

- (void)batchInserDataToTable:(NSString *)tableName
                attrAndValues:(NSArray *)attrAndValues {
    [_dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: DATE_FORMAT];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [db setDateFormat:dateFormatter];
        for (id obj in attrAndValues) {
            NSSafeMutableDictionary *attrAndValue = nil;
            if ([obj isMemberOfClass:[NSSafeMutableDictionary class]]) {
                attrAndValue = obj;
            }else {
                NSSafeMutableDictionary *dic = [NSSafeMutableDictionary dictionary];
                [dic addEntriesFromDictionary:[obj modelToJSONObject]];
                attrAndValue = dic;
            }
            NSMutableArray* quoteAttrStrArr = [NSMutableArray array];
            for (NSString* attrStr in [attrAndValue allKeys]){
                NSString* quoteAttrStr = [self addQuote:attrStr];
                [quoteAttrStrArr addObject:quoteAttrStr];
            }
            NSMutableArray* quoteValueStrArr = [NSMutableArray array];
            for (NSString* valueStr in [attrAndValue allValues]){
                NSString* quoteValueStr = [self addQuote:valueStr];
                [quoteValueStrArr addObject:quoteValueStr];
            }
            NSString* attrStr = [quoteAttrStrArr componentsJoinedByString:@","];
            NSString* valueStr = [quoteValueStrArr componentsJoinedByString:@","];
            
            NSString* insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)" , tableName , attrStr , valueStr];
            [db executeUpdate:insertSQL];
        }
    }];
}

- (void)batchSaveWithTableName:(NSString *)tableName
                 keyFieldNames:(NSArray *)keyFieldNames
                 attrAndValues:(NSArray *)attrAndValues
       excludeUpdateFieldNames:(NSArray *)excludeUpdateFieldNames
{
    [_dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: DATE_FORMAT];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [db setDateFormat:dateFormatter];
        
        for (id obj in attrAndValues) {
            NSSafeMutableDictionary *attrAndValue = nil;
            if ([obj isMemberOfClass:[NSSafeMutableDictionary class]]) {
                attrAndValue = obj;
            }else {
                NSSafeMutableDictionary *dic = [NSSafeMutableDictionary dictionary];
                [dic addEntriesFromDictionary:[obj modelToJSONObject]];
                attrAndValue = dic;
            }
            //要更新的字段与值
            NSSafeMutableDictionary *updateDic = [NSSafeMutableDictionary dictionary];
            //表中的唯一键值对
            NSSafeMutableDictionary *keyDic = [NSSafeMutableDictionary dictionary];
            
            NSMutableArray* quoteAttrStrArr = [NSMutableArray array];
            for (NSString* attrStr in [attrAndValue allKeys]){
                NSString* quoteAttrStr = [self addQuote:attrStr];
                [quoteAttrStrArr addObject:quoteAttrStr];
                
                //没有表的唯一键值对的话就不用检查数据是否存在了
                if (keyFieldNames.count>0) {
                    //排除更新的字段
                    BOOL haveExcludeUpdateFieldName = NO;
                    for (NSString *fiedldName in excludeUpdateFieldNames) {
                        if ([fiedldName isEqualToString:attrStr]) {
                            haveExcludeUpdateFieldName = YES;
                            break;
                        }
                    }
                    if (!haveExcludeUpdateFieldName) {
                        [updateDic setObject:[attrAndValue objectForKey:attrStr] forKey:attrStr];
                    }
                    // 组合唯一键值对
                    for (NSString *fiedldName in keyFieldNames) {
                        if ([fiedldName isEqualToString:attrStr]) {
                            [keyDic setObject:[attrAndValue objectForKey:attrStr] forKey:attrStr];
                        }
                    }
                }
            }
            
            if ([self isExistsData:db tableName:tableName whereColumnNameAndVal:keyDic]) {
                if(![self updateDataToTable:tableName fmdb:db attrAndValue:updateDic conditions:keyDic])
                {
                    DLog(@"更新失败");
                }else{
                    DLog(@"更新成功");
                }
            }else
            {
                NSMutableArray* quoteValueStrArr = [NSMutableArray array];
                for (NSString* valueStr in [attrAndValue allValues]){
                    NSString* quoteValueStr = [self addQuote:valueStr];
                    [quoteValueStrArr addObject:quoteValueStr];
                }
                
                NSString* attrStr = [quoteAttrStrArr componentsJoinedByString:@","];
                NSString* valueStr = [quoteValueStrArr componentsJoinedByString:@","];
                
                NSString* insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)" , tableName , attrStr , valueStr];
                bool isSuccess = [db executeUpdate:insertSQL];
                if (!isSuccess) {
                    DLog(@"插入SQL失败：%@",insertSQL);
                }else {
                    DLog(@"插入成功");
                }
            }
        }
    }];
}

- (void)batchSaveWithTableName:(NSString *)tableName
                 keyFieldNames:(NSArray *)keyFieldNames
                 attrAndValues:(NSArray *)attrAndValues
                  addCountRows:(NSArray *)addCounts
       excludeUpdateFieldNames:(NSArray *)excludeUpdateFieldNames {
    [_dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: DATE_FORMAT];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [db setDateFormat:dateFormatter];
        
        for (id obj in attrAndValues) {
            NSSafeMutableDictionary *attrAndValue = nil;
            if ([obj isMemberOfClass:[NSSafeMutableDictionary class]]) {
                attrAndValue = obj;
            }else {
                NSSafeMutableDictionary *dic = [NSSafeMutableDictionary dictionary];
                [dic addEntriesFromDictionary:[obj modelToJSONObject]];
                attrAndValue = dic;
            }
            
            //要更新的字段与值
            NSSafeMutableDictionary *updateDic = [NSSafeMutableDictionary dictionary];
            //表中的唯一键值对
            NSSafeMutableDictionary *keyDic = [NSSafeMutableDictionary dictionary];
            NSMutableArray *values = [NSMutableArray array];
            NSMutableArray* quoteAttrStrArr = [NSMutableArray array];
            NSInteger index = 0;
            for (NSString* attrStr in [attrAndValue allKeys]){
                NSString* quoteAttrStr = [self addQuote:attrStr];
                
                //没有表的唯一键值对的话就不用检查数据是否存在了
                if (keyFieldNames.count>0) {
                    //排除更新的字段
                    BOOL haveExcludeUpdateFieldName = NO;
                    for (NSString *fiedldName in excludeUpdateFieldNames) {
                        if ([fiedldName isEqualToString:attrStr]) {
                            haveExcludeUpdateFieldName = YES;
                            break;
                        }
                    }
                    if (!haveExcludeUpdateFieldName) {
                        [quoteAttrStrArr addObject:quoteAttrStr];
                        [values addObject:[[attrAndValue allValues] objectAtIndex:index]];
                        [updateDic setObject:[attrAndValue objectForKey:attrStr] forKey:attrStr];
                    }
                    
                    {//组合唯一键值对
                        for (NSString *fiedldName in keyFieldNames) {
                            if ([fiedldName isEqualToString:attrStr]) {
                                [keyDic setObject:[attrAndValue objectForKey:attrStr] forKey:attrStr];
                            }
                        }
                    }
                }
                index ++;
            }
            
            
            if ([self isExistsData:db tableName:tableName whereColumnNameAndVal:keyDic]) {
                
                if(![self updateDataToTable:tableName fmdb:db attrAndValue:updateDic conditions:keyDic addCountRows:addCounts])
                {
                    DLog(@"更新失败");
                }
            }else
            {
                NSMutableArray* quoteValueStrArr = [NSMutableArray array];
                for (NSString* valueStr in values){
                    NSString* quoteValueStr = [self addQuote:valueStr];
                    [quoteValueStrArr addObject:quoteValueStr];
                }
                
                for (NSString *addRow in addCounts) {
                    if (![quoteAttrStrArr containsObject:addRow]) {
                        [quoteAttrStrArr addObject:addRow];
                        [quoteValueStrArr addObject:@"1"];
                    }
                }
                
                NSString* attrStr = [quoteAttrStrArr componentsJoinedByString:@","];
                NSString* valueStr = [quoteValueStrArr componentsJoinedByString:@","];
                
                NSString* insertSQL = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)" , tableName , attrStr , valueStr];
                [db executeUpdate:insertSQL];
            }
            
        }
    }];
}

- (NSDate*) topUpdateTimeInTable:(NSString *)tableName
                    dateAttrName:(NSString*) dateAttrName
               whereAttrAndValue:(NSSafeMutableDictionary *)attrAndValue
                     isAscending:(BOOL)isAscending{
    NSString* funcStr = isAscending?@"MAX":@"MIN";
    NSMutableString* sql = [NSMutableString stringWithFormat:@"SELECT %@(%@) FROM %@",funcStr,dateAttrName,tableName];
    if ([attrAndValue allKeys].count > 0){
        NSString* whereStr = [self whereStrByAttrAndValue:attrAndValue];
        [sql appendFormat:@" WHERE %@",whereStr];
    }
    
    __block NSDate *date = nil;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet* rs = [db executeQuery:sql];
        if ([rs next])
            date = [rs dateForColumnIndex:0];
        [rs close];
    }];
    return date;
}

- (NSInteger)countRowToTable:(NSString *)tableName
                    andWhere:(NSString *)where {
    NSMutableString *sql = [NSMutableString stringWithFormat:@"select count(1) from %@",tableName];
    if (where) {
        [sql appendFormat:@" where %@",where];
    }
    __block NSInteger count = 0;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet* rs = [db executeQuery:sql];
        if ([rs next])
            count = [rs intForColumnIndex:0];
        [rs close];
    }];
    return count;
}

- (NSInteger)sumRowToTable:(NSString *)tableName
                       row:(NSString *)row
                     where:(NSString *)where {
    NSMutableString *sql = [NSMutableString stringWithFormat:@"select sum(%@) from %@", row, tableName];
    if (where) {
        [sql appendFormat:@" where %@",where];
    }
    __block NSInteger count = 0;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet* rs = [db executeQuery:sql];
        if ([rs next])
            count = [rs intForColumnIndex:0];
        [rs close];
    }];
    return count;
}

//- (id) getDataEdgeFromTable:(NSString *)tableName
//               attrAndValue:(NSSafeMutableDictionary *)attrAndValue
//                  sortField:(NSString *)sortField
//                     sortBy:(DBSortByType)sortByType
//                    isFirst:(BOOL)isFirst
//           convertDataBlock:(NSArray *(^)(FMResultSet *fs))convertDataBlock {
//    NSArray *array = [self fetchDataFromTable:tableName attrAndValue:attrAndValue sortField:sortField sortBy:sortByType convertDataBlock:convertDataBlock];
//    return isFirst ? [array firstObject] : [array lastObject];
//}

@end
