//
//  NSSafeMutableDictionary.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSSafeMutableDictionary : NSObject

+ (NSSafeMutableDictionary*) dictionary;

- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey;
- (id) objectForKey:(id<NSCopying>) aKey;
- (NSArray*) allKeys;
- (NSArray*) allValues;
- (void)addEntriesFromDictionary:(NSDictionary *)dic;
- (void) removeAllObjects;
- (void) removeObjectForKey:(NSString *)key;

/**
 *  字段的数据
 */
@property (nonatomic) NSInteger count;

@end


