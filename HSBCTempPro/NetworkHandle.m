//
//  NetworkHandle.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/9.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "NetworkHandle.h"
#import "NetWorkHelper.h"

@implementation NetworkHandle


- (void)ExcuteOnBlackGroundWithBlock:(void (^)())block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (block) {
            block();
        }
    });
}

- (void)ExcuteOnMainTreadWithBlock:(void (^)())block
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (block) {
            block();
        }
    });
}


+ (void)postRequestForApi:(NSString *)Api mockObj:(BaseMockData *)mockObj
                  params:(NSDictionary *)params
                  handle:(NetworkHandleBlock)handle
                 success:(RequestSuccessBlock)successBlock
                 failure:(RequestFailureBlock)failBlock
{
    if (mockObj) {
        mockObj.parameters = params;
        
        [mockObj Excute];
        
        if (handle) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                handle(mockObj.callbackDicData);
            });
        }
        switch (mockObj.callbackData.count) {
            case 1:
                successBlock([mockObj.callbackData objectAtIndex:0]);
                break;
            case 2:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1]);
                break;
            case 4:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2]);
                break;
            case 5:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2],[mockObj.callbackData objectAtIndex:3]);
                break;
            case 6:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2],[mockObj.callbackData objectAtIndex:3],[mockObj.callbackData objectAtIndex:4]);
                break;
            default:
                successBlock();
                break;
        }
    }else
        [NetWorkHelper postRequestForApi:Api params:params handle:handle success:successBlock failure:failBlock];
}

+ (void)getRequestForApi:(NSString *)api  mockObj:(BaseMockData *)mockObj
                  params:(NSDictionary *)params
                  handle:(NetworkHandleBlock)handle
                 success:(RequestSuccessBlock)successBlock
                 failure:(RequestFailureBlock)failBlock{
    
    if (mockObj) {
        mockObj.parameters = params;
        
        [mockObj Excute];
        
        if (handle) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                handle(mockObj.callbackDicData);
            });
        }
        switch (mockObj.callbackData.count) {
            case 1:
                successBlock([mockObj.callbackData objectAtIndex:0]);
                break;
            case 2:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1]);
                break;
            case 4:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2]);
                break;
            case 5:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2],[mockObj.callbackData objectAtIndex:3]);
                break;
            case 6:
                successBlock([mockObj.callbackData objectAtIndex:0],[mockObj.callbackData objectAtIndex:1],[mockObj.callbackData objectAtIndex:2],[mockObj.callbackData objectAtIndex:3],[mockObj.callbackData objectAtIndex:4]);
                break;
            default:
                successBlock();
                break;
        }
    }else{
        [NetWorkHelper getRequestForApi:api params:params handle:handle success:successBlock failure:failBlock];
    }
}


@end







