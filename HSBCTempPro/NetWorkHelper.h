//
//  NetWorkHelper.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/8.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "AFHTTPSessionManager.h"
#import "NetworkHandle.h"

typedef NSArray* (^NetworkHandleBlock)(id response);

//  图片所属模块分类
typedef enum {
    UploadTypeAlbum,
}UploadType;


@interface NetWorkHelper : NSObject


+ (NetWorkHelper *)sharedNetworkHelper;

+ (NSURLSessionDataTask *)getRequestForApi:(NSString *)Api
                                     params:(NSDictionary *)params
                                     handle:(NetworkHandleBlock)handle
                                    success:(RequestSuccessBlock)success
                                    failure:(RequestFailureBlock)failure;


// post 网络请求
+ (NSURLSessionDataTask *)postRequestForApi:(NSString *)Api
                                    params:(NSDictionary *)params
                                    handle:(NetworkHandleBlock)handle
                                   success:(RequestSuccessBlock)success
                                   failure:(RequestFailureBlock)failure;


// 图片上传
+ (void)requestForUploadImageSync:(UIImage *)image
                               withModule:(NSString *)module
                                  success:(RequestSuccessBlock)success
                                  failure:(RequestFailureBlock)failure;


// 上传简历文件
+ (void)requestForUploadFileSync:(NSString *)path
                         success:(RequestSuccessBlock)success
                         failure:(RequestFailureBlock)failure;
/**
 上传视频

 @param video 视频path
 @param module 类型
 @param successBlock 成功
 @param failBlock 失败
 */
+ (void)updateVideoWithvideo:(NSURL *)video
                                         module:(NSString *)module
                                        Success:(RequestSuccessBlock)successBlock
                                        Failure:(RequestFailureBlock)failBlock;


/**
 *  是否有网络（蜂窝移动网，或WIFI都为TRUE，断网或未完成检测为NO）
 *  注意：网络还没有完成检查，该方法也是返回NO
 *  @return YES:有网，NO：无网
 */
- (BOOL)isNetworkValid;



@end
