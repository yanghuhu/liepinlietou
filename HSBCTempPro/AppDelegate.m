//
//  AppDelegate.m
//  HSBCTempPro
//
//  Created by Michael on 2017/10/26.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AppDelegate.h"
#import "MapHomeViewController.h"
#import "UserHomeViewController.h"
//#import <AMapFoundationKit/AMapFoundationKit.h>
#import "BaseNavigationController.h"

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import <FBSDKMessengerShareKit/FBSDKMessengerSharer.h>
#import "LoginViewController.h"
#import "IQKeyboardManager.h"

#import "CandidateDetailViewController.h"
#import "PositionDemandViewController.h"
#import "RecruitModule.h"
#import "LocalResumeViewController.h"
#import <AlipaySDK/AlipaySDK.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = RGB16(0x1E2024);
    
    // 自动登录判断
    BOOL isloginAuto = [[DataDefault objectForKey:DataDefaultAutologinKey isPrivate:NO] boolValue];
    UIViewController * vc = nil;
    if (isloginAuto) {
        UserModel * model = [BaseHelper getUserFromDataDefault];
        if (model) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = model.netToken;
            vc = [[PositionDemandViewController alloc] init];
        }else{
            vc = [[LoginViewController alloc] init];
        }
    }else{
        vc = [[LoginViewController alloc] init];
    }
    
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = nav;
    
//    [self initTabbarViewCtr];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;

    //  高德地图配置
//    [[AMapServices sharedServices] setEnableHTTPS:YES];
//    [AMapServices sharedServices].apiKey = MAMapKey;
    [self thirdLoginSetting];
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options {
    if (options) {
        if([[url absoluteString] containsString:@"Documents/Inbox"]){   //第三方查看简历跳转过来
            NSDictionary * info = @{@"url":url,@"backSchemes":options[@"UIApplicationOpenURLOptionsSourceApplicationKey"]};
            [HSBCGlobalInstance sharedHSBCGlobalInstance].fileInfoFromThirdApp = info;
            [HSBCGlobalInstance sharedHSBCGlobalInstance].isThirdResumeToSpecificPage = YES;
            
            if ([HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel) {
                [BaseHelper thirdResumeFileSave:url];
                [RecruitModule localResumeUploadWithPath:[BaseHelper resumeLocalPahtWithName:[url lastPathComponent]] Success:^{
                    [BaseHelper showProgressHud:@"简历上传成功" showLoading:NO canHide:YES];
                } failure:^(NSString *error, ResponseType responseType) {
                    [BaseHelper showProgressHud:@"简历上传失败" showLoading:NO canHide:YES];
                }];
                [self showLocalResumeVC];
            }
        }else if ([url.host isEqualToString:@"safepay"]) {
            // 支付跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
            
            // 授权跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
                // 解析 auth code
                NSString *result = resultDic[@"result"];
                NSString *authCode = nil;
                if (result.length>0) {
                    NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                    for (NSString *subResult in resultArr) {
                        if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                            authCode = [subResult substringFromIndex:10];
                            break;
                        }
                    }
                }
                NSLog(@"授权结果 authCode = %@", authCode?:@"");
            }];
        }
    }
    return YES;
}

- (void)thirdLoginSetting{
    [ShareSDK registerActivePlatforms:@[@(SSDKPlatformTypeSinaWeibo),
                                        @(SSDKPlatformTypeFacebook),
                                        @(SSDKPlatformTypeTwitter),
                                        @(SSDKPlatformTypeGooglePlus),
                                        @(SSDKPlatformTypeWechat),
                                        @(SSDKPlatformTypeQQ)
                                        ] onImport:^(SSDKPlatformType platformType) {
                                            switch (platformType) {
                                                case SSDKPlatformTypeQQ:{
                                                    [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                                                    break;
                                                }
                                                case SSDKPlatformTypeWechat:{
                                                    [ShareSDKConnector connectWeChat:[WXApi class]];
                                                    break;
                                                }
                                                case SSDKPlatformTypeSinaWeibo:{
                                                    [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                                                    break;
                                                }
                                                case SSDKPlatformTypeFacebook:{
                                                    [ShareSDKConnector connectFacebookMessenger:[FBSDKMessengerSharer class]];
                                                    break;
                                                }
                                                default:
                                                    break;
                                            }
                                        } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
                                            switch (platformType) {
                                                case SSDKPlatformTypeQQ:{
                                                    [appInfo SSDKSetupQQByAppId:QQID appKey:QQKey authType:SSDKAuthTypeBoth];
                                                    break;
                                                }
                                                case SSDKPlatformTypeWechat:{
                                                    [appInfo SSDKSetupWeChatByAppId:WechatID appSecret:WechatSecret];
                                                    break;
                                                }
                                                    

                                                case SSDKPlatformTypeSinaWeibo:{
                                                    [appInfo SSDKSetupSinaWeiboByAppKey:SinaWeiboKey
                                                                              appSecret:SinaWeiboSecret
                                                                            redirectUri:SinaWeiboCallBack
                                                                               authType:SSDKAuthTypeBoth];
                                                    break;
                                                }
                                                case SSDKPlatformTypeFacebook:{
                                                    [appInfo SSDKSetupFacebookByApiKey:FacebookKey appSecret:FacebookSecret authType:SSDKAuthTypeBoth];
                                                    break;
                                                }
                                                case SSDKPlatformTypeTwitter:{
                                                    [appInfo SSDKSetupTwitterByConsumerKey:TwitterKey consumerSecret:TwitterSecret redirectUri:TwitterCallBack];
                                                    break;
                                                }
                                                case SSDKPlatformTypeGooglePlus:{
                                                    [appInfo SSDKSetupGooglePlusByClientID:GooglePlusClientID clientSecret:GooglePlusSecret redirectUri:GooglePlusCallBack];
                                                    break;
                                                }
                                                default:
                                                    break;
                                            }
                                        }];
}

- (void)showLocalResumeVC{
    UIViewController * vc = [BaseHelper windowTopViewController];
    LocalResumeViewController * resumeVC = [[LocalResumeViewController alloc] init];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:resumeVC];
    [vc presentViewController:nav animated:YES completion:nil];
}

// 9.0 之前走旧接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    return YES;
}

@end
