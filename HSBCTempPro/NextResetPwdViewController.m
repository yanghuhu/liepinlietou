//
//  NextResetPwdViewController.m
//  HSBCTempPro
//
//  Created by Deve on 2018/3/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "NextResetPwdViewController.h"
#import "AccountModule.h"

@interface NextResetPwdViewController ()<UITextFieldDelegate>
{
    UITextField * pwdTf;
    UITextField * newPwdTf;
    UIButton * sureBt;
}
@end

@implementation NextResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = ViewControllerBkColor;
    self.navigationItem.title = @"重置密码";
    [self createViews];

}

- (void)createViews{
    CGFloat verpadding = VerPxFit(36);
    CGFloat tfH = VerPxFit(80);
    
    pwdTf = [self tfWihtPlaceHolder:@"请输入至少8位有效密码"];
    pwdTf.secureTextEntry = YES;
    pwdTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self.view addSubview:pwdTf];
    [pwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(50));
        make.top.equalTo(self.view).mas_offset(VerPxFit(200));
        make.right.equalTo(self.view).mas_offset(-HorPxFit(50));
        make.height.mas_equalTo(tfH);
    }];
    
    newPwdTf = [self tfWihtPlaceHolder:@"请输入确认密码"];
    newPwdTf.secureTextEntry = YES;
    newPwdTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self.view addSubview:newPwdTf];
    [newPwdTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).mas_offset(HorPxFit(50));
        make.top.equalTo(pwdTf.mas_bottom).mas_offset(verpadding);
        make.right.equalTo(self.view).mas_offset(-HorPxFit(50));
        make.height.mas_equalTo(tfH);
    }];
   

    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"AccountSureBt"""] forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:16];
    sureBt.layer.cornerRadius = 5;
    [self.view addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(pwdTf);
        make.top.mas_equalTo(newPwdTf.mas_bottom).mas_offset(VerPxFit(60));
        make.height.mas_equalTo(VerPxFit(90));
    }];
}

- (UITextField *)tfWihtPlaceHolder:(NSString *)placeHolder{
    UITextField * tf = [[UITextField alloc] init];
    tf.delegate = self;
    tf.layer.cornerRadius = HorPxFit(80)/2.0;
    tf.borderStyle = UITextBorderStyleNone;
    tf.backgroundColor = [UIColor whiteColor];
    tf.font = [UIFont systemFontOfSize:15];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 0)];
    tf.leftView = view;
    tf.leftViewMode = UITextFieldViewModeAlways;
//    tf.placeholder = @"密码长度不能小于8位";
    tf.placeholder = placeHolder;
    return tf;
}

#pragma mark -- Action---
- (void)resetAction{
    [self resignFirstResponder];
    if (!pwdTf.text ||  pwdTf.text.length ==0 ) {
        [BaseHelper showProgressHud:@"请输入新密码" showLoading:NO canHide:YES];
        return;
    }
    
    if (!newPwdTf.text  || newPwdTf.text.length ==0) {
        [BaseHelper showProgressHud:@"请输入新密码" showLoading:NO canHide:YES];
        return;
    }
    
    if(![BaseHelper passwordlength:pwdTf.text] ||
       ![BaseHelper passwordlength:newPwdTf.text]){
        [BaseHelper showProgressHud:@"密码长度不能小于8位" showLoading:NO canHide:YES];
        return;
    }
    
    if (![pwdTf.text isEqualToString:newPwdTf.text]) {
        [BaseHelper showProgressHud:@"两次密码输入不一致" showLoading:NO canHide:YES];
        return;
    }
    
    @weakify(self);
    NSString *token = [[HSBCGlobalInstance sharedHSBCGlobalInstance] netWorkToken];
    [AccountModule nextResetPasswordWithNewPwd:pwdTf.text token:token success:^{
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"密码修改成功!" showLoading:NO canHide:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == pwdTf) {
        [newPwdTf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
